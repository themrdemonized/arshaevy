-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Сен 16 2021 г., 08:21
-- Версия сервера: 10.3.13-MariaDB-log
-- Версия PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `arshaevy`
--
CREATE DATABASE IF NOT EXISTS `arshaevy` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `arshaevy`;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_address`
--

CREATE TABLE `oc_address` (
  `address_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `company` varchar(40) NOT NULL,
  `address_1` varchar(255) NOT NULL,
  `address_2` varchar(128) NOT NULL,
  `city` varchar(128) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `country_id` int(11) NOT NULL DEFAULT 0,
  `zone_id` int(11) NOT NULL DEFAULT 0,
  `custom_field` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_address`
--

INSERT INTO `oc_address` (`address_id`, `customer_id`, `firstname`, `lastname`, `company`, `address_1`, `address_2`, `city`, `postcode`, `country_id`, `zone_id`, `custom_field`) VALUES
(6, 20, 'fdsf', '', '', '680000, Хабаровский край, г Хабаровск', '', '', '', 176, 0, ''),
(10, 20, 'тест', '', '', '352117, Краснодарский край, Тихорецкий р-н, тер Автодорога Павловская-Махачкала (ст-ца Архангельская)', '', '', '', 176, 0, '');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_api`
--

CREATE TABLE `oc_api` (
  `api_id` int(11) NOT NULL,
  `username` varchar(64) NOT NULL,
  `key` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_api`
--

INSERT INTO `oc_api` (`api_id`, `username`, `key`, `status`, `date_added`, `date_modified`) VALUES
(1, 'Default', 'WuFD0U9EnzwLT2JA9Z85Yg6ZyTiY12vAorNF3XHlGCGMHUIYmNz8fgX6PXAdbb6UV0CndSAaTfAwCY4WV1oWUfTztQBmIaBm2INUZfsCn0WcrDD0eaWvBmqQ77g8QJpJ86VoC1dhqfDpjKnaXQ0jGrJBXZiW76AER7FGZNcxHb3uiaLcHVNrdXoA7whfePMMi8GBu1Y6g1QKZkdHYLIu7hMZHvE6dCMgjzoJoIauNQAKXwYZYHZdtUVWq9HfzerB', 1, '2019-05-16 14:35:22', '2019-05-16 14:35:22');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_api_ip`
--

CREATE TABLE `oc_api_ip` (
  `api_ip_id` int(11) NOT NULL,
  `api_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_api_ip`
--

INSERT INTO `oc_api_ip` (`api_ip_id`, `api_id`, `ip`) VALUES
(1, 1, '192.168.11.1'),
(2, 1, '192.168.99.1'),
(3, 1, '127.0.0.1');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_api_session`
--

CREATE TABLE `oc_api_session` (
  `api_session_id` int(11) NOT NULL,
  `api_id` int(11) NOT NULL,
  `session_id` varchar(32) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_api_session`
--

INSERT INTO `oc_api_session` (`api_session_id`, `api_id`, `session_id`, `ip`, `date_added`, `date_modified`) VALUES
(149, 1, 'f5cd9dfcdfcec408706e6d3c98', '127.0.0.1', '2021-09-13 16:05:12', '2021-09-13 16:11:53'),
(150, 1, '2d8bf9dcc9ab6cd05f44fd91c6', '127.0.0.1', '2021-09-14 13:24:48', '2021-09-14 13:24:48'),
(151, 1, 'c6a4b191a1d978d04e6839a72c', '127.0.0.1', '2021-09-16 15:05:35', '2021-09-16 15:05:35');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_article`
--

CREATE TABLE `oc_article` (
  `article_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `date_available` date NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT 0,
  `article_review` tinyint(1) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `noindex` tinyint(1) NOT NULL DEFAULT 1,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `viewed` int(5) NOT NULL DEFAULT 0,
  `gstatus` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_article_description`
--

CREATE TABLE `oc_article_description` (
  `article_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_h1` varchar(255) NOT NULL,
  `tag` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_article_image`
--

CREATE TABLE `oc_article_image` (
  `article_image_id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_article_related`
--

CREATE TABLE `oc_article_related` (
  `article_id` int(11) NOT NULL,
  `related_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_article_related_mn`
--

CREATE TABLE `oc_article_related_mn` (
  `article_id` int(11) NOT NULL,
  `manufacturer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_article_related_product`
--

CREATE TABLE `oc_article_related_product` (
  `article_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_article_related_product`
--

INSERT INTO `oc_article_related_product` (`article_id`, `product_id`) VALUES
(30, 123),
(31, 123),
(43, 123),
(45, 123);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_article_related_wb`
--

CREATE TABLE `oc_article_related_wb` (
  `article_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_article_to_blog_category`
--

CREATE TABLE `oc_article_to_blog_category` (
  `article_id` int(11) NOT NULL,
  `blog_category_id` int(11) NOT NULL,
  `main_blog_category` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_article_to_download`
--

CREATE TABLE `oc_article_to_download` (
  `article_id` int(11) NOT NULL,
  `download_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_article_to_layout`
--

CREATE TABLE `oc_article_to_layout` (
  `article_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_article_to_store`
--

CREATE TABLE `oc_article_to_store` (
  `article_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_attribute`
--

CREATE TABLE `oc_attribute` (
  `attribute_id` int(11) NOT NULL,
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_attribute_description`
--

CREATE TABLE `oc_attribute_description` (
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_attribute_group`
--

CREATE TABLE `oc_attribute_group` (
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_attribute_group_description`
--

CREATE TABLE `oc_attribute_group_description` (
  `attribute_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_banner`
--

CREATE TABLE `oc_banner` (
  `banner_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `space_between` int(10) NOT NULL DEFAULT 0,
  `slides_per_view` int(10) NOT NULL DEFAULT 1,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_banner_image`
--

CREATE TABLE `oc_banner_image` (
  `banner_image_id` int(11) NOT NULL,
  `banner_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_blog_category`
--

CREATE TABLE `oc_blog_category` (
  `blog_category_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT 0,
  `top` tinyint(1) NOT NULL,
  `column` int(3) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL,
  `noindex` tinyint(1) NOT NULL DEFAULT 1,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_blog_category_description`
--

CREATE TABLE `oc_blog_category_description` (
  `blog_category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_h1` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_blog_category_path`
--

CREATE TABLE `oc_blog_category_path` (
  `blog_category_id` int(11) NOT NULL,
  `path_id` int(11) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_blog_category_to_layout`
--

CREATE TABLE `oc_blog_category_to_layout` (
  `blog_category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_blog_category_to_store`
--

CREATE TABLE `oc_blog_category_to_store` (
  `blog_category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_cart`
--

CREATE TABLE `oc_cart` (
  `cart_id` int(11) UNSIGNED NOT NULL,
  `api_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `session_id` varchar(32) NOT NULL,
  `product_id` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `option` text NOT NULL,
  `quantity` int(5) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_cart`
--

INSERT INTO `oc_cart` (`cart_id`, `api_id`, `customer_id`, `session_id`, `product_id`, `recurring_id`, `option`, `quantity`, `date_added`) VALUES
(143, 0, 20, 'd74c6a89870f3df333bfc3eedd', 426, 0, '{\"887\":\"6979\"}', 1, '2021-03-02 01:14:14');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_category`
--

CREATE TABLE `oc_category` (
  `category_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT 0,
  `top` tinyint(1) NOT NULL,
  `column` int(3) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `noindex` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_category`
--

INSERT INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`, `noindex`) VALUES
(50, '', 0, 0, 1, 0, 1, '2021-09-14 13:29:23', '2021-09-14 13:29:50', 1),
(51, '', 50, 1, 1, 0, 1, '2021-09-13 13:30:02', '2021-09-14 13:30:02', 1),
(52, '', 50, 1, 1, 0, 1, '2021-09-13 13:30:02', '2021-09-14 13:30:02', 1),
(53, '', 52, 0, 1, 0, 1, '2021-09-13 13:30:02', '2021-09-14 13:30:02', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_category_description`
--

CREATE TABLE `oc_category_description` (
  `category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_h1` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_category_description`
--

INSERT INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`, `meta_h1`) VALUES
(50, 1, 'Каталог', '', 'Каталог', 'Каталог', 'Каталог', 'Каталог'),
(51, 1, 'Обувь', 'Обувь', 'Купить Обувь в Аршаев по лучшей цене', 'Покупайте Обувь в магазине Аршаев по лучшей цене', 'Обувь,Аршаев', 'Обувь'),
(52, 1, 'Одежда', 'Одежда', 'Купить Одежда в Аршаев по лучшей цене', 'Покупайте Одежда в магазине Аршаев по лучшей цене', 'Одежда,Аршаев', 'Одежда'),
(53, 1, 'Трикотаж', 'Трикотаж', 'Купить Трикотаж в Аршаев по лучшей цене', 'Покупайте Трикотаж в магазине Аршаев по лучшей цене', 'Одежда,Трикотаж,Аршаев', 'Трикотаж');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_category_filter`
--

CREATE TABLE `oc_category_filter` (
  `category_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_category_path`
--

CREATE TABLE `oc_category_path` (
  `category_id` int(11) NOT NULL,
  `path_id` int(11) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_category_path`
--

INSERT INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(50, 50, 0),
(51, 50, 0),
(51, 51, 1),
(52, 50, 0),
(52, 52, 1),
(53, 50, 0),
(53, 52, 1),
(53, 53, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_category_to_layout`
--

CREATE TABLE `oc_category_to_layout` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_category_to_layout`
--

INSERT INTO `oc_category_to_layout` (`category_id`, `store_id`, `layout_id`) VALUES
(50, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_category_to_store`
--

CREATE TABLE `oc_category_to_store` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_category_to_store`
--

INSERT INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(50, 0),
(51, 0),
(52, 0),
(53, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_cdek_city`
--

CREATE TABLE `oc_cdek_city` (
  `id` varchar(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `cityName` varchar(64) NOT NULL,
  `regionName` varchar(64) NOT NULL,
  `center` tinyint(1) NOT NULL DEFAULT 0,
  `cache_limit` float(5,4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_cdek_dispatch`
--

CREATE TABLE `oc_cdek_dispatch` (
  `dispatch_id` int(11) NOT NULL,
  `dispatch_number` varchar(30) NOT NULL,
  `date` varchar(32) NOT NULL,
  `server_date` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_cdek_order`
--

CREATE TABLE `oc_cdek_order` (
  `order_id` int(11) NOT NULL,
  `dispatch_id` int(11) NOT NULL,
  `act_number` varchar(20) DEFAULT NULL,
  `dispatch_number` varchar(20) NOT NULL,
  `return_dispatch_number` varchar(20) NOT NULL,
  `city_id` int(11) NOT NULL,
  `city_name` varchar(128) NOT NULL,
  `city_postcode` int(6) DEFAULT NULL,
  `recipient_city_id` int(11) NOT NULL,
  `recipient_city_name` varchar(128) NOT NULL,
  `recipient_city_postcode` int(6) DEFAULT NULL,
  `recipient_name` varchar(128) NOT NULL,
  `recipient_email` varchar(255) DEFAULT NULL,
  `phone` varchar(50) NOT NULL,
  `tariff_id` int(4) NOT NULL,
  `mode_id` int(1) NOT NULL,
  `status_id` int(11) NOT NULL,
  `reason_id` int(11) DEFAULT 0,
  `delay_id` int(4) DEFAULT NULL,
  `delivery_recipient_cost` float(15,4) DEFAULT 0.0000,
  `cod` float(8,4) DEFAULT 0.0000,
  `cod_fact` float(8,4) DEFAULT 0.0000,
  `comment` varchar(255) DEFAULT NULL,
  `seller_name` varchar(255) DEFAULT NULL,
  `address_street` varchar(50) DEFAULT NULL,
  `address_house` varchar(30) DEFAULT NULL,
  `address_flat` varchar(10) DEFAULT NULL,
  `address_pvz_code` varchar(10) DEFAULT NULL,
  `delivery_cost` float(8,4) DEFAULT 0.0000,
  `delivery_last_change` varchar(32) DEFAULT NULL,
  `delivery_date` varchar(32) NOT NULL,
  `delivery_recipient_name` varchar(50) DEFAULT NULL,
  `currency` varchar(3) DEFAULT 'RUB',
  `currency_cod` varchar(3) DEFAULT 'RUB',
  `last_exchange` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_cdek_order_add_service`
--

CREATE TABLE `oc_cdek_order_add_service` (
  `service_id` int(4) NOT NULL,
  `order_id` int(11) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `price` float(8,4) NOT NULL DEFAULT 0.0000
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_cdek_order_call`
--

CREATE TABLE `oc_cdek_order_call` (
  `call_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `date` int(10) NOT NULL,
  `time_beg` time NOT NULL,
  `time_end` time NOT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `recipient_name` varchar(128) DEFAULT NULL,
  `delivery_recipient_cost` float(15,4) DEFAULT 0.0000,
  `address_street` varchar(50) NOT NULL,
  `address_house` varchar(30) NOT NULL,
  `address_flat` varchar(10) NOT NULL,
  `comment` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_cdek_order_call_history_delay`
--

CREATE TABLE `oc_cdek_order_call_history_delay` (
  `order_id` int(11) NOT NULL,
  `date` int(10) NOT NULL,
  `date_next` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_cdek_order_call_history_fail`
--

CREATE TABLE `oc_cdek_order_call_history_fail` (
  `order_id` int(11) NOT NULL,
  `fail_id` int(4) NOT NULL,
  `date` int(10) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_cdek_order_call_history_good`
--

CREATE TABLE `oc_cdek_order_call_history_good` (
  `order_id` int(11) NOT NULL,
  `date` int(10) NOT NULL,
  `date_deliv` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_cdek_order_courier`
--

CREATE TABLE `oc_cdek_order_courier` (
  `courier_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `date` int(10) NOT NULL,
  `time_beg` time NOT NULL,
  `time_end` time NOT NULL,
  `lunch_beg` time DEFAULT NULL,
  `lunch_end` time DEFAULT NULL,
  `city_id` int(11) NOT NULL,
  `city_name` varchar(128) NOT NULL,
  `send_phone` varchar(255) NOT NULL,
  `sender_name` varchar(255) NOT NULL,
  `address_street` varchar(50) NOT NULL,
  `address_house` varchar(30) NOT NULL,
  `address_flat` varchar(10) NOT NULL,
  `comment` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_cdek_order_delay_history`
--

CREATE TABLE `oc_cdek_order_delay_history` (
  `order_id` int(11) NOT NULL,
  `delay_id` int(4) NOT NULL,
  `date` int(10) NOT NULL,
  `description` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_cdek_order_package`
--

CREATE TABLE `oc_cdek_order_package` (
  `package_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `number` varchar(20) NOT NULL,
  `brcode` varchar(20) NOT NULL,
  `weight` int(11) NOT NULL,
  `size_a` float(15,4) DEFAULT 0.0000,
  `size_b` float(15,4) DEFAULT 0.0000,
  `size_c` float(15,4) DEFAULT 0.0000
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_cdek_order_package_item`
--

CREATE TABLE `oc_cdek_order_package_item` (
  `package_item_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `ware_key` varchar(20) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `weight` int(8) NOT NULL DEFAULT 0,
  `amount` int(8) NOT NULL,
  `cost` float(15,4) NOT NULL DEFAULT 0.0000,
  `payment` float(15,4) NOT NULL DEFAULT 0.0000
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_cdek_order_reason`
--

CREATE TABLE `oc_cdek_order_reason` (
  `reason_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `date` int(10) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_cdek_order_schedule`
--

CREATE TABLE `oc_cdek_order_schedule` (
  `attempt_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `date` int(10) NOT NULL,
  `time_beg` time NOT NULL,
  `time_end` time NOT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `recipient_name` varchar(128) DEFAULT NULL,
  `address_street` varchar(50) DEFAULT NULL,
  `address_house` varchar(30) DEFAULT NULL,
  `address_flat` varchar(10) DEFAULT NULL,
  `address_pvz_code` varchar(10) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_cdek_order_schedule_delay`
--

CREATE TABLE `oc_cdek_order_schedule_delay` (
  `order_id` int(11) NOT NULL,
  `attempt_id` int(11) NOT NULL,
  `delay_id` int(11) NOT NULL,
  `description` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_cdek_order_status_history`
--

CREATE TABLE `oc_cdek_order_status_history` (
  `order_id` int(11) NOT NULL,
  `status_id` int(8) NOT NULL,
  `description` varchar(100) NOT NULL,
  `date` int(10) NOT NULL,
  `city_id` int(11) NOT NULL,
  `city_name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_city`
--

CREATE TABLE `oc_city` (
  `city_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `default_city` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_country`
--

CREATE TABLE `oc_country` (
  `country_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `iso_code_2` varchar(2) NOT NULL,
  `iso_code_3` varchar(3) NOT NULL,
  `address_format` text NOT NULL,
  `postcode_required` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_country`
--

INSERT INTO `oc_country` (`country_id`, `name`, `iso_code_2`, `iso_code_3`, `address_format`, `postcode_required`, `status`) VALUES
(1, 'Afghanistan', 'AF', 'AFG', '', 0, 1),
(2, 'Albania', 'AL', 'ALB', '', 0, 1),
(3, 'Algeria', 'DZ', 'DZA', '', 0, 1),
(4, 'American Samoa', 'AS', 'ASM', '', 0, 1),
(5, 'Andorra', 'AD', 'AND', '', 0, 1),
(6, 'Angola', 'AO', 'AGO', '', 0, 1),
(7, 'Anguilla', 'AI', 'AIA', '', 0, 1),
(8, 'Antarctica', 'AQ', 'ATA', '', 0, 1),
(9, 'Antigua and Barbuda', 'AG', 'ATG', '', 0, 1),
(10, 'Argentina', 'AR', 'ARG', '', 0, 1),
(11, 'Армения', 'AM', 'ARM', '', 0, 1),
(12, 'Aruba', 'AW', 'ABW', '', 0, 1),
(13, 'Australia', 'AU', 'AUS', '', 0, 1),
(14, 'Austria', 'AT', 'AUT', '', 0, 1),
(15, 'Азербайджан', 'AZ', 'AZE', '', 0, 1),
(16, 'Bahamas', 'BS', 'BHS', '', 0, 1),
(17, 'Bahrain', 'BH', 'BHR', '', 0, 1),
(18, 'Bangladesh', 'BD', 'BGD', '', 0, 1),
(19, 'Barbados', 'BB', 'BRB', '', 0, 1),
(20, 'Белоруссия (Беларусь)', 'BY', 'BLR', '', 0, 1),
(21, 'Belgium', 'BE', 'BEL', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 0, 1),
(22, 'Belize', 'BZ', 'BLZ', '', 0, 1),
(23, 'Benin', 'BJ', 'BEN', '', 0, 1),
(24, 'Bermuda', 'BM', 'BMU', '', 0, 1),
(25, 'Bhutan', 'BT', 'BTN', '', 0, 1),
(26, 'Bolivia', 'BO', 'BOL', '', 0, 1),
(27, 'Bosnia and Herzegovina', 'BA', 'BIH', '', 0, 1),
(28, 'Botswana', 'BW', 'BWA', '', 0, 1),
(29, 'Bouvet Island', 'BV', 'BVT', '', 0, 1),
(30, 'Brazil', 'BR', 'BRA', '', 0, 1),
(31, 'British Indian Ocean Territory', 'IO', 'IOT', '', 0, 1),
(32, 'Brunei Darussalam', 'BN', 'BRN', '', 0, 1),
(33, 'Bulgaria', 'BG', 'BGR', '', 0, 1),
(34, 'Burkina Faso', 'BF', 'BFA', '', 0, 1),
(35, 'Burundi', 'BI', 'BDI', '', 0, 1),
(36, 'Cambodia', 'KH', 'KHM', '', 0, 1),
(37, 'Cameroon', 'CM', 'CMR', '', 0, 1),
(38, 'Canada', 'CA', 'CAN', '', 0, 1),
(39, 'Cape Verde', 'CV', 'CPV', '', 0, 1),
(40, 'Cayman Islands', 'KY', 'CYM', '', 0, 1),
(41, 'Central African Republic', 'CF', 'CAF', '', 0, 1),
(42, 'Chad', 'TD', 'TCD', '', 0, 1),
(43, 'Chile', 'CL', 'CHL', '', 0, 1),
(44, 'China', 'CN', 'CHN', '', 0, 1),
(45, 'Christmas Island', 'CX', 'CXR', '', 0, 1),
(46, 'Cocos (Keeling) Islands', 'CC', 'CCK', '', 0, 1),
(47, 'Colombia', 'CO', 'COL', '', 0, 1),
(48, 'Comoros', 'KM', 'COM', '', 0, 1),
(49, 'Congo', 'CG', 'COG', '', 0, 1),
(50, 'Cook Islands', 'CK', 'COK', '', 0, 1),
(51, 'Costa Rica', 'CR', 'CRI', '', 0, 1),
(52, 'Cote D\'Ivoire', 'CI', 'CIV', '', 0, 1),
(53, 'Croatia', 'HR', 'HRV', '', 0, 1),
(54, 'Cuba', 'CU', 'CUB', '', 0, 1),
(55, 'Cyprus', 'CY', 'CYP', '', 0, 1),
(56, 'Czech Republic', 'CZ', 'CZE', '', 0, 1),
(57, 'Denmark', 'DK', 'DNK', '', 0, 1),
(58, 'Djibouti', 'DJ', 'DJI', '', 0, 1),
(59, 'Dominica', 'DM', 'DMA', '', 0, 1),
(60, 'Dominican Republic', 'DO', 'DOM', '', 0, 1),
(61, 'East Timor', 'TL', 'TLS', '', 0, 1),
(62, 'Ecuador', 'EC', 'ECU', '', 0, 1),
(63, 'Egypt', 'EG', 'EGY', '', 0, 1),
(64, 'El Salvador', 'SV', 'SLV', '', 0, 1),
(65, 'Equatorial Guinea', 'GQ', 'GNQ', '', 0, 1),
(66, 'Eritrea', 'ER', 'ERI', '', 0, 1),
(67, 'Estonia', 'EE', 'EST', '', 0, 1),
(68, 'Ethiopia', 'ET', 'ETH', '', 0, 1),
(69, 'Falkland Islands (Malvinas)', 'FK', 'FLK', '', 0, 1),
(70, 'Faroe Islands', 'FO', 'FRO', '', 0, 1),
(71, 'Fiji', 'FJ', 'FJI', '', 0, 1),
(72, 'Finland', 'FI', 'FIN', '', 0, 1),
(74, 'France, Metropolitan', 'FR', 'FRA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(75, 'French Guiana', 'GF', 'GUF', '', 0, 1),
(76, 'French Polynesia', 'PF', 'PYF', '', 0, 1),
(77, 'French Southern Territories', 'TF', 'ATF', '', 0, 1),
(78, 'Gabon', 'GA', 'GAB', '', 0, 1),
(79, 'Gambia', 'GM', 'GMB', '', 0, 1),
(80, 'Грузия', 'GE', 'GEO', '', 0, 1),
(81, 'Germany', 'DE', 'DEU', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(82, 'Ghana', 'GH', 'GHA', '', 0, 1),
(83, 'Gibraltar', 'GI', 'GIB', '', 0, 1),
(84, 'Greece', 'GR', 'GRC', '', 0, 1),
(85, 'Greenland', 'GL', 'GRL', '', 0, 1),
(86, 'Grenada', 'GD', 'GRD', '', 0, 1),
(87, 'Guadeloupe', 'GP', 'GLP', '', 0, 1),
(88, 'Guam', 'GU', 'GUM', '', 0, 1),
(89, 'Guatemala', 'GT', 'GTM', '', 0, 1),
(90, 'Guinea', 'GN', 'GIN', '', 0, 1),
(91, 'Guinea-Bissau', 'GW', 'GNB', '', 0, 1),
(92, 'Guyana', 'GY', 'GUY', '', 0, 1),
(93, 'Haiti', 'HT', 'HTI', '', 0, 1),
(94, 'Heard and Mc Donald Islands', 'HM', 'HMD', '', 0, 1),
(95, 'Honduras', 'HN', 'HND', '', 0, 1),
(96, 'Hong Kong', 'HK', 'HKG', '', 0, 1),
(97, 'Hungary', 'HU', 'HUN', '', 0, 1),
(98, 'Iceland', 'IS', 'ISL', '', 0, 1),
(99, 'India', 'IN', 'IND', '', 0, 1),
(100, 'Indonesia', 'ID', 'IDN', '', 0, 1),
(101, 'Iran (Islamic Republic of)', 'IR', 'IRN', '', 0, 1),
(102, 'Iraq', 'IQ', 'IRQ', '', 0, 1),
(103, 'Ireland', 'IE', 'IRL', '', 0, 1),
(104, 'Israel', 'IL', 'ISR', '', 0, 1),
(105, 'Italy', 'IT', 'ITA', '', 0, 1),
(106, 'Jamaica', 'JM', 'JAM', '', 0, 1),
(107, 'Japan', 'JP', 'JPN', '', 0, 1),
(108, 'Jordan', 'JO', 'JOR', '', 0, 1),
(109, 'Казахстан', 'KZ', 'KAZ', '', 0, 1),
(110, 'Kenya', 'KE', 'KEN', '', 0, 1),
(111, 'Kiribati', 'KI', 'KIR', '', 0, 1),
(112, 'North Korea', 'KP', 'PRK', '', 0, 1),
(113, 'South Korea', 'KR', 'KOR', '', 0, 1),
(114, 'Kuwait', 'KW', 'KWT', '', 0, 1),
(115, 'Киргизия (Кыргызстан)', 'KG', 'KGZ', '', 0, 1),
(116, 'Lao People\'s Democratic Republic', 'LA', 'LAO', '', 0, 1),
(117, 'Latvia', 'LV', 'LVA', '', 0, 1),
(118, 'Lebanon', 'LB', 'LBN', '', 0, 1),
(119, 'Lesotho', 'LS', 'LSO', '', 0, 1),
(120, 'Liberia', 'LR', 'LBR', '', 0, 1),
(121, 'Libyan Arab Jamahiriya', 'LY', 'LBY', '', 0, 1),
(122, 'Liechtenstein', 'LI', 'LIE', '', 0, 1),
(123, 'Lithuania', 'LT', 'LTU', '', 0, 1),
(124, 'Luxembourg', 'LU', 'LUX', '', 0, 1),
(125, 'Macau', 'MO', 'MAC', '', 0, 1),
(126, 'FYROM', 'MK', 'MKD', '', 0, 1),
(127, 'Madagascar', 'MG', 'MDG', '', 0, 1),
(128, 'Malawi', 'MW', 'MWI', '', 0, 1),
(129, 'Malaysia', 'MY', 'MYS', '', 0, 1),
(130, 'Maldives', 'MV', 'MDV', '', 0, 1),
(131, 'Mali', 'ML', 'MLI', '', 0, 1),
(132, 'Malta', 'MT', 'MLT', '', 0, 1),
(133, 'Marshall Islands', 'MH', 'MHL', '', 0, 1),
(134, 'Martinique', 'MQ', 'MTQ', '', 0, 1),
(135, 'Mauritania', 'MR', 'MRT', '', 0, 1),
(136, 'Mauritius', 'MU', 'MUS', '', 0, 1),
(137, 'Mayotte', 'YT', 'MYT', '', 0, 1),
(138, 'Mexico', 'MX', 'MEX', '', 0, 1),
(139, 'Micronesia, Federated States of', 'FM', 'FSM', '', 0, 1),
(140, 'Молдова', 'MD', 'MDA', '', 0, 1),
(141, 'Monaco', 'MC', 'MCO', '', 0, 1),
(142, 'Mongolia', 'MN', 'MNG', '', 0, 1),
(143, 'Montserrat', 'MS', 'MSR', '', 0, 1),
(144, 'Morocco', 'MA', 'MAR', '', 0, 1),
(145, 'Mozambique', 'MZ', 'MOZ', '', 0, 1),
(146, 'Myanmar', 'MM', 'MMR', '', 0, 1),
(147, 'Namibia', 'NA', 'NAM', '', 0, 1),
(148, 'Nauru', 'NR', 'NRU', '', 0, 1),
(149, 'Nepal', 'NP', 'NPL', '', 0, 1),
(150, 'Netherlands', 'NL', 'NLD', '', 0, 1),
(151, 'Netherlands Antilles', 'AN', 'ANT', '', 0, 1),
(152, 'New Caledonia', 'NC', 'NCL', '', 0, 1),
(153, 'New Zealand', 'NZ', 'NZL', '', 0, 1),
(154, 'Nicaragua', 'NI', 'NIC', '', 0, 1),
(155, 'Niger', 'NE', 'NER', '', 0, 1),
(156, 'Nigeria', 'NG', 'NGA', '', 0, 1),
(157, 'Niue', 'NU', 'NIU', '', 0, 1),
(158, 'Norfolk Island', 'NF', 'NFK', '', 0, 1),
(159, 'Northern Mariana Islands', 'MP', 'MNP', '', 0, 1),
(160, 'Norway', 'NO', 'NOR', '', 0, 1),
(161, 'Oman', 'OM', 'OMN', '', 0, 1),
(162, 'Pakistan', 'PK', 'PAK', '', 0, 1),
(163, 'Palau', 'PW', 'PLW', '', 0, 1),
(164, 'Panama', 'PA', 'PAN', '', 0, 1),
(165, 'Papua New Guinea', 'PG', 'PNG', '', 0, 1),
(166, 'Paraguay', 'PY', 'PRY', '', 0, 1),
(167, 'Peru', 'PE', 'PER', '', 0, 1),
(168, 'Philippines', 'PH', 'PHL', '', 0, 1),
(169, 'Pitcairn', 'PN', 'PCN', '', 0, 1),
(170, 'Poland', 'PL', 'POL', '', 0, 1),
(171, 'Portugal', 'PT', 'PRT', '', 0, 1),
(172, 'Puerto Rico', 'PR', 'PRI', '', 0, 1),
(173, 'Qatar', 'QA', 'QAT', '', 0, 1),
(174, 'Reunion', 'RE', 'REU', '', 0, 1),
(175, 'Romania', 'RO', 'ROM', '', 0, 1),
(176, 'Российская Федерация', 'RU', 'RUS', '', 0, 1),
(177, 'Rwanda', 'RW', 'RWA', '', 0, 1),
(178, 'Saint Kitts and Nevis', 'KN', 'KNA', '', 0, 1),
(179, 'Saint Lucia', 'LC', 'LCA', '', 0, 1),
(180, 'Saint Vincent and the Grenadines', 'VC', 'VCT', '', 0, 1),
(181, 'Samoa', 'WS', 'WSM', '', 0, 1),
(182, 'San Marino', 'SM', 'SMR', '', 0, 1),
(183, 'Sao Tome and Principe', 'ST', 'STP', '', 0, 1),
(184, 'Saudi Arabia', 'SA', 'SAU', '', 0, 1),
(185, 'Senegal', 'SN', 'SEN', '', 0, 1),
(186, 'Seychelles', 'SC', 'SYC', '', 0, 1),
(187, 'Sierra Leone', 'SL', 'SLE', '', 0, 1),
(188, 'Singapore', 'SG', 'SGP', '', 0, 1),
(189, 'Slovak Republic', 'SK', 'SVK', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city} {postcode}\r\n{zone}\r\n{country}', 0, 1),
(190, 'Slovenia', 'SI', 'SVN', '', 0, 1),
(191, 'Solomon Islands', 'SB', 'SLB', '', 0, 1),
(192, 'Somalia', 'SO', 'SOM', '', 0, 1),
(193, 'South Africa', 'ZA', 'ZAF', '', 0, 1),
(194, 'South Georgia &amp; South Sandwich Islands', 'GS', 'SGS', '', 0, 1),
(195, 'Spain', 'ES', 'ESP', '', 0, 1),
(196, 'Sri Lanka', 'LK', 'LKA', '', 0, 1),
(197, 'St. Helena', 'SH', 'SHN', '', 0, 1),
(198, 'St. Pierre and Miquelon', 'PM', 'SPM', '', 0, 1),
(199, 'Sudan', 'SD', 'SDN', '', 0, 1),
(200, 'Suriname', 'SR', 'SUR', '', 0, 1),
(201, 'Svalbard and Jan Mayen Islands', 'SJ', 'SJM', '', 0, 1),
(202, 'Swaziland', 'SZ', 'SWZ', '', 0, 1),
(203, 'Sweden', 'SE', 'SWE', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(204, 'Switzerland', 'CH', 'CHE', '', 0, 1),
(205, 'Syrian Arab Republic', 'SY', 'SYR', '', 0, 1),
(206, 'Taiwan', 'TW', 'TWN', '', 0, 1),
(207, 'Таджикистан', 'TJ', 'TJK', '', 0, 1),
(208, 'Tanzania, United Republic of', 'TZ', 'TZA', '', 0, 1),
(209, 'Thailand', 'TH', 'THA', '', 0, 1),
(210, 'Togo', 'TG', 'TGO', '', 0, 1),
(211, 'Tokelau', 'TK', 'TKL', '', 0, 1),
(212, 'Tonga', 'TO', 'TON', '', 0, 1),
(213, 'Trinidad and Tobago', 'TT', 'TTO', '', 0, 1),
(214, 'Tunisia', 'TN', 'TUN', '', 0, 1),
(215, 'Turkey', 'TR', 'TUR', '', 0, 1),
(216, 'Туркменистан', 'TM', 'TKM', '', 0, 1),
(217, 'Turks and Caicos Islands', 'TC', 'TCA', '', 0, 1),
(218, 'Tuvalu', 'TV', 'TUV', '', 0, 1),
(219, 'Uganda', 'UG', 'UGA', '', 0, 1),
(220, 'Украина', 'UA', 'UKR', '', 0, 1),
(221, 'United Arab Emirates', 'AE', 'ARE', '', 0, 1),
(222, 'United Kingdom', 'GB', 'GBR', '', 1, 1),
(223, 'United States', 'US', 'USA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city}, {zone} {postcode}\r\n{country}', 0, 1),
(224, 'United States Minor Outlying Islands', 'UM', 'UMI', '', 0, 1),
(225, 'Uruguay', 'UY', 'URY', '', 0, 1),
(226, 'Узбекистан', 'UZ', 'UZB', '', 0, 1),
(227, 'Vanuatu', 'VU', 'VUT', '', 0, 1),
(228, 'Vatican City State (Holy See)', 'VA', 'VAT', '', 0, 1),
(229, 'Venezuela', 'VE', 'VEN', '', 0, 1),
(230, 'Viet Nam', 'VN', 'VNM', '', 0, 1),
(231, 'Virgin Islands (British)', 'VG', 'VGB', '', 0, 1),
(232, 'Virgin Islands (U.S.)', 'VI', 'VIR', '', 0, 1),
(233, 'Wallis and Futuna Islands', 'WF', 'WLF', '', 0, 1),
(234, 'Western Sahara', 'EH', 'ESH', '', 0, 1),
(235, 'Yemen', 'YE', 'YEM', '', 0, 1),
(237, 'Democratic Republic of Congo', 'CD', 'COD', '', 0, 1),
(238, 'Zambia', 'ZM', 'ZMB', '', 0, 1),
(239, 'Zimbabwe', 'ZW', 'ZWE', '', 0, 1),
(242, 'Montenegro', 'ME', 'MNE', '', 0, 1),
(243, 'Serbia', 'RS', 'SRB', '', 0, 1),
(244, 'Aaland Islands', 'AX', 'ALA', '', 0, 1),
(245, 'Bonaire, Sint Eustatius and Saba', 'BQ', 'BES', '', 0, 1),
(246, 'Curacao', 'CW', 'CUW', '', 0, 1),
(247, 'Palestinian Territory, Occupied', 'PS', 'PSE', '', 0, 1),
(248, 'South Sudan', 'SS', 'SSD', '', 0, 1),
(249, 'St. Barthelemy', 'BL', 'BLM', '', 0, 1),
(250, 'St. Martin (French part)', 'MF', 'MAF', '', 0, 1),
(251, 'Canary Islands', 'IC', 'ICA', '', 0, 1),
(252, 'Ascension Island (British)', 'AC', 'ASC', '', 0, 1),
(253, 'Kosovo, Republic of', 'XK', 'UNK', '', 0, 1),
(254, 'Isle of Man', 'IM', 'IMN', '', 0, 1),
(255, 'Tristan da Cunha', 'TA', 'SHN', '', 0, 1),
(256, 'Guernsey', 'GG', 'GGY', '', 0, 1),
(257, 'Jersey', 'JE', 'JEY', '', 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_coupon`
--

CREATE TABLE `oc_coupon` (
  `coupon_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(20) NOT NULL,
  `type` char(1) NOT NULL,
  `discount` decimal(15,4) NOT NULL,
  `logged` tinyint(1) NOT NULL,
  `shipping` tinyint(1) NOT NULL,
  `total` decimal(15,4) NOT NULL,
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00',
  `uses_total` int(11) NOT NULL,
  `uses_customer` varchar(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_coupon`
--

INSERT INTO `oc_coupon` (`coupon_id`, `name`, `code`, `type`, `discount`, `logged`, `shipping`, `total`, `date_start`, `date_end`, `uses_total`, `uses_customer`, `status`, `date_added`) VALUES
(4, '-10% Discount', '2222', 'P', '10.0000', 0, 0, '0.0000', '2014-01-01', '2020-01-01', 10, '10', 0, '2009-01-27 13:55:03'),
(5, 'Free Shipping', '3333', 'P', '0.0000', 0, 1, '100.0000', '2014-01-01', '2014-02-01', 10, '10', 0, '2009-03-14 21:13:53'),
(6, '-10.00 Discount', '1111', 'F', '10.0000', 0, 0, '10.0000', '2014-01-01', '2020-01-01', 100000, '10000', 0, '2009-03-14 21:15:18');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_coupon_category`
--

CREATE TABLE `oc_coupon_category` (
  `coupon_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_coupon_history`
--

CREATE TABLE `oc_coupon_history` (
  `coupon_history_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_coupon_product`
--

CREATE TABLE `oc_coupon_product` (
  `coupon_product_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_currency`
--

CREATE TABLE `oc_currency` (
  `currency_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `code` varchar(3) NOT NULL,
  `symbol_left` varchar(12) NOT NULL,
  `symbol_right` varchar(12) NOT NULL,
  `decimal_place` char(1) NOT NULL,
  `value` double(15,8) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_currency`
--

INSERT INTO `oc_currency` (`currency_id`, `title`, `code`, `symbol_left`, `symbol_right`, `decimal_place`, `value`, `status`, `date_modified`) VALUES
(1, 'Рубль', 'RUB', '', ' ₽', '2', 1.00000000, 1, '2021-09-16 05:05:35'),
(2, 'US Dollar', 'USD', '$', '', '2', 0.01697793, 0, '2019-05-16 14:48:35'),
(3, 'Euro', 'EUR', '', '€', '2', 0.01476363, 0, '2019-05-16 14:48:31'),
(4, 'Гривна', 'UAH', '', 'грн.', '2', 0.44016022, 0, '2019-05-16 14:48:43');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer`
--

CREATE TABLE `oc_customer` (
  `customer_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT 0,
  `language_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `cart` text DEFAULT NULL,
  `wishlist` text DEFAULT NULL,
  `newsletter` tinyint(1) NOT NULL DEFAULT 0,
  `address_id` int(11) NOT NULL DEFAULT 0,
  `custom_field` text NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `safe` tinyint(1) NOT NULL,
  `token` text NOT NULL,
  `code` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_customer`
--

INSERT INTO `oc_customer` (`customer_id`, `customer_group_id`, `store_id`, `language_id`, `firstname`, `lastname`, `email`, `telephone`, `fax`, `password`, `salt`, `cart`, `wishlist`, `newsletter`, `address_id`, `custom_field`, `ip`, `status`, `safe`, `token`, `code`, `date_added`) VALUES
(20, 1, 0, 1, 'demonized', 'demonized', 'demonized@4ait.ru', '89144108722', '', 'fac47aed677a9b7cdd7af46eb425094415e04080', 'L00TgNdHB', NULL, NULL, 1, 10, '', '127.0.0.1', 1, 0, '', '', '2021-02-25 18:37:05'),
(21, 1, 0, 1, 'ddd', 'ddd', 'themrdemonized@gmail.com', '84549479742', '', 'bbf0ee96f65914ea4eb2c5caf8dacfbd46395860', 'QXycPYPaB', NULL, NULL, 1, 0, '', '127.0.0.1', 1, 0, '', '', '2021-03-02 00:15:29');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_activity`
--

CREATE TABLE `oc_customer_activity` (
  `customer_activity_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `key` varchar(64) NOT NULL,
  `data` text NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_customer_activity`
--

INSERT INTO `oc_customer_activity` (`customer_activity_id`, `customer_id`, `key`, `data`, `ip`, `date_added`) VALUES
(1, 0, 'order_guest', '{\"name\":\"Dmitry \",\"order_id\":42}', '192.168.11.1', '2019-08-05 20:20:00'),
(2, 0, 'order_guest', '{\"name\":\"Dmitry \",\"order_id\":43}', '192.168.11.1', '2019-08-05 20:42:43'),
(3, 0, 'order_guest', '{\"name\":\"Dmitry \",\"order_id\":45}', '192.168.11.1', '2019-08-05 21:50:48'),
(4, 0, 'order_guest', '{\"name\":\"Dmitry \",\"order_id\":46}', '192.168.11.1', '2019-08-05 22:06:05'),
(5, 0, 'order_guest', '{\"name\":\" \",\"order_id\":47}', '192.168.11.1', '2019-08-06 11:07:32'),
(6, 0, 'order_guest', '{\"name\":\"\\u041c\\u0430\\u0440\\u0435\\u043d\\u043a\\u0430\\u043d \\u0412 \\u0421 \",\"order_id\":48}', '192.168.11.1', '2019-08-06 11:47:55'),
(7, 0, 'order_guest', '{\"name\":\"Qwe \",\"order_id\":49}', '192.168.11.1', '2019-08-06 11:49:44'),
(8, 0, 'order_guest', '{\"name\":\"Dmitry \",\"order_id\":50}', '192.168.11.1', '2019-08-06 14:43:48'),
(9, 0, 'order_guest', '{\"name\":\" \",\"order_id\":51}', '192.168.11.1', '2019-08-06 16:56:50'),
(10, 0, 'order_guest', '{\"name\":\" \",\"order_id\":52}', '192.168.11.1', '2019-08-06 17:02:49'),
(11, 0, 'order_guest', '{\"name\":\" \",\"order_id\":53}', '192.168.11.1', '2019-08-06 18:08:36'),
(12, 0, 'order_guest', '{\"name\":\" \",\"order_id\":54}', '192.168.11.1', '2019-08-06 18:09:46'),
(13, 0, 'order_guest', '{\"name\":\"wqeqw \",\"order_id\":55}', '192.168.11.1', '2019-08-06 18:11:54'),
(14, 0, 'order_guest', '{\"name\":\" \",\"order_id\":57}', '192.168.11.1', '2019-08-06 18:54:54'),
(15, 0, 'order_guest', '{\"name\":\" \",\"order_id\":58}', '192.168.11.1', '2019-08-07 11:27:38'),
(16, 0, 'order_guest', '{\"name\":\" \",\"order_id\":60}', '192.168.11.1', '2019-08-07 13:02:14'),
(17, 0, 'order_guest', '{\"name\":\" \",\"order_id\":61}', '192.168.11.1', '2019-08-07 13:03:50'),
(18, 0, 'order_guest', '{\"name\":\" \",\"order_id\":75}', '192.168.11.1', '2019-08-07 14:19:56'),
(19, 0, 'order_guest', '{\"name\":\" \",\"order_id\":78}', '192.168.11.1', '2019-08-07 14:45:28'),
(20, 0, 'order_guest', '{\"name\":\" \",\"order_id\":79}', '192.168.11.1', '2019-08-07 15:39:12'),
(21, 0, 'order_guest', '{\"name\":\" \",\"order_id\":80}', '192.168.11.1', '2019-08-07 15:44:45'),
(22, 0, 'order_guest', '{\"name\":\" \",\"order_id\":82}', '192.168.11.1', '2019-08-07 17:33:16'),
(23, 0, 'order_guest', '{\"name\":\" \",\"order_id\":83}', '192.168.11.1', '2019-08-07 17:44:12'),
(24, 0, 'order_guest', '{\"name\":\" \",\"order_id\":84}', '192.168.11.1', '2019-08-07 17:48:00'),
(25, 0, 'order_guest', '{\"name\":\" \",\"order_id\":85}', '192.168.11.1', '2019-08-07 19:05:54'),
(26, 0, 'order_guest', '{\"name\":\" \",\"order_id\":86}', '192.168.11.1', '2019-08-07 19:09:00'),
(27, 0, 'order_guest', '{\"name\":\" \",\"order_id\":87}', '192.168.11.1', '2019-08-07 19:16:01'),
(28, 0, 'order_guest', '{\"name\":\" \",\"order_id\":88}', '192.168.11.1', '2019-08-07 19:38:26'),
(29, 0, 'order_guest', '{\"name\":\" \",\"order_id\":89}', '192.168.11.1', '2019-08-07 19:41:17'),
(30, 0, 'order_guest', '{\"name\":\" \",\"order_id\":90}', '192.168.11.1', '2019-08-07 19:48:08'),
(31, 0, 'order_guest', '{\"name\":\" \",\"order_id\":91}', '192.168.11.1', '2019-08-07 19:50:20'),
(32, 0, 'order_guest', '{\"name\":\" \",\"order_id\":92}', '192.168.11.1', '2019-08-07 19:56:14'),
(33, 0, 'order_guest', '{\"name\":\" \",\"order_id\":93}', '192.168.11.1', '2019-08-08 10:10:46'),
(34, 0, 'order_guest', '{\"name\":\" \",\"order_id\":94}', '192.168.11.1', '2019-08-08 14:06:55'),
(35, 0, 'order_guest', '{\"name\":\" \",\"order_id\":95}', '192.168.11.1', '2019-08-08 14:09:14'),
(36, 0, 'order_guest', '{\"name\":\" \",\"order_id\":96}', '192.168.11.1', '2019-08-08 15:02:31'),
(37, 0, 'order_guest', '{\"name\":\"Dmitry \",\"order_id\":97}', '192.168.11.1', '2019-08-08 15:08:28'),
(38, 0, 'order_guest', '{\"name\":\" \",\"order_id\":98}', '192.168.11.1', '2019-08-08 15:15:13'),
(39, 0, 'order_guest', '{\"name\":\"Dmitry \",\"order_id\":99}', '192.168.11.1', '2019-08-08 15:15:48'),
(40, 0, 'order_guest', '{\"name\":\" \",\"order_id\":100}', '192.168.11.1', '2019-08-09 13:22:45'),
(41, 0, 'order_guest', '{\"name\":\" \",\"order_id\":101}', '192.168.11.1', '2019-08-11 17:20:18'),
(42, 0, 'order_guest', '{\"name\":\" \",\"order_id\":103}', '192.168.11.1', '2019-08-16 12:32:10'),
(57, 0, 'order_guest', '{\"name\":\" \",\"order_id\":106}', '192.168.11.1', '2019-08-20 14:10:59'),
(58, 0, 'order_guest', '{\"name\":\" \",\"order_id\":107}', '192.168.11.1', '2019-08-21 19:23:56'),
(59, 0, 'order_guest', '{\"name\":\" \",\"order_id\":108}', '192.168.11.1', '2019-09-02 16:47:56'),
(60, 0, 'order_guest', '{\"name\":\"Daniil \",\"order_id\":109}', '192.168.11.1', '2019-09-06 19:19:50'),
(61, 0, 'order_guest', '{\"name\":\" \",\"order_id\":110}', '192.168.11.1', '2019-09-06 19:47:02'),
(62, 0, 'order_guest', '{\"name\":\" \",\"order_id\":111}', '192.168.11.1', '2019-09-07 13:02:44'),
(67, 0, 'order_guest', '{\"name\":\" \",\"order_id\":113}', '192.168.11.1', '2019-09-10 21:22:14'),
(68, 0, 'order_guest', '{\"name\":\" \",\"order_id\":114}', '192.168.11.1', '2019-09-11 12:03:32'),
(69, 0, 'order_guest', '{\"name\":\" \",\"order_id\":115}', '192.168.11.1', '2019-09-16 15:03:23'),
(71, 0, 'order_guest', '{\"name\":\"\\u041c\\u0430\\u043c\\u0435\\u0448\\u0438\\u043d \\u0410\\u043b\\u0435\\u043a\\u0441\\u0435\\u0439 \\u0410\\u043b\\u0435\\u043a\\u0441\\u0430\\u043d\\u0434\\u0440\\u043e\\u0432\\u0438\\u0447  \",\"order_id\":116}', '192.168.11.1', '2019-09-16 21:16:22'),
(72, 0, 'order_guest', '{\"name\":\" \",\"order_id\":117}', '192.168.11.1', '2019-09-18 15:26:42'),
(73, 0, 'order_guest', '{\"name\":\"\\u0414\\u043c\\u0438\\u0442\\u0440\\u0438\\u0439 \",\"order_id\":118}', '192.168.11.1', '2019-09-18 15:38:45'),
(74, 0, 'order_guest', '{\"name\":\"Dmitry \",\"order_id\":119}', '192.168.11.1', '2019-09-18 15:44:49'),
(75, 0, 'order_guest', '{\"name\":\"Dmitry \",\"order_id\":120}', '192.168.11.1', '2019-09-18 15:54:52'),
(76, 0, 'order_guest', '{\"name\":\"Dmitry \",\"order_id\":121}', '192.168.11.1', '2019-09-18 16:11:23'),
(77, 0, 'order_guest', '{\"name\":\"Dmitry \",\"order_id\":122}', '192.168.11.1', '2019-09-18 16:14:25'),
(78, 0, 'order_guest', '{\"name\":\"Dmitry \",\"order_id\":123}', '192.168.11.1', '2019-09-18 16:15:34'),
(79, 0, 'order_guest', '{\"name\":\"Dmitry \",\"order_id\":124}', '192.168.11.1', '2019-09-18 16:17:43'),
(80, 0, 'order_guest', '{\"name\":\"Dmitry Chernyavsky \",\"order_id\":125}', '192.168.11.1', '2019-09-18 16:21:53'),
(82, 0, 'order_guest', '{\"name\":\"\\u041a\\u043b\\u0438\\u043c\\u043a\\u043e\\u0432\\u0438\\u0447 \\u041d\\u0438\\u043d\\u0430 \",\"order_id\":127}', '192.168.11.1', '2019-09-24 17:32:33'),
(83, 0, 'order_guest', '{\"name\":\" \",\"order_id\":128}', '192.168.11.1', '2019-10-01 09:25:50'),
(85, 0, 'order_guest', '{\"name\":\"\\u041a\\u043b\\u0438\\u043c\\u043a\\u043e\\u0432\\u0438\\u0447 \\u041c\\u0438\\u0445\\u0430\\u0438\\u043b  \",\"order_id\":129}', '192.168.11.1', '2019-10-03 21:35:05'),
(86, 0, 'order_guest', '{\"name\":\" \",\"order_id\":130}', '192.168.99.1', '2019-10-22 15:42:00'),
(87, 0, 'order_guest', '{\"name\":\"\\u041f\\u0440\\u043e\\u0438\\u0437\\u0432\\u043e\\u0434\\u0441\\u0442\\u0432\\u043e \",\"order_id\":131}', '192.168.99.1', '2019-10-29 18:50:46'),
(88, 0, 'order_guest', '{\"name\":\"Dmitry \",\"order_id\":132}', '192.168.99.1', '2019-10-31 18:39:01'),
(91, 0, 'order_guest', '{\"name\":\"\\u0422\\u043e\\u0440\\u043e\\u0441\\u044f\\u043d \\u0420\\u0430\\u0444\\u0430\\u044d\\u043b\\u043b\\u0430 \",\"order_id\":133}', '192.168.99.1', '2019-11-27 16:39:07'),
(94, 0, 'order_guest', '{\"name\":\"\\u0421\\u0432\\u0435\\u0442\\u043b\\u0430\\u043d\\u0430 \\u0415\\u0440\\u043e\\u0445\\u0438\\u043d\\u0430 \",\"order_id\":134}', '192.168.99.1', '2019-12-06 11:41:32'),
(97, 0, 'order_guest', '{\"name\":\" \",\"order_id\":135}', '192.168.99.1', '2019-12-18 14:18:17'),
(99, 0, 'order_guest', '{\"name\":\"\\u0428\\u0435\\u0441\\u0442\\u0430\\u043a\\u043e\\u0432\\u0430 \\u041d\\u0438\\u043d\\u0430 \",\"order_id\":136}', '192.168.99.1', '2019-12-26 16:52:03'),
(102, 0, 'order_guest', '{\"name\":\" \",\"order_id\":137}', '192.168.99.1', '2020-01-07 19:05:37'),
(104, 0, 'order_guest', '{\"name\":\"\\u0418\\u041f \\u041c\\u0430\\u0441\\u043b\\u0430\\u043a\\u043e\\u0432\\u0430 \\u0418. \\u0414.  \",\"order_id\":138}', '192.168.99.1', '2020-01-10 10:01:51'),
(105, 0, 'order_guest', '{\"name\":\" \",\"order_id\":139}', '192.168.99.1', '2020-01-10 12:55:36'),
(117, 0, 'order_guest', '{\"name\":\" \",\"order_id\":140}', '192.168.99.1', '2020-01-20 16:29:27'),
(118, 0, 'order_guest', '{\"name\":\" \",\"order_id\":141}', '192.168.99.1', '2020-01-22 15:16:22'),
(120, 0, 'order_guest', '{\"name\":\" \",\"order_id\":142}', '192.168.99.1', '2020-01-29 14:39:26'),
(121, 0, 'order_guest', '{\"name\":\" \",\"order_id\":143}', '192.168.99.1', '2020-02-18 16:36:49'),
(122, 0, 'order_guest', '{\"name\":\" \",\"order_id\":144}', '192.168.99.1', '2020-02-19 20:37:29'),
(123, 0, 'order_guest', '{\"name\":\" \",\"order_id\":145}', '192.168.99.1', '2020-02-20 18:51:57'),
(124, 0, 'order_guest', '{\"name\":\"\\u041c\\u0430\\u043c\\u0435\\u0448\\u0438\\u043d \\u0410. \\u0410.  \",\"order_id\":146}', '192.168.99.1', '2020-02-21 11:15:18'),
(125, 0, 'order_guest', '{\"name\":\" \",\"order_id\":147}', '192.168.99.1', '2020-03-02 14:17:16'),
(126, 0, 'order_guest', '{\"name\":\" \",\"order_id\":148}', '192.168.99.1', '2020-03-02 14:22:01'),
(128, 0, 'order_guest', '{\"name\":\"\\u041f\\u043e\\u0441\\u0442\\u043e\\u043b\\u0430\\u043a\\u044f\\u043d \\u0412\\u0430\\u0440\\u0434\\u0443\\u0438 \",\"order_id\":149}', '192.168.99.1', '2020-03-10 10:29:06'),
(129, 0, 'order_guest', '{\"name\":\" \",\"order_id\":150}', '192.168.99.1', '2020-03-16 15:33:06'),
(130, 0, 'order_guest', '{\"name\":\"\\u0410\\u0440\\u0442\\u0435\\u043c \\u0410\\u043d\\u0442\\u043e\\u0448\\u0438\\u043a \",\"order_id\":151}', '192.168.99.1', '2020-03-25 11:53:20'),
(131, 0, 'order_guest', '{\"name\":\" \",\"order_id\":152}', '127.0.0.1', '2020-04-02 23:08:26'),
(132, 0, 'order_guest', '{\"name\":\" \",\"order_id\":153}', '192.168.99.1', '2020-04-03 12:28:42'),
(133, 20, 'register', '{\"customer_id\":20,\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-25 18:37:05'),
(134, 20, 'edit', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-25 21:53:13'),
(135, 20, 'edit', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-25 22:17:32'),
(136, 20, 'edit', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-25 22:17:37'),
(137, 20, 'edit', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-26 01:29:48'),
(138, 20, 'address_add', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-26 16:16:42'),
(139, 20, 'login', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-26 17:51:33'),
(140, 20, 'address_edit', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-26 18:01:50'),
(141, 20, 'address_edit', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-26 18:11:38'),
(142, 20, 'address_edit', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-26 18:12:13'),
(143, 20, 'address_edit', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-26 18:12:27'),
(144, 20, 'address_edit', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-26 18:18:32'),
(145, 20, 'address_add', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-26 18:22:13'),
(146, 20, 'address_add', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-26 18:44:01'),
(147, 20, 'address_edit', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-26 18:44:08'),
(148, 20, 'address_edit', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-26 18:44:18'),
(149, 20, 'address_edit', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-26 18:44:35'),
(150, 20, 'address_edit', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-26 18:44:40'),
(151, 20, 'address_edit', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-26 18:44:48'),
(152, 20, 'address_edit', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-26 18:46:53'),
(153, 20, 'address_edit', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-26 18:47:13'),
(154, 20, 'address_edit', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-26 18:47:24'),
(155, 20, 'address_edit', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-26 19:03:44'),
(156, 20, 'address_edit', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-26 19:04:46'),
(157, 20, 'address_edit', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-26 19:05:23'),
(158, 20, 'address_edit', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-26 20:00:24'),
(159, 20, 'address_edit', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-26 20:00:25'),
(160, 20, 'address_edit', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-26 20:00:30'),
(161, 20, 'address_delete', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-26 20:01:45'),
(162, 20, 'address_delete', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-26 20:04:48'),
(163, 20, 'address_add', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-26 20:05:24'),
(164, 20, 'address_edit', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-26 20:14:51'),
(165, 20, 'address_add', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-26 20:15:35'),
(166, 20, 'address_delete', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-26 20:15:38'),
(167, 20, 'order_account', '{\"customer_id\":\"20\",\"name\":\"fdsf \",\"order_id\":155}', '127.0.0.1', '2021-02-26 20:17:02'),
(168, 0, 'order_guest', '{\"name\":\" \",\"order_id\":156}', '127.0.0.1', '2021-02-26 20:23:05'),
(169, 20, 'login', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-26 20:24:02'),
(170, 20, 'order_account', '{\"customer_id\":\"20\",\"name\":\"fdsf \",\"order_id\":157}', '127.0.0.1', '2021-02-26 20:24:21'),
(171, 0, 'order_guest', '{\"name\":\"\\u0442\\u0435\\u0441\\u0442 \",\"order_id\":159}', '127.0.0.1', '2021-02-26 20:26:33'),
(172, 20, 'login', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-26 20:27:24'),
(173, 20, 'edit', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-26 20:28:52'),
(174, 20, 'edit', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-26 20:39:58'),
(175, 20, 'address_edit', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-26 20:40:12'),
(176, 20, 'address_add', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-26 20:40:57'),
(177, 20, 'address_delete', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-26 20:43:11'),
(178, 20, 'address_add', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-26 20:43:47'),
(179, 20, 'address_edit', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-26 20:44:06'),
(180, 20, 'address_edit', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-02-27 00:11:16'),
(181, 20, 'login', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-03-01 16:19:58'),
(182, 20, 'login', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-03-01 18:52:29'),
(183, 20, 'order_account', '{\"customer_id\":\"20\",\"name\":\"\\u0442\\u0435\\u0441\\u0442 \",\"order_id\":160}', '127.0.0.1', '2021-03-01 23:25:34'),
(184, 21, 'register', '{\"customer_id\":21,\"name\":\"ddd ddd\"}', '127.0.0.1', '2021-03-02 00:15:29'),
(185, 20, 'login', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-03-02 00:16:31'),
(186, 20, 'login', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-03-02 00:49:48'),
(187, 20, 'address_edit', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-03-02 01:11:20'),
(188, 20, 'order_account', '{\"customer_id\":\"20\",\"name\":\"\\u0442\\u0435\\u0441\\u0442 \",\"order_id\":161}', '127.0.0.1', '2021-03-02 01:11:55'),
(189, 20, 'address_edit', '{\"customer_id\":\"20\",\"name\":\"demonized demonized\"}', '127.0.0.1', '2021-03-02 01:14:29'),
(190, 0, 'order_guest', '{\"name\":\" \",\"order_id\":163}', '127.0.0.1', '2021-09-13 16:11:21');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_affiliate`
--

CREATE TABLE `oc_customer_affiliate` (
  `customer_id` int(11) NOT NULL,
  `company` varchar(40) NOT NULL,
  `website` varchar(255) NOT NULL,
  `tracking` varchar(64) NOT NULL,
  `commission` decimal(4,2) NOT NULL DEFAULT 0.00,
  `tax` varchar(64) NOT NULL,
  `payment` varchar(6) NOT NULL,
  `cheque` varchar(100) NOT NULL,
  `paypal` varchar(64) NOT NULL,
  `bank_name` varchar(64) NOT NULL,
  `bank_branch_number` varchar(64) NOT NULL,
  `bank_swift_code` varchar(64) NOT NULL,
  `bank_account_name` varchar(64) NOT NULL,
  `bank_account_number` varchar(64) NOT NULL,
  `custom_field` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_approval`
--

CREATE TABLE `oc_customer_approval` (
  `customer_approval_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `type` varchar(9) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_group`
--

CREATE TABLE `oc_customer_group` (
  `customer_group_id` int(11) NOT NULL,
  `approval` int(1) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_customer_group`
--

INSERT INTO `oc_customer_group` (`customer_group_id`, `approval`, `sort_order`) VALUES
(1, 0, 1),
(4, 0, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_group_description`
--

CREATE TABLE `oc_customer_group_description` (
  `customer_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_customer_group_description`
--

INSERT INTO `oc_customer_group_description` (`customer_group_id`, `language_id`, `name`, `description`) VALUES
(1, 1, 'Физическое лицо', 'Физическое лицо'),
(4, 1, 'Юридическое лицо', 'Юридическое лицо');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_history`
--

CREATE TABLE `oc_customer_history` (
  `customer_history_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_ip`
--

CREATE TABLE `oc_customer_ip` (
  `customer_ip_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_customer_ip`
--

INSERT INTO `oc_customer_ip` (`customer_ip_id`, `customer_id`, `ip`, `date_added`) VALUES
(21, 20, '127.0.0.1', '2021-02-25 18:37:05'),
(22, 21, '127.0.0.1', '2021-03-02 00:15:30');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_login`
--

CREATE TABLE `oc_customer_login` (
  `customer_login_id` int(11) NOT NULL,
  `email` varchar(96) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `total` int(4) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_customer_login`
--

INSERT INTO `oc_customer_login` (`customer_login_id`, `email`, `ip`, `total`, `date_added`, `date_modified`) VALUES
(1, 'nata.quality1980.rozovskaya@mail.ru', '192.168.11.1', 1, '2019-09-11 18:40:33', '2019-09-11 18:40:33'),
(2, '666@4ait.ru', '192.168.11.1', 2, '2019-09-18 02:47:51', '2019-09-18 02:48:02'),
(3, 'dos@4ait.ru', '192.168.11.1', 3, '2019-09-18 02:48:20', '2019-09-18 02:49:40'),
(5, '', '127.0.0.1', 1, '2021-02-25 07:58:04', '2021-02-25 07:58:04');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_online`
--

CREATE TABLE `oc_customer_online` (
  `ip` varchar(40) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `url` text NOT NULL,
  `referer` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_customer_online`
--

INSERT INTO `oc_customer_online` (`ip`, `customer_id`, `url`, `referer`, `date_added`) VALUES
('127.0.0.1', 0, 'http://arshaevy/', '', '2021-09-16 05:12:20');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_reward`
--

CREATE TABLE `oc_customer_reward` (
  `customer_reward_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT 0,
  `order_id` int(11) NOT NULL DEFAULT 0,
  `description` text NOT NULL,
  `points` int(8) NOT NULL DEFAULT 0,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_search`
--

CREATE TABLE `oc_customer_search` (
  `customer_search_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sub_category` tinyint(1) NOT NULL,
  `description` tinyint(1) NOT NULL,
  `products` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_transaction`
--

CREATE TABLE `oc_customer_transaction` (
  `customer_transaction_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_wishlist`
--

CREATE TABLE `oc_customer_wishlist` (
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_custom_field`
--

CREATE TABLE `oc_custom_field` (
  `custom_field_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `value` text NOT NULL,
  `validation` varchar(255) NOT NULL,
  `location` varchar(10) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `sort_order` int(3) NOT NULL,
  `dadata_field` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_custom_field`
--

INSERT INTO `oc_custom_field` (`custom_field_id`, `type`, `value`, `validation`, `location`, `status`, `sort_order`, `dadata_field`) VALUES
(1, 'text', '', '', 'address', 1, 10, 'PARTY');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_custom_field_customer_group`
--

CREATE TABLE `oc_custom_field_customer_group` (
  `custom_field_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `required` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_custom_field_customer_group`
--

INSERT INTO `oc_custom_field_customer_group` (`custom_field_id`, `customer_group_id`, `required`) VALUES
(1, 4, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_custom_field_description`
--

CREATE TABLE `oc_custom_field_description` (
  `custom_field_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_custom_field_description`
--

INSERT INTO `oc_custom_field_description` (`custom_field_id`, `language_id`, `name`) VALUES
(1, 1, 'Компания');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_custom_field_value`
--

CREATE TABLE `oc_custom_field_value` (
  `custom_field_value_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_custom_field_value_description`
--

CREATE TABLE `oc_custom_field_value_description` (
  `custom_field_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_download`
--

CREATE TABLE `oc_download` (
  `download_id` int(11) NOT NULL,
  `filename` varchar(160) NOT NULL,
  `mask` varchar(128) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_download_description`
--

CREATE TABLE `oc_download_description` (
  `download_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_email_template`
--

CREATE TABLE `oc_email_template` (
  `email_template_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `type` varchar(255) COLLATE utf8_bin NOT NULL,
  `background` varchar(7) COLLATE utf8_bin NOT NULL,
  `body` varchar(7) COLLATE utf8_bin NOT NULL,
  `heading` varchar(7) COLLATE utf8_bin NOT NULL,
  `image` varchar(255) COLLATE utf8_bin NOT NULL,
  `store_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `oc_email_template`
--

INSERT INTO `oc_email_template` (`email_template_id`, `name`, `type`, `background`, `body`, `heading`, `image`, `store_id`, `priority`, `date_start`, `date_end`, `date_added`) VALUES
(1, 'Регистрация аккаунта', 'register', '#333333', '#eeeeee', '#19bef0', '', 0, 0, '2013-04-16', '2025-01-01', '2013-04-18 13:07:26'),
(2, 'Регистрация партнера', 'affiliate', '#333333', '#eeeeee', '#19bef0', '', 0, 0, '2013-04-16', '2025-01-01', '2013-04-18 13:07:31'),
(3, 'Обратная связь', 'contact', '#333333', '#eeeeee', '#19bef0', '', 0, 0, '2013-04-16', '2025-01-01', '2013-04-18 13:07:36'),
(4, 'Восстановление пароля', 'forgotten', '#333333', '#eeeeee', '#19bef0', '', 0, 0, '2013-04-16', '2025-01-01', '2013-04-18 13:07:40'),
(5, 'Оформление заказа', 'order', '#333333', '#eeeeee', '#19bef0', '', 0, 0, '2013-04-16', '2025-01-01', '2013-04-18 13:07:48'),
(7, 'Изменение статуса заказа', '2', '#333333', '#eeeeee', '#19bef0', '', 0, 0, '2013-04-16', '2025-01-01', '2013-04-18 13:07:54'),
(8, 'Добавление бонусов', 'reward', '#333333', '#eeeeee', '#19bdf0', '', 0, 0, '2013-04-16', '2025-01-01', '2013-05-30 15:51:00'),
(9, 'Подтверждение аккаунта', 'account_approve', '#333333', '#eeeeee', '#19bdf0', '', 0, 0, '2013-04-16', '2025-01-01', '2013-05-30 20:06:54'),
(10, 'Кредит магазина', 'account_transaction', '#333333', '#eeeeee', '#19bdf0', '', 0, 0, '2013-04-16', '2025-01-01', '2013-05-30 20:08:35'),
(11, 'Подтверждение аккаунта', 'affiliate_approve', '#333333', '#eeeeee', '#19bdf0', '', 0, 0, '2013-04-16', '2025-01-01', '2013-05-30 20:10:38'),
(12, 'Комиссия партнера', 'affiliate_transaction', '#333333', '#eeeeee', '#19bdf0', '', 0, 0, '2013-04-16', '2025-01-01', '2013-05-30 20:12:43'),
(13, 'Изменение статуса возврата', 'return_3', '#333333', '#eeeeee', '#19bdf0', '', 0, 0, '2013-04-16', '2025-01-01', '2013-05-30 20:12:43'),
(14, 'Подарочный сертификат', 'gift_voucher', '#333333', '#eeeeee', '#19bdf0', '', 0, 0, '2013-04-16', '2025-01-01', '2013-05-30 20:12:43');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_email_template_description`
--

CREATE TABLE `oc_email_template_description` (
  `email_template_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `subject` varchar(255) COLLATE utf8_bin NOT NULL,
  `message` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `oc_email_template_description`
--

INSERT INTO `oc_email_template_description` (`email_template_id`, `language_id`, `subject`, `message`) VALUES
(1, 1, 'Welcome to 4paper - магазин чековых лент и этикеток', '&lt;p&gt;Welcome {firstname} and thank you for registering at 4paper - магазин чековых лент и этикеток!&lt;/p&gt;\r\n\r\n&lt;p&gt;Your account has now been created and you can log in by using your email address and password by visiting our website or at the following URL:&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;raquo; Login:&amp;nbsp;&lt;a href=&quot;https://4paper.ru/index.php?route=account/login&quot;&gt;https://4paper.ru/index.php?route=account/login&lt;/a&gt;&lt;br /&gt;\r\n&lt;span style=&quot;font-size: 13px;&quot;&gt;&amp;raquo; Email: {email}&lt;/span&gt;&lt;br /&gt;\r\n&lt;span style=&quot;line-height: 1.6em;&quot;&gt;&amp;raquo; Password: {password}&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Upon logging in, you will be able to access other services including reviewing past orders, printing invoices and editing your account information.&lt;/p&gt;\r\n\r\n&lt;p&gt;Best regards,&lt;br /&gt;\r\n4paper - магазин чековых лент и этикеток&lt;/p&gt;\r\n\r\n'),
(2, 1, 'Welcome to 4paper - магазин чековых лент и этикеток\'s Affiliate Program', '&lt;p&gt;Welcome {firstname} and thank you for joining 4paper - магазин чековых лент и этикеток\'s Affiliate Program.&lt;/p&gt;\r\n\r\n&lt;p&gt;Your account must be approved before you can login. Once approved you can log in by using your email address and password by visiting our website or at the following URL:&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size: 13px;&quot;&gt;&amp;raquo; Login:&amp;nbsp;&lt;/span&gt;&lt;a href=&quot;https://4paper.ru/index.php?route=affiliate/login&quot;&gt;https://4paper.ru/index.php?route=affiliate/login&lt;/a&gt;&lt;br style=&quot;font-size: 13px;&quot; /&gt;\r\n&lt;span style=&quot;font-size: 13px;&quot;&gt;&amp;raquo; Email: {email}&lt;/span&gt;&lt;br style=&quot;font-size: 13px;&quot; /&gt;\r\n&lt;span style=&quot;font-size: 13px; line-height: 1.6em;&quot;&gt;&amp;raquo; Password: {password}&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Upon logging in, you will be able to generate tracking codes, track commission payments and edit your account information.&lt;/p&gt;\r\n\r\n&lt;p&gt;Best regards,&lt;br /&gt;\r\n4paper - магазин чековых лент и этикеток&lt;/p&gt;\r\n\r\n'),
(3, 1, 'Thank you for contacting 4paper - магазин чековых лент и этикеток', '&lt;p&gt;Hi {firstname},&lt;/p&gt;\r\n\r\n&lt;p&gt;Thank you for contacting us. We will get back to you as soon as possible.&lt;/p&gt;\r\n\r\n&lt;p&gt;Below is what you had sent to us:&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;{enquiry}&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;If you had entered anything wrongly, feel free to fill in another contact form.&lt;/p&gt;\r\n\r\n&lt;p&gt;Best regards,&lt;br /&gt;\r\n4paper - магазин чековых лент и этикеток&lt;/p&gt;\r\n\r\n'),
(4, 1, 'Восстановление пароля', '&lt;p&gt;Привет {firstname},&lt;/p&gt;\r\n\r\n&lt;p&gt;Вы запросили восстановление пароля. Для того, чтобы сбросить пароль перейдите по ссылке:&lt;/p&gt;\r\n\r\n&lt;span style=&quot;font-size: 13px; line-height: 1.6em;&quot;&gt;&amp;raquo; Ссылка: {restore_link}&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Если вы не запрашивали восстановение пароля - проигнорируйте это письмо.&lt;/p&gt;\r\n\r\n&lt;p&gt;С найлучшими пожеланиями,&lt;br /&gt;\r\n4paper - магазин чековых лент и этикеток&lt;/p&gt;\r\n\r\n'),
(5, 1, 'Your Order at 4paper - магазин чековых лент и этикеток', '&lt;p&gt;Thank you for your interest in our products. Your order has been received and will be processed once payment has been confirmed.&lt;/p&gt;\r\n\r\n&lt;table style=&quot;border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;&quot;&gt;\r\n	&lt;thead&gt;\r\n		&lt;tr&gt;\r\n			&lt;td colspan=&quot;2&quot; style=&quot;font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #cccccc; font-weight: bold; text-align: left; padding: 7px; color: #222222;&quot;&gt;Order Details&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n	&lt;/thead&gt;\r\n	&lt;tbody&gt;\r\n		&lt;tr&gt;\r\n			&lt;td style=&quot;font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;background:#ffffff&quot;&gt;&lt;b&gt;Order ID:&lt;/b&gt; {order_id}&lt;br /&gt;\r\n			&lt;b&gt;Date Added:&lt;/b&gt; {date_added}&lt;br /&gt;\r\n			&lt;b&gt;Payment Method:&lt;/b&gt; {payment_method}&lt;br /&gt;\r\n			&lt;b&gt;Shipping Method:&lt;/b&gt; {shipping_method}&lt;/td&gt;\r\n			&lt;td style=&quot;font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;background:#ffffff&quot;&gt;&lt;b&gt;Email:&lt;/b&gt; {email}&lt;br /&gt;\r\n			&lt;b&gt;Telephone:&lt;/b&gt; {telephone}&lt;br /&gt;\r\n			&lt;b&gt;IP Address:&lt;/b&gt; {ip}&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n	&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n\r\n&lt;p&gt;{comment_table}&lt;/p&gt;\r\n\r\n&lt;table style=&quot;border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;&quot;&gt;\r\n	&lt;thead&gt;\r\n		&lt;tr&gt;\r\n			&lt;td style=&quot;font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #cccccc; font-weight: bold; text-align: left; padding: 7px; color: #222222;&quot;&gt;Payment Address:&lt;/td&gt;\r\n			&lt;td style=&quot;font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #cccccc; font-weight: bold; text-align: left; padding: 7px; color: #222222;&quot;&gt;Shipping Address:&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n	&lt;/thead&gt;\r\n	&lt;tbody&gt;\r\n		&lt;tr&gt;\r\n			&lt;td style=&quot;font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;background:#ffffff;&quot;&gt;{payment_address}&lt;/td&gt;\r\n			&lt;td style=&quot;font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;background:#ffffff&quot;&gt;{shipping_address}&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n	&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n\r\n&lt;p&gt;{product_table}&lt;/p&gt;\r\n\r\n&lt;p&gt;Best regards,&lt;br /&gt;\r\n4paper - магазин чековых лент и этикеток&lt;/p&gt;\r\n\r\n'),
(7, 1, 'Processing your Order #{order_id}', '&lt;p&gt;Hi {firstname},&lt;/p&gt;\r\n\r\n&lt;p&gt;We are currently processing your order. Feel free drop us an email if you have any queries.&lt;/p&gt;\r\n\r\n&lt;p&gt;{comment}&lt;/p&gt;\r\n\r\n&lt;p&gt;Best regards,&lt;br /&gt;\r\n4paper - магазин чековых лент и этикеток&lt;/p&gt;\r\n'),
(8, 1, 'Reward Points', '&lt;p&gt;Hi {firstname},&lt;/p&gt;\r\n\r\n&lt;p&gt;You have received {points} reward points! You may now spend it in our store.&lt;/p&gt;\r\n\r\n&lt;p&gt;You currently have a total of {total_points} reward points.&lt;/p&gt;\r\n\r\n&lt;p&gt;Best regards,&lt;br /&gt;\r\n4paper - магазин чековых лент и этикеток&lt;/p&gt;\r\n\r\n'),
(9, 1, 'Account Approved', '&lt;p&gt;Hi {firstname},&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size: 13px;&quot;&gt;Your account has now been approved and you can log in by using your email address and password by visiting our website or at the following URL:&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size: 13px;&quot;&gt;&amp;raquo; Login:&amp;nbsp;&lt;/span&gt;&lt;a href=&quot;https://4paper.ru/index.php?route=account/login&quot; style=&quot;font-size: 13px;&quot;&gt;https://4paper.ru/index.php?route=account/login&lt;/a&gt;&lt;br style=&quot;font-size: 13px;&quot; /&gt;\r\n&lt;span style=&quot;font-size: 13px;&quot;&gt;&amp;raquo; Email: {email}&lt;/span&gt;&lt;br style=&quot;font-size: 13px;&quot; /&gt;\r\n&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-size: 13px;&quot;&gt;Upon logging in, you will be able to access other services including reviewing past orders, printing invoices and editing your account information.&lt;/p&gt;\r\n\r\n'),
(10, 1, 'Store Credits', '&lt;p style=&quot;font-size: 13px;&quot;&gt;Hi {firstname},&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-size: 13px;&quot;&gt;You have received {credits} store credits! You may now spend it in our store.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-size: 13px;&quot;&gt;You currently have a total of {total_credits} store credits.&lt;/p&gt;\r\n\r\n'),
(11, 1, 'Affiliate Approved', '&lt;p style=&quot;font-size: 13px;&quot;&gt;Welcome {firstname} and thank you for joining 4paper - магазин чековых лент и этикеток\'s Affiliate Program.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-size: 13px;&quot;&gt;Your account has been approved.&amp;nbsp;You can now log in by using your email address and password by visiting our website or at the following URL:&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-size: 13px;&quot;&gt;&amp;raquo; Login:&amp;nbsp;&lt;a href=&quot;https://4paper.ru/index.php?route=affiliate/login&quot;&gt;https://4paper.ru/index.php?route=affiliate/login&lt;/a&gt;&lt;br /&gt;\r\n&amp;raquo; Email: {email}&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-size: 13px;&quot;&gt;Upon logging in, you will be able to generate tracking codes, track commission payments and edit your account information.&lt;/p&gt;\r\n\r\n'),
(12, 1, 'Commission Earned', '&lt;p style=&quot;font-size: 13px;&quot;&gt;Hi {firstname},&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-size: 13px;&quot;&gt;You have received {commission} commission!&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-size: 13px;&quot;&gt;You have currently earned&amp;nbsp;a total of {total_commission} commission.&lt;/p&gt;\r\n\r\n'),
(13, 1, 'Product Return Request Complete', '&lt;p&gt;Hi {firstname},&lt;/p&gt;&lt;p&gt;Your return request (Return ID {return_id}) has been completed.&lt;/p&gt;&lt;p&gt;{comment}&lt;/p&gt;&lt;p&gt;Best regards,&lt;br /&gt;4paper - магазин чековых лент и этикеток&lt;/p&gt;'),
(14, 1, 'You have been sent a gift voucher from {from_name}', '&lt;p&gt;Hi {to_name},&lt;/p&gt;&lt;p&gt;{voucher_theme}&lt;/p&gt;&lt;p&gt;You have received a gift voucher worth {amount}.&lt;/p&gt;&lt;p&gt;Message from {from_name}:&lt;/p&gt;&lt;p&gt;{message}&lt;/p&gt;&lt;p&gt;You can redeem the gift voucher with the&amp;nbsp;code &lt;strong&gt;{code}&lt;/strong&gt; on our website.&lt;/p&gt;&lt;p&gt;Best regards,&lt;br /&gt;4paper - магазин чековых лент и этикеток&lt;/p&gt;');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_event`
--

CREATE TABLE `oc_event` (
  `event_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `trigger` text NOT NULL,
  `action` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_event`
--

INSERT INTO `oc_event` (`event_id`, `code`, `trigger`, `action`, `status`, `sort_order`) VALUES
(1, 'activity_customer_add', 'catalog/model/account/customer/addCustomer/after', 'event/activity/addCustomer', 1, 0),
(2, 'activity_customer_edit', 'catalog/model/account/customer/editCustomer/after', 'event/activity/editCustomer', 1, 0),
(3, 'activity_customer_password', 'catalog/model/account/customer/editPassword/after', 'event/activity/editPassword', 1, 0),
(4, 'activity_customer_forgotten', 'catalog/model/account/customer/editCode/after', 'event/activity/forgotten', 1, 0),
(5, 'activity_transaction', 'catalog/model/account/customer/addTransaction/after', 'event/activity/addTransaction', 1, 0),
(6, 'activity_customer_login', 'catalog/model/account/customer/deleteLoginAttempts/after', 'event/activity/login', 1, 0),
(7, 'activity_address_add', 'catalog/model/account/address/addAddress/after', 'event/activity/addAddress', 1, 0),
(8, 'activity_address_edit', 'catalog/model/account/address/editAddress/after', 'event/activity/editAddress', 1, 0),
(9, 'activity_address_delete', 'catalog/model/account/address/deleteAddress/after', 'event/activity/deleteAddress', 1, 0),
(10, 'activity_affiliate_add', 'catalog/model/account/customer/addAffiliate/after', 'event/activity/addAffiliate', 1, 0),
(11, 'activity_affiliate_edit', 'catalog/model/account/customer/editAffiliate/after', 'event/activity/editAffiliate', 1, 0),
(12, 'activity_order_add', 'catalog/model/checkout/order/addOrderHistory/before', 'event/activity/addOrderHistory', 1, 0),
(13, 'activity_return_add', 'catalog/model/account/return/addReturn/after', 'event/activity/addReturn', 1, 0),
(14, 'mail_transaction', 'catalog/model/account/customer/addTransaction/after', 'mail/transaction', 1, 0),
(15, 'mail_forgotten', 'catalog/model/account/customer/editCode/after', 'mail/forgotten', 1, 0),
(16, 'mail_customer_add', 'catalog/model/account/customer/addCustomer/after', 'mail/register', 1, 0),
(17, 'mail_customer_alert', 'catalog/model/account/customer/addCustomer/after', 'mail/register/alert', 1, 0),
(18, 'mail_affiliate_add', 'catalog/model/account/customer/addAffiliate/after', 'mail/affiliate', 1, 0),
(19, 'mail_affiliate_alert', 'catalog/model/account/customer/addAffiliate/after', 'mail/affiliate/alert', 1, 0),
(20, 'mail_voucher', 'catalog/model/checkout/order/addOrderHistory/after', 'extension/total/voucher/send', 1, 0),
(21, 'mail_order_add', 'catalog/model/checkout/order/addOrderHistory/before', 'mail/order', 1, 0),
(22, 'mail_order_alert', 'catalog/model/checkout/order/addOrderHistory/before', 'mail/order/alert', 1, 0),
(23, 'statistics_review_add', 'catalog/model/catalog/review/addReview/after', 'event/statistics/addReview', 1, 0),
(24, 'statistics_return_add', 'catalog/model/account/return/addReturn/after', 'event/statistics/addReturn', 1, 0),
(25, 'statistics_order_history', 'catalog/model/checkout/order/addOrderHistory/after', 'event/statistics/addOrderHistory', 1, 0),
(26, 'admin_mail_affiliate_approve', 'admin/model/customer/customer_approval/approveAffiliate/after', 'mail/affiliate/approve', 1, 0),
(27, 'admin_mail_affiliate_deny', 'admin/model/customer/customer_approval/denyAffiliate/after', 'mail/affiliate/deny', 1, 0),
(28, 'admin_mail_customer_approve', 'admin/model/customer/customer_approval/approveCustomer/after', 'mail/customer/approve', 1, 0),
(29, 'admin_mail_customer_deny', 'admin/model/customer/customer_approval/denyCustomer/after', 'mail/customer/deny', 1, 0),
(30, 'admin_mail_reward', 'admin/model/customer/customer/addReward/after', 'mail/reward', 1, 0),
(31, 'admin_mail_transaction', 'admin/model/customer/customer/addTransaction/after', 'mail/transaction', 1, 0),
(32, 'admin_mail_return', 'admin/model/sale/return/addReturn/after', 'mail/return', 1, 0),
(33, 'admin_mail_forgotten', 'admin/model/user/user/editCode/after', 'mail/forgotten', 1, 0),
(34, 'module_quickcheckout', 'catalog/controller/checkout/checkout/before', 'extension/quickcheckout/checkout/eventPreControllerCheckoutCheckout', 1, 0),
(35, 'module_quickcheckout', 'catalog/controller/checkout/success/before', 'extension/quickcheckout/checkout/eventPreControllerCheckoutSuccess', 1, 0),
(36, 'module_template_switcher', 'catalog/view/*/before', 'extension/module/template_switcher/override', 1, 499),
(37, 'module_template_switcher', 'catalog/view/*/before', 'extension/module/template_switcher/render', 1, 999),
(38, 'module_template_switcher', 'catalog/controller/*/before', 'extension/module/template_switcher/before', 1, 0),
(39, 'module_template_switcher', 'admin/view/*/before', 'extension/module/template_switcher/override', 1, 0),
(40, 'module_template_switcher', 'admin/view/design/layout_form/before', 'extension/module/template_switcher/eventViewDesignLayoutFormBefore', 1, 0),
(41, 'module_template_switcher', 'catalog/view/*/before', 'extension/module/template_switcher/override', 1, 499),
(42, 'module_template_switcher', 'catalog/view/*/before', 'extension/module/template_switcher/render', 1, 999),
(43, 'module_template_switcher', 'catalog/controller/*/before', 'extension/module/template_switcher/before', 1, 0),
(44, 'module_template_switcher', 'admin/view/*/before', 'extension/module/template_switcher/override', 1, 0),
(45, 'module_template_switcher', 'admin/view/design/layout_form/before', 'extension/module/template_switcher/eventViewDesignLayoutFormBefore', 1, 0),
(46, 'xshippingpro', 'catalog/view/mail/order_add/before', 'extension/shipping/xshippingpro/onOrderEmail', 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_extension`
--

CREATE TABLE `oc_extension` (
  `extension_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `code` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_extension`
--

INSERT INTO `oc_extension` (`extension_id`, `type`, `code`) VALUES
(1, 'payment', 'cod'),
(2, 'total', 'shipping'),
(4, 'total', 'tax'),
(5, 'total', 'total'),
(8, 'total', 'credit'),
(10, 'total', 'handling'),
(11, 'total', 'low_order_fee'),
(20, 'theme', 'default'),
(21, 'dashboard', 'activity'),
(22, 'dashboard', 'sale'),
(23, 'dashboard', 'recent'),
(24, 'dashboard', 'order'),
(25, 'dashboard', 'online'),
(26, 'dashboard', 'map'),
(27, 'dashboard', 'customer'),
(28, 'dashboard', 'chart'),
(29, 'report', 'sale_coupon'),
(31, 'report', 'customer_search'),
(32, 'report', 'customer_transaction'),
(33, 'report', 'product_purchased'),
(34, 'report', 'product_viewed'),
(35, 'report', 'sale_return'),
(36, 'report', 'sale_order'),
(37, 'report', 'sale_shipping'),
(38, 'report', 'sale_tax'),
(39, 'report', 'customer_activity'),
(40, 'report', 'customer_order'),
(41, 'report', 'customer_reward'),
(52, 'module', 'extendedsearch'),
(53, 'total', 'sub_total'),
(54, 'module', 'quickcheckout'),
(63, 'module', 'trade_import'),
(64, 'module', 'manager'),
(65, 'module', 'account'),
(66, 'analytics', 'metrika'),
(67, 'feed', 'yandex_sitemap'),
(68, 'feed', 'yandex_market'),
(70, 'module', 'template_switcher'),
(71, 'module', 'ocfilter'),
(73, 'payment', 'rbs'),
(76, 'shipping', 'xshippingpro'),
(77, 'feed', 'unixml'),
(78, 'module', 'cdek_integrator'),
(79, 'payment', 'cod_cdek'),
(80, 'total', 'cdek');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_extension_install`
--

CREATE TABLE `oc_extension_install` (
  `extension_install_id` int(11) NOT NULL,
  `extension_download_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_extension_install`
--

INSERT INTO `oc_extension_install` (`extension_install_id`, `extension_download_id`, `filename`, `date_added`) VALUES
(1, 0, 'svg-support.ocmod.zip', '2019-05-16 14:45:33'),
(2, 0, 'opencart-3-x-export-import-multilingual-3-20-cloud.ocmod.zip', '2019-05-16 14:45:56'),
(3, 0, 'Limit_Autocomplete_v3.3.ocmod.zip', '2019-05-16 14:46:10'),
(4, 0, 'user.group.visual.3.x.ocmod.zip', '2019-05-16 14:47:00'),
(7, 1253857, 'ExtendedSearch - extends the standard search functionality - extended-search3x.ocmod.zip', '2019-05-16 16:11:10'),
(8, 0, 'quickcheckout190_oc3.ocmod.zip', '2019-05-22 20:18:39'),
(13, 0, 'admin_detail_order_email.ocmod.zip', '2019-05-24 17:22:29'),
(15, 0, 'localcopy_oc3.ocmod.zip', '2019-08-07 15:31:32'),
(16, 0, 'order_manager_oc3.ocmod.zip', '2019-08-07 15:31:44'),
(17, 0, 'order_manager_oc3.ocmod.zip', '2019-08-07 15:32:01'),
(18, 0, 'yandexsitemap.ocmod.zip', '2019-09-16 15:55:23'),
(19, 0, 'ocfilter.ocmod.zip', '2020-05-20 04:18:56'),
(20, 0, 'sessions_gc.ocmod.zip', '2020-05-20 04:19:03'),
(21, 0, 'opencart3.0fixes.ocmod.zip', '2020-05-20 04:19:08'),
(22, 0, 'modificationmanager.ocmod.zip', '2020-05-20 04:19:14'),
(23, 0, 'option-markups_oc3x.ocmod.zip', '2020-05-28 14:18:06'),
(25, 0, 'xshippingpro-oc3-cloud.ocmod.zip', '2020-07-22 12:05:38');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_extension_path`
--

CREATE TABLE `oc_extension_path` (
  `extension_path_id` int(11) NOT NULL,
  `extension_install_id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_extension_path`
--

INSERT INTO `oc_extension_path` (`extension_path_id`, `extension_install_id`, `path`, `date_added`) VALUES
(1, 2, 'system/library/export_import', '2019-05-16 14:45:57'),
(2, 2, 'admin/controller/extension/export_import.php', '2019-05-16 14:45:57'),
(3, 2, 'admin/model/extension/export_import.php', '2019-05-16 14:45:57'),
(4, 2, 'admin/view/image/export-import', '2019-05-16 14:45:57'),
(5, 2, 'admin/view/stylesheet/export_import.css', '2019-05-16 14:45:57'),
(6, 2, 'system/library/export_import/Classes', '2019-05-16 14:45:57'),
(7, 2, 'admin/language/en-gb/extension/export_import.php', '2019-05-16 14:45:57'),
(8, 2, 'admin/view/image/export-import/loading.gif', '2019-05-16 14:45:57'),
(9, 2, 'admin/view/template/extension/export_import.twig', '2019-05-16 14:45:57'),
(10, 2, 'system/library/export_import/Classes/PHPExcel', '2019-05-16 14:45:57'),
(11, 2, 'system/library/export_import/Classes/PHPExcel.php', '2019-05-16 14:45:57'),
(12, 2, 'system/library/export_import/Classes/PHPExcel/Autoloader.php', '2019-05-16 14:45:57'),
(13, 2, 'system/library/export_import/Classes/PHPExcel/CachedObjectStorage', '2019-05-16 14:45:57'),
(14, 2, 'system/library/export_import/Classes/PHPExcel/CachedObjectStorageFactory.php', '2019-05-16 14:45:57'),
(15, 2, 'system/library/export_import/Classes/PHPExcel/CalcEngine', '2019-05-16 14:45:57'),
(16, 2, 'system/library/export_import/Classes/PHPExcel/Calculation', '2019-05-16 14:45:57'),
(17, 2, 'system/library/export_import/Classes/PHPExcel/Calculation.php', '2019-05-16 14:45:57'),
(18, 2, 'system/library/export_import/Classes/PHPExcel/Cell', '2019-05-16 14:45:57'),
(19, 2, 'system/library/export_import/Classes/PHPExcel/Cell.php', '2019-05-16 14:45:57'),
(20, 2, 'system/library/export_import/Classes/PHPExcel/Chart', '2019-05-16 14:45:57'),
(21, 2, 'system/library/export_import/Classes/PHPExcel/Chart.php', '2019-05-16 14:45:57'),
(22, 2, 'system/library/export_import/Classes/PHPExcel/Comment.php', '2019-05-16 14:45:57'),
(23, 2, 'system/library/export_import/Classes/PHPExcel/DocumentProperties.php', '2019-05-16 14:45:57'),
(24, 2, 'system/library/export_import/Classes/PHPExcel/DocumentSecurity.php', '2019-05-16 14:45:57'),
(25, 2, 'system/library/export_import/Classes/PHPExcel/Exception.php', '2019-05-16 14:45:57'),
(26, 2, 'system/library/export_import/Classes/PHPExcel/HashTable.php', '2019-05-16 14:45:57'),
(27, 2, 'system/library/export_import/Classes/PHPExcel/Helper', '2019-05-16 14:45:57'),
(28, 2, 'system/library/export_import/Classes/PHPExcel/IComparable.php', '2019-05-16 14:45:57'),
(29, 2, 'system/library/export_import/Classes/PHPExcel/IOFactory.php', '2019-05-16 14:45:57'),
(30, 2, 'system/library/export_import/Classes/PHPExcel/NamedRange.php', '2019-05-16 14:45:57'),
(31, 2, 'system/library/export_import/Classes/PHPExcel/Reader', '2019-05-16 14:45:57'),
(32, 2, 'system/library/export_import/Classes/PHPExcel/ReferenceHelper.php', '2019-05-16 14:45:57'),
(33, 2, 'system/library/export_import/Classes/PHPExcel/RichText', '2019-05-16 14:45:57'),
(34, 2, 'system/library/export_import/Classes/PHPExcel/RichText.php', '2019-05-16 14:45:57'),
(35, 2, 'system/library/export_import/Classes/PHPExcel/Settings.php', '2019-05-16 14:45:57'),
(36, 2, 'system/library/export_import/Classes/PHPExcel/Shared', '2019-05-16 14:45:57'),
(37, 2, 'system/library/export_import/Classes/PHPExcel/Style', '2019-05-16 14:45:57'),
(38, 2, 'system/library/export_import/Classes/PHPExcel/Style.php', '2019-05-16 14:45:57'),
(39, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet', '2019-05-16 14:45:57'),
(40, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet.php', '2019-05-16 14:45:57'),
(41, 2, 'system/library/export_import/Classes/PHPExcel/WorksheetIterator.php', '2019-05-16 14:45:57'),
(42, 2, 'system/library/export_import/Classes/PHPExcel/Writer', '2019-05-16 14:45:57'),
(43, 2, 'system/library/export_import/Classes/PHPExcel/locale', '2019-05-16 14:45:57'),
(44, 2, 'system/library/export_import/Classes/PHPExcel/CachedObjectStorage/APC.php', '2019-05-16 14:45:57'),
(45, 2, 'system/library/export_import/Classes/PHPExcel/CachedObjectStorage/CacheBase.php', '2019-05-16 14:45:57'),
(46, 2, 'system/library/export_import/Classes/PHPExcel/CachedObjectStorage/DiscISAM.php', '2019-05-16 14:45:57'),
(47, 2, 'system/library/export_import/Classes/PHPExcel/CachedObjectStorage/ICache.php', '2019-05-16 14:45:57'),
(48, 2, 'system/library/export_import/Classes/PHPExcel/CachedObjectStorage/Igbinary.php', '2019-05-16 14:45:57'),
(49, 2, 'system/library/export_import/Classes/PHPExcel/CachedObjectStorage/Memcache.php', '2019-05-16 14:45:57'),
(50, 2, 'system/library/export_import/Classes/PHPExcel/CachedObjectStorage/Memory.php', '2019-05-16 14:45:57'),
(51, 2, 'system/library/export_import/Classes/PHPExcel/CachedObjectStorage/MemoryGZip.php', '2019-05-16 14:45:57'),
(52, 2, 'system/library/export_import/Classes/PHPExcel/CachedObjectStorage/MemorySerialized.php', '2019-05-16 14:45:57'),
(53, 2, 'system/library/export_import/Classes/PHPExcel/CachedObjectStorage/PHPTemp.php', '2019-05-16 14:45:57'),
(54, 2, 'system/library/export_import/Classes/PHPExcel/CachedObjectStorage/SQLite.php', '2019-05-16 14:45:57'),
(55, 2, 'system/library/export_import/Classes/PHPExcel/CachedObjectStorage/SQLite3.php', '2019-05-16 14:45:57'),
(56, 2, 'system/library/export_import/Classes/PHPExcel/CachedObjectStorage/Wincache.php', '2019-05-16 14:45:57'),
(57, 2, 'system/library/export_import/Classes/PHPExcel/CalcEngine/CyclicReferenceStack.php', '2019-05-16 14:45:57'),
(58, 2, 'system/library/export_import/Classes/PHPExcel/CalcEngine/Logger.php', '2019-05-16 14:45:57'),
(59, 2, 'system/library/export_import/Classes/PHPExcel/Calculation/Database.php', '2019-05-16 14:45:57'),
(60, 2, 'system/library/export_import/Classes/PHPExcel/Calculation/DateTime.php', '2019-05-16 14:45:57'),
(61, 2, 'system/library/export_import/Classes/PHPExcel/Calculation/Engineering.php', '2019-05-16 14:45:57'),
(62, 2, 'system/library/export_import/Classes/PHPExcel/Calculation/Exception.php', '2019-05-16 14:45:57'),
(63, 2, 'system/library/export_import/Classes/PHPExcel/Calculation/ExceptionHandler.php', '2019-05-16 14:45:57'),
(64, 2, 'system/library/export_import/Classes/PHPExcel/Calculation/Financial.php', '2019-05-16 14:45:57'),
(65, 2, 'system/library/export_import/Classes/PHPExcel/Calculation/FormulaParser.php', '2019-05-16 14:45:57'),
(66, 2, 'system/library/export_import/Classes/PHPExcel/Calculation/FormulaToken.php', '2019-05-16 14:45:57'),
(67, 2, 'system/library/export_import/Classes/PHPExcel/Calculation/Function.php', '2019-05-16 14:45:57'),
(68, 2, 'system/library/export_import/Classes/PHPExcel/Calculation/Functions.php', '2019-05-16 14:45:57'),
(69, 2, 'system/library/export_import/Classes/PHPExcel/Calculation/Logical.php', '2019-05-16 14:45:57'),
(70, 2, 'system/library/export_import/Classes/PHPExcel/Calculation/LookupRef.php', '2019-05-16 14:45:57'),
(71, 2, 'system/library/export_import/Classes/PHPExcel/Calculation/MathTrig.php', '2019-05-16 14:45:57'),
(72, 2, 'system/library/export_import/Classes/PHPExcel/Calculation/Statistical.php', '2019-05-16 14:45:57'),
(73, 2, 'system/library/export_import/Classes/PHPExcel/Calculation/TextData.php', '2019-05-16 14:45:57'),
(74, 2, 'system/library/export_import/Classes/PHPExcel/Calculation/Token', '2019-05-16 14:45:57'),
(75, 2, 'system/library/export_import/Classes/PHPExcel/Calculation/functionlist.txt', '2019-05-16 14:45:57'),
(76, 2, 'system/library/export_import/Classes/PHPExcel/Cell/AdvancedValueBinder.php', '2019-05-16 14:45:57'),
(77, 2, 'system/library/export_import/Classes/PHPExcel/Cell/DataType.php', '2019-05-16 14:45:57'),
(78, 2, 'system/library/export_import/Classes/PHPExcel/Cell/DataValidation.php', '2019-05-16 14:45:57'),
(79, 2, 'system/library/export_import/Classes/PHPExcel/Cell/DefaultValueBinder.php', '2019-05-16 14:45:57'),
(80, 2, 'system/library/export_import/Classes/PHPExcel/Cell/ExportImportValueBinder.php', '2019-05-16 14:45:57'),
(81, 2, 'system/library/export_import/Classes/PHPExcel/Cell/Hyperlink.php', '2019-05-16 14:45:57'),
(82, 2, 'system/library/export_import/Classes/PHPExcel/Cell/IValueBinder.php', '2019-05-16 14:45:57'),
(83, 2, 'system/library/export_import/Classes/PHPExcel/Chart/Axis.php', '2019-05-16 14:45:57'),
(84, 2, 'system/library/export_import/Classes/PHPExcel/Chart/DataSeries.php', '2019-05-16 14:45:57'),
(85, 2, 'system/library/export_import/Classes/PHPExcel/Chart/DataSeriesValues.php', '2019-05-16 14:45:57'),
(86, 2, 'system/library/export_import/Classes/PHPExcel/Chart/Exception.php', '2019-05-16 14:45:57'),
(87, 2, 'system/library/export_import/Classes/PHPExcel/Chart/GridLines.php', '2019-05-16 14:45:57'),
(88, 2, 'system/library/export_import/Classes/PHPExcel/Chart/Layout.php', '2019-05-16 14:45:57'),
(89, 2, 'system/library/export_import/Classes/PHPExcel/Chart/Legend.php', '2019-05-16 14:45:57'),
(90, 2, 'system/library/export_import/Classes/PHPExcel/Chart/PlotArea.php', '2019-05-16 14:45:57'),
(91, 2, 'system/library/export_import/Classes/PHPExcel/Chart/Properties.php', '2019-05-16 14:45:57'),
(92, 2, 'system/library/export_import/Classes/PHPExcel/Chart/Renderer', '2019-05-16 14:45:57'),
(93, 2, 'system/library/export_import/Classes/PHPExcel/Chart/Title.php', '2019-05-16 14:45:57'),
(94, 2, 'system/library/export_import/Classes/PHPExcel/Helper/HTML.php', '2019-05-16 14:45:57'),
(95, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Abstract.php', '2019-05-16 14:45:57'),
(96, 2, 'system/library/export_import/Classes/PHPExcel/Reader/CSV.php', '2019-05-16 14:45:57'),
(97, 2, 'system/library/export_import/Classes/PHPExcel/Reader/DefaultReadFilter.php', '2019-05-16 14:45:57'),
(98, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Excel2003XML.php', '2019-05-16 14:45:57'),
(99, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Excel2007', '2019-05-16 14:45:57'),
(100, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Excel2007.php', '2019-05-16 14:45:57'),
(101, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Excel5', '2019-05-16 14:45:57'),
(102, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Excel5.php', '2019-05-16 14:45:57'),
(103, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Exception.php', '2019-05-16 14:45:57'),
(104, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Gnumeric.php', '2019-05-16 14:45:57'),
(105, 2, 'system/library/export_import/Classes/PHPExcel/Reader/HTML.php', '2019-05-16 14:45:57'),
(106, 2, 'system/library/export_import/Classes/PHPExcel/Reader/IReadFilter.php', '2019-05-16 14:45:57'),
(107, 2, 'system/library/export_import/Classes/PHPExcel/Reader/IReader.php', '2019-05-16 14:45:57'),
(108, 2, 'system/library/export_import/Classes/PHPExcel/Reader/OOCalc.php', '2019-05-16 14:45:57'),
(109, 2, 'system/library/export_import/Classes/PHPExcel/Reader/SYLK.php', '2019-05-16 14:45:57'),
(110, 2, 'system/library/export_import/Classes/PHPExcel/RichText/ITextElement.php', '2019-05-16 14:45:57'),
(111, 2, 'system/library/export_import/Classes/PHPExcel/RichText/Run.php', '2019-05-16 14:45:57'),
(112, 2, 'system/library/export_import/Classes/PHPExcel/RichText/TextElement.php', '2019-05-16 14:45:57'),
(113, 2, 'system/library/export_import/Classes/PHPExcel/Shared/CodePage.php', '2019-05-16 14:45:57'),
(114, 2, 'system/library/export_import/Classes/PHPExcel/Shared/Date.php', '2019-05-16 14:45:57'),
(115, 2, 'system/library/export_import/Classes/PHPExcel/Shared/Drawing.php', '2019-05-16 14:45:57'),
(116, 2, 'system/library/export_import/Classes/PHPExcel/Shared/Escher', '2019-05-16 14:45:57'),
(117, 2, 'system/library/export_import/Classes/PHPExcel/Shared/Escher.php', '2019-05-16 14:45:57'),
(118, 2, 'system/library/export_import/Classes/PHPExcel/Shared/Excel5.php', '2019-05-16 14:45:57'),
(119, 2, 'system/library/export_import/Classes/PHPExcel/Shared/File.php', '2019-05-16 14:45:57'),
(120, 2, 'system/library/export_import/Classes/PHPExcel/Shared/Font.php', '2019-05-16 14:45:57'),
(121, 2, 'system/library/export_import/Classes/PHPExcel/Shared/JAMA', '2019-05-16 14:45:57'),
(122, 2, 'system/library/export_import/Classes/PHPExcel/Shared/OLE', '2019-05-16 14:45:57'),
(123, 2, 'system/library/export_import/Classes/PHPExcel/Shared/OLE.php', '2019-05-16 14:45:57'),
(124, 2, 'system/library/export_import/Classes/PHPExcel/Shared/OLERead.php', '2019-05-16 14:45:57'),
(125, 2, 'system/library/export_import/Classes/PHPExcel/Shared/PCLZip', '2019-05-16 14:45:57'),
(126, 2, 'system/library/export_import/Classes/PHPExcel/Shared/PasswordHasher.php', '2019-05-16 14:45:57'),
(127, 2, 'system/library/export_import/Classes/PHPExcel/Shared/String.php', '2019-05-16 14:45:57'),
(128, 2, 'system/library/export_import/Classes/PHPExcel/Shared/TimeZone.php', '2019-05-16 14:45:57'),
(129, 2, 'system/library/export_import/Classes/PHPExcel/Shared/XMLWriter.php', '2019-05-16 14:45:57'),
(130, 2, 'system/library/export_import/Classes/PHPExcel/Shared/ZipArchive.php', '2019-05-16 14:45:57'),
(131, 2, 'system/library/export_import/Classes/PHPExcel/Shared/ZipStreamWrapper.php', '2019-05-16 14:45:57'),
(132, 2, 'system/library/export_import/Classes/PHPExcel/Shared/trend', '2019-05-16 14:45:57'),
(133, 2, 'system/library/export_import/Classes/PHPExcel/Style/Alignment.php', '2019-05-16 14:45:57'),
(134, 2, 'system/library/export_import/Classes/PHPExcel/Style/Border.php', '2019-05-16 14:45:57'),
(135, 2, 'system/library/export_import/Classes/PHPExcel/Style/Borders.php', '2019-05-16 14:45:57'),
(136, 2, 'system/library/export_import/Classes/PHPExcel/Style/Color.php', '2019-05-16 14:45:57'),
(137, 2, 'system/library/export_import/Classes/PHPExcel/Style/Conditional.php', '2019-05-16 14:45:57'),
(138, 2, 'system/library/export_import/Classes/PHPExcel/Style/Fill.php', '2019-05-16 14:45:57'),
(139, 2, 'system/library/export_import/Classes/PHPExcel/Style/Font.php', '2019-05-16 14:45:57'),
(140, 2, 'system/library/export_import/Classes/PHPExcel/Style/NumberFormat.php', '2019-05-16 14:45:57'),
(141, 2, 'system/library/export_import/Classes/PHPExcel/Style/Protection.php', '2019-05-16 14:45:57'),
(142, 2, 'system/library/export_import/Classes/PHPExcel/Style/Supervisor.php', '2019-05-16 14:45:57'),
(143, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/AutoFilter', '2019-05-16 14:45:57'),
(144, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/AutoFilter.php', '2019-05-16 14:45:57'),
(145, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/BaseDrawing.php', '2019-05-16 14:45:57'),
(146, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/CellIterator.php', '2019-05-16 14:45:57'),
(147, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/Column.php', '2019-05-16 14:45:57'),
(148, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/ColumnCellIterator.php', '2019-05-16 14:45:57'),
(149, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/ColumnDimension.php', '2019-05-16 14:45:57'),
(150, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/ColumnIterator.php', '2019-05-16 14:45:57'),
(151, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/Dimension.php', '2019-05-16 14:45:57'),
(152, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/Drawing', '2019-05-16 14:45:57'),
(153, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/Drawing.php', '2019-05-16 14:45:57'),
(154, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/HeaderFooter.php', '2019-05-16 14:45:57'),
(155, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/HeaderFooterDrawing.php', '2019-05-16 14:45:57'),
(156, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/MemoryDrawing.php', '2019-05-16 14:45:57'),
(157, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/PageMargins.php', '2019-05-16 14:45:57'),
(158, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/PageSetup.php', '2019-05-16 14:45:57'),
(159, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/Protection.php', '2019-05-16 14:45:57'),
(160, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/Row.php', '2019-05-16 14:45:57'),
(161, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/RowCellIterator.php', '2019-05-16 14:45:57'),
(162, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/RowDimension.php', '2019-05-16 14:45:57'),
(163, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/RowIterator.php', '2019-05-16 14:45:57'),
(164, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/SheetView.php', '2019-05-16 14:45:57'),
(165, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Abstract.php', '2019-05-16 14:45:57'),
(166, 2, 'system/library/export_import/Classes/PHPExcel/Writer/CSV.php', '2019-05-16 14:45:57'),
(167, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel2007', '2019-05-16 14:45:57'),
(168, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel2007.php', '2019-05-16 14:45:57'),
(169, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel5', '2019-05-16 14:45:57'),
(170, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel5.php', '2019-05-16 14:45:57'),
(171, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Exception.php', '2019-05-16 14:45:57'),
(172, 2, 'system/library/export_import/Classes/PHPExcel/Writer/HTML.php', '2019-05-16 14:45:57'),
(173, 2, 'system/library/export_import/Classes/PHPExcel/Writer/IWriter.php', '2019-05-16 14:45:57'),
(174, 2, 'system/library/export_import/Classes/PHPExcel/Writer/OpenDocument', '2019-05-16 14:45:57'),
(175, 2, 'system/library/export_import/Classes/PHPExcel/Writer/OpenDocument.php', '2019-05-16 14:45:57'),
(176, 2, 'system/library/export_import/Classes/PHPExcel/Writer/PDF', '2019-05-16 14:45:57'),
(177, 2, 'system/library/export_import/Classes/PHPExcel/Writer/PDF.php', '2019-05-16 14:45:57'),
(178, 2, 'system/library/export_import/Classes/PHPExcel/locale/bg', '2019-05-16 14:45:57'),
(179, 2, 'system/library/export_import/Classes/PHPExcel/locale/cs', '2019-05-16 14:45:57'),
(180, 2, 'system/library/export_import/Classes/PHPExcel/locale/da', '2019-05-16 14:45:57'),
(181, 2, 'system/library/export_import/Classes/PHPExcel/locale/de', '2019-05-16 14:45:57'),
(182, 2, 'system/library/export_import/Classes/PHPExcel/locale/en', '2019-05-16 14:45:57'),
(183, 2, 'system/library/export_import/Classes/PHPExcel/locale/es', '2019-05-16 14:45:57'),
(184, 2, 'system/library/export_import/Classes/PHPExcel/locale/fi', '2019-05-16 14:45:57'),
(185, 2, 'system/library/export_import/Classes/PHPExcel/locale/fr', '2019-05-16 14:45:57'),
(186, 2, 'system/library/export_import/Classes/PHPExcel/locale/hu', '2019-05-16 14:45:57'),
(187, 2, 'system/library/export_import/Classes/PHPExcel/locale/it', '2019-05-16 14:45:57'),
(188, 2, 'system/library/export_import/Classes/PHPExcel/locale/nl', '2019-05-16 14:45:57'),
(189, 2, 'system/library/export_import/Classes/PHPExcel/locale/no', '2019-05-16 14:45:57'),
(190, 2, 'system/library/export_import/Classes/PHPExcel/locale/pl', '2019-05-16 14:45:57'),
(191, 2, 'system/library/export_import/Classes/PHPExcel/locale/pt', '2019-05-16 14:45:57'),
(192, 2, 'system/library/export_import/Classes/PHPExcel/locale/ru', '2019-05-16 14:45:57'),
(193, 2, 'system/library/export_import/Classes/PHPExcel/locale/sv', '2019-05-16 14:45:57'),
(194, 2, 'system/library/export_import/Classes/PHPExcel/locale/tr', '2019-05-16 14:45:57'),
(195, 2, 'system/library/export_import/Classes/PHPExcel/Calculation/Token/Stack.php', '2019-05-16 14:45:57'),
(196, 2, 'system/library/export_import/Classes/PHPExcel/Chart/Renderer/PHP Charting Libraries.txt', '2019-05-16 14:45:57'),
(197, 2, 'system/library/export_import/Classes/PHPExcel/Chart/Renderer/jpgraph.php', '2019-05-16 14:45:57'),
(198, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Excel2007/Chart.php', '2019-05-16 14:45:57'),
(199, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Excel2007/Theme.php', '2019-05-16 14:45:57'),
(200, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Excel5/Color', '2019-05-16 14:45:57'),
(201, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Excel5/Color.php', '2019-05-16 14:45:57'),
(202, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Excel5/ErrorCode.php', '2019-05-16 14:45:57'),
(203, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Excel5/Escher.php', '2019-05-16 14:45:57'),
(204, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Excel5/MD5.php', '2019-05-16 14:45:57'),
(205, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Excel5/RC4.php', '2019-05-16 14:45:57'),
(206, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Excel5/Style', '2019-05-16 14:45:57'),
(207, 2, 'system/library/export_import/Classes/PHPExcel/Shared/Escher/DgContainer', '2019-05-16 14:45:57'),
(208, 2, 'system/library/export_import/Classes/PHPExcel/Shared/Escher/DgContainer.php', '2019-05-16 14:45:57'),
(209, 2, 'system/library/export_import/Classes/PHPExcel/Shared/Escher/DggContainer', '2019-05-16 14:45:57'),
(210, 2, 'system/library/export_import/Classes/PHPExcel/Shared/Escher/DggContainer.php', '2019-05-16 14:45:57'),
(211, 2, 'system/library/export_import/Classes/PHPExcel/Shared/JAMA/CHANGELOG.TXT', '2019-05-16 14:45:57'),
(212, 2, 'system/library/export_import/Classes/PHPExcel/Shared/JAMA/CholeskyDecomposition.php', '2019-05-16 14:45:57'),
(213, 2, 'system/library/export_import/Classes/PHPExcel/Shared/JAMA/EigenvalueDecomposition.php', '2019-05-16 14:45:57'),
(214, 2, 'system/library/export_import/Classes/PHPExcel/Shared/JAMA/LUDecomposition.php', '2019-05-16 14:45:57'),
(215, 2, 'system/library/export_import/Classes/PHPExcel/Shared/JAMA/Matrix.php', '2019-05-16 14:45:57'),
(216, 2, 'system/library/export_import/Classes/PHPExcel/Shared/JAMA/QRDecomposition.php', '2019-05-16 14:45:57'),
(217, 2, 'system/library/export_import/Classes/PHPExcel/Shared/JAMA/SingularValueDecomposition.php', '2019-05-16 14:45:57'),
(218, 2, 'system/library/export_import/Classes/PHPExcel/Shared/JAMA/utils', '2019-05-16 14:45:57'),
(219, 2, 'system/library/export_import/Classes/PHPExcel/Shared/OLE/ChainedBlockStream.php', '2019-05-16 14:45:57'),
(220, 2, 'system/library/export_import/Classes/PHPExcel/Shared/OLE/PPS', '2019-05-16 14:45:57'),
(221, 2, 'system/library/export_import/Classes/PHPExcel/Shared/OLE/PPS.php', '2019-05-16 14:45:57'),
(222, 2, 'system/library/export_import/Classes/PHPExcel/Shared/PCLZip/gnu-lgpl.txt', '2019-05-16 14:45:57'),
(223, 2, 'system/library/export_import/Classes/PHPExcel/Shared/PCLZip/pclzip.lib.php', '2019-05-16 14:45:57'),
(224, 2, 'system/library/export_import/Classes/PHPExcel/Shared/PCLZip/readme.txt', '2019-05-16 14:45:57'),
(225, 2, 'system/library/export_import/Classes/PHPExcel/Shared/trend/bestFitClass.php', '2019-05-16 14:45:57'),
(226, 2, 'system/library/export_import/Classes/PHPExcel/Shared/trend/exponentialBestFitClass.php', '2019-05-16 14:45:57'),
(227, 2, 'system/library/export_import/Classes/PHPExcel/Shared/trend/linearBestFitClass.php', '2019-05-16 14:45:57'),
(228, 2, 'system/library/export_import/Classes/PHPExcel/Shared/trend/logarithmicBestFitClass.php', '2019-05-16 14:45:57'),
(229, 2, 'system/library/export_import/Classes/PHPExcel/Shared/trend/polynomialBestFitClass.php', '2019-05-16 14:45:57'),
(230, 2, 'system/library/export_import/Classes/PHPExcel/Shared/trend/powerBestFitClass.php', '2019-05-16 14:45:57'),
(231, 2, 'system/library/export_import/Classes/PHPExcel/Shared/trend/trendClass.php', '2019-05-16 14:45:57'),
(232, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/AutoFilter/Column', '2019-05-16 14:45:57'),
(233, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/AutoFilter/Column.php', '2019-05-16 14:45:57'),
(234, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/Drawing/Shadow.php', '2019-05-16 14:45:57'),
(235, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel2007/Chart.php', '2019-05-16 14:45:57'),
(236, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel2007/Comments.php', '2019-05-16 14:45:57'),
(237, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel2007/ContentTypes.php', '2019-05-16 14:45:57'),
(238, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel2007/DocProps.php', '2019-05-16 14:45:57'),
(239, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel2007/Drawing.php', '2019-05-16 14:45:57'),
(240, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel2007/Rels.php', '2019-05-16 14:45:57'),
(241, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel2007/RelsRibbon.php', '2019-05-16 14:45:57'),
(242, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel2007/RelsVBA.php', '2019-05-16 14:45:57'),
(243, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel2007/StringTable.php', '2019-05-16 14:45:57'),
(244, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel2007/Style.php', '2019-05-16 14:45:57'),
(245, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel2007/Theme.php', '2019-05-16 14:45:57'),
(246, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel2007/Workbook.php', '2019-05-16 14:45:57'),
(247, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel2007/Worksheet.php', '2019-05-16 14:45:57'),
(248, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel2007/WriterPart.php', '2019-05-16 14:45:57'),
(249, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel5/BIFFwriter.php', '2019-05-16 14:45:57'),
(250, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel5/Escher.php', '2019-05-16 14:45:57'),
(251, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel5/Font.php', '2019-05-16 14:45:57'),
(252, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel5/Parser.php', '2019-05-16 14:45:57'),
(253, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel5/Workbook.php', '2019-05-16 14:45:57'),
(254, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel5/Worksheet.php', '2019-05-16 14:45:57'),
(255, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel5/Xf.php', '2019-05-16 14:45:57'),
(256, 2, 'system/library/export_import/Classes/PHPExcel/Writer/OpenDocument/Cell', '2019-05-16 14:45:57'),
(257, 2, 'system/library/export_import/Classes/PHPExcel/Writer/OpenDocument/Content.php', '2019-05-16 14:45:57'),
(258, 2, 'system/library/export_import/Classes/PHPExcel/Writer/OpenDocument/Meta.php', '2019-05-16 14:45:57'),
(259, 2, 'system/library/export_import/Classes/PHPExcel/Writer/OpenDocument/MetaInf.php', '2019-05-16 14:45:57'),
(260, 2, 'system/library/export_import/Classes/PHPExcel/Writer/OpenDocument/Mimetype.php', '2019-05-16 14:45:57'),
(261, 2, 'system/library/export_import/Classes/PHPExcel/Writer/OpenDocument/Settings.php', '2019-05-16 14:45:57'),
(262, 2, 'system/library/export_import/Classes/PHPExcel/Writer/OpenDocument/Styles.php', '2019-05-16 14:45:57'),
(263, 2, 'system/library/export_import/Classes/PHPExcel/Writer/OpenDocument/Thumbnails.php', '2019-05-16 14:45:57'),
(264, 2, 'system/library/export_import/Classes/PHPExcel/Writer/OpenDocument/WriterPart.php', '2019-05-16 14:45:57'),
(265, 2, 'system/library/export_import/Classes/PHPExcel/Writer/PDF/Core.php', '2019-05-16 14:45:57'),
(266, 2, 'system/library/export_import/Classes/PHPExcel/Writer/PDF/DomPDF.php', '2019-05-16 14:45:57'),
(267, 2, 'system/library/export_import/Classes/PHPExcel/Writer/PDF/mPDF.php', '2019-05-16 14:45:57'),
(268, 2, 'system/library/export_import/Classes/PHPExcel/Writer/PDF/tcPDF.php', '2019-05-16 14:45:57'),
(269, 2, 'system/library/export_import/Classes/PHPExcel/locale/bg/config', '2019-05-16 14:45:57'),
(270, 2, 'system/library/export_import/Classes/PHPExcel/locale/cs/config', '2019-05-16 14:45:57'),
(271, 2, 'system/library/export_import/Classes/PHPExcel/locale/cs/functions', '2019-05-16 14:45:57'),
(272, 2, 'system/library/export_import/Classes/PHPExcel/locale/da/config', '2019-05-16 14:45:57'),
(273, 2, 'system/library/export_import/Classes/PHPExcel/locale/da/functions', '2019-05-16 14:45:57'),
(274, 2, 'system/library/export_import/Classes/PHPExcel/locale/de/config', '2019-05-16 14:45:57'),
(275, 2, 'system/library/export_import/Classes/PHPExcel/locale/de/functions', '2019-05-16 14:45:57'),
(276, 2, 'system/library/export_import/Classes/PHPExcel/locale/en/uk', '2019-05-16 14:45:57'),
(277, 2, 'system/library/export_import/Classes/PHPExcel/locale/es/config', '2019-05-16 14:45:57'),
(278, 2, 'system/library/export_import/Classes/PHPExcel/locale/es/functions', '2019-05-16 14:45:57'),
(279, 2, 'system/library/export_import/Classes/PHPExcel/locale/fi/config', '2019-05-16 14:45:57'),
(280, 2, 'system/library/export_import/Classes/PHPExcel/locale/fi/functions', '2019-05-16 14:45:57'),
(281, 2, 'system/library/export_import/Classes/PHPExcel/locale/fr/config', '2019-05-16 14:45:57'),
(282, 2, 'system/library/export_import/Classes/PHPExcel/locale/fr/functions', '2019-05-16 14:45:57'),
(283, 2, 'system/library/export_import/Classes/PHPExcel/locale/hu/config', '2019-05-16 14:45:57'),
(284, 2, 'system/library/export_import/Classes/PHPExcel/locale/hu/functions', '2019-05-16 14:45:57'),
(285, 2, 'system/library/export_import/Classes/PHPExcel/locale/it/config', '2019-05-16 14:45:57'),
(286, 2, 'system/library/export_import/Classes/PHPExcel/locale/it/functions', '2019-05-16 14:45:57'),
(287, 2, 'system/library/export_import/Classes/PHPExcel/locale/nl/config', '2019-05-16 14:45:57'),
(288, 2, 'system/library/export_import/Classes/PHPExcel/locale/nl/functions', '2019-05-16 14:45:57'),
(289, 2, 'system/library/export_import/Classes/PHPExcel/locale/no/config', '2019-05-16 14:45:57'),
(290, 2, 'system/library/export_import/Classes/PHPExcel/locale/no/functions', '2019-05-16 14:45:57'),
(291, 2, 'system/library/export_import/Classes/PHPExcel/locale/pl/config', '2019-05-16 14:45:57'),
(292, 2, 'system/library/export_import/Classes/PHPExcel/locale/pl/functions', '2019-05-16 14:45:57'),
(293, 2, 'system/library/export_import/Classes/PHPExcel/locale/pt/br', '2019-05-16 14:45:57'),
(294, 2, 'system/library/export_import/Classes/PHPExcel/locale/pt/config', '2019-05-16 14:45:57'),
(295, 2, 'system/library/export_import/Classes/PHPExcel/locale/pt/functions', '2019-05-16 14:45:57'),
(296, 2, 'system/library/export_import/Classes/PHPExcel/locale/ru/config', '2019-05-16 14:45:57'),
(297, 2, 'system/library/export_import/Classes/PHPExcel/locale/ru/functions', '2019-05-16 14:45:57'),
(298, 2, 'system/library/export_import/Classes/PHPExcel/locale/sv/config', '2019-05-16 14:45:57'),
(299, 2, 'system/library/export_import/Classes/PHPExcel/locale/sv/functions', '2019-05-16 14:45:57'),
(300, 2, 'system/library/export_import/Classes/PHPExcel/locale/tr/config', '2019-05-16 14:45:57'),
(301, 2, 'system/library/export_import/Classes/PHPExcel/locale/tr/functions', '2019-05-16 14:45:57'),
(302, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Excel5/Color/BIFF5.php', '2019-05-16 14:45:57'),
(303, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Excel5/Color/BIFF8.php', '2019-05-16 14:45:57'),
(304, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Excel5/Color/BuiltIn.php', '2019-05-16 14:45:57'),
(305, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Excel5/Style/Border.php', '2019-05-16 14:45:57'),
(306, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Excel5/Style/FillPattern.php', '2019-05-16 14:45:57'),
(307, 2, 'system/library/export_import/Classes/PHPExcel/Shared/Escher/DgContainer/SpgrContainer', '2019-05-16 14:45:57'),
(308, 2, 'system/library/export_import/Classes/PHPExcel/Shared/Escher/DgContainer/SpgrContainer.php', '2019-05-16 14:45:57'),
(309, 2, 'system/library/export_import/Classes/PHPExcel/Shared/Escher/DggContainer/BstoreContainer', '2019-05-16 14:45:57'),
(310, 2, 'system/library/export_import/Classes/PHPExcel/Shared/Escher/DggContainer/BstoreContainer.php', '2019-05-16 14:45:57'),
(311, 2, 'system/library/export_import/Classes/PHPExcel/Shared/JAMA/utils/Error.php', '2019-05-16 14:45:57'),
(312, 2, 'system/library/export_import/Classes/PHPExcel/Shared/JAMA/utils/Maths.php', '2019-05-16 14:45:57'),
(313, 2, 'system/library/export_import/Classes/PHPExcel/Shared/OLE/PPS/File.php', '2019-05-16 14:45:57'),
(314, 2, 'system/library/export_import/Classes/PHPExcel/Shared/OLE/PPS/Root.php', '2019-05-16 14:45:57'),
(315, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/AutoFilter/Column/Rule.php', '2019-05-16 14:45:57'),
(316, 2, 'system/library/export_import/Classes/PHPExcel/Writer/OpenDocument/Cell/Comment.php', '2019-05-16 14:45:57'),
(317, 2, 'system/library/export_import/Classes/PHPExcel/locale/en/uk/config', '2019-05-16 14:45:57'),
(318, 2, 'system/library/export_import/Classes/PHPExcel/locale/pt/br/config', '2019-05-16 14:45:57'),
(319, 2, 'system/library/export_import/Classes/PHPExcel/locale/pt/br/functions', '2019-05-16 14:45:57'),
(320, 2, 'system/library/export_import/Classes/PHPExcel/Shared/Escher/DgContainer/SpgrContainer/SpContainer.php', '2019-05-16 14:45:57'),
(321, 2, 'system/library/export_import/Classes/PHPExcel/Shared/Escher/DggContainer/BstoreContainer/BSE', '2019-05-16 14:45:57'),
(322, 2, 'system/library/export_import/Classes/PHPExcel/Shared/Escher/DggContainer/BstoreContainer/BSE.php', '2019-05-16 14:45:57'),
(323, 2, 'system/library/export_import/Classes/PHPExcel/Shared/Escher/DggContainer/BstoreContainer/BSE/Blip.php', '2019-05-16 14:45:57'),
(324, 3, 'admin/view/stylesheet/autocomplete.css', '2019-05-16 14:46:10'),
(700, 7, 'admin/controller/extension/module/extendedsearch.php', '2019-05-16 16:11:10'),
(701, 7, 'admin/language/en-gb/extension/module/extendedsearch.php', '2019-05-16 16:11:10'),
(702, 7, 'admin/language/ru-ru/extension/module/extendedsearch.php', '2019-05-16 16:11:10'),
(703, 7, 'admin/view/template/extension/module/extendedsearch.twig', '2019-05-16 16:11:10'),
(704, 8, 'admin/language/uk-ua', '2019-05-22 20:18:39'),
(705, 8, 'catalog/language/uk-ua', '2019-05-22 20:18:39'),
(706, 8, 'admin/language/uk-ua/extension', '2019-05-22 20:18:39'),
(707, 8, 'admin/model/extension/module', '2019-05-22 20:18:39'),
(708, 8, 'admin/view/javascript/quickcheckout', '2019-05-22 20:18:39'),
(709, 8, 'admin/view/stylesheet/quickcheckout.css', '2019-05-22 20:18:39'),
(710, 8, 'catalog/controller/extension/quickcheckout', '2019-05-22 20:18:39'),
(711, 8, 'catalog/language/uk-ua/extension', '2019-05-22 20:18:39'),
(712, 8, 'admin/controller/extension/module/quickcheckout.php', '2019-05-22 20:18:39'),
(713, 8, 'admin/language/uk-ua/extension/module', '2019-05-22 20:18:39'),
(714, 8, 'admin/model/extension/module/quickcheckout.php', '2019-05-22 20:18:39'),
(715, 8, 'admin/view/javascript/quickcheckout/bootstrap-slider', '2019-05-22 20:18:39'),
(716, 8, 'admin/view/javascript/quickcheckout/bootstrap-sortable.js', '2019-05-22 20:18:39'),
(717, 8, 'admin/view/javascript/quickcheckout/tinysort', '2019-05-22 20:18:39'),
(718, 8, 'catalog/controller/extension/quickcheckout/cart.php', '2019-05-22 20:18:39'),
(719, 8, 'catalog/controller/extension/quickcheckout/checkout.php', '2019-05-22 20:18:39'),
(720, 8, 'catalog/controller/extension/quickcheckout/confirm.php', '2019-05-22 20:18:39'),
(721, 8, 'catalog/controller/extension/quickcheckout/guest.php', '2019-05-22 20:18:39'),
(722, 8, 'catalog/controller/extension/quickcheckout/guest_shipping.php', '2019-05-22 20:18:39'),
(723, 8, 'catalog/controller/extension/quickcheckout/login.php', '2019-05-22 20:18:39'),
(724, 8, 'catalog/controller/extension/quickcheckout/payment_address.php', '2019-05-22 20:18:39'),
(725, 8, 'catalog/controller/extension/quickcheckout/payment_method.php', '2019-05-22 20:18:39'),
(726, 8, 'catalog/controller/extension/quickcheckout/register.php', '2019-05-22 20:18:39'),
(727, 8, 'catalog/controller/extension/quickcheckout/shipping_address.php', '2019-05-22 20:18:39'),
(728, 8, 'catalog/controller/extension/quickcheckout/shipping_method.php', '2019-05-22 20:18:39'),
(729, 8, 'catalog/controller/extension/quickcheckout/terms.php', '2019-05-22 20:18:39'),
(730, 8, 'catalog/controller/extension/quickcheckout/voucher.php', '2019-05-22 20:18:39'),
(731, 8, 'catalog/language/en-gb/extension/quickcheckout', '2019-05-22 20:18:39'),
(732, 8, 'catalog/language/ru-ru/extension/quickcheckout', '2019-05-22 20:18:39'),
(733, 8, 'catalog/language/uk-ua/extension/quickcheckout', '2019-05-22 20:18:39'),
(734, 8, 'catalog/view/javascript/jquery/quickcheckout', '2019-05-22 20:18:39'),
(735, 8, 'admin/language/en-gb/extension/module/quickcheckout.php', '2019-05-22 20:18:39'),
(736, 8, 'admin/language/ru-ru/extension/module/quickcheckout.php', '2019-05-22 20:18:39'),
(737, 8, 'admin/language/uk-ua/extension/module/quickcheckout.php', '2019-05-22 20:18:39'),
(738, 8, 'admin/view/javascript/quickcheckout/bootstrap-slider/css', '2019-05-22 20:18:39'),
(739, 8, 'admin/view/javascript/quickcheckout/bootstrap-slider/js', '2019-05-22 20:18:39'),
(740, 8, 'admin/view/javascript/quickcheckout/tinysort/jquery.tinysort.min.js', '2019-05-22 20:18:39'),
(741, 8, 'admin/view/template/extension/module/quickcheckout.twig', '2019-05-22 20:18:39'),
(742, 8, 'catalog/language/en-gb/extension/quickcheckout/checkout.php', '2019-05-22 20:18:39'),
(743, 8, 'catalog/language/ru-ru/extension/quickcheckout/checkout.php', '2019-05-22 20:18:39'),
(744, 8, 'catalog/language/uk-ua/extension/quickcheckout/checkout.php', '2019-05-22 20:18:39'),
(745, 8, 'catalog/view/javascript/jquery/quickcheckout/bootstrap-datetimepicker.min.css', '2019-05-22 20:18:39'),
(746, 8, 'catalog/view/javascript/jquery/quickcheckout/bootstrap-datetimepicker.min.js', '2019-05-22 20:18:39'),
(747, 8, 'catalog/view/javascript/jquery/quickcheckout/jquery.tinysort.min.js', '2019-05-22 20:18:39'),
(748, 8, 'catalog/view/javascript/jquery/quickcheckout/moment-with-locales.min.js', '2019-05-22 20:18:39'),
(749, 8, 'catalog/view/javascript/jquery/quickcheckout/quickcheckout.block.js', '2019-05-22 20:18:39'),
(750, 8, 'catalog/view/javascript/jquery/quickcheckout/quickcheckout.js', '2019-05-22 20:18:39'),
(751, 8, 'catalog/view/theme/default/stylesheet/quickcheckout.css', '2019-05-22 20:18:39'),
(752, 8, 'catalog/view/theme/default/stylesheet/quickcheckout_custom.css', '2019-05-22 20:18:39'),
(753, 8, 'catalog/view/theme/default/stylesheet/quickcheckout_mobile.css', '2019-05-22 20:18:39'),
(754, 8, 'catalog/view/theme/default/stylesheet/quickcheckout_one.css', '2019-05-22 20:18:39'),
(755, 8, 'catalog/view/theme/default/stylesheet/quickcheckout_three.css', '2019-05-22 20:18:39'),
(756, 8, 'catalog/view/theme/default/stylesheet/quickcheckout_two.css', '2019-05-22 20:18:39'),
(757, 8, 'admin/view/javascript/quickcheckout/bootstrap-slider/css/slider.css', '2019-05-22 20:18:39'),
(758, 8, 'admin/view/javascript/quickcheckout/bootstrap-slider/js/bootstrap-slider.js', '2019-05-22 20:18:39'),
(759, 8, 'catalog/view/theme/default/template/extension/quickcheckout', '2019-05-22 20:18:39'),
(760, 8, 'catalog/view/theme/default/template/extension/quickcheckout/cart.twig', '2019-05-22 20:18:39'),
(761, 8, 'catalog/view/theme/default/template/extension/quickcheckout/checkout.twig', '2019-05-22 20:18:39'),
(762, 8, 'catalog/view/theme/default/template/extension/quickcheckout/confirm.twig', '2019-05-22 20:18:39'),
(763, 8, 'catalog/view/theme/default/template/extension/quickcheckout/guest.twig', '2019-05-22 20:18:39'),
(764, 8, 'catalog/view/theme/default/template/extension/quickcheckout/guest_shipping.twig', '2019-05-22 20:18:39'),
(765, 8, 'catalog/view/theme/default/template/extension/quickcheckout/login.twig', '2019-05-22 20:18:39'),
(766, 8, 'catalog/view/theme/default/template/extension/quickcheckout/payment_address.twig', '2019-05-22 20:18:39'),
(767, 8, 'catalog/view/theme/default/template/extension/quickcheckout/payment_method.twig', '2019-05-22 20:18:39'),
(768, 8, 'catalog/view/theme/default/template/extension/quickcheckout/register.twig', '2019-05-22 20:18:39'),
(769, 8, 'catalog/view/theme/default/template/extension/quickcheckout/shipping_address.twig', '2019-05-22 20:18:39'),
(770, 8, 'catalog/view/theme/default/template/extension/quickcheckout/shipping_method.twig', '2019-05-22 20:18:39'),
(771, 8, 'catalog/view/theme/default/template/extension/quickcheckout/terms.twig', '2019-05-22 20:18:39'),
(772, 8, 'catalog/view/theme/default/template/extension/quickcheckout/voucher.twig', '2019-05-22 20:18:39'),
(955, 17, 'admin/controller/dashboard', '2019-08-07 15:32:01'),
(956, 17, 'admin/view/fontawesome', '2019-08-07 15:32:01'),
(957, 17, 'admin/controller/dashboard/manager.php', '2019-08-07 15:32:01'),
(958, 17, 'admin/model/sale/manager.php', '2019-08-07 15:32:01'),
(959, 17, 'admin/view/fontawesome/css', '2019-08-07 15:32:01'),
(960, 17, 'admin/view/fontawesome/fonts', '2019-08-07 15:32:01'),
(961, 17, 'admin/view/fontawesome/less', '2019-08-07 15:32:01'),
(962, 17, 'admin/view/fontawesome/scss', '2019-08-07 15:32:01'),
(963, 17, 'admin/view/stylesheet/manager.css', '2019-08-07 15:32:01'),
(964, 17, 'admin/view/template/dashboard', '2019-08-07 15:32:01'),
(965, 17, 'admin/controller/extension/module/manager.php', '2019-08-07 15:32:01'),
(966, 17, 'admin/view/fontawesome/css/font-awesome.css', '2019-08-07 15:32:01'),
(967, 17, 'admin/view/fontawesome/css/font-awesome.min.css', '2019-08-07 15:32:01'),
(968, 17, 'admin/view/fontawesome/fonts/FontAwesome.otf', '2019-08-07 15:32:01'),
(969, 17, 'admin/view/fontawesome/fonts/fontawesome-webfont.eot', '2019-08-07 15:32:01'),
(970, 17, 'admin/view/fontawesome/fonts/fontawesome-webfont.svg', '2019-08-07 15:32:01'),
(971, 17, 'admin/view/fontawesome/fonts/fontawesome-webfont.ttf', '2019-08-07 15:32:01'),
(972, 17, 'admin/view/fontawesome/fonts/fontawesome-webfont.woff', '2019-08-07 15:32:01'),
(973, 17, 'admin/view/fontawesome/fonts/fontawesome-webfont.woff2', '2019-08-07 15:32:01'),
(974, 17, 'admin/view/fontawesome/less/animated.less', '2019-08-07 15:32:01'),
(975, 17, 'admin/view/fontawesome/less/bordered-pulled.less', '2019-08-07 15:32:01'),
(976, 17, 'admin/view/fontawesome/less/core.less', '2019-08-07 15:32:01'),
(977, 17, 'admin/view/fontawesome/less/fixed-width.less', '2019-08-07 15:32:01'),
(978, 17, 'admin/view/fontawesome/less/font-awesome.less', '2019-08-07 15:32:01'),
(979, 17, 'admin/view/fontawesome/less/icons.less', '2019-08-07 15:32:01'),
(980, 17, 'admin/view/fontawesome/less/larger.less', '2019-08-07 15:32:01'),
(981, 17, 'admin/view/fontawesome/less/list.less', '2019-08-07 15:32:01'),
(982, 17, 'admin/view/fontawesome/less/mixins.less', '2019-08-07 15:32:01'),
(983, 17, 'admin/view/fontawesome/less/path.less', '2019-08-07 15:32:01'),
(984, 17, 'admin/view/fontawesome/less/rotated-flipped.less', '2019-08-07 15:32:01'),
(985, 17, 'admin/view/fontawesome/less/stacked.less', '2019-08-07 15:32:01'),
(986, 17, 'admin/view/fontawesome/less/variables.less', '2019-08-07 15:32:01'),
(987, 17, 'admin/view/fontawesome/scss/_animated.scss', '2019-08-07 15:32:01'),
(988, 17, 'admin/view/fontawesome/scss/_bordered-pulled.scss', '2019-08-07 15:32:01'),
(989, 17, 'admin/view/fontawesome/scss/_core.scss', '2019-08-07 15:32:01'),
(990, 17, 'admin/view/fontawesome/scss/_fixed-width.scss', '2019-08-07 15:32:01'),
(991, 17, 'admin/view/fontawesome/scss/_icons.scss', '2019-08-07 15:32:01'),
(992, 17, 'admin/view/fontawesome/scss/_larger.scss', '2019-08-07 15:32:01'),
(993, 17, 'admin/view/fontawesome/scss/_list.scss', '2019-08-07 15:32:01'),
(994, 17, 'admin/view/fontawesome/scss/_mixins.scss', '2019-08-07 15:32:01'),
(995, 17, 'admin/view/fontawesome/scss/_path.scss', '2019-08-07 15:32:01'),
(996, 17, 'admin/view/fontawesome/scss/_rotated-flipped.scss', '2019-08-07 15:32:01'),
(997, 17, 'admin/view/fontawesome/scss/_stacked.scss', '2019-08-07 15:32:01'),
(998, 17, 'admin/view/fontawesome/scss/_variables.scss', '2019-08-07 15:32:01'),
(999, 17, 'admin/view/fontawesome/scss/font-awesome.scss', '2019-08-07 15:32:01'),
(1000, 17, 'admin/view/template/dashboard/manager.twig', '2019-08-07 15:32:01'),
(1001, 17, 'admin/language/en-gb/extension/module/manager.php', '2019-08-07 15:32:01'),
(1002, 17, 'admin/language/ru-ru/extension/module/manager.php', '2019-08-07 15:32:01'),
(1003, 17, 'admin/view/template/extension/module/manager.twig', '2019-08-07 15:32:01'),
(1004, 18, 'admin/controller/extension/feed/yandex_sitemap.php', '2019-09-16 15:55:23'),
(1005, 18, 'catalog/controller/extension/feed/yandex_sitemap.php', '2019-09-16 15:55:23'),
(1006, 18, 'admin/language/en-gb/extension/feed/yandex_sitemap.php', '2019-09-16 15:55:23'),
(1007, 18, 'admin/view/template/extension/feed/yandex_sitemap.twig', '2019-09-16 15:55:23'),
(1008, 19, 'admin/language/en', '2020-05-20 04:18:57'),
(1009, 19, 'admin/language/en-us', '2020-05-20 04:18:57'),
(1010, 19, 'admin/language/english', '2020-05-20 04:18:57'),
(1011, 19, 'admin/language/ru', '2020-05-20 04:18:57'),
(1012, 19, 'admin/language/russian', '2020-05-20 04:18:57'),
(1013, 19, 'catalog/language/en', '2020-05-20 04:18:57'),
(1014, 19, 'catalog/language/en-us', '2020-05-20 04:18:57'),
(1015, 19, 'catalog/language/english', '2020-05-20 04:18:57'),
(1016, 19, 'catalog/language/ru', '2020-05-20 04:18:57'),
(1017, 19, 'catalog/language/russian', '2020-05-20 04:18:57'),
(1018, 19, 'system/config/ocfilter.php', '2020-05-20 04:18:57'),
(1019, 19, 'system/library/ocfilter.php', '2020-05-20 04:18:57'),
(1020, 19, 'admin/language/en/extension', '2020-05-20 04:18:57'),
(1021, 19, 'admin/language/en-us/extension', '2020-05-20 04:18:57'),
(1022, 19, 'admin/language/english/extension', '2020-05-20 04:18:57'),
(1023, 19, 'admin/language/ru/extension', '2020-05-20 04:18:57'),
(1024, 19, 'admin/language/russian/extension', '2020-05-20 04:18:57'),
(1025, 19, 'admin/model/extension/ocfilter.php', '2020-05-20 04:18:57'),
(1026, 19, 'admin/model/extension/ocfilter_page.php', '2020-05-20 04:18:57'),
(1027, 19, 'admin/view/image/ocfilter', '2020-05-20 04:18:57'),
(1028, 19, 'admin/view/javascript/ocfilter', '2020-05-20 04:18:57'),
(1029, 19, 'admin/view/stylesheet/ocfilter', '2020-05-20 04:18:57'),
(1030, 19, 'catalog/language/en/extension', '2020-05-20 04:18:57'),
(1031, 19, 'catalog/language/en-us/extension', '2020-05-20 04:18:57'),
(1032, 19, 'catalog/language/english/extension', '2020-05-20 04:18:57'),
(1033, 19, 'catalog/language/ru/extension', '2020-05-20 04:18:57'),
(1034, 19, 'catalog/language/russian/extension', '2020-05-20 04:18:57'),
(1035, 19, 'catalog/view/javascript/ocfilter', '2020-05-20 04:18:57'),
(1036, 19, 'admin/controller/extension/module/ocfilter.php', '2020-05-20 04:18:57'),
(1037, 19, 'admin/language/en/extension/module', '2020-05-20 04:18:57'),
(1038, 19, 'admin/language/en-us/extension/module', '2020-05-20 04:18:57'),
(1039, 19, 'admin/language/english/extension/module', '2020-05-20 04:18:57'),
(1040, 19, 'admin/language/ru/extension/module', '2020-05-20 04:18:57'),
(1041, 19, 'admin/language/russian/extension/module', '2020-05-20 04:18:57'),
(1042, 19, 'admin/view/image/ocfilter/delete-value.png', '2020-05-20 04:18:57'),
(1043, 19, 'admin/view/image/ocfilter/select-text.png', '2020-05-20 04:18:57'),
(1044, 19, 'admin/view/image/ocfilter/sort-handler.png', '2020-05-20 04:18:57'),
(1045, 19, 'admin/view/javascript/ocfilter/ocfilter.js', '2020-05-20 04:18:57'),
(1046, 19, 'admin/view/stylesheet/ocfilter/ocfilter.css', '2020-05-20 04:18:57'),
(1047, 19, 'catalog/controller/extension/feed/ocfilter_sitemap.php', '2020-05-20 04:18:57'),
(1048, 19, 'catalog/controller/extension/module/ocfilter.php', '2020-05-20 04:18:57'),
(1049, 19, 'catalog/language/en/extension/module', '2020-05-20 04:18:57'),
(1050, 19, 'catalog/language/en-us/extension/module', '2020-05-20 04:18:57'),
(1051, 19, 'catalog/language/english/extension/module', '2020-05-20 04:18:57'),
(1052, 19, 'catalog/language/ru/extension/module', '2020-05-20 04:18:57'),
(1053, 19, 'catalog/language/russian/extension/module', '2020-05-20 04:18:57'),
(1054, 19, 'catalog/model/extension/module/ocfilter.php', '2020-05-20 04:18:57'),
(1055, 19, 'catalog/view/javascript/ocfilter/nouislider.min.css', '2020-05-20 04:18:57'),
(1056, 19, 'catalog/view/javascript/ocfilter/nouislider.min.js', '2020-05-20 04:18:57'),
(1057, 19, 'catalog/view/javascript/ocfilter/ocfilter.js', '2020-05-20 04:18:57'),
(1058, 19, 'admin/language/en/extension/module/ocfilter.php', '2020-05-20 04:18:57'),
(1059, 19, 'admin/language/en-gb/extension/module/ocfilter.php', '2020-05-20 04:18:57'),
(1060, 19, 'admin/language/en-us/extension/module/ocfilter.php', '2020-05-20 04:18:57'),
(1061, 19, 'admin/language/english/extension/module/ocfilter.php', '2020-05-20 04:18:57'),
(1062, 19, 'admin/language/ru/extension/module/ocfilter.php', '2020-05-20 04:18:57'),
(1063, 19, 'admin/language/ru-ru/extension/module/ocfilter.php', '2020-05-20 04:18:57'),
(1064, 19, 'admin/language/russian/extension/module/ocfilter.php', '2020-05-20 04:18:57'),
(1065, 19, 'admin/view/template/extension/module/ocfilter.twig', '2020-05-20 04:18:57'),
(1066, 19, 'admin/view/template/extension/module/ocfilter_form.twig', '2020-05-20 04:18:57'),
(1067, 19, 'admin/view/template/extension/module/ocfilter_list.twig', '2020-05-20 04:18:57'),
(1068, 19, 'admin/view/template/extension/module/ocfilter_page_form.twig', '2020-05-20 04:18:57'),
(1069, 19, 'admin/view/template/extension/module/ocfilter_page_list.twig', '2020-05-20 04:18:57'),
(1070, 19, 'catalog/language/en/extension/module/ocfilter.php', '2020-05-20 04:18:57'),
(1071, 19, 'catalog/language/en-gb/extension/module/ocfilter.php', '2020-05-20 04:18:57'),
(1072, 19, 'catalog/language/en-us/extension/module/ocfilter.php', '2020-05-20 04:18:57'),
(1073, 19, 'catalog/language/english/extension/module/ocfilter.php', '2020-05-20 04:18:57'),
(1074, 19, 'catalog/language/ru/extension/module/ocfilter.php', '2020-05-20 04:18:57'),
(1075, 19, 'catalog/language/ru-ru/extension/module/ocfilter.php', '2020-05-20 04:18:57'),
(1076, 19, 'catalog/language/russian/extension/module/ocfilter.php', '2020-05-20 04:18:57'),
(1077, 19, 'catalog/view/theme/default/image/ocfilter', '2020-05-20 04:18:57'),
(1078, 19, 'catalog/view/theme/default/stylesheet/ocfilter', '2020-05-20 04:18:57'),
(1079, 19, 'catalog/view/theme/default/image/ocfilter/diagram-bg-repeat.png', '2020-05-20 04:18:57'),
(1080, 19, 'catalog/view/theme/default/stylesheet/ocfilter/ocfilter.css', '2020-05-20 04:18:57'),
(1081, 19, 'catalog/view/theme/default/stylesheet/ocfilter/src', '2020-05-20 04:18:57'),
(1082, 19, 'catalog/view/theme/default/stylesheet/ocfilter/src/drop-arrow.svg', '2020-05-20 04:18:57'),
(1083, 19, 'catalog/view/theme/default/template/extension/module/ocfilter', '2020-05-20 04:18:57'),
(1084, 19, 'catalog/view/theme/default/template/extension/module/ocfilter/filter_item.twig', '2020-05-20 04:18:57'),
(1085, 19, 'catalog/view/theme/default/template/extension/module/ocfilter/filter_list.twig', '2020-05-20 04:18:57');
INSERT INTO `oc_extension_path` (`extension_path_id`, `extension_install_id`, `path`, `date_added`) VALUES
(1086, 19, 'catalog/view/theme/default/template/extension/module/ocfilter/filter_price.twig', '2020-05-20 04:18:57'),
(1087, 19, 'catalog/view/theme/default/template/extension/module/ocfilter/filter_slider_item.twig', '2020-05-20 04:18:57'),
(1088, 19, 'catalog/view/theme/default/template/extension/module/ocfilter/module.twig', '2020-05-20 04:18:57'),
(1089, 19, 'catalog/view/theme/default/template/extension/module/ocfilter/selected_filter.twig', '2020-05-20 04:18:57'),
(1090, 19, 'catalog/view/theme/default/template/extension/module/ocfilter/value_item.twig', '2020-05-20 04:18:57'),
(1091, 19, 'catalog/view/theme/default/template/extension/module/ocfilter/value_list.twig', '2020-05-20 04:18:57'),
(1092, 21, 'admin/controller/event/compatibility.php', '2020-05-20 04:19:08'),
(1093, 21, 'catalog/controller/event/compatibility.php', '2020-05-20 04:19:08'),
(1094, 21, 'admin/controller/extension/module/template_switcher.php', '2020-05-20 04:19:08'),
(1095, 21, 'catalog/controller/extension/module/template_switcher.php', '2020-05-20 04:19:08'),
(1096, 21, 'admin/language/en-gb/extension/module/template_switcher.php', '2020-05-20 04:19:08'),
(1097, 21, 'admin/view/template/extension/module/template_switcher.twig', '2020-05-20 04:19:08'),
(1098, 22, 'admin/model/extension/module/modification_manager.php', '2020-05-20 04:19:15'),
(1099, 22, 'admin/view/javascript/codemirror/mode', '2020-05-20 04:19:15'),
(1100, 22, 'admin/view/javascript/codemirror/lib/codemirror.css', '2020-05-20 04:19:15'),
(1101, 22, 'admin/view/javascript/codemirror/lib/codemirror.js', '2020-05-20 04:19:15'),
(1102, 22, 'admin/view/javascript/codemirror/mode/xml', '2020-05-20 04:19:15'),
(1103, 22, 'admin/view/template/extension/module/modification_manager', '2020-05-20 04:19:15'),
(1104, 22, 'admin/view/javascript/codemirror/mode/xml/xml.js', '2020-05-20 04:19:15'),
(1105, 22, 'admin/view/template/extension/module/modification_manager/form.twig', '2020-05-20 04:19:15'),
(1106, 22, 'admin/view/template/extension/module/modification_manager/list.twig', '2020-05-20 04:19:15'),
(1114, 25, 'system/library/ocm', '2020-07-22 12:05:38'),
(1115, 25, 'admin/model/extension/shipping', '2020-07-22 12:05:38'),
(1116, 25, 'admin/view/javascript/ocm', '2020-07-22 12:05:38'),
(1117, 25, 'catalog/controller/extension/shipping', '2020-07-22 12:05:38'),
(1118, 25, 'catalog/view/javascript/xshippingpro.min.js', '2020-07-22 12:05:38'),
(1119, 25, 'system/library/ocm/back.php', '2020-07-22 12:05:38'),
(1120, 25, 'system/library/ocm/common.php', '2020-07-22 12:05:38'),
(1121, 25, 'system/library/ocm/elements', '2020-07-22 12:05:38'),
(1122, 25, 'system/library/ocm/form.php', '2020-07-22 12:05:38'),
(1123, 25, 'system/library/ocm/front.php', '2020-07-22 12:05:38'),
(1124, 25, 'system/library/ocm/misc.php', '2020-07-22 12:05:38'),
(1125, 25, 'system/library/ocm/setting.php', '2020-07-22 12:05:38'),
(1126, 25, 'system/library/ocm/url.php', '2020-07-22 12:05:38'),
(1127, 25, 'system/library/ocm/util.php', '2020-07-22 12:05:38'),
(1128, 25, 'admin/controller/extension/shipping/xshippingpro.php', '2020-07-22 12:05:38'),
(1129, 25, 'admin/model/extension/shipping/xshippingpro.php', '2020-07-22 12:05:38'),
(1130, 25, 'admin/view/javascript/ocm/ocm.css', '2020-07-22 12:05:38'),
(1131, 25, 'admin/view/javascript/ocm/ocm.js', '2020-07-22 12:05:38'),
(1132, 25, 'catalog/controller/extension/shipping/xshippingpro.php', '2020-07-22 12:05:38'),
(1133, 25, 'catalog/model/extension/shipping/xshippingpro.php', '2020-07-22 12:05:38'),
(1134, 25, 'system/library/ocm/elements/autofill.php', '2020-07-22 12:05:38'),
(1135, 25, 'system/library/ocm/elements/bare.php', '2020-07-22 12:05:38'),
(1136, 25, 'system/library/ocm/elements/base.php', '2020-07-22 12:05:38'),
(1137, 25, 'system/library/ocm/elements/checkbox.php', '2020-07-22 12:05:38'),
(1138, 25, 'system/library/ocm/elements/checkgroup.php', '2020-07-22 12:05:38'),
(1139, 25, 'system/library/ocm/elements/checkrow.php', '2020-07-22 12:05:38'),
(1140, 25, 'system/library/ocm/elements/image.php', '2020-07-22 12:05:38'),
(1141, 25, 'system/library/ocm/elements/input.php', '2020-07-22 12:05:38'),
(1142, 25, 'system/library/ocm/elements/inputgroup.php', '2020-07-22 12:05:38'),
(1143, 25, 'system/library/ocm/elements/radio.php', '2020-07-22 12:05:38'),
(1144, 25, 'system/library/ocm/elements/radiorow.php', '2020-07-22 12:05:38'),
(1145, 25, 'system/library/ocm/elements/range.php', '2020-07-22 12:05:38'),
(1146, 25, 'system/library/ocm/elements/select.php', '2020-07-22 12:05:38'),
(1147, 25, 'system/library/ocm/elements/textarea.php', '2020-07-22 12:05:38'),
(1148, 25, 'admin/language/en-gb/extension/shipping/xshippingpro.php', '2020-07-22 12:05:38'),
(1149, 25, 'admin/view/template/extension/shipping/xshippingpro.twig', '2020-07-22 12:05:38'),
(1150, 25, 'catalog/language/en-gb/extension/shipping/xshippingpro.php', '2020-07-22 12:05:38');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_filter`
--

CREATE TABLE `oc_filter` (
  `filter_id` int(11) NOT NULL,
  `filter_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_filter_description`
--

CREATE TABLE `oc_filter_description` (
  `filter_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `filter_group_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_filter_group`
--

CREATE TABLE `oc_filter_group` (
  `filter_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_filter_group_description`
--

CREATE TABLE `oc_filter_group_description` (
  `filter_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_geo_zone`
--

CREATE TABLE `oc_geo_zone` (
  `geo_zone_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_geo_zone`
--

INSERT INTO `oc_geo_zone` (`geo_zone_id`, `name`, `description`, `date_added`, `date_modified`) VALUES
(3, 'UK VAT Zone', 'UK VAT', '2009-01-06 23:26:25', '2010-02-26 22:33:24'),
(4, 'UK Shipping', 'UK Shipping Zones', '2009-06-23 01:14:53', '2010-12-15 15:18:13');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_information`
--

CREATE TABLE `oc_information` (
  `information_id` int(11) NOT NULL,
  `bottom` int(1) NOT NULL DEFAULT 0,
  `sort_order` int(3) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `noindex` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_information`
--

INSERT INTO `oc_information` (`information_id`, `bottom`, `sort_order`, `status`, `noindex`) VALUES
(5, 0, 1, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_information_description`
--

CREATE TABLE `oc_information_description` (
  `information_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` mediumtext NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_h1` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_information_description`
--

INSERT INTO `oc_information_description` (`information_id`, `language_id`, `title`, `description`, `meta_title`, `meta_description`, `meta_keyword`, `meta_h1`) VALUES
(5, 1, 'Политика конфиденциальности и обработки персональных данных', '&lt;p&gt;Политика конфиденциальности и обработки персональных данных&lt;br&gt;&lt;/p&gt;', 'Политика конфиденциальности и обработки персональных данных', 'Политика конфиденциальности и обработки персональных данных', 'Политика конфиденциальности и обработки персональных данных', 'Политика конфиденциальности и обработки персональных данных');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_information_to_layout`
--

CREATE TABLE `oc_information_to_layout` (
  `information_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_information_to_layout`
--

INSERT INTO `oc_information_to_layout` (`information_id`, `store_id`, `layout_id`) VALUES
(5, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_information_to_store`
--

CREATE TABLE `oc_information_to_store` (
  `information_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_information_to_store`
--

INSERT INTO `oc_information_to_store` (`information_id`, `store_id`) VALUES
(5, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_language`
--

CREATE TABLE `oc_language` (
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `code` varchar(5) NOT NULL,
  `locale` varchar(255) NOT NULL,
  `image` varchar(64) NOT NULL,
  `directory` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_language`
--

INSERT INTO `oc_language` (`language_id`, `name`, `code`, `locale`, `image`, `directory`, `sort_order`, `status`) VALUES
(1, 'Russian', 'ru-ru', 'ru_RU.UTF-8,ru_RU,russian', 'gb.png', 'english', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_layout`
--

CREATE TABLE `oc_layout` (
  `layout_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_layout`
--

INSERT INTO `oc_layout` (`layout_id`, `name`) VALUES
(1, 'Главная'),
(2, 'Товар'),
(3, 'Категория'),
(4, 'По-умолчанию'),
(5, 'Список Производителей'),
(6, 'Аккаунт'),
(7, 'Оформление заказа'),
(8, 'Контакты'),
(9, 'Карта сайта'),
(10, 'Партнерская программа'),
(11, 'Информация'),
(12, 'Сравнение'),
(13, 'Поиск'),
(14, 'Блог'),
(15, 'Категории Блога'),
(16, 'Статьи Блога'),
(17, 'Страница Производителя'),
(18, 'Custom Quick Checkout');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_layout_module`
--

CREATE TABLE `oc_layout_module` (
  `layout_module_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `position` varchar(14) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_layout_module`
--

INSERT INTO `oc_layout_module` (`layout_module_id`, `layout_id`, `code`, `position`, `sort_order`) VALUES
(2, 4, '0', 'content_top', 0),
(3, 4, '0', 'content_top', 1),
(20, 5, '0', 'column_left', 2),
(97, 3, 'ocfilter', 'column_left', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_layout_route`
--

CREATE TABLE `oc_layout_route` (
  `layout_route_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `route` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_layout_route`
--

INSERT INTO `oc_layout_route` (`layout_route_id`, `layout_id`, `store_id`, `route`) VALUES
(17, 10, 0, 'affiliate/%'),
(20, 2, 0, 'product/product'),
(23, 7, 0, 'checkout/%'),
(24, 11, 0, 'information/information'),
(31, 8, 0, 'information/contact'),
(32, 9, 0, 'information/sitemap'),
(34, 4, 0, ''),
(38, 6, 0, 'account/%'),
(45, 5, 0, 'product/manufacturer'),
(52, 12, 0, 'product/compare'),
(53, 13, 0, 'product/search'),
(56, 16, 0, 'blog/article'),
(57, 14, 0, 'blog/latest'),
(58, 15, 0, 'blog/category'),
(63, 17, 0, 'product/manufacturer/info'),
(65, 18, 0, 'extension/quickcheckout/checkout'),
(68, 1, 0, 'common/home'),
(69, 3, 0, 'product/category');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_length_class`
--

CREATE TABLE `oc_length_class` (
  `length_class_id` int(11) NOT NULL,
  `value` decimal(15,8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_length_class`
--

INSERT INTO `oc_length_class` (`length_class_id`, `value`) VALUES
(1, '1.00000000'),
(2, '10.00000000'),
(3, '0.39370000');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_length_class_description`
--

CREATE TABLE `oc_length_class_description` (
  `length_class_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_length_class_description`
--

INSERT INTO `oc_length_class_description` (`length_class_id`, `language_id`, `title`, `unit`) VALUES
(1, 1, 'Сантиметр', 'см'),
(1, 2, 'Centimeter', 'cm'),
(2, 1, 'Миллиметр', 'мм'),
(2, 2, 'Millimeter', 'mm'),
(3, 1, 'Дюйм', 'in'),
(3, 2, 'Inch', 'in');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_location`
--

CREATE TABLE `oc_location` (
  `location_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `address` text NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `geocode` varchar(32) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `open` text NOT NULL,
  `comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_manufacturer`
--

CREATE TABLE `oc_manufacturer` (
  `manufacturer_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL,
  `noindex` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_manufacturer_description`
--

CREATE TABLE `oc_manufacturer_description` (
  `manufacturer_id` int(11) NOT NULL DEFAULT 0,
  `language_id` int(11) NOT NULL DEFAULT 0,
  `description` text NOT NULL,
  `description3` text NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_h1` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_manufacturer_to_layout`
--

CREATE TABLE `oc_manufacturer_to_layout` (
  `manufacturer_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_manufacturer_to_store`
--

CREATE TABLE `oc_manufacturer_to_store` (
  `manufacturer_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_marketing`
--

CREATE TABLE `oc_marketing` (
  `marketing_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL,
  `code` varchar(64) NOT NULL,
  `clicks` int(5) NOT NULL DEFAULT 0,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_modification`
--

CREATE TABLE `oc_modification` (
  `modification_id` int(11) NOT NULL,
  `extension_install_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `code` varchar(64) NOT NULL,
  `author` varchar(64) NOT NULL,
  `version` varchar(32) NOT NULL,
  `link` varchar(255) NOT NULL,
  `xml` mediumtext NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_modification`
--

INSERT INTO `oc_modification` (`modification_id`, `extension_install_id`, `name`, `code`, `author`, `version`, `link`, `xml`, `status`, `date_added`, `date_modified`) VALUES
(1, 1, 'SVG-support', 'SVG-support', 'Ihor Chyshkala', '0.2 Beta', '', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<modification>\n	<code>SVG-support</code>\n	<name>SVG-support</name>\n	<id>SVG-support</id>\n	<version>0.2 Beta</version>\n	<author>Ihor Chyshkala</author>\n\n	<file path=\"admin/controller/common/filemanager.php\">\n		<operation error=\"log\">\n			<search>\n				<![CDATA[jpg,jpeg,png,gif,JPG,JPEG,PNG,GIF]]>\n			</search>\n			<add position=\"replace\">\n				<![CDATA[jpg,jpeg,png,gif,JPG,JPEG,PNG,GIF,svg,SVG]]]>\n			</add>\n		</operation>\n		<operation error=\"log\">\n			<search>\n				<![CDATA[\'jpg\',]]>\n			</search>\n			<add position=\"after\">\n				<![CDATA[						\'svg\',]]>\n			</add>\n		</operation>\n				<operation error=\"log\">\n			<search>\n				<![CDATA[\'image/jpeg\',]]>\n			</search>\n			<add position=\"after\">\n				<![CDATA[						\'image/svg+xml\',]]>\n			</add>\n		</operation>\n	</file>\n	<file path=\"admin/model/tool/image.php\">\n		<operation error=\"log\">\n			<search>\n				<![CDATA[$extension = pathinfo($filename, PATHINFO_EXTENSION);]]>\n			</search>\n			<add position=\"after\">\n				<![CDATA[\n		if(\'svg\' == $extension) {\n            if ($this->request->server[\'HTTPS\']) {\n                return HTTPS_CATALOG . \'image/\' . $filename;\n            } else {\n                return HTTP_CATALOG . \'image/\' . $filename;\n            }\n    	}]]>\n			</add>\n		</operation>\n	</file>\n\n	<file path=\"catalog/model/tool/image.php\">\n		<operation error=\"log\">\n			<search>\n				<![CDATA[$extension = pathinfo($filename, PATHINFO_EXTENSION);]]>\n			</search>\n			<add position=\"after\">\n				<![CDATA[\n		if(\'svg\' == $extension) {\n            if ($this->request->server[\'HTTPS\']) {\n                return HTTPS_SERVER . \'image/\' . $filename;\n            } else {\n                return HTTP_SERVER . \'image/\' . $filename;\n            }\n    	}]]>\n			</add>\n		</operation>\n	</file>\n</modification>', 1, '2019-05-16 14:45:33', '2019-05-16 14:45:33'),
(2, 2, 'Export/Import Tool (V3.20) for OpenCart 3.x', 'Export/Import Tool (V3.20) for OpenCart 3.x', 'mhccorp.com', '3.x-3.20', 'https://www.mhccorp.com', '<modification>\n	<name>Export/Import Tool (V3.20) for OpenCart 3.x</name>\n	<code>Export/Import Tool (V3.20) for OpenCart 3.x</code>\n	<version>3.x-3.20</version>\n	<author>mhccorp.com</author>\n	<link>https://www.mhccorp.com</link>\n	<file path=\"admin/controller/common/column_left.php\">\n		<operation>\n			<search><![CDATA[if ($this->user->hasPermission(\'access\', \'tool/upload\')) {]]></search>\n			<add position=\"before\"><![CDATA[\n			if ($this->user->hasPermission(\'access\', \'extension/export_import\')) {\n				$maintenance[] = array(\n					\'name\'	   => $this->language->get(\'text_export_import\'),\n					\'href\'     => $this->url->link(\'extension/export_import\', \'user_token=\' . $this->session->data[\'user_token\'], true),\n					\'children\' => array()		\n				);\n			}\n			]]></add>\n		</operation>\n	</file>\n	<file path=\"admin/language/*/common/column_left.php\">\n		<operation>\n			<search><![CDATA[$_[\'text_backup\']]]></search>\n			<add position=\"after\"><![CDATA[\n$_[\'text_export_import\']             = \'Export / Import\';\n			]]></add>\n		</operation>\n	</file>\n</modification>\n', 1, '2019-05-16 14:45:57', '2019-05-16 14:45:57'),
(3, 3, 'LimitAutocomplete', 'Limit Autocomplete', 'RoS / XDX / Mars1an', 'v3.3', 'http://forum.opencart-russia.ru/threads/ocmod-nastrojka-limita-avtozapolnenija-cherez-adminku.2278/', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<modification>\n  <name>LimitAutocomplete</name>\n  <version>v3.3</version>\n  <author>RoS / XDX / Mars1an</author>\n  <code>Limit Autocomplete</code>\n  <link>http://forum.opencart-russia.ru/threads/ocmod-nastrojka-limita-avtozapolnenija-cherez-adminku.2278/</link>\n  \n  <file path=\"admin/controller/setting/setting.php\">\n	<operation error=\"skip\">	\n	  <search><![CDATA[\n		if (isset($this->error[\'encryption\'])) {\n	  ]]></search>\n	  <add position=\"before\" trim=\"true\"><![CDATA[\n		if (isset($this->error[\'autocomplete_limit\'])) {\n		  $data[\'error_autocomplete_limit\'] = $this->error[\'autocomplete_limit\'];\n		} else {\n		  $data[\'error_autocomplete_limit\'] = \'\';\n		}\n	  ]]></add>\n	</operation>	\n	<operation error=\"skip\">	\n	  <search><![CDATA[\n		if (isset($this->request->post[\'config_product_count\'])) {\n	  ]]></search>\n	  <add position=\"before\" trim=\"true\"><![CDATA[\n		if (isset($this->request->post[\'config_autocomplete_limit\'])) {\n		  $data[\'config_autocomplete_limit\'] = $this->request->post[\'config_autocomplete_limit\'];\n		} else {\n		  $data[\'config_autocomplete_limit\'] = $this->config->get(\'config_autocomplete_limit\');\n		}\n	  ]]></add>\n	</operation>	\n	<operation error=\"skip\">	\n	  <search><![CDATA[\n		if (!$this->request->post[\'config_limit_admin\']) {\n	  ]]></search>\n	  <add position=\"before\" trim=\"true\"><![CDATA[\n		if (!$this->request->post[\'config_autocomplete_limit\']) {\n		  $this->error[\'autocomplete_limit\'] = $this->language->get(\'error_limit\');\n		}\n	  ]]></add>\n	</operation>\n  </file> \n  <file path=\"admin/language/ru-ru/setting/setting.php\">\n	<operation error=\"skip\">\n	  <search><![CDATA[\n		$_[\'entry_limit_admin\']\n	  ]]></search>\n	  <add position=\"before\" trim=\"true\"><![CDATA[\n		$_[\'entry_autocomplete_limit\']              = \'Лимит в поле Автозаполнения (Admin)\';\n	  ]]></add>\n	</operation>	\n	<operation error=\"skip\">\n	  <search><![CDATA[\n		$_[\'help_limit_admin\']\n	  ]]></search>\n	  <add position=\"before\"><![CDATA[\n		$_[\'help_autocomplete_limit\']  = \'Определяет, сколько элементов отображать при автозаполении (в панели администрирования: товары, категории, Клиенты итд).\';\n	  ]]></add>\n	</operation>\n  </file>\n  <file path=\"admin/language/en-gb/setting/setting.php\">\n	<operation error=\"skip\">\n	  <search><![CDATA[\n		$_[\'entry_limit_admin\']\n	  ]]></search>\n	  <add position=\"before\"><![CDATA[\n		$_[\'entry_autocomplete_limit\']              = \'The limit in the autocomplete field. (Admin)\';\n	  ]]></add>\n	</operation>	\n	<operation error=\"skip\">\n	  <search><![CDATA[\n		$_[\'help_limit_admin\']\n	  ]]></search>\n	  <add position=\"before\"><![CDATA[\n		$_[\'help_autocomplete_limit\']  = \'Determines how many items to display in autocomplete. (in the admin panel: Products, Categories, Clients, etc.).\';\n	  ]]></add>\n	</operation>\n  </file>\n  <file path=\"admin/view/template/common/header.twig\">\n  <operation error=\"skip\">\n	  <search><![CDATA[\n		<link type=\"text/css\" href=\"view/stylesheet/stylesheet.css\" rel=\"stylesheet\" media=\"screen\" />\n	  ]]></search>\n	  <add position=\"after\"><![CDATA[\n		<link type=\"text/css\" href=\"view/stylesheet/autocomplete.css\" rel=\"stylesheet\" media=\"screen\" />\n	  ]]></add>\n  </operation>\n  </file>\n  <file path=\"admin/view/template/setting/setting.twig\">\n	<operation error=\"skip\">\n	  <search><![CDATA[\n		<legend>{{ text_review }}</legend>\n	  ]]></search>\n	  <add position=\"before\" offset=\"2\"><![CDATA[\n		<div class=\"form-group required\">\n		  <label class=\"col-sm-2 control-label\" for=\"input-autocomplete-limit\"><span data-toggle=\"tooltip\" title=\"{{ help_autocomplete_limit }}\">{{ entry_autocomplete_limit }}</span></label>\n		  <div class=\"col-sm-10\">\n			<input type=\"text\" name=\"config_autocomplete_limit\" value=\"{{ config_autocomplete_limit }}\" placeholder=\"{{ entry_autocomplete_limit }}\" id=\"input-autocomplete-limit\" class=\"form-control\" />\n			{% if (error_autocomplete_limit) %} \n			  <div class=\"text-danger\">{{ error_autocomplete_limit }}</div>\n			{% endif %} \n		  </div>\n		</div>\n	  ]]></add>\n	</operation>\n  </file>	\n  <file path=\"admin/controller/*/*.php\">\n	<operation error=\"skip\">\n	  <search><![CDATA[\n		\'limit\'       => 5\n	  ]]></search>\n	  <add position=\"replace\" trim=\"true\"><![CDATA[\n		\'limit\'        => $this->config->get(\'config_autocomplete_limit\')\n	  ]]></add>\n	</operation>\n	<operation error=\"skip\">\n	  <search><![CDATA[\n		$limit = 5;\n	  ]]></search>\n	  <add position=\"replace\" trim=\"true\"><![CDATA[\n		$limit = $this->config->get(\'config_autocomplete_limit\');\n	  ]]></add>\n	</operation>\n	<operation error=\"skip\">\n	  <search><![CDATA[\n		\'limit\'        => 5\n	  ]]></search>\n	  <add position=\"replace\" trim=\"true\"><![CDATA[\n		\'limit\'        => $this->config->get(\'config_autocomplete_limit\')\n	  ]]></add>\n	</operation>\n  </file>\n</modification>', 1, '2019-05-16 14:46:10', '2019-05-16 14:46:10'),
(4, 4, 'user_group_visual.ocmod', 'user_group_visual.ocmod', 'SlaSoft', '3.0', '', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<modification>\r\n  <name>user_group_visual.ocmod</name>\r\n  <code>user_group_visual.ocmod</code>\r\n  <version>3.0</version>\r\n  <author>SlaSoft</author>\r\n  <file path=\"admin/view/template/user/user_group_form.twig\">\r\n	<operation>\r\n      <search><![CDATA[{% for permission in permissions %}]]></search>\r\n      <add position=\"replace\"><![CDATA[{% for key, permission in permissions %}]]></add>\r\n    </operation>\r\n\r\n	<operation>\r\n      <search><![CDATA[<input type=\"checkbox\" name=\"permission[modify][]\"]]></search>\r\n      <add position=\"after\" offset=\"1\"><![CDATA[&nbsp;::&nbsp;<b>{{ name_permissions[key] }}</b>]]></add>\r\n    </operation>\r\n	<operation>\r\n      <search><![CDATA[<input type=\"checkbox\" name=\"permission[access][]\"]]></search>\r\n      <add position=\"after\" offset=\"1\"><![CDATA[&nbsp;::&nbsp;<b>{{ name_permissions[key] }}</b>]]></add>\r\n    </operation>\r\n  </file>\r\n  <file path=\"admin/controller/user/user_permission.php\">\r\n	<operation>\r\n      <search><![CDATA[common/startup]]></search>\r\n      <add position=\"after\"><![CDATA[\'common/column_left\',]]></add>\r\n    </operation>\r\n	<operation>\r\n      <search><![CDATA[$data[\'permissions\'][] = $permission;]]></search>\r\n      <add position=\"after\"><![CDATA[\r\n				$this->load->language($permission);\r\n				$heading_title = strip_tags($this->language->get(\'heading_title\'));\r\n				if ($heading_title == $old_heading_title) { \r\n					$heading_title = \'\';\r\n				} else {\r\n					$old_heading_title = $heading_title;\r\n				}\r\n				$data[\'name_permissions\'][] = $heading_title;\r\n]]>\r\n      </add>\r\n    </operation>\r\n\r\n	<operation>\r\n      <search><![CDATA[$data[\'permissions\'] = array();]]></search>\r\n      <add position=\"after\"><![CDATA[\r\n		$data[\'name_permissions\'] = array();\r\n		$old_heading_title = \'\';\r\n]]>\r\n      </add>\r\n    </operation>\r\n  </file>\r\n</modification>\r\n', 1, '2019-05-16 14:47:01', '2019-05-16 14:47:01'),
(7, 7, 'ExtendedSearch', 'extendedsearch', 'AlexDW', '1.04', 'https://www.opencart.com/index.php?route=marketplace/extension&filter_member=AlexDW', '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<modification>\r\n	<name>ExtendedSearch</name>\r\n	<version>1.04</version>\r\n	<author>AlexDW</author>\r\n	<link>https://www.opencart.com/index.php?route=marketplace/extension&amp;filter_member=AlexDW</link>\r\n	<code>extendedsearch</code>\r\n	<file path=\"catalog/model/catalog/product.php\">		\r\n		<operation error=\"abort\">\r\n		<search><![CDATA[\r\n$sql .= \" LEFT JOIN \" . DB_PREFIX . \"product_description pd ON (p.product_id = pd.product_id) LEFT JOIN \" . DB_PREFIX . \"product_to_store p2s\r\n			]]></search>\r\n			<add position=\"before\" ><![CDATA[\r\n// ExtendedSearch\r\n		if ((!empty($data[\'filter_name\'])) && $this->config->get(\'module_extendedsearch_status\') && $this->config->get(\'module_extendedsearch_attr\')) $sql .= \" LEFT JOIN \" . DB_PREFIX . \"product_attribute pa ON (p.product_id = pa.product_id) \";\r\n// ExtendedSearch END\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/model/catalog/product.php\">		\r\n		<operation error=\"abort\">\r\n		<search><![CDATA[\r\n$implode[] = \"pd.name LIKE \'%\" . $this->db->escape($word) . \"%\'\";\r\n			]]></search>\r\n			<add position=\"replace\" ><![CDATA[\r\n// ExtendedSearch\r\n					$adw_es = \'module_extendedsearch_\';\r\n					$es = \" (LCASE(pd.name) LIKE \'%\" . $this->db->escape(utf8_strtolower($word)) . \"%\'\";\r\n				if ($this->config->get($adw_es.\'status\') && $this->config->get($adw_es.\'model\')) $es .= \" OR LCASE(p.model) LIKE \'%\" . $this->db->escape(utf8_strtolower($word)) . \"%\'\";\r\n				if ($this->config->get($adw_es.\'status\') && $this->config->get($adw_es.\'sku\'))$es .= \" OR LCASE(p.sku) LIKE \'%\" . $this->db->escape(utf8_strtolower($word)) . \"%\'\";\r\n				if ($this->config->get($adw_es.\'status\') && $this->config->get($adw_es.\'upc\'))$es .= \" OR LCASE(p.upc) LIKE \'%\" . $this->db->escape(utf8_strtolower($word)) . \"%\'\";\r\n				if ($this->config->get($adw_es.\'status\') && $this->config->get($adw_es.\'ean\'))$es .= \" OR LCASE(p.ean) LIKE \'%\" . $this->db->escape(utf8_strtolower($word)) . \"%\'\";\r\n				if ($this->config->get($adw_es.\'status\') && $this->config->get($adw_es.\'jan\'))$es .= \" OR LCASE(p.jan) LIKE \'%\" . $this->db->escape(utf8_strtolower($word)) . \"%\'\";\r\n				if ($this->config->get($adw_es.\'status\') && $this->config->get($adw_es.\'isbn\'))$es .= \" OR LCASE(p.isbn) LIKE \'%\" . $this->db->escape(utf8_strtolower($word)) . \"%\'\";\r\n				if ($this->config->get($adw_es.\'status\') && $this->config->get($adw_es.\'mpn\'))$es .= \" OR LCASE(p.mpn) LIKE \'%\" . $this->db->escape(utf8_strtolower($word)) . \"%\'\";\r\n				if ($this->config->get($adw_es.\'status\') && $this->config->get($adw_es.\'location\'))$es .= \" OR LCASE(p.location) LIKE \'%\" . $this->db->escape(utf8_strtolower($word)) . \"%\'\";\r\n				if ($this->config->get($adw_es.\'status\') && $this->config->get($adw_es.\'attr\'))$es .= \" OR LCASE(pa.text) LIKE \'%\" . $this->db->escape(utf8_strtolower($word)) . \"%\'\";\r\n					$es .= \") \";\r\n					$implode[] = $es;\r\n// ExtendedSearch END\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/model/catalog/product.php\">		\r\n		<operation error=\"skip\">\r\n		<search><![CDATA[\r\n$sql .= \" OR LCASE(p.model) = \'\" . $this->db->escape(utf8_strtolower($data[\'filter_name\'])) . \"\'\";\r\n			]]></search>\r\n			<add position=\"replace\" ><![CDATA[\r\n// ExtendedSearch\r\n			if (!$this->config->get(\'module_extendedsearch_status\') && !$this->config->get(\'module_extendedsearch_model\')) {\r\n				$sql .= \" OR LCASE(p.model) = \'\" . $this->db->escape(utf8_strtolower($data[\'filter_name\'])) . \"\'\";\r\n			}\r\n// ExtendedSearch END\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/model/catalog/product.php\">\r\n		<operation error=\"skip\">\r\n		<search><![CDATA[\r\n$sql .= \" OR LCASE(p.sku) = \'\" . $this->db->escape(utf8_strtolower($data[\'filter_name\'])) . \"\'\";\r\n			]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n// ExtendedSearch\r\n			if (!$this->config->get(\'module_extendedsearch_status\') && !$this->config->get(\'module_extendedsearch_sku\')) {\r\n				$sql .= \" OR LCASE(p.sku) = \'\" . $this->db->escape(utf8_strtolower($data[\'filter_name\'])) . \"\'\";\r\n			}\r\n// ExtendedSearch END\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/model/catalog/product.php\">\r\n		<operation error=\"skip\">\r\n		<search><![CDATA[\r\n$sql .= \" OR LCASE(p.upc) = \'\" . $this->db->escape(utf8_strtolower($data[\'filter_name\'])) . \"\'\";\r\n			]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n// ExtendedSearch\r\n			if (!$this->config->get(\'module_extendedsearch_status\') && !$this->config->get(\'module_extendedsearch_upc\')) {\r\n				$sql .= \" OR LCASE(p.upc) = \'\" . $this->db->escape(utf8_strtolower($data[\'filter_name\'])) . \"\'\";\r\n			}\r\n// ExtendedSearch END\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/model/catalog/product.php\">\r\n		<operation error=\"skip\">\r\n		<search><![CDATA[\r\n$sql .= \" OR LCASE(p.ean) = \'\" . $this->db->escape(utf8_strtolower($data[\'filter_name\'])) . \"\'\";\r\n			]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n// ExtendedSearch\r\n			if (!$this->config->get(\'module_extendedsearch_status\') && !$this->config->get(\'module_extendedsearch_ean\')) {\r\n				$sql .= \" OR LCASE(p.ean) = \'\" . $this->db->escape(utf8_strtolower($data[\'filter_name\'])) . \"\'\";\r\n			}\r\n// ExtendedSearch END\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/model/catalog/product.php\">\r\n		<operation error=\"skip\">\r\n		<search><![CDATA[\r\n$sql .= \" OR LCASE(p.jan) = \'\" . $this->db->escape(utf8_strtolower($data[\'filter_name\'])) . \"\'\";\r\n			]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n// ExtendedSearch\r\n			if (!$this->config->get(\'module_extendedsearch_status\') && !$this->config->get(\'module_extendedsearch_jan\')) {\r\n				$sql .= \" OR LCASE(p.jan) = \'\" . $this->db->escape(utf8_strtolower($data[\'filter_name\'])) . \"\'\";\r\n			}\r\n// ExtendedSearch END\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/model/catalog/product.php\">\r\n		<operation error=\"skip\">\r\n		<search><![CDATA[\r\n$sql .= \" OR LCASE(p.isbn) = \'\" . $this->db->escape(utf8_strtolower($data[\'filter_name\'])) . \"\'\";\r\n			]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n// ExtendedSearch\r\n			if (!$this->config->get(\'module_extendedsearch_status\') && !$this->config->get(\'module_extendedsearch_isbn\')) {\r\n				$sql .= \" OR LCASE(p.isbn) = \'\" . $this->db->escape(utf8_strtolower($data[\'filter_name\'])) . \"\'\";\r\n			}\r\n// ExtendedSearch END\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/model/catalog/product.php\">\r\n		<operation error=\"skip\">\r\n		<search><![CDATA[\r\n$sql .= \" OR LCASE(p.mpn) = \'\" . $this->db->escape(utf8_strtolower($data[\'filter_name\'])) . \"\'\";\r\n			]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n// ExtendedSearch\r\n			if (!$this->config->get(\'module_extendedsearch_status\') && !$this->config->get(\'module_extendedsearch_mpn\')) {\r\n				$sql .= \" OR LCASE(p.mpn) = \'\" . $this->db->escape(utf8_strtolower($data[\'filter_name\'])) . \"\'\";\r\n			}\r\n// ExtendedSearch END\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n</modification>', 1, '2019-05-16 16:11:10', '2019-05-16 16:11:10'),
(9, 8, 'Упрощенный заказ [Custom Quick Checkout]', 'quickcheckout_oc3', 'opencart3x.ru', '1.9', 'https://opencart3x.ru', '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<modification>\n	<name>Упрощенный заказ [Custom Quick Checkout]</name>\n	<code>quickcheckout_oc3</code>\n	<version>1.9</version>\n	<author>opencart3x.ru</author>\n	<link>https://opencart3x.ru</link>\n	<file path=\"catalog/controller/checkout/cart.php\">\n		<operation>\n			<search><![CDATA[public function index() {]]></search>\n			<add position=\"after\"><![CDATA[		\n			if ($this->cart->hasProducts() && $this->config->get(\'quickcheckout_skip_cart\')){\n				$this->response->redirect($this->url->link(\'extension/quickcheckout/checkout\'));\n			}\n            ]]></add>\n		</operation>\n	</file>\n  	<file path=\"catalog/model/setting/extension.php\">\n		<operation>\n			<search trim=\"true\" index=\"0\"><![CDATA[\n				SELECT * FROM \" . DB_PREFIX . \"extension WHERE `type` =\n			]]></search>\n			<add position=\"after\" trim=\"false\" offset=\"0\"><![CDATA[\n				if ($type == \'payment\' && isset($this->session->data[\'shipping_method\'][\'code\']) && $this->config->get(\'quickcheckout_shipping_reload\')) {\n					$payments = $query->rows;\n\n					$rules = array();\n\n					foreach ($this->config->get(\'quickcheckout_payment2shipping_shippings\') as $shippings) {\n						if (strpos($this->session->data[\'shipping_method\'][\'code\'], $shippings[\'shipping\']) === 0) {\n							$rules[] = $shippings[\'payment\'];\n						}\n					}\n\n					if (!empty($rules)) {\n						foreach ($payments as $idx => $payment) {\n							if (!in_array($payment[\'code\'], $rules)) {\n								unset($payments[$idx]);\n							}\n						}\n\n						$query->rows = $payments;\n					}\n\n					//echo \'<pre>\'.__METHOD__.\' [\'.__LINE__.\']: \'; print_r($rules); echo \'</pre>\';\n					//echo \'<pre>\'.__METHOD__.\' [\'.__LINE__.\']: \'; print_r($this->session->data[\'shipping_method\']); echo \'</pre>\';\n					//echo \'<pre>\'.__METHOD__.\' [\'.__LINE__.\']: \'; print_r($query->rows); echo \'</pre>\';\n				}\n			]]></add>\n		</operation>\n	</file>\n</modification>', 1, '2019-05-22 20:18:39', '2019-05-22 20:18:39'),
(13, 13, 'Admin Detail Order Email', 'admin_detail_order_email_helpforsite.com', 'Volodymyr Chornovol', '1.0', 'https://helpforsite.com', '<modification>\n  <name>Admin Detail Order Email</name>\n  <code>admin_detail_order_email_helpforsite.com</code>\n	<version>1.0</version>\n	<link>https://helpforsite.com</link>\n	<author>Volodymyr Chornovol</author>\n	\n	<file path=\"catalog/controller/mail/order.php\">\n		<operation>\n			<search><![CDATA[public function alert(&$route, &$args) {]]></search>\n			<add position=\"after\"><![CDATA[\n        return false;\n      ]]></add>\n		</operation>\n		<operation>\n			<search index=\"0\"><![CDATA[$mail->send();]]></search>\n			<add position=\"after\"><![CDATA[\n	\n    // admin alert\n		if (in_array(\'order\', (array)$this->config->get(\'config_mail_alert\'))) {  \n      $this->load->language(\'mail/order_alert\');\n			$data[\'text_greeting\'] = $this->language->get(\'text_received\');\n			$data[\'text_footer\'] = \'\';\n    \n			\n			$mail->setTo($this->config->get(\'config_email\'));\n			$mail->setFrom($this->config->get(\'config_email\'));\n			$mail->setSender(html_entity_decode($order_info[\'store_name\'], ENT_QUOTES, \'UTF-8\'));\n			$mail->setSubject(html_entity_decode(sprintf($this->language->get(\'text_subject\'), $this->config->get(\'config_name\'), $order_info[\'order_id\']), ENT_QUOTES, \'UTF-8\'));\n			$mail->setHtml($this->load->view(\'mail/order_add\', $data));\n			$mail->send();\n			\n			// Send to additional alert emails\n			$emails = explode(\',\', $this->config->get(\'config_mail_alert_email\'));\n			\n			foreach ($emails as $email) {\n				if ($email && filter_var($email, FILTER_VALIDATE_EMAIL)) {\n					$mail->setTo($email);\n					$mail->send();\n				}\n			}\n		}\n\n			]]></add>\n		</operation>\n	</file>	\n</modification>\n\n\n', 1, '2019-05-24 17:22:29', '2019-05-24 17:22:29'),
(14, 15, 'Localcopy OCMOD Install Fix', 'localcopy-oc3', 'opencart3x.ru', '1.0', 'https://opencart3x.ru', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<modification>\n  <name>Localcopy OCMOD Install Fix</name>\n  <code>localcopy-oc3</code>\n  <version>1.0</version>\n  <author>opencart3x.ru</author>\n  <link>https://opencart3x.ru</link>\n\n  <file path=\"admin/controller/marketplace/install.php\">\n	<operation>\n      <search>\n        <![CDATA[if ($safe) {]]>\n      </search>\n      <add position=\"before\">\n        <![CDATA[		\n		    $safe = true;\n		    ]]>\n      </add>\n    </operation>\n    <operation>\n      <search>\n        <![CDATA[if (is_dir($file) && !is_dir($path)) {]]>\n      </search>\n      <add position=\"before\">\n        <![CDATA[		\n			  if ($path == \'\') {\n  				$app_root = explode(\'/\',DIR_APPLICATION);\n  				unset($app_root[count($app_root)-2]);\n  				$app_root = implode(\'/\',$app_root);\n  				$path = $app_root . $destination;\n			  }\n		    ]]>\n      </add>\n    </operation>\n  </file> \n</modification>\n', 1, '2019-08-07 15:31:32', '2019-08-07 15:31:32'),
(15, 17, 'Order Manager', 'Order-Manager', 'opencart3x.ru', '3.0', 'https://opencart3x.ru', '<modification>\r\n	<name>Order Manager</name>\r\n	<code>Order-Manager</code>\r\n	<version>3.0</version>\r\n	<author>opencart3x.ru</author>\r\n	<link>https://opencart3x.ru</link>\r\n\r\n	<file path=\"admin/controller/common/dashboard.php\">	\r\n		<operation>\r\n			<search><![CDATA[$data[\'footer\'] = $this->load->controller(\'common/footer\');]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n			\r\n			if ($this->config->get(\'module_manager_status\')) {\r\n				$data[\'manager\'] = $this->load->controller(\'dashboard/manager\');\r\n				\r\n				$data[\'module_manager_status\'] = $this->config->get(\'module_manager_status\');\r\n				$data[\'module_manager_hide_dashboard\'] = $this->config->get(\'module_manager_hide_dashboard\');\r\n\r\n				if (isset($this->session->data[\'module_manager_success\'])) {\r\n					$data[\'module_manager_success\'] = $this->session->data[\'module_manager_success\'];\r\n					unset($this->session->data[\'module_manager_success\']);\r\n				} else {\r\n					$data[\'module_manager_success\'] = \"\";\r\n				}\r\n\r\n				if (isset($this->session->data[\'module_manager_error\'])) {\r\n					$data[\'module_manager_error\'] = $this->session->data[\'module_manager_error\'];\r\n					unset($this->session->data[\'module_manager_error\']);\r\n				} else {\r\n					$data[\'module_manager_error\'] = \"\";\r\n				}\r\n			}\r\n			\r\n			]]></add>\r\n		</operation>		\r\n	</file>\r\n	\r\n	<file path=\"admin/view/template/common/dashboard.twig\">\r\n		<operation>\r\n			<search><![CDATA[{% if error_install %}]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			{% if module_manager_status  and  module_manager_status %} \r\n				{% if module_manager_success  and  module_manager_success %} \r\n  					<div class=\"alert alert-success\"><i class=\"fa fa-check-circle\"></i> {{ module_manager_success }} \r\n      					<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\r\n    				</div>\r\n  				{% endif %} \r\n  			\r\n				{% if module_manager_error  and  module_manager_error %} \r\n  					<div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> {{ module_manager_error }} \r\n      					<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\r\n    				</div>\r\n  				{% endif %} \r\n  			{% endif %} \r\n  						\r\n			]]></add>\r\n		</operation>  				\r\n		<operation>\r\n			<search><![CDATA[{% for row in rows %}]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			{% if module_manager_status  and  module_manager_status %} \r\n				{{ manager }} \r\n			{% endif %} \r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[<div class=\"row\">]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n\r\n			<div class=\"row\" {% if module_manager_hide_dashboard  and  module_manager_hide_dashboard %} {{ \' style=\"display: none;\"\' }} {% endif %} >\r\n			\r\n			]]></add>\r\n		</operation>								\r\n	</file>\r\n\r\n	<!-- Admin: Customer -->\r\n\r\n	<file path=\"admin/controller/customer/customer.php\">	\r\n		<operation>\r\n			<search><![CDATA[$this->response->redirect($this->url->link(]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			if (!empty($this->request->get[\'return\'])) {\r\n				$this->response->redirect($this->url->link(\'common/dashboard\', base64_decode($this->request->get[\'return\']), true));\r\n			}\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[$this->response->setOutput($this->load->view(\'customer/customer_form\']]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			if (!empty($this->request->get[\'return\'])) {\r\n				$url = \'user_token=\'.$this->session->data[\'user_token\'].\'&return=\'.$this->request->get[\'return\'];\r\n				\r\n				if (!isset($this->request->get[\'customer_id\'])) {\r\n					$data[\'action\'] = $this->url->link(\'customer/customer/add\', $url, true);\r\n				} else {\r\n					$data[\'action\'] = $this->url->link(\'customer/customer/edit\', $url.\'&customer_id=\'.$this->request->get[\'customer_id\'], true);\r\n				}\r\n\r\n				$data[\'cancel\'] = $this->url->link(\'common/dashboard\', base64_decode($this->request->get[\'return\']), true);\r\n			}\r\n			\r\n			]]></add>\r\n		</operation>				\r\n	</file>\r\n\r\n	<!-- Admin: Order -->\r\n\r\n	<file path=\"admin/controller/sale/order.php\">	\r\n		<operation>\r\n			<search><![CDATA[$this->response->setOutput($this->load->view(\'sale/order_form\']]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			if (!empty($this->request->get[\'return\'])) {\r\n				$data[\'cancel\'] = $this->url->link(\'common/dashboard\', base64_decode($this->request->get[\'return\']), true);\r\n			}\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[$this->response->setOutput($this->load->view(\'sale/order_info\']]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			if (!empty($this->request->get[\'return\'])) {\r\n				$data[\'cancel\'] = $this->url->link(\'common/dashboard\', base64_decode($this->request->get[\'return\']), true);\r\n			}\r\n			\r\n			]]></add>\r\n		</operation>		\r\n	</file>\r\n\r\n	<file path=\"catalog/controller/api/login.php\">	\r\n		<operation error=\"skip\">\r\n			<search><![CDATA[if (!in_array($this->request->server[\'REMOTE_ADDR\'], $ip_data)) {]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			$ip_data[] = $this->request->server[\'REMOTE_ADDR\'];\r\n			\r\n			]]></add>\r\n		</operation>		\r\n	</file>\r\n	\r\n	<file path=\"catalog/controller/api/*.php\">	\r\n		<operation error=\"skip\">\r\n			<search><![CDATA[if (!isset($this->session->data[\'api_id\'])) {]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n			\r\n			if (isset($this->session->data[\'opencart3x\'])) {\r\n			\r\n			]]></add>\r\n		</operation>		\r\n	</file>\r\n	\r\n</modification>', 1, '2019-08-07 15:32:01', '2019-08-07 15:32:01'),
(16, 19, 'OCFilter Modification', 'ocfilter-product-filter', 'prowebber.ru', '4.7.5', 'https://prowebber.ru/', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<modification>\r\n  <name>OCFilter Modification</name>\r\n  <code>ocfilter-product-filter</code>\r\n  <version>4.7.5</version>\r\n  <author>prowebber.ru</author>\r\n  <link>https://prowebber.ru/</link>\r\n\r\n  <!-- CONTROLLER -->\r\n	<file path=\"admin/controller/catalog/product.php\">\r\n    <operation error=\"skip\">\r\n      <search><![CDATA[function getForm() {]]></search>\r\n      <add position=\"after\"><![CDATA[\r\n    // OCFilter start\r\n    $this->document->addStyle(\'view/stylesheet/ocfilter/ocfilter.css\');\r\n    $this->document->addScript(\'view/javascript/ocfilter/ocfilter.js\');\r\n    // OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n    <operation error=\"skip\">\r\n      <search><![CDATA[$this->language->get(\'tab_general\');]]></search>\r\n      <add position=\"after\"><![CDATA[\r\n    // OCFilter start\r\n    $data[\'tab_ocfilter\'] = $this->language->get(\'tab_ocfilter\');\r\n    $data[\'entry_values\'] = $this->language->get(\'entry_values\');\r\n    $data[\'ocfilter_select_category\'] = $this->language->get(\'ocfilter_select_category\');\r\n    // OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n  </file>\r\n\r\n	<file path=\"admin/controller/common/column_left.php\">\r\n    <operation error=\"skip\">\r\n      <search><![CDATA[if ($this->user->hasPermission(\'access\', \'catalog/filter\')) {]]></search>\r\n      <add position=\"before\"><![CDATA[\r\n      // OCFilter start\r\n			$ocfilter = array();\r\n\r\n			if ($this->user->hasPermission(\'access\', \'extension/module/ocfilter\')) {\r\n				$ocfilter[] = array(\r\n					\'name\'     => $this->language->get(\'text_ocfilter_option\'),\r\n					\'href\'     => $this->url->link(\'extension/module/ocfilter/filter\', \'user_token=\' . $this->session->data[\'user_token\'], true),\r\n					\'children\' => array()\r\n				);\r\n			}\r\n\r\n			if ($this->user->hasPermission(\'access\', \'extension/module/ocfilter\')) {\r\n				$ocfilter[] = array(\r\n					\'name\'	   => $this->language->get(\'text_ocfilter_page\'),\r\n					\'href\'     => $this->url->link(\'extension/module/ocfilter/page\', \'user_token=\' . $this->session->data[\'user_token\'], true),\r\n					\'children\' => array()\r\n				);\r\n			}\r\n\r\n			if ($ocfilter) {\r\n				$catalog[] = array(\r\n					\'name\'	   => $this->language->get(\'text_ocfilter\'),\r\n					\'href\'     => \'\',\r\n					\'children\' => $ocfilter\r\n				);\r\n			}\r\n		  // OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n  </file><!-- /admin/controller/common/column_left.php -->\r\n  <!-- /CONTROLLER -->\r\n\r\n  <!-- LANGUAGE -->\r\n	<file path=\"admin/language/{en}*/catalog/product.php\">\r\n    <operation error=\"skip\">\r\n      <search><![CDATA[$_[\'text_success\']]]></search>\r\n      <add position=\"before\"><![CDATA[\r\n// OCFilter start\r\n$_[\'entry_values\']          		= \'Add the values ​​for this option.\';\r\n$_[\'tab_ocfilter\']          		= \'OCFilter Options\';\r\n$_[\'ocfilter_select_category\'] 	= \'To start, select a category for this product.\';\r\n// OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n  </file>\r\n\r\n	<file path=\"admin/language/{ru}*/catalog/product.php\">\r\n    <operation error=\"skip\">\r\n      <search><![CDATA[$_[\'text_success\']]]></search>\r\n      <add position=\"before\"><![CDATA[\r\n// OCFilter start\r\n$_[\'entry_values\']          		= \'Добавьте значения для этой опции.\';\r\n$_[\'tab_ocfilter\']          		= \'Опции фильтра\';\r\n$_[\'ocfilter_select_category\'] 	= \'Для начала, выберите категории для этого товара.\';\r\n// OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n  </file>\r\n\r\n	<file path=\"admin/language/{en}*/common/column_left.php\">\r\n    <operation error=\"skip\">\r\n      <search><![CDATA[$_[\'text_option\']]]></search>\r\n      <add position=\"before\"><![CDATA[\r\n// OCFilter start\r\n$_[\'text_ocfilter\']                    = \'OCFilter\';\r\n$_[\'text_ocfilter_option\']             = \'Filters\';\r\n$_[\'text_ocfilter_page\']               = \'SEO Pages\';\r\n$_[\'text_ocfilter_setting\']            = \'Settings\';\r\n// OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n  </file>\r\n\r\n	<file path=\"admin/language/{ru}*/common/column_left.php\">\r\n    <operation error=\"skip\">\r\n      <search><![CDATA[$_[\'text_option\']]]></search>\r\n      <add position=\"before\"><![CDATA[\r\n// OCFilter start\r\n$_[\'text_ocfilter\']                    = \'OCFilter\';\r\n$_[\'text_ocfilter_option\']             = \'Фильтры\';\r\n$_[\'text_ocfilter_page\']               = \'Страницы\';\r\n$_[\'text_ocfilter_setting\']            = \'Настройки\';\r\n// OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n  </file>\r\n\r\n  <!-- /LANGUAGE -->\r\n\r\n  <!-- MODEL -->\r\n	<file path=\"admin/model/catalog/product.php\">\r\n    <operation error=\"skip\">\r\n      <search index=\"0\"><![CDATA[if (isset($data[\'product_recurring\'])) {]]></search>\r\n      <add position=\"before\"><![CDATA[\r\n    // OCFilter start\r\n		if (isset($data[\'ocfilter_product_option\'])) {\r\n			foreach ($data[\'ocfilter_product_option\'] as $option_id => $values) {\r\n				foreach ($values[\'values\'] as $value_id => $value) {\r\n					if (isset($value[\'selected\'])) {\r\n						$this->db->query(\"INSERT INTO \" . DB_PREFIX . \"ocfilter_option_value_to_product SET product_id = \'\" . (int)$product_id . \"\', option_id = \'\" . (int)$option_id . \"\', value_id = \'\" . (string)$value_id . \"\', slide_value_min = \'\" . (isset($value[\'slide_value_min\']) ? (float)$value[\'slide_value_min\'] : 0) . \"\', slide_value_max = \'\" . (isset($value[\'slide_value_max\']) ? (float)$value[\'slide_value_max\'] : 0) . \"\'\");\r\n					}\r\n				}\r\n			}\r\n		}\r\n		// OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n    <operation error=\"skip\">\r\n      <search index=\"1\"><![CDATA[if (isset($data[\'product_recurring\'])) {]]></search>\r\n      <add position=\"before\"><![CDATA[\r\n    // OCFilter start\r\n    $this->db->query(\"DELETE FROM \" . DB_PREFIX . \"ocfilter_option_value_to_product WHERE product_id = \'\" . (int)$product_id . \"\'\");\r\n\r\n		if (isset($data[\'ocfilter_product_option\'])) {\r\n			foreach ($data[\'ocfilter_product_option\'] as $option_id => $values) {\r\n				foreach ($values[\'values\'] as $value_id => $value) {\r\n					if (isset($value[\'selected\'])) {\r\n						$this->db->query(\"INSERT INTO \" . DB_PREFIX . \"ocfilter_option_value_to_product SET product_id = \'\" . (int)$product_id . \"\', option_id = \'\" . (int)$option_id . \"\', value_id = \'\" . (string)$value_id . \"\', slide_value_min = \'\" . (isset($value[\'slide_value_min\']) ? (float)$value[\'slide_value_min\'] : 0) . \"\', slide_value_max = \'\" . (isset($value[\'slide_value_max\']) ? (float)$value[\'slide_value_max\'] : 0) . \"\'\");\r\n					}\r\n				}\r\n			}\r\n		}\r\n		// OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n    <operation error=\"skip\">\r\n      <search><![CDATA[$data[\'product_attribute\'] = $this->getProductAttributes($product_id);]]></search>\r\n      <add position=\"after\"><![CDATA[\r\n 		// OCFilter start\r\n		$this->load->model(\'extension/ocfilter\');\r\n\r\n		$data[\'ocfilter_product_option\'] = $this->model_extension_ocfilter->getProductOCFilterValues($product_id);\r\n		// OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n    <operation error=\"skip\">\r\n      <search><![CDATA[$this->db->query(\"DELETE FROM \" . DB_PREFIX . \"product WHERE product_id = \'\" . (int)$product_id . \"\'\");]]></search>\r\n      <add position=\"after\"><![CDATA[\r\n		// OCFilter start\r\n		$this->db->query(\"DELETE FROM \" . DB_PREFIX . \"ocfilter_option_value_to_product WHERE product_id = \'\" . (int)$product_id . \"\'\");\r\n		// OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n  </file><!-- /admin/model/catalog/product.php -->\r\n\r\n  <!-- /MODEL -->\r\n\r\n  <!-- VIEW -->\r\n	<file path=\"admin/view/template/catalog/product_form.twig\">\r\n    <operation error=\"skip\">\r\n      <search index=\"0\"><![CDATA[</script></div>]]></search>\r\n      <add position=\"replace\"><![CDATA[\r\n  </script>\r\n  <!-- OCFilter start -->\r\n  <script type=\"text/javascript\"><!--\r\n  ocfilter.php = {\r\n  	text_select: \'{{ text_select }}\',\r\n  	ocfilter_select_category: \'{{ ocfilter_select_category }}\',\r\n  	entry_values: \'{{ entry_values }}\',\r\n  	tab_ocfilter: \'{{ tab_ocfilter }}\'\r\n  };\r\n\r\n  ocfilter.php.languages = [];\r\n\r\n  {% for language in languages %}\r\n  ocfilter.php.languages.push({\r\n  	\'language_id\': {{ language.language_id }},\r\n  	\'name\': \'{{ language.name }}\',\r\n    \'image\': \'{{ language.image }}\'\r\n  });\r\n  {% endfor %}\r\n  //--></script>\r\n  <!-- OCFilter end -->\r\n  </div>\r\n      ]]></add>\r\n    </operation>\r\n  </file><!-- /admin/view/template/catalog/product_form.twig -->\r\n  <!-- /VIEW -->\r\n\r\n  <!-- CATALOG -->\r\n\r\n  <file path=\"catalog/controller/startup/startup.php\">\r\n    <operation error=\"skip\">\r\n      <search><![CDATA[Cart($this->registry));]]></search>\r\n      <add position=\"after\"><![CDATA[\r\n		// OCFilter\r\n		$this->registry->set(\'ocfilter\', new OCFilter($this->registry));\r\n      ]]></add>\r\n    </operation>\r\n  </file>\r\n\r\n	<file path=\"catalog/controller/startup/seo_url.php\">\r\n    <operation error=\"skip\">\r\n      <search><![CDATA[$this->url->addRewrite($this);]]></search>\r\n      <add position=\"after\"><![CDATA[\r\n      // OCFilter start\r\n      if ($this->registry->has(\'ocfilter\')) {\r\n  			$this->url->addRewrite($this->ocfilter);\r\n  		}\r\n      // OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n  </file>\r\n\r\n	<file path=\"catalog/controller/{common,startup}/seo_pro.php\">\r\n    <operation error=\"skip\">\r\n      <search><![CDATA[$this->url->addRewrite($this);]]></search>\r\n      <add position=\"after\"><![CDATA[\r\n      // OCFilter start\r\n      if ($this->registry->has(\'ocfilter\')) {\r\n  			$this->url->addRewrite($this->ocfilter);\r\n  		}\r\n      // OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n  </file>\r\n\r\n	<file path=\"catalog/controller/{common,startup}/seo_pro.php\">\r\n    <operation error=\"skip\">\r\n      <search><![CDATA[$this->url->addRewrite($this, $lang_data);]]></search>\r\n      <add position=\"after\"><![CDATA[\r\n      // OCFilter start\r\n      if ($this->registry->has(\'ocfilter\')) {\r\n  			$this->url->addRewrite($this->ocfilter);\r\n  		}\r\n      // OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n  </file>\r\n\r\n	<file path=\"catalog/model/catalog/product.php\">\r\n    <operation error=\"abort\">\r\n      <search><![CDATA[$sql .= \" LEFT JOIN \" . DB_PREFIX . \"product_description]]></search>\r\n      <add position=\"before\"><![CDATA[\r\n		// OCFilter start\r\n		if (!empty($data[\'filter_ocfilter\'])) {\r\n    	$this->load->model(\'extension/module/ocfilter\');\r\n\r\n      $ocfilter_product_sql = $this->model_extension_module_ocfilter->getSearchSQL($data[\'filter_ocfilter\']);\r\n		} else {\r\n      $ocfilter_product_sql = false;\r\n    }\r\n\r\n    if ($ocfilter_product_sql && $ocfilter_product_sql->join) {\r\n    	$sql .= $ocfilter_product_sql->join;\r\n    }\r\n    // OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n\r\n    <operation error=\"abort\">\r\n      <search><![CDATA[if (!empty($data[\'filter_manufacturer_id]]></search>\r\n      <add position=\"before\"><![CDATA[\r\n    // OCFilter start\r\n    if (!empty($ocfilter_product_sql) && $ocfilter_product_sql->where) {\r\n    	$sql .= $ocfilter_product_sql->where;\r\n    }\r\n    // OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n  </file>\r\n\r\n	<file path=\"catalog/controller/product/category.php\">\r\n    <operation error=\"abort\">\r\n      <search index=\"0\"><![CDATA[$data[\'breadcrumbs\'] = array();]]></search>\r\n      <add position=\"before\"><![CDATA[\r\n		// OCFilter start\r\n    if (isset($this->request->get[\'filter_ocfilter\'])) {\r\n      $filter_ocfilter = $this->request->get[\'filter_ocfilter\'];\r\n    } else {\r\n      $filter_ocfilter = \'\';\r\n    }\r\n		// OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n\r\n    <!-- Filter params to product model -->\r\n\r\n    <operation error=\"abort\">\r\n      <search><![CDATA[$product_total =]]></search>\r\n      <add position=\"before\"><![CDATA[\r\n  		// OCFilter start\r\n  		$filter_data[\'filter_ocfilter\'] = $filter_ocfilter;\r\n  		// OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n\r\n    <!-- Add url -->\r\n\r\n    <operation error=\"skip\">\r\n      <search index=\"2\"><![CDATA[if (isset($this->request->get[\'filter\'])) {]]></search>\r\n      <add position=\"before\"><![CDATA[\r\n      // OCFilter start\r\n			if (isset($this->request->get[\'filter_ocfilter\'])) {\r\n				$url .= \'&filter_ocfilter=\' . $this->request->get[\'filter_ocfilter\'];\r\n			}\r\n      // OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n\r\n    <operation error=\"skip\">\r\n      <search index=\"3\"><![CDATA[if (isset($this->request->get[\'filter\'])) {]]></search>\r\n      <add position=\"before\"><![CDATA[\r\n      // OCFilter start\r\n			if (isset($this->request->get[\'filter_ocfilter\'])) {\r\n				$url .= \'&filter_ocfilter=\' . $this->request->get[\'filter_ocfilter\'];\r\n			}\r\n      // OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n\r\n    <operation error=\"skip\">\r\n      <search index=\"4\"><![CDATA[if (isset($this->request->get[\'filter\'])) {]]></search>\r\n      <add position=\"before\"><![CDATA[\r\n      // OCFilter start\r\n			if (isset($this->request->get[\'filter_ocfilter\'])) {\r\n				$url .= \'&filter_ocfilter=\' . $this->request->get[\'filter_ocfilter\'];\r\n			}\r\n      // OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n\r\n    <operation error=\"skip\">\r\n      <search limit=\"1\"><![CDATA[$data[\'limit\'] = $limit;]]></search>\r\n      <add position=\"after\"><![CDATA[\r\n      // OCFilter Start\r\n      if ($this->ocfilter->getParams()) {\r\n        if (isset($product_total) && !$product_total) {\r\n      	  $this->response->redirect($this->url->link(\'product/category\', \'path=\' . $this->request->get[\'path\']));\r\n        }\r\n\r\n        $this->document->setTitle($this->ocfilter->getPageMetaTitle($this->document->getTitle()));\r\n			  $this->document->setDescription($this->ocfilter->getPageMetaDescription($this->document->getDescription()));\r\n        $this->document->setKeywords($this->ocfilter->getPageMetaKeywords($this->document->getKeywords()));\r\n\r\n        $data[\'heading_title\'] = $this->ocfilter->getPageHeadingTitle($data[\'heading_title\']);\r\n        $data[\'description\'] = $this->ocfilter->getPageDescription();\r\n\r\n        if (!trim(strip_tags(html_entity_decode($data[\'description\'], ENT_QUOTES, \'UTF-8\')))) {\r\n        	$data[\'thumb\'] = \'\';\r\n        }\r\n\r\n        $breadcrumb = $this->ocfilter->getPageBreadCrumb();\r\n\r\n        if ($breadcrumb) {\r\n  			  $data[\'breadcrumbs\'][] = $breadcrumb;\r\n        }\r\n\r\n        $this->document->deleteLink(\'canonical\');\r\n      }\r\n      // OCFilter End\r\n      ]]></add>\r\n    </operation>\r\n  </file><!-- /catalog/controller/product/category.php -->\r\n\r\n  <!-- Document Noindex & Canonical -->\r\n\r\n	<file path=\"system/library/document.php\">\r\n    <operation error=\"abort\">\r\n      <search><![CDATA[public function getLinks]]></search>\r\n      <add position=\"before\"><![CDATA[\r\n  // OCFilter canonical fix start\r\n	public function deleteLink($rel) {\r\n    foreach ($this->links as $href => $link) {\r\n      if ($link[\'rel\'] == $rel) {\r\n      	unset($this->links[$href]);\r\n      }\r\n    }\r\n	}\r\n  // OCFilter canonical fix end\r\n      ]]></add>\r\n    </operation>\r\n\r\n    <operation>\r\n      <search><![CDATA[private $keywords;]]></search>\r\n      <add position=\"after\"><![CDATA[\r\n  // OCFilter start\r\n  private $noindex = false;\r\n  // OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n\r\n    <operation>\r\n      <search><![CDATA[public function setTitle]]></search>\r\n      <add position=\"before\"><![CDATA[\r\n  // OCFilter start\r\n  public function setNoindex($state = false) {\r\n  	$this->noindex = $state;\r\n  }\r\n\r\n	public function isNoindex() {\r\n		return $this->noindex;\r\n	}\r\n  // OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n  </file>\r\n\r\n	<file path=\"catalog/controller/common/header.php\">\r\n    <operation error=\"abort\">\r\n      <search><![CDATA[$data[\'scripts\'] = $this->document->getScripts]]></search>\r\n      <add position=\"before\"><![CDATA[\r\n    // OCFilter start\r\n    $data[\'noindex\'] = $this->document->isNoindex();\r\n    // OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n  </file>\r\n\r\n  <file path=\"catalog/view/theme/*/template/common/header.twig\">\r\n    <operation error=\"skip\">\r\n      <search><![CDATA[</title>]]></search>\r\n      <add position=\"after\"><![CDATA[\r\n{% if noindex %}\r\n<!-- OCFilter Start -->\r\n<meta name=\"robots\" content=\"noindex,nofollow\" />\r\n<!-- OCFilter End -->\r\n{% endif %}\r\n      ]]></add>\r\n    </operation>\r\n  </file>\r\n</modification>', 1, '2020-05-20 04:18:57', '2020-05-20 04:18:57');
INSERT INTO `oc_modification` (`modification_id`, `extension_install_id`, `name`, `code`, `author`, `version`, `link`, `xml`, `status`, `date_added`, `date_modified`) VALUES
(17, 20, 'GC Sessions Basic', '6629190803', 'alex cherednichenko', '3.x', 'https://www.alcher.info', '﻿<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<modification>\n	<name>GC Sessions Basic</name>\n	<code>6629190803</code>\n	<version>3.x</version>\n	<author>alex cherednichenko</author>\n	<link>https://www.alcher.info</link>\n\n	<file path=\"system/library/session.php\">\n		<operation>\n			<search>\n				<![CDATA[\n	public function close() {\n				]]>\n			</search>\n			<add position=\"replace\" offset=\"2\">\n				<![CDATA[\n	public function close() {\n		$this->adaptor->close($this->session_id, $this->data);\n	}\n				]]>\n			</add>\n		</operation>\n	</file>\n\n	<file path=\"system/library/session/db.php\">\n		<operation>\n			<search>\n				<![CDATA[\n		$query = $this->db->query(\"SELECT `data` FROM `\" . DB_PREFIX . \"session` WHERE session_id = \'\" . $this->db->escape($session_id) . \"\' AND expire > \" . (int)time());\n				]]>\n			</search>\n			<add position=\"replace\">\n				<![CDATA[\n		$query = $this->db->query(\"SELECT `data` FROM `\" . DB_PREFIX . \"session` WHERE session_id = \'\" . $this->db->escape($session_id) . \"\' AND expire > DATE_SUB(NOW(), INTERVAL \" . $this->expire . \" SECOND);\");\n				]]>\n			</add>\n		</operation>\n\n		<operation>\n			<search>\n				<![CDATA[\n		public function gc($expire) {\n				]]>\n			</search>\n			<add position=\"before\">\n				<![CDATA[\n		public function close( $session_id, $data ) {\n			$this->write($session_id, $data);\n			$this->gc($this->expire);\n		}\n				]]>\n			</add>\n		</operation>\n\n		<operation>\n			<search>\n				<![CDATA[\n		$this->db->query(\"DELETE FROM `\" . DB_PREFIX . \"session` WHERE expire < \" . ((int)time() + $expire));\n				]]>\n			</search>\n			<add position=\"replace\">\n				<![CDATA[\n		$this->db->query(\"DELETE FROM `\" . DB_PREFIX . \"session` WHERE expire < DATE_SUB(NOW(), INTERVAL \" . ((int)$expire) . \" SECOND);\");\n				]]>\n			</add>\n		</operation>\n\n	</file>\n\n<!-- InnoDB -->\n<!--\nCREATE TABLE `oc_session` (\n  `session_id` varchar(32) NOT NULL,\n  `data` text NOT NULL,\n  `ip_address` varchar(255) DEFAULT NULL,\n  `user_agent` tinytext,\n  `expire` datetime NOT NULL,\n  PRIMARY KEY (`session_id`)\n) ENGINE=InnoDB DEFAULT CHARSET=utf8\n-->\n<!--\n	<file path=\"system/library/session/db.php\">\n		<operation>\n			<search position=\"before\">\n				<![CDATA[\n		public function gc($expire) {\n				]]>\n			</search>\n			<add>\n				<![CDATA[\n		public function close( $session_id, $data ) {\n			$this->write($session_id, $data);\n			$this->gc($this->expire);\n		}\n				]]>\n			</add>\n		</operation>\n\n		<operation>\n			<search position=\"replace\">\n				<![CDATA[\n		$query = $this->db->query(\"SELECT `data` FROM `\" . DB_PREFIX . \"session` WHERE session_id = \'\" . $this->db->escape($session_id) . \"\' AND expire > DATE_SUB(NOW(), INTERVAL \" . $this->expire . \" SECOND);\");\n				]]>\n			</search>\n			<add>\n				<![CDATA[\n		$this->db->query(\"START TRANSACTION READ ONLY\");\n		$query = $this->db->query(\"SELECT `data` FROM `\" . DB_PREFIX . \"session` WHERE session_id = \'\" . $this->db->escape($session_id) . \"\' AND expire > \" . (int)time());\n		$this->db->query(\"COMMIT\");\n				]]>\n			</add>\n		</operation>\n\n		<operation>\n			<search position=\"replace\">\n				<![CDATA[\n		$this->db->query(\"REPLACE INTO `\" . DB_PREFIX . \"session` SET session_id = \'\" . $this->db->escape($session_id) . \"\', `data` = \'\" . $this->db->escape(json_encode($data)) . \"\', expire = \'\" . $this->db->escape(date(\'Y-m-d H:i:s\', time() + $this->expire)) . \"\'\");\n				]]>\n			</search>\n			<add>\n				<![CDATA[\n		$this->db->query(\"START TRANSACTION\");\n		$this->db->query(\"REPLACE INTO `\" . DB_PREFIX . \"session` SET session_id = \'\" . $this->db->escape($session_id) . \"\', `data` = \'\" . $this->db->escape(json_encode($data)) . \"\', user_agent = \'\" . $this->db->escape($_SERVER[\'HTTP_USER_AGENT\']) . \"\', ip_address = \'\".  $this->db->escape($_SERVER[\'REMOTE_ADDR\']) . \"\', expire = \'\" . $this->db->escape(date(\'Y-m-d H:i:s\', time() + $this->expire)) . \"\'\");\n		$this->db->query(\"COMMIT\");\n				]]>\n			</add>\n		</operation>\n\n		<operation>\n			<search position=\"replace\">\n				<![CDATA[\n		$this->db->query(\"DELETE FROM `\" . DB_PREFIX . \"session` WHERE session_id = \'\" . $this->db->escape($session_id) . \"\'\");\n				]]>\n			</search>\n			<add>\n				<![CDATA[\n		$this->db->query(\"START TRANSACTION\");\n		$this->db->query(\"DELETE FROM `\" . DB_PREFIX . \"session` WHERE session_id = \'\" . $this->db->escape($session_id) . \"\'\");\n		$this->db->query(\"COMMIT\");\n				]]>\n			</add>\n		</operation>\n\n		<operation>\n			<search position=\"replace\">\n				<![CDATA[\n		$this->db->query(\"DELETE FROM `\" . DB_PREFIX . \"session` WHERE expire < \" . ((int)time() + $expire));\n				]]>\n			</search>\n			<add>\n				<![CDATA[\n		$this->db->query(\"START TRANSACTION\");\n		$this->db->query(\"DELETE FROM `\" . DB_PREFIX . \"session` WHERE expire < DATE_SUB(NOW(), INTERVAL \" . $expire . \" SECOND);\"); // + 86400\n		$this->db->query(\"COMMIT\");\n				]]>\n			</add>\n		</operation>\n\n	</file>\n-->\n</modification>', 1, '2020-05-20 04:19:03', '2020-05-20 04:19:03'),
(18, 21, 'OpenCart 3.0 Compatibility Fixes', 'compatibility_fixes', 'The Krotek', '3.1.0', 'https://thekrotek.com', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<modification>\r\n	<name>OpenCart 3.0 Compatibility Fixes</name>\r\n	<code>compatibility_fixes</code>\r\n	<version>3.1.0</version>\r\n	<author>The Krotek</author>\r\n    <link>https://thekrotek.com</link>\r\n 			\r\n	<!-- System: Loader -->\r\n\r\n	<file path=\"system/engine/loader.php\">\r\n		<operation>\r\n			<search><![CDATA[if (!$this->registry->has(\'model_\']]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			$this->registry->get(\'event\')->trigger(\'model/\'.$route.\'/before\', array(&$route));\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[throw new \\Exception(\'Error: Could not load model \']]></search>\r\n			<add position=\"after\" offset=\"1\"><![CDATA[\r\n			\r\n			$this->registry->get(\'event\')->trigger(\'model/\'.$route.\'/after\', array(&$route));\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[include_once($file);]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n			\r\n			include_once(modification($file));\r\n			\r\n			]]></add>\r\n		</operation>		\r\n	</file>\r\n	\r\n	<!-- System: Language -->\r\n\r\n	<file path=\"system/library/language.php\">\r\n		<operation>\r\n			<search><![CDATA[private $default = \'en-gb\';]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n			\r\n			public $default = \'en-gb\';\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[public function load(]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n	\r\n			public function load($filename, $key = \'\')\r\n			{\r\n				if (!$key) {\r\n					$_ = array();\r\n\r\n					$file = DIR_LANGUAGE.\'english/\'.$filename.\'.php\';\r\n					$old_file = DIR_LANGUAGE.\'english/\'.str_replace(\'extension/\', \'\', $filename).\'.php\';\r\n		\r\n					if (is_file($file)) require(modification($file));\r\n					elseif (is_file($old_file)) require(modification($old_file));\r\n			\r\n					$file = DIR_LANGUAGE.$this->default.\'/\'.$filename.\'.php\';\r\n					$old_file = DIR_LANGUAGE.$this->default.\'/\'.str_replace(\'extension/\', \'\', $filename).\'.php\';\r\n		\r\n					if (is_file($file)) require(modification($file));\r\n					elseif (is_file($old_file)) require(modification($old_file));\r\n								\r\n					$file = DIR_LANGUAGE.$this->directory.\'/\'.$filename.\'.php\';\r\n					$old_file = DIR_LANGUAGE.$this->directory.\'/\'.str_replace(\'extension/\', \'\', $filename).\'.php\';\r\n		\r\n					if (is_file($file)) require(modification($file));\r\n					elseif (is_file($old_file)) require(modification($old_file));\r\n								\r\n					$this->data = array_merge($this->data, $_);\r\n				} else {\r\n					$this->data[$key] = new Language($this->directory);\r\n					$this->data[$key]->load($filename);\r\n				}\r\n		\r\n				return $this->data;\r\n			}\r\n				\r\n			public function loadDefault(]]></add>\r\n		</operation>		\r\n	</file>\r\n	\r\n	<!-- System: Config -->\r\n	\r\n	<file path=\"system/config/admin.php\">\r\n		<operation>\r\n			<search><![CDATA[$_[\'action_event\'] = array(]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n			\r\n			\'model/*/before\' => array(\r\n				\'event/compatibility/beforemodel\'\r\n			),\r\n			\r\n			\'model/*/after\' => array(\r\n				\'event/compatibility/aftermodel\'\r\n			),\r\n	\r\n			\'language/*/after\' => array(\r\n				\'event/compatibility/language\',\r\n				\'event/translation\'\r\n			),\r\n				\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[\'controller/*/before\' => array(]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n			\r\n			\'event/compatibility/controller\',\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[\'controller/*/after\' => array(]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n\r\n			\'event/compatibility/controller\',\r\n			\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n	\r\n	<file path=\"system/config/catalog.php\">\r\n		<operation>\r\n			<search><![CDATA[$_[\'action_event\'] = array(]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n			\r\n			\'model/*/before\' => array(\r\n				\'event/compatibility/beforemodel\'\r\n			),\r\n			\r\n			\'model/*/after\' => array(\r\n				\'event/compatibility/aftermodel\'\r\n			),\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[\'controller/*/before\' => array(]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n			\r\n			\'event/compatibility/controller\',\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[\'controller/*/after\' => array(]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n\r\n			\'event/compatibility/controller\',\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[\'language/*/after\' => array(]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n						\r\n			\'event/compatibility/language\',\r\n					\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n	   \r\n	<!-- Admin: Extensions -->\r\n\r\n	<file path=\"admin/controller/extension/extension/{analytics,captcha,dashboard,feed,fraud,menu,module,payment,report,shipping,theme,total}*.php\">\r\n		<operation>\r\n			<search><![CDATA[$this->load->controller(\'extension]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			if (!empty($this->request->get[\'extension\'])) {\r\n				$type = strtolower(str_replace(\'ControllerExtensionExtension\', \'\', __CLASS__));\r\n\r\n				if (__FUNCTION__ == \'install\') {\r\n					$this->model_user_user_group->addPermission($this->user->getGroupId(), \'access\', $type.\'/\'.$this->request->get[\'extension\']);\r\n					$this->model_user_user_group->addPermission($this->user->getGroupId(), \'modify\', $type.\'/\'.$this->request->get[\'extension\']);\r\n				}\r\n\r\n				$this->load->controller($type.\'/\'.$this->request->get[\'extension\'].\'/\'.__FUNCTION__);\r\n			}\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[$this->response->setOutput($this->load->view(\'extension/extension]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			if (!empty($data[\'extensions\'])) {\r\n				$data[\'extensions\'] = array_unique($data[\'extensions\'], SORT_REGULAR);\r\n			\r\n				usort($data[\'extensions\'], function($a, $b){ return strcmp($a[\"name\"], $b[\"name\"]); });\r\n			}\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[if ($files) {]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			$type = strtolower(str_replace(\'ControllerExtensionExtension\', \'\', __CLASS__));\r\n			\r\n			$files = glob(DIR_APPLICATION . \'controller/{extension/\'.$type.\',\'.$type.\'}/*.php\', GLOB_BRACE);\r\n			\r\n			]]></add>\r\n		</operation>		\r\n	</file>\r\n	\r\n	<!-- Admin: User Groups -->\r\n\r\n	<file path=\"admin/controller/user/user_permission.php\">\r\n		<operation>\r\n			<search><![CDATA[ControllerUserUserPermission extends Controller {]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n			\r\n			private function checkPermissions(&$permissions)\r\n			{\r\n				$folders = array(\'analytics\', \'captcha\', \'dashboard\', \'feed\', \'fraud\', \'module\', \'payment\', \'shipping\', \'theme\', \'total\');\r\n				\r\n				foreach ($permissions as $type => $extensions) {\r\n					foreach ($extensions as $route) {\r\n						$route = explode(\'/\', $route);\r\n						\r\n						if (($route[0] != \'extension\') && in_array($route[0], $folders) && is_file(DIR_APPLICATION.\'controller/\'.$route[0].\'/\'.$route[1].\'.php\')) {\r\n							$permissions[$type][] = \'extension/\'.$route[0].\'/\'.$route[1];\r\n						}\r\n					}\r\n					\r\n					sort($permissions[$type]);\r\n				}\r\n			}\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[$this->model_user_user_group->addUserGroup(]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			if (isset($this->request->post[\'permission\'])) {\r\n				$this->checkPermissions($this->request->post[\'permission\']);\r\n			}\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[$this->model_user_user_group->editUserGroup(]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			if (isset($this->request->post[\'permission\'])) {\r\n				$this->checkPermissions($this->request->post[\'permission\']);\r\n			}\r\n			\r\n			]]></add>\r\n		</operation>		\r\n	</file>\r\n		\r\n	<!-- Admin: Menu -->\r\n\r\n	<file path=\"admin/controller/common/column_left.php\">\r\n		<operation>\r\n			<search><![CDATA[if ($this->user->hasPermission(\'access\', \'marketplace/extension\')) {]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n			\r\n			$this->load->language(\'marketplace/marketplace\');			\r\n			\r\n			$this->load->language(\'extension/extension/analytics\');\r\n			\r\n			$extension[] = array(\r\n				\'name\'	   => $this->language->get(\'heading_title\'),\r\n				\'href\'     => $this->url->link(\'marketplace/extension\', \'user_token=\'.$this->session->data[\'user_token\'].\'&type=analytics\', true),\r\n				\'children\' => array());\r\n			\r\n			$this->load->language(\'extension/extension/captcha\');\r\n					\r\n			$extension[] = array(\r\n				\'name\'	   => $this->language->get(\'heading_title\'),\r\n				\'href\'     => $this->url->link(\'marketplace/extension\', \'user_token=\'.$this->session->data[\'user_token\'].\'&type=captcha\', true),\r\n				\'children\' => array());\r\n\r\n			$this->load->language(\'extension/extension/dashboard\');\r\n			\r\n			$extension[] = array(\r\n				\'name\'	   => $this->language->get(\'text_dashboard\'),\r\n				\'href\'     => $this->url->link(\'marketplace/extension\', \'user_token=\'.$this->session->data[\'user_token\'].\'&type=dashboard\', true),\r\n				\'children\' => array());\r\n				\r\n			$this->load->language(\'extension/extension/feed\');\r\n					\r\n			$extension[] = array(\r\n				\'name\'	   => $this->language->get(\'text_feed\'),\r\n				\'href\'     => $this->url->link(\'marketplace/extension\', \'user_token=\'.$this->session->data[\'user_token\'].\'&type=feed\', true),\r\n				\'children\' => array());\r\n			\r\n			$this->load->language(\'extension/extension/fraud\');\r\n			\r\n			$extension[] = array(\r\n				\'name\'	   => $this->language->get(\'heading_title\'),\r\n				\'href\'     => $this->url->link(\'marketplace/extension\', \'user_token=\'.$this->session->data[\'user_token\'].\'&type=fraud\', true),\r\n				\'children\' => array());\r\n			\r\n			$this->load->language(\'extension/extension/menu\');\r\n			\r\n			$extension[] = array(\r\n				\'name\'	   => $this->language->get(\'text_menu\'),\r\n				\'href\'     => $this->url->link(\'marketplace/extension\', \'user_token=\'.$this->session->data[\'user_token\'].\'&type=menu\', true),\r\n				\'children\' => array());\r\n				\r\n			$this->load->language(\'extension/extension/module\');\r\n										\r\n			$extension[] = array(\r\n				\'name\'	   => $this->language->get(\'text_module\'),\r\n				\'href\'     => $this->url->link(\'marketplace/extension\', \'user_token=\'.$this->session->data[\'user_token\'].\'&type=module\', true),\r\n				\'children\' => array());\r\n			\r\n			$this->load->language(\'extension/extension/payment\');\r\n					\r\n			$extension[] = array(\r\n				\'name\'	   => $this->language->get(\'text_payment\'),\r\n				\'href\'     => $this->url->link(\'marketplace/extension\', \'user_token=\'.$this->session->data[\'user_token\'].\'&type=payment\', true),\r\n				\'children\' => array());\r\n			\r\n			$this->load->language(\'extension/extension/report\');\r\n			\r\n			$extension[] = array(\r\n				\'name\'	   => $this->language->get(\'text_report\'),\r\n				\'href\'     => $this->url->link(\'marketplace/extension\', \'user_token=\'.$this->session->data[\'user_token\'].\'&type=report\', true),\r\n				\'children\' => array());\r\n				\r\n			$this->load->language(\'extension/extension/shipping\');\r\n										\r\n			$extension[] = array(\r\n				\'name\'	   => $this->language->get(\'text_shipping\'),\r\n				\'href\'     => $this->url->link(\'marketplace/extension\', \'user_token=\'.$this->session->data[\'user_token\'].\'&type=shipping\', true),\r\n				\'children\' => array());\r\n				\r\n			$this->load->language(\'extension/extension/theme\');\r\n					\r\n			$extension[] = array(\r\n				\'name\'	   => $this->language->get(\'text_theme\'),\r\n				\'href\'     => $this->url->link(\'marketplace/extension\', \'user_token=\'.$this->session->data[\'user_token\'].\'&type=theme\', true),\r\n				\'children\' => array());\r\n			\r\n			$this->load->language(\'extension/extension/total\');\r\n					\r\n			$extension[] = array(\r\n				\'name\'	   => $this->language->get(\'text_total\'),\r\n				\'href\'     => $this->url->link(\'marketplace/extension\', \'user_token=\'.$this->session->data[\'user_token\'].\'&type=total\', true),\r\n				\'children\' => array());\r\n			\r\n			$this->load->language(\'common/column_left\');\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[if ($this->user->hasPermission(\'access\', \'marketplace/modification\')) {]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			if (!empty($extension)) {\r\n				$sorted = array_values($marketplace);\r\n				$element = end($sorted);\r\n			\r\n				$element[\'href\'] = \'\';\r\n				$element[\'children\'] = $extension;\r\n				\r\n				$marketplace[count($marketplace) - 1] = $element;\r\n			}\r\n			\r\n			]]></add>\r\n		</operation>		\r\n	</file>\r\n	\r\n	<!-- Catalog: Checkout -->\r\n	\r\n	<file path=\"catalog/controller/checkout/cart.php\">\r\n		<operation>\r\n			<search><![CDATA[if ($files) {]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			$files = array_merge($files, glob(DIR_APPLICATION.\'controller/total/*.php\'));\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[if ($files) {]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n			\r\n			$modules = array();\r\n			\r\n			]]></add>\r\n		</operation>			\r\n		<operation>\r\n			<search><![CDATA[if ($result) {]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n				$modules[] = basename($file, \'.php\');\r\n			}\r\n\r\n			foreach (array_unique($modules) as $module) {\r\n				$result = $this->load->controller(\'extension/total/\'.$module);\r\n			\r\n			]]></add>\r\n		</operation>		\r\n	</file>\r\n				\r\n</modification>', 1, '2020-05-20 04:19:08', '2020-05-20 04:19:08'),
(19, 22, 'Modification Manager', 'modification_manager', 'opencart3x.ru', '3.x', 'https://opencart3x.ru', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<modification>\n	<name>Modification Manager</name>\n	<code>modification_manager</code>\n	<version>3.x</version>\n	<author>opencart3x.ru</author>\n	<link>https://opencart3x.ru</link>\n\n	<file path=\"admin/language/ru-ru/marketplace/modification.php\">\n		<operation>\n			<search index=\"0\"><![CDATA[<?php]]></search>\n			<add position=\"after\"><![CDATA[\n$_[\'tab_error\'] = \'Ошибки\';\n$_[\'tab_files\'] = \'Файлы\';\n\n$_[\'text_add\'] = \'Создать\';\n$_[\'text_edit\'] = \'Редактировать: %s\';\n\n$_[\'text_enabled\'] = \'Включено\';\n$_[\'text_disabled\'] = \'Отключено\';\n\n$_[\'entry_author\'] = \'Автор\';\n$_[\'entry_name\'] = \'Название\';\n$_[\'entry_xml\'] = \'XML\';\n\n$_[\'button_filter\'] = \'Фильтр\';\n$_[\'button_reset\'] = \'Сброс\';\n\n$_[\'column_date_modified\'] = \'Дата измен.\';\n\n$_[\'error_warning\'] = \'Возникла ошибка, проверьте данные и попробуйте еще раз\';\n$_[\'error_required\'] = \'Это поле обязательно\';\n$_[\'error_name\'] = \'Пропущен тег name\';\n$_[\'error_code\'] = \'Пропущен тег code\';\n$_[\'error_exists\'] = \'Модификация \\\'%s\\\' уже использует идентификатор code: %s!\';]]></add>\n		</operation>\n	</file>\n\n	<file path=\"admin/language/en-gb/marketplace/modification.php\">\n		<operation>\n			<search index=\"0\"><![CDATA[<?php]]></search>\n			<add position=\"after\"><![CDATA[\n$_[\'tab_error\'] = \'Error\';\n$_[\'tab_files\'] = \'Files\';\n\n$_[\'text_add\'] = \'Add Modification\';\n$_[\'text_edit\'] = \'Edit Modification: %s\';\n\n$_[\'text_enabled\'] = \'Enabled\';\n$_[\'text_disabled\'] = \'Disabled\';\n\n$_[\'entry_author\'] = \'Author\';\n$_[\'entry_name\'] = \'Name\';\n$_[\'entry_xml\'] = \'XML\';\n\n$_[\'button_filter\'] = \'Filter\';\n$_[\'button_reset\'] = \'Reset\';\n\n$_[\'column_date_modified\'] = \'Last Modified\';\n\n$_[\'error_warning\'] = \'There has been an error. Please check your data and try again\';\n$_[\'error_required\'] = \'This field is required\';\n$_[\'error_name\'] = \'Missing name tag\';\n$_[\'error_code\'] = \'Missing code tag\';\n$_[\'error_exists\'] = \'Modification \\\'%s\\\' is already using the same code: %s!\';]]></add>\n		</operation>\n	</file>\n\n	<file path=\"admin/controller/marketplace/modification.php\">\n	    <operation>\n			<search index=\"0\"><![CDATA[public function index() {]]></search>\n			<add position=\"after\"><![CDATA[      	$this->load->model(\'extension/module/modification_manager\');\n\n		$this->model_extension_module_modification_manager->install();\n]]></add>\n		</operation>\n		<operation>\n			<search index=\"0\"><![CDATA[$handle = fopen(DIR_LOGS . \'ocmod.log\', \'w+\');]]></search>\n			<add position=\"before\"><![CDATA[      	fclose($handle);\n			\n					$handle = fopen(DIR_LOGS . \'ocmod_error.log\', \'w+\');]]></add>\n		</operation>\n		<operation>\n			<search index=\"0\"><![CDATA[$maintenance = $this->config->get(\'config_maintenance\');]]></search>\n			<add position=\"after\"><![CDATA[\n			// Clear logs on refresh\n			$handle = fopen(DIR_LOGS . \'ocmod.log\', \'w+\');\n			fclose($handle);\n\n			$handle = fopen(DIR_LOGS . \'ocmod_error.log\', \'w+\');\n			fclose($handle);\n]]></add>\n		</operation>\n		<operation>\n			<search index=\"0\"><![CDATA[$data[\'breadcrumbs\'] = array();]]></search>\n			<add position=\"before\"><![CDATA[      	$this->load->model(\'extension/module/modification_manager\');\n\n		if (isset($this->request->get[\'filter_name\'])) {\n			$filter_name = $this->request->get[\'filter_name\'];\n		} else {\n			$filter_name = null;\n		}\n\n      	if (isset($this->request->get[\'filter_xml\'])) {\n			$filter_xml = $this->request->get[\'filter_xml\'];\n		} else {\n			$filter_xml = null;\n		}\n\n		if (isset($this->request->get[\'filter_author\'])) {\n			$filter_author = $this->request->get[\'filter_author\'];\n		} else {\n			$filter_author = null;\n		}\n\n		$url = $this->getListUrlParams();\n\n		$data[\'add\'] = $this->url->link(\'marketplace/modification/add\', \'user_token=\' . $this->session->data[\'user_token\'] . $url, true);\n		$data[\'clear_log\'] = $this->url->link(\'marketplace/modification/clearlog\', \'user_token=\' . $this->session->data[\'user_token\'] . $url, true);\n		$data[\'filter_action\'] = $this->url->link(\'marketplace/modification\', \'user_token=\' . $this->session->data[\'user_token\'], true);\n		$data[\'reset_url\'] = $this->url->link(\'marketplace/modification\', \'user_token=\' . $this->session->data[\'user_token\'], true);\n\n		$data[\'tab_files\'] = $this->language->get(\'tab_files\');\n		$data[\'tab_error\'] = $this->language->get(\'tab_error\');]]></add>\n		</operation>\n		<operation>\n			<search index=\"0\"><![CDATA[$data[\'sort_name\'] =]]></search>\n			<add position=\"before\"><![CDATA[      	if (isset($this->request->get[\'filter_name\'])) {\n			$url .= \'&filter_name=\' . urlencode(html_entity_decode($this->request->get[\'filter_name\'], ENT_QUOTES, \'UTF-8\'));\n		}\n\n		if (isset($this->request->get[\'filter_author\'])) {\n			$url .= \'&filter_author=\' . urlencode(html_entity_decode($this->request->get[\'filter_author\'], ENT_QUOTES, \'UTF-8\'));\n		}\n\n		if (isset($this->request->get[\'filter_xml\'])) {\n			$url .= \'&filter_xml=\' . urlencode(html_entity_decode($this->request->get[\'filter_xml\'], ENT_QUOTES, \'UTF-8\'));\n		}\n\n		$data[\'sort_date_modified\'] = $this->url->link(\'marketplace/modification\', \'user_token=\' . $this->session->data[\'user_token\'] . \'&sort=date_modified\' . $url, true);]]></add>\n		</operation>\n		<operation>\n			<search index=\"0\"><![CDATA[$filter_data = array(]]></search>\n			<add position=\"after\"><![CDATA[      	\'filter_name\'	  => $filter_name,\n			\'filter_author\'	  => $filter_author,\n			\'filter_xml\'	  => $filter_xml,]]></add>\n		</operation>\n		<operation>\n			<search index=\"0\"><![CDATA[$modification_total = $this->model_setting_modification->getTotalModifications();]]></search>\n			<add position=\"replace\"><![CDATA[$modification_total = $this->model_extension_module_modification_manager->getTotalModifications($filter_data);]]></add>\n		</operation>\n		<operation>\n			<search index=\"0\"><![CDATA[$results = $this->model_setting_modification->getModifications($filter_data);]]></search>\n			<add position=\"replace\"><![CDATA[$results = $this->model_extension_module_modification_manager->getModifications($filter_data);]]></add>\n		</operation>\n		<operation>\n			<search index=\"0\"><![CDATA[$data[\'modifications\'][] = array(]]></search>\n			<add position=\"after\"><![CDATA[				\'date_modified\'      => isset($result[\'date_modified\']) ? (date(\'Ymd\') == date(\'Ymd\', strtotime($result[\'date_modified\'])) ? date(\'G:i\', strtotime($result[\'date_modified\'])) : date($this->language->get(\'date_format_short\'), strtotime($result[\'date_modified\']))) : null,\n				\'edit\'			     => $this->url->link(\'marketplace/modification/edit\', \'user_token=\' . $this->session->data[\'user_token\'] . \'&modification_id=\' . $result[\'modification_id\'] . $url, true),]]></add>\n		</operation>\n		<operation>\n			<search index=\"0\"><![CDATA[$pagination = new Pagination();]]></search>\n			<add position=\"before\"><![CDATA[      	if (isset($this->request->get[\'filter_name\'])) {\n			$url .= \'&filter_name=\' . urlencode(html_entity_decode($this->request->get[\'filter_name\'], ENT_QUOTES, \'UTF-8\'));\n		}\n\n		if (isset($this->request->get[\'filter_author\'])) {\n			$url .= \'&filter_author=\' . urlencode(html_entity_decode($this->request->get[\'filter_author\'], ENT_QUOTES, \'UTF-8\'));\n		}\n\n		if (isset($this->request->get[\'filter_xml\'])) {\n			$url .= \'&filter_xml=\' . urlencode(html_entity_decode($this->request->get[\'filter_xml\'], ENT_QUOTES, \'UTF-8\'));\n		}]]></add>\n		</operation>\n		<operation>\n			<search index=\"0\"><![CDATA[$data[\'clear_log\'] =]]></search>\n			<add position=\"before\"><![CDATA[		$data[\'filter_name\'] = $filter_name;\n		$data[\'filter_author\'] = $filter_author;\n		$data[\'filter_xml\'] = $filter_xml;\n\n		$data[\'modified_files\'] = array();\n\n		$modified_files = self::modifiedFiles(DIR_MODIFICATION);\n\n		$modification_files = $this->getModificationXmlFiles();\n\n		foreach($modified_files as $modified_file) {\n			if(isset($modification_files[$modified_file])){\n				$modifications = $modification_files[$modified_file];\n			} else {\n				$modifications = array();\n			}\n\n			$data[\'modified_files\'][] = array(\n				\'file\' => $modified_file,\n				\'modifications\' => $modifications\n			);\n		}\n\n		// Error log\n		$error_file = DIR_LOGS . \'ocmod_error.log\';\n\n		if (file_exists($error_file)) {\n			$data[\'error_log\'] = htmlentities(file_get_contents($error_file, FILE_USE_INCLUDE_PATH, null));\n		} else {\n			$data[\'error_log\'] = \'\';\n		}\n		]]></add>\n		</operation>\n		<operation>\n			<search index=\"0\"><![CDATA[$this->load->view(\'marketplace/modification\']]></search>\n			<add position=\"replace\"><![CDATA[$this->load->view(\'extension/module/modification_manager/list\']]></add>\n		</operation>\n		<operation>\n			<search index=\"0\"><![CDATA[$this->response->redirect($this->url->link(!empty($data[\'redirect\']) ? $data[\'redirect\'] : \'marketplace/modification\', \'user_token=\' . $this->session->data[\'user_token\'] . $url, true));]]></search>\n			<ignoreif position=\"replace\"><![CDATA[if (!empty($data[\'redirect\'])) {]]></ignoreif>\n			<add position=\"replace\"><![CDATA[$url = $this->getListUrlParams();\n\n			if (!empty($data[\'redirect\'])) {\n				$redirect = $data[\'redirect\'];\n			} elseif (!empty($this->request->get[\'redirect\'])) {\n				$redirect = $this->request->get[\'redirect\'];\n			} else {\n				$redirect = \'marketplace/modification\';\n			}\n\n			$this->response->redirect($this->url->link($redirect, \'user_token=\' . $this->session->data[\'user_token\'] . $url, true));]]></add>\n		</operation>\n	    <operation>\n			<search index=\"0\"><![CDATA[if ($this->validate()) {]]></search>\n			<add position=\"after\"><![CDATA[				$error_log = array();\n\n			// Clear vqmod cache\n			$vqmod_path = substr(DIR_SYSTEM, 0, -7) . \'vqmod/\';\n\n			if (file_exists($vqmod_path)) {\n				$vqmod_cache = glob($vqmod_path.\'vqcache/vq*\');\n\n				if ($vqmod_cache) {\n					foreach ($vqmod_cache as $file) {\n						if (file_exists($file)) {\n							@unlink($file);\n						}\n					}\n				}\n\n				if (file_exists($vqmod_path.\'mods.cache\')) {\n					@unlink($vqmod_path.\'mods.cache\');\n				}\n\n				if (file_exists($vqmod_path.\'checked.cache\')) {\n					@unlink($vqmod_path.\'checked.cache\');\n				}\n			}\n]]></add>\n		</operation>\n	    <operation>\n			<search index=\"0\"><![CDATA[$log[] = \'MOD:]]></search>\n			<add position=\"after\"><![CDATA[				$error_log_mod = \'MOD: \' . $dom->getElementsByTagName(\'name\')->item(0)->textContent;\n]]></add>\n		</operation>\n	    <operation>\n			<search index=\"0\"><![CDATA[$operations = $file->getElementsByTagName(\'operation\');]]></search>\n			<add position=\"after\"><![CDATA[				\n					$file_error = $file->getAttribute(\'error\');]]></add>\n		</operation>\n	    <operation>\n			<search index=\"0\"><![CDATA[$files = glob($path, GLOB_BRACE);]]></search>\n			<add position=\"after\"><![CDATA[							if (!$files) {\n								if ($file_error != \'skip\') {\n									$error_log[] = \'----------------------------------------------------------------\';\n									$error_log[] = $error_log_mod;\n									$error_log[] = \'MISSING FILE!\';\n									$error_log[] = $path;									\n								}\n							}]]></add>\n		</operation>\n	    <operation>\n			<search index=\"0\"><![CDATA[if (!$status) {]]></search>\n			<add position=\"after\"><![CDATA[											if ($error != \'skip\') {\n												$error_log[] = \"\\n\";\n												$error_log[] = $error_log_mod;\n												$error_log[] = \'NOT FOUND!\';\n												$error_log[] = \'CODE: \' . $search;\n												$error_log[] = \'FILE: \' . $key;\n											}]]></add>\n		</operation>\n	    <operation>\n			<search index=\"0\"><![CDATA[$ocmod->write(implode(\"\\n\", $log));]]></search>\n			<add position=\"after\"><![CDATA[\n			if ($error_log) {\n				$ocmod = new Log(\'ocmod_error.log\');\n				$ocmod->write(implode(\"\\n\", $error_log));\n			}]]></add>\n		</operation>\n		<operation>\n			<search index=\"0\"><![CDATA[protected function validate(]]></search>\n			<add position=\"before\"><![CDATA[\n\npublic function add() {\n		$this->load->language(\'marketplace/modification\');\n\n		$this->load->model(\'setting/modification\');\n\n		if (($this->request->server[\'REQUEST_METHOD\'] == \'POST\') && $this->validateForm()) {\n			$xml = html_entity_decode($this->request->post[\'xml\'], ENT_QUOTES, \'UTF-8\');\n\n			$dom = new DOMDocument(\'1.0\', \'UTF-8\');\n			$dom->preserveWhiteSpace = false;\n			$dom->loadXml($xml);\n\n			$data = array(\n				\'version\' => \'\',\n				\'author\' => \'\',\n				\'link\' => \'\',\n				\'status\' => 1\n			);\n\n			$data[\'xml\'] = $xml;\n\n			$data[\'name\'] = $dom->getElementsByTagName(\'name\')->item(0)->textContent;\n\n			$data[\'code\'] = $dom->getElementsByTagName(\'code\')->item(0)->textContent;\n\n			if ($dom->getElementsByTagName(\'version\')->length) {\n				$data[\'version\'] = $dom->getElementsByTagName(\'version\')->item(0)->textContent;\n			}\n\n			if ($dom->getElementsByTagName(\'author\')->length) {\n				$data[\'author\'] = $dom->getElementsByTagName(\'author\')->item(0)->textContent;\n			}\n\n			$this->model_setting_modification->addModification($data);\n\n			$modification_id = $this->db->getLastId();\n\n			$this->session->data[\'success\'] = $this->language->get(\'text_success\');\n\n			$this->response->redirect($this->url->link(\'marketplace/modification/edit\', \'user_token=\' . $this->session->data[\'user_token\'] . $this->getListUrlParams(array(\'modification_id\' => $modification_id)), true));\n		}\n\n		$this->getForm();\n	}\n\n	public function edit() {\n		$this->load->language(\'marketplace/modification\');\n\n		$this->load->model(\'setting/modification\');\n		\n		if (($this->request->server[\'REQUEST_METHOD\'] == \'POST\') && !empty($this->request->get[\'modification_id\']) && $this->validateForm()) {\n			$modification_id = $this->request->get[\'modification_id\'];\n\n			$xml = html_entity_decode($this->request->post[\'xml\'], ENT_QUOTES, \'UTF-8\');\n\n			$dom = new DOMDocument(\'1.0\', \'UTF-8\');\n			$dom->preserveWhiteSpace = false;\n			$dom->loadXml($xml);\n\n			$data = array();\n\n			$data[\'xml\'] = $xml;\n\n			$data[\'name\'] = $dom->getElementsByTagName(\'name\')->item(0)->textContent;\n\n			$data[\'code\'] = $dom->getElementsByTagName(\'code\')->item(0)->textContent;\n\n			if ($dom->getElementsByTagName(\'version\')->length) {\n				$data[\'version\'] = $dom->getElementsByTagName(\'version\')->item(0)->textContent;\n			} else {\n				$data[\'version\'] = \'\';\n			}\n\n			if ($dom->getElementsByTagName(\'author\')->length) {\n				$data[\'author\'] = $dom->getElementsByTagName(\'author\')->item(0)->textContent;\n			} else {\n				$data[\'author\'] = \'\';\n			}\n\n			if ($dom->getElementsByTagName(\'link\')->length) {\n				$data[\'link\'] = $dom->getElementsByTagName(\'link\')->item(0)->textContent;\n			} else {\n				$data[\'link\'] = \'\';\n			}\n\n			$this->load->model(\'extension/module/modification_manager\');\n\n			$this->model_extension_module_modification_manager->editModification($modification_id, $data);\n\n			$url = $this->getListUrlParams(array(\'modification_id\' => $modification_id));\n\n			if (isset($this->request->get[\'refresh\'])) {\n				$this->response->redirect($this->url->link(\'marketplace/modification/refresh\', \'user_token=\' . $this->session->data[\'user_token\'] . $url, true));\n			}\n\n			if ($this->db->countAffected()) {\n				$this->session->data[\'success\'] = $this->language->get(\'text_success\');\n\n				$this->response->redirect($this->url->link(\'marketplace/modification/edit\', \'user_token=\' . $this->session->data[\'user_token\'] . $url, true));\n			}\n		}\n\n		$this->getForm();\n	}\n\n	public function getForm() {\n		$data[\'heading_title\'] = $this->language->get(\'heading_title\');\n\n		$data[\'text_enabled\'] = $this->language->get(\'text_enabled\');\n		$data[\'text_disabled\'] = $this->language->get(\'text_disabled\');\n\n		$data[\'button_save\'] = $this->language->get(\'button_save\');\n		$data[\'button_refresh\'] = $this->language->get(\'button_refresh\');\n		$data[\'button_cancel\'] = $this->language->get(\'button_cancel\');\n\n		if (isset($this->error[\'warning\'])) {\n			$data[\'error_warning\'] = $this->error[\'warning\'];\n		} elseif (!empty($this->error)) {\n			$data[\'error_warning\'] = $this->language->get(\'error_warning\');\n		} else {\n			$data[\'error_warning\'] = \'\';\n		}\n\n		if (isset($this->session->data[\'success\'])) {\n			$data[\'success\'] = $this->session->data[\'success\'];\n\n			unset($this->session->data[\'success\']);\n		} else {\n			$data[\'success\'] = false;\n		}\n\n		if (isset($this->error[\'xml\'])) {\n			$data[\'error_xml\'] = $this->error[\'xml\'];\n		}\n\n		$data[\'breadcrumbs\'] = array();\n\n		$data[\'breadcrumbs\'][] = array(\n			\'text\' => $this->language->get(\'text_home\'),\n			\'href\' => $this->url->link(\'common/dashboard\', \'user_token=\' . $this->session->data[\'user_token\'], true)\n		);\n\n		$data[\'breadcrumbs\'][] = array(\n			\'text\' => $this->language->get(\'heading_title\'),\n			\'href\' => $this->url->link(\'marketplace/modification\', \'user_token=\' . $this->session->data[\'user_token\'] . $this->getListUrlParams(), true)\n		);\n\n		if (isset($this->request->get[\'modification_id\'])) {\n			$modification_info = $this->model_setting_modification->getModification($this->request->get[\'modification_id\']);\n			if (!$modification_info) exit;\n\n			$data[\'text_form\'] = sprintf($this->language->get(\'text_edit\'), $modification_info[\'name\']);\n\n			$data[\'action\'] = $this->url->link(\'marketplace/modification/edit\', \'&modification_id=\' . $modification_info[\'modification_id\'] . \'&user_token=\' . $this->session->data[\'user_token\'], true);\n\n			$data[\'refresh\'] = $this->url->link(\'marketplace/modification/edit\', \'&modification_id=\' . $modification_info[\'modification_id\'] . \'&refresh=1&user_token=\' . $this->session->data[\'user_token\'], true);\n\n			$this->document->setTitle($modification_info[\'name\'] . \' » \' . $data[\'heading_title\']);\n		} else {\n			$data[\'text_form\'] = $this->language->get(\'text_add\');\n\n			$data[\'refresh\'] = false;\n\n			$data[\'action\'] = $this->url->link(\'marketplace/modification/add\', \'user_token=\' . $this->session->data[\'user_token\'], true);\n\n			$this->document->setTitle($data[\'heading_title\']);\n		}\n\n		$data[\'cancel\'] = $this->url->link(\'marketplace/modification\', \'user_token=\' . $this->session->data[\'user_token\'] . $this->getListUrlParams(), true);\n\n		$data[\'modification\'] = array();\n\n		if (!empty($modification_info)) {\n			$data[\'modification\'][\'status\'] = $modification_info[\'status\'];\n		} else {\n			$data[\'modification\'][\'status\'] = 0;\n		}\n\n		if (isset($this->request->post[\'xml\'])) {\n			$data[\'modification\'][\'xml\'] = html_entity_decode($this->request->post[\'xml\'], ENT_QUOTES, \'UTF-8\');\n		} elseif (!empty($modification_info)) {\n			$data[\'modification\'][\'xml\'] = $modification_info[\'xml\'];\n		} else {\n			$data[\'modification\'][\'xml\'] = \'\';\n		}\n\n		$this->document->addStyle(\'view/javascript/codemirror/lib/codemirror.css\');\n		$this->document->addScript(\'view/javascript/codemirror/lib/codemirror.js\');\n		$this->document->addScript(\'view/javascript/codemirror/mode/xml/xml.js\');\n\n		$data[\'header\'] = $this->load->controller(\'common/header\');\n		$data[\'column_left\'] = $this->load->controller(\'common/column_left\');\n		$data[\'footer\'] = $this->load->controller(\'common/footer\');\n\n		$this->response->setOutput($this->load->view(\'extension/module/modification_manager/form\', $data));\n	}\n\n	private function validateForm() {\n		if (!$this->user->hasPermission(\'modify\', \'marketplace/modification\')) {\n			$this->error[\'warning\'] = $this->language->get(\'error_permission\');\n		}\n\n		$error = false;\n\n		// Required\n		if (empty($this->request->post[\'xml\'])) {\n			$error = $this->language->get(\'error_required\');\n		}\n\n		// 2. Validate XML\n		if (!$error) {\n			libxml_use_internal_errors(true);\n\n			$dom = new DOMDocument(\'1.0\', \'UTF-8\');\n\n			if(!$dom->loadXml(html_entity_decode($this->request->post[\'xml\'], ENT_QUOTES, \'UTF-8\'))){\n\n			    foreach (libxml_get_errors() as $error) {\n			        $msg = \'\';\n\n			        switch ($error->level) {\n			            case LIBXML_ERR_WARNING :\n			                $msg .= \"Warning $error->code: \";\n			                break;\n			            case LIBXML_ERR_ERROR :\n			                $msg .= \"Error $error->code: \";\n			                break;\n			            case LIBXML_ERR_FATAL :\n			                $msg .= \"Fatal Error $error->code: \";\n			                break;\n			        }\n\n			        $msg .= trim ( $error->message ) . \"\\nLine: $error->line\";\n\n			        $error = $msg;\n			    }\n\n			    libxml_clear_errors();\n			}\n\n			libxml_use_internal_errors(false);\n		}\n\n		// 3. Required tags\n		if (!$error && (!$dom->getElementsByTagName(\'name\') || $dom->getElementsByTagName(\'name\')->length == 0 || $dom->getElementsByTagName(\'name\')->item(0)->textContent == \'\')) {\n			$error = $this->language->get(\'error_name\');\n		}\n\n		if (!$error && (!$dom->getElementsByTagName(\'code\') || $dom->getElementsByTagName(\'code\')->length == 0 || $dom->getElementsByTagName(\'code\')->item(0)->textContent == \'\')) {\n			$error = $this->language->get(\'error_code\');\n		}\n\n		// 4. Check code isn\'t duplicate\n		if (!$error) {\n			$code = $dom->getElementsByTagName(\'code\')->item(0)->textContent;\n\n			$this->load->model(\'setting/modification\');\n			\n			$modification_info = $this->model_setting_modification->getModificationByCode($code);\n\n			if ($modification_info && (!isset($this->request->get[\'modification_id\']) || $modification_info[\'modification_id\'] != $this->request->get[\'modification_id\'])) {\n				$error = sprintf($this->language->get(\'error_exists\'), $modification_info[\'name\'], $modification_info[\'code\']);\n			}\n		}\n\n		if ($error) {\n			$this->error[\'xml\'] = $error;\n		}\n\n		return !$this->error;\n	}\n\n	static function modifiedFiles($dir, $dirLen = 0) {\n		$tree = glob(rtrim($dir, \'/\') . \'/*\');\n		if (!$dirLen) {\n			$dirLen = strlen($dir);\n		}\n		$files = array();\n\n	    if (is_array($tree)) {\n	        foreach($tree as $file) {\n	        	if ($file == $dir . \'index.html\') {\n					continue;\n				} elseif (is_file($file)) {\n	                $files[] = substr($file, $dirLen);\n	            } elseif (is_dir($file)) {\n	                $files = array_merge($files, self::modifiedFiles($file, $dirLen));\n	            }\n	        }\n	    }\n\n	    return $files;\n	}\n\n	protected function getListUrlParams(array $params = array()) {\n		if (isset($params[\'sort\'])) {\n			$params[\'sort\'] = $params[\'sort\'];\n		} elseif (isset($this->request->get[\'sort\'])) {\n			$params[\'sort\'] = $this->request->get[\'sort\'];\n		}\n\n		if (isset($params[\'order\'])) {\n			$params[\'order\'] = $params[\'order\'];\n		} elseif (isset($this->request->get[\'order\'])) {\n			$params[\'order\'] = $this->request->get[\'order\'];\n		}\n\n		if (isset($params[\'filter_name\'])) {\n			$params[\'filter_name\'] = urlencode(html_entity_decode($params[\'filter_name\'], ENT_QUOTES, \'UTF-8\'));\n		} elseif (isset($this->request->get[\'filter_name\'])) {\n			$params[\'filter_name\'] = urlencode(html_entity_decode($this->request->get[\'filter_name\'], ENT_QUOTES, \'UTF-8\'));\n		}\n\n		if (isset($params[\'filter_author\'])) {\n			$params[\'filter_author\'] = urlencode(html_entity_decode($params[\'filter_author\'], ENT_QUOTES, \'UTF-8\'));\n		} elseif (isset($this->request->get[\'filter_author\'])) {\n			$params[\'filter_author\'] = urlencode(html_entity_decode($this->request->get[\'filter_author\'], ENT_QUOTES, \'UTF-8\'));\n		}\n\n		if (isset($params[\'filter_xml\'])) {\n			$params[\'filter_xml\'] = urlencode(html_entity_decode($params[\'filter_xml\'], ENT_QUOTES, \'UTF-8\'));\n		} elseif (isset($this->request->get[\'filter_xml\'])) {\n			$params[\'filter_xml\'] = urlencode(html_entity_decode($this->request->get[\'filter_xml\'], ENT_QUOTES, \'UTF-8\'));\n		}\n\n		if (isset($params[\'page\'])) {\n			$params[\'page\'] = $params[\'page\'];\n		} elseif (isset($this->request->get[\'page\'])) {\n			$params[\'page\'] = $this->request->get[\'page\'];\n		}\n\n		$paramsJoined = array();\n\n		foreach($params as $param => $value) {\n			$paramsJoined[] = \"$param=$value\";\n		}\n\n		return \'&\' . implode(\'&\', $paramsJoined);\n	}\n\n	protected function getModificationXmlFiles() {\n		$return = array();\n\n		$baseLen = strlen(substr(DIR_SYSTEM, 0, -7));\n\n		$xml = array();\n\n		$xml[] = file_get_contents(DIR_SYSTEM . \'modification.xml\');\n\n		$files = glob(DIR_SYSTEM . \'*.ocmod.xml\');\n\n		if ($files) {\n			foreach ($files as $file) {\n				$xml[] = file_get_contents($file);\n			}\n		}\n\n		$results = $this->model_setting_modification->getModifications();\n\n		foreach ($results as $result) {\n			if ($result[\'status\']) {\n				$xml[] = $result[\'xml\'];\n			}\n		}\n\n		foreach ($xml as $xml) {\n			if (empty($xml)){\n				continue;\n			}\n\n			$dom = new DOMDocument(\'1.0\', \'UTF-8\');\n			$dom->preserveWhiteSpace = false;\n			$dom->loadXml($xml);\n\n			$files = $dom->getElementsByTagName(\'modification\')->item(0)->getElementsByTagName(\'file\');\n\n			foreach ($files as $file) {\n				$operations = $file->getElementsByTagName(\'operation\');\n\n				$files = explode(\',\', $file->getAttribute(\'path\'));\n\n				foreach ($files as $file) {\n					$path = \'\';\n\n					// Get the full path of the files that are going to be used for modification\n					if (substr($file, 0, 7) == \'catalog\') {\n						$path = DIR_CATALOG . str_replace(\'../\', \'\', substr($file, 8));\n					}\n\n					if (substr($file, 0, 5) == \'admin\') {\n						$path = DIR_APPLICATION . str_replace(\'../\', \'\', substr($file, 6));\n					}\n\n					if (substr($file, 0, 6) == \'system\') {\n						$path = DIR_SYSTEM . str_replace(\'../\', \'\', substr($file, 7));\n					}\n\n					if ($path) {\n						$files = glob($path, GLOB_BRACE);\n\n						if ($files) {\n							foreach ($files as $file) {\n								$file = substr($file, $baseLen);\n\n								if (!isset($return[$file])) {\n									$return[$file] = array();\n								}\n\n								if ($dom->getElementsByTagName(\'author\')->length) {\n									$author = $dom->getElementsByTagName(\'author\')->item(0)->textContent;\n								} else {\n									$author = \'\';\n								}\n\n								$return[$file][] = array(\n									\'code\' => $dom->getElementsByTagName(\'code\')->item(0)->textContent,\n									\'name\' => $dom->getElementsByTagName(\'name\')->item(0)->textContent,\n									\'author\' => $author\n								);\n							}\n						}\n					}\n				}\n			}\n		}\n\n		return $return;\n	}\n\n]]></add>\n		</operation>\n	</file>\n</modification>', 1, '2020-05-20 04:19:15', '2020-05-20 04:19:15');
INSERT INTO `oc_modification` (`modification_id`, `extension_install_id`, `name`, `code`, `author`, `version`, `link`, `xml`, `status`, `date_added`, `date_modified`) VALUES
(20, 23, 'Расширенные наценки для опций', 'options-markups', 'https://prowebber.ru/', '2.0', 'https://prowebber.ru/', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<modification>\r\n  <name>Расширенные наценки для опций</name>\r\n  <code>options-markups</code>\r\n  <version>2.0</version>\r\n  <author>https://prowebber.ru/</author>\r\n  <link>https://prowebber.ru/</link>\r\n  \r\n  <file path=\"catalog/controller/product/product.php\">\r\n    <operation>\r\n      <search><![CDATA[$price = $this->currency->format($this->tax->calculate($option_value[\'price\'], $product_info[\'tax_class_id\'], $this->config->get(\'config_tax\') ? \'P\' : false)]]></search>\r\n      <add position=\"replace\"><![CDATA[\r\n        if ($option_value[\'price_prefix\']==\"u\") {\r\n            $price = \'+\' . (float)$option_value[\'price\'].\'%\';\r\n        }\r\n        elseif ($option_value[\'price_prefix\']==\"d\") {\r\n            $price = \'-\' . (float)$option_value[\'price\'].\'%\';\r\n        }\r\n        elseif ($option_value[\'price_prefix\']==\"*\") {\r\n            $price = \'*\' . (float)$option_value[\'price\'];\r\n        }\r\n        elseif ($option_value[\'price_prefix\']==\"/\") {\r\n            $price = \'/\' . (float)$option_value[\'price\'];\r\n        }\r\n        else{\r\n            $price = $option_value[\'price_prefix\'].$this->currency->format($this->tax->calculate($option_value[\'price\'], $product_info[\'tax_class_id\'], $this->config->get(\'config_tax\') ? \'P\' : false), $this->session->data[\'currency\']);\r\n        }\r\n        // $price = $this->currency->format($this->tax->calculate($option_value[\'price\'], $product_info[\'tax_class_id\'], $this->config->get(\'config_tax\') ? \'P\' : false)]]></add>\r\n    </operation>\r\n  </file>\r\n  <file path=\"catalog/view/theme/*/template/product/product.twig\">\r\n    <operation>\r\n      <search><![CDATA[{{ option_value.price_prefix }}]]></search>\r\n      <add position=\"replace\"><![CDATA[]]></add>\r\n    </operation>\r\n  </file>  \r\n  <file path=\"system/library/cart/cart.php\">\r\n    <operation>\r\n      <search><![CDATA[$option_price = 0;]]></search>\r\n      <add position=\"replace\"><![CDATA[$option_price = array();]]></add>\r\n    </operation>\r\n      <operation>\r\n        <search><![CDATA[if ($option_value_query->row[\'price_prefix\'] == \'+\') {]]></search>\r\n        <add position=\"replace\" offset=\"4\"><![CDATA[\r\n          if ($option_value_query->row[\'price_prefix\'] == \'=\') {\r\n              $sort_key=count($option_price) + 1;\r\n          } else {\r\n              $sort_key=count($option_price)+10;\r\n          }\r\n          \r\n          $option_price[$sort_key] = array(\r\n              $option_value_query->row[\'price_prefix\']=>$option_value_query->row[\'price\'],\r\n          );\r\n        ]]></add>\r\n      </operation>\r\n      <operation>\r\n        <search><![CDATA[if (!$product_query->row[\'quantity\']]]></search>\r\n        <add position=\"before\"><![CDATA[\r\n          $newprice = $price;\r\n 		  $defprice = $price;\r\n          \r\n          ksort($option_price);\r\n\r\n          foreach($option_price as $operations){\r\n              foreach($operations as $operation=>$value){\r\n                  if ($operation == \'=\') {\r\n\r\n                      if ($price!=0 ){\r\n                          $newprice = $value;\r\n                          $price=0;\r\n                      }\r\n                      else{\r\n                          $newprice += $value;\r\n                      }\r\n                      \r\n                  }\r\n                  elseif ($operation == \'+\') {\r\n                      $newprice += $value;\r\n                  }\r\n                  elseif ($operation == \'-\') {\r\n                      $newprice -= $value;\r\n                  }\r\n                  elseif ($operation == \'*\') {\r\n                      $newprice = $newprice * $value;\r\n                  }\r\n                  elseif ($operation == \'/\') {\r\n                      $newprice = $newprice / $value;\r\n                  }\r\n                  elseif ($operation == \'u\') {\r\n                      $newprice = $newprice + (( $defprice * $value ) / 100);\r\n                      }\r\n                  elseif ($operation == \'d\') {\r\n                      $newprice = $newprice - (( $defprice * $value ) / 100);\r\n                  }\r\n              }\r\n          }\r\n        ]]> \r\n      </add>\r\n    </operation>\r\n    <operation>\r\n      <search><![CDATA[$price + $option_price]]></search>\r\n      <add replace=\"replace\"><![CDATA[$newprice]]></add>\r\n    </operation>\r\n  </file>\r\n  \r\n  <file path=\"admin/view/template/editors/product/product_form.twig\">\r\n    <operation>\r\n      <search><![CDATA[{% if product_option_value.price_prefix == \'+\' %}]]></search>\r\n      <add position=\"before\"><![CDATA[\r\n        {% if product_option_value.price_prefix == \'=\' %} \r\n            <option value=\"=\" selected=\"selected\">=</option>\r\n        {% else %} \r\n            <option value=\"=\">=</option>\r\n        {% endif %} \r\n        {% if product_option_value.price_prefix == \'*\' %} \r\n            <option value=\"*\" selected=\"selected\">*</option>\r\n        {% else %} \r\n            <option value=\"*\">*</option>\r\n        {% endif %} \r\n        {% if product_option_value.price_prefix == \'/\' %} \r\n            <option value=\"/\" selected=\"selected\">/</option>\r\n        {% else %} \r\n            <option value=\"/\">/</option>\r\n        {% endif %} \r\n        {% if product_option_value.price_prefix == \'u\' %} \r\n            <option value=\"u\" selected=\"selected\">+%</option>\r\n        {% else %} \r\n            <option value=\"u\">+%</option>\r\n        {% endif %} \r\n        {% if product_option_value.price_prefix == \'d\' %} \r\n            <option value=\"d\" selected=\"selected\">-%</option>\r\n        {% else %} \r\n            <option value=\"d\">-%</option>\r\n        {% endif %}\r\n      ]]></add>\r\n    </operation>\r\n    <operation>\r\n      <search><![CDATA[<select name=\"product_option[\' + option_row + \'][product_option_value][\' + option_value_row + \'][price_prefix]\"]]></search>\r\n      <add position=\"after\" offset=\"2\"><![CDATA[\r\n        html += \'      <option value=\"=\">=</option>\';\r\n        html += \'      <option value=\"*\">*</option>\';\r\n        html += \'      <option value=\"/\">/</option>\';\r\n        html += \'      <option value=\"u\">+%</option>\';\r\n        html += \'      <option value=\"d\">-%</option>\';\r\n      ]]></add>\r\n    </operation>\r\n  </file>\r\n  <file path=\"admin/view/template/catalog/product_form.twig\">\r\n    <operation>\r\n      <search><![CDATA[{% if product_option_value.price_prefix == \'+\' %}]]></search>\r\n      <add position=\"before\"><![CDATA[\r\n        {% if product_option_value.price_prefix == \'=\' %} \r\n            <option value=\"=\" selected=\"selected\">=</option>\r\n        {% else %} \r\n            <option value=\"=\">=</option>\r\n        {% endif %} \r\n        {% if product_option_value.price_prefix == \'*\' %} \r\n            <option value=\"*\" selected=\"selected\">*</option>\r\n        {% else %} \r\n            <option value=\"*\">*</option>\r\n        {% endif %} \r\n        {% if product_option_value.price_prefix == \'/\' %} \r\n            <option value=\"/\" selected=\"selected\">/</option>\r\n        {% else %} \r\n            <option value=\"/\">/</option>\r\n        {% endif %} \r\n        {% if product_option_value.price_prefix == \'u\' %} \r\n            <option value=\"u\" selected=\"selected\">+%</option>\r\n        {% else %} \r\n            <option value=\"u\">+%</option>\r\n        {% endif %} \r\n        {% if product_option_value.price_prefix == \'d\' %} \r\n            <option value=\"d\" selected=\"selected\">-%</option>\r\n        {% else %} \r\n            <option value=\"d\">-%</option>\r\n        {% endif %}\r\n      ]]></add>\r\n    </operation>\r\n    <operation>\r\n      <search><![CDATA[<select name=\"product_option[\' + option_row + \'][product_option_value][\' + option_value_row + \'][price_prefix]\"]]></search>\r\n      <add position=\"after\" offset=\"2\"><![CDATA[\r\n        html += \'      <option value=\"=\">=</option>\';\r\n        html += \'      <option value=\"*\">*</option>\';\r\n        html += \'      <option value=\"/\">/</option>\';\r\n        html += \'      <option value=\"u\">+%</option>\';\r\n        html += \'      <option value=\"d\">-%</option>\';\r\n      ]]></add>\r\n    </operation>\r\n  </file>\r\n  \r\n</modification>', 1, '2020-05-28 14:18:06', '0000-00-00 00:00:00'),
(21, 25, 'X-Shipping Pro', 'xshippingpro', 'OpenCartMart', '3.2.2', 'http://www.opencartmart.com', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<modification>\n    <name>X-Shipping Pro</name>\n    <code>xshippingpro</code>\n    <version>3.2.2</version>\n    <author>OpenCartMart</author>\n    <link>http://www.opencartmart.com</link>\n    <file path=\"catalog/controller/extension/module/xtensions/checkout/xfooter.php\">\n        <operation error=\"skip\">\n            <search><![CDATA[ $this->load->model(\'catalog/information\'); ]]></search>\n            <add position=\"before\"><![CDATA[\n                  $ocm = ($ocm = $this->registry->get(\'ocm_front\')) ? $ocm : new OCM\\Front($this->registry);\n                  $data[\'_ocm_script\'] = $ocm->getScript();\n            ]]></add>\n        </operation>\n    </file>\n    <file path=\"catalog/view/theme/*/template/extension/module/xtensions/checkout/xfooter.twig\">\n      <operation error=\"skip\">\n        <search><![CDATA[ </body> ]]></search>\n        <add position=\"before\"><![CDATA[\n            {{ _ocm_script }}\n        ]]></add>\n      </operation>\n   </file>\n       <file path=\"catalog/controller/common/footer.php\">\n        <operation error=\"log\">\n            <ignoreif regex=\"true\"><![CDATA[ /ocm->getScript/ ]]></ignoreif>\n            <search index=\"0\"><![CDATA[ $this->load->model(\'catalog/information\'); ]]></search>\n            <add position=\"before\"><![CDATA[\n                  $ocm = ($ocm = $this->registry->get(\'ocm_front\')) ? $ocm : new OCM\\Front($this->registry);\n                  $data[\'_ocm_script\'] = $ocm->getScript();\n            ]]></add>\n        </operation>\n    </file>\n   <!--  d_twig taints OC 2.x tpl with twig  -->\n   <file path=\"catalog/view/theme/*/template/common/footer.twi*\">\n      <operation>\n        <ignoreif regex=\"true\"><![CDATA[ /_ocm_script/ ]]></ignoreif>\n        <search><![CDATA[ </body> ]]></search>\n        <add position=\"before\"><![CDATA[\n            {{_ocm_script}}\n        ]]></add>\n      </operation>\n   </file>\n</modification>', 1, '2020-07-22 12:05:38', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_module`
--

CREATE TABLE `oc_module` (
  `module_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `code` varchar(32) NOT NULL,
  `setting` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_ocfilter_option`
--

CREATE TABLE `oc_ocfilter_option` (
  `option_id` int(11) NOT NULL,
  `type` varchar(16) NOT NULL DEFAULT 'checkbox',
  `keyword` varchar(255) NOT NULL DEFAULT '',
  `selectbox` tinyint(1) NOT NULL DEFAULT 0,
  `grouping` tinyint(2) NOT NULL DEFAULT 0,
  `color` tinyint(1) NOT NULL DEFAULT 0,
  `image` tinyint(1) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `sort_order` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_ocfilter_option_description`
--

CREATE TABLE `oc_ocfilter_option_description` (
  `option_id` int(11) NOT NULL,
  `language_id` tinyint(2) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `postfix` varchar(32) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_ocfilter_option_to_category`
--

CREATE TABLE `oc_ocfilter_option_to_category` (
  `option_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_ocfilter_option_to_store`
--

CREATE TABLE `oc_ocfilter_option_to_store` (
  `option_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_ocfilter_option_value`
--

CREATE TABLE `oc_ocfilter_option_value` (
  `value_id` bigint(20) NOT NULL,
  `option_id` int(11) NOT NULL DEFAULT 0,
  `keyword` varchar(255) NOT NULL DEFAULT '',
  `color` varchar(6) NOT NULL DEFAULT '',
  `image` varchar(255) NOT NULL DEFAULT '',
  `sort_order` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_ocfilter_option_value_description`
--

CREATE TABLE `oc_ocfilter_option_value_description` (
  `value_id` bigint(20) NOT NULL,
  `option_id` int(11) NOT NULL,
  `language_id` tinyint(2) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_ocfilter_option_value_to_product`
--

CREATE TABLE `oc_ocfilter_option_value_to_product` (
  `ocfilter_option_value_to_product_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `value_id` bigint(20) NOT NULL,
  `slide_value_min` decimal(15,4) NOT NULL DEFAULT 0.0000,
  `slide_value_max` decimal(15,4) NOT NULL DEFAULT 0.0000
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_ocfilter_option_value_to_product_description`
--

CREATE TABLE `oc_ocfilter_option_value_to_product_description` (
  `product_id` int(11) NOT NULL,
  `value_id` bigint(20) NOT NULL,
  `option_id` int(11) NOT NULL,
  `language_id` tinyint(2) NOT NULL,
  `description` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_ocfilter_page`
--

CREATE TABLE `oc_ocfilter_page` (
  `ocfilter_page_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT 0,
  `keyword` varchar(255) NOT NULL,
  `params` varchar(255) NOT NULL,
  `over` set('domain','category') NOT NULL DEFAULT 'category',
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_ocfilter_page_description`
--

CREATE TABLE `oc_ocfilter_page_description` (
  `ocfilter_page_id` int(11) NOT NULL DEFAULT 0,
  `language_id` int(11) NOT NULL DEFAULT 0,
  `meta_title` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `title` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_option`
--

CREATE TABLE `oc_option` (
  `option_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_option`
--

INSERT INTO `oc_option` (`option_id`, `type`, `sort_order`) VALUES
(1, 'select', 0),
(2, 'select', 0),
(3, 'select', 0),
(4, 'select', 0),
(5, 'select', 0),
(6, 'select', 0),
(7, 'select', 0),
(8, 'select', 0),
(9, 'select', 0),
(10, 'select', 0),
(11, 'select', 0),
(12, 'select', 0),
(13, 'select', 0),
(14, 'select', 0),
(15, 'select', 0),
(16, 'select', 0),
(17, 'select', 0),
(18, 'select', 0),
(19, 'select', 0),
(20, 'select', 0),
(21, 'select', 0),
(22, 'select', 0),
(23, 'select', 0),
(24, 'select', 0),
(25, 'select', 0),
(26, 'select', 0),
(27, 'select', 0),
(28, 'select', 0),
(29, 'select', 0),
(30, 'select', 0),
(31, 'select', 0),
(32, 'select', 0),
(33, 'select', 0),
(34, 'select', 0),
(35, 'select', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_option_characteristic`
--

CREATE TABLE `oc_option_characteristic` (
  `characteristic_id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_option_characteristic_description`
--

CREATE TABLE `oc_option_characteristic_description` (
  `characteristic_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_option_description`
--

CREATE TABLE `oc_option_description` (
  `option_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_option_description`
--

INSERT INTO `oc_option_description` (`option_id`, `language_id`, `name`) VALUES
(11, 1, 'Amali Arshaevy 1011'),
(10, 1, 'Frida Arshaevy 1010'),
(7, 1, 'Khalifa Arshaevy 1007'),
(8, 1, 'Madlen Arshaevy 1008'),
(9, 1, 'Miya Arshaevy 1009'),
(6, 1, 'Silvi Arshaevy 1006'),
(14, 1, 'Texas Arshaevy 1014'),
(13, 1, 'Ариель Arshaevy 1013'),
(5, 1, 'Бонька Arshaevy 1005'),
(28, 1, 'Буркини Arshaevy 1028'),
(29, 1, 'Буркини Arshaevy 1029'),
(30, 1, 'Буркини Arshaevy 1030'),
(31, 1, 'Буркини Arshaevy 1031'),
(32, 1, 'Буркини Arshaevy 1032'),
(35, 1, 'Буркини Arshaevy бюджетная версия 1035'),
(33, 1, 'Буркини Arshaevy комлект X 1033'),
(34, 1, 'Буркини Arshaevy комлект X 1034'),
(19, 1, 'Килоты arshaevy 1019'),
(12, 1, 'Корсет платье Arshaevy 1012'),
(15, 1, 'Костюм Arshaevy 1015'),
(18, 1, 'Костюм Arshaevy 1018'),
(23, 1, 'Костюм Kendi Arshaevy 1023'),
(24, 1, 'Костюм Kendi Arshaevy 1024'),
(16, 1, 'Костюм romb arshaevy 1016'),
(1, 1, 'Логслив Arshaevy 1001'),
(3, 1, 'Лонг слив Платье  Arshaevy 1003'),
(4, 1, 'Лонг футболка Платье  Arshaevy 1004'),
(21, 1, 'Пальто LV Arshaevy 1021'),
(22, 1, 'Пальто Неопренд Arshaevy 1022'),
(2, 1, 'Футболка Arshaevy 1002'),
(25, 1, 'Худи Arshaevy 1025'),
(26, 1, 'Худи Arshaevy 1026'),
(27, 1, 'Худи Arshaevy 1027'),
(17, 1, 'Шерстяной костюм arshaevy 1017'),
(20, 1, 'Юбка romb arshaevy 1020');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_option_value`
--

CREATE TABLE `oc_option_value` (
  `option_value_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `image` text NOT NULL,
  `image_serialized` tinyint(1) NOT NULL DEFAULT 0,
  `sort_order` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_option_value`
--

INSERT INTO `oc_option_value` (`option_value_id`, `option_id`, `image`, `image_serialized`, `sort_order`) VALUES
(1, 1, '', 1, 0),
(2, 1, '', 1, 1),
(3, 1, '', 1, 2),
(4, 1, '', 1, 3),
(5, 1, '', 1, 4),
(6, 1, '', 1, 5),
(7, 1, '', 1, 6),
(8, 1, '', 1, 7),
(9, 1, '', 1, 8),
(10, 1, '', 1, 9),
(11, 1, '', 1, 10),
(12, 1, '', 1, 11),
(13, 1, '', 1, 12),
(14, 2, '', 1, 0),
(15, 2, '', 1, 1),
(16, 2, '', 1, 2),
(17, 2, '', 1, 3),
(18, 2, '', 1, 4),
(19, 2, '', 1, 5),
(20, 2, '', 1, 6),
(21, 2, '', 1, 7),
(22, 2, '', 1, 8),
(23, 2, '', 1, 9),
(24, 2, '', 1, 10),
(25, 2, '', 1, 11),
(26, 2, '', 1, 12),
(27, 3, '', 1, 0),
(28, 3, '', 1, 1),
(29, 3, '', 1, 2),
(30, 3, '', 1, 3),
(31, 3, '', 1, 4),
(32, 3, '', 1, 5),
(33, 3, '', 1, 6),
(34, 3, '', 1, 7),
(35, 3, '', 1, 8),
(36, 3, '', 1, 9),
(37, 3, '', 1, 10),
(38, 3, '', 1, 11),
(39, 3, '', 1, 12),
(40, 4, '', 1, 0),
(41, 4, '', 1, 1),
(42, 4, '', 1, 2),
(43, 4, '', 1, 3),
(44, 4, '', 1, 4),
(45, 4, '', 1, 5),
(46, 4, '', 1, 6),
(47, 4, '', 1, 7),
(48, 4, '', 1, 8),
(49, 4, '', 1, 9),
(50, 4, '', 1, 10),
(51, 4, '', 1, 11),
(52, 4, '', 1, 12),
(53, 5, '', 1, 0),
(54, 5, '', 1, 1),
(55, 5, '', 1, 2),
(56, 5, '', 1, 3),
(57, 5, '', 1, 4),
(58, 5, '', 1, 5),
(59, 5, '', 1, 6),
(60, 5, '', 1, 7),
(61, 5, '', 1, 8),
(62, 5, '', 1, 9),
(63, 5, '', 1, 10),
(64, 5, '', 1, 11),
(65, 5, '', 1, 12),
(66, 6, '', 1, 0),
(67, 6, '', 1, 1),
(68, 6, '', 1, 2),
(69, 6, '', 1, 3),
(70, 6, '', 1, 4),
(71, 6, '', 1, 5),
(72, 6, '', 1, 6),
(73, 6, '', 1, 7),
(74, 6, '', 1, 8),
(75, 6, '', 1, 9),
(76, 6, '', 1, 10),
(77, 6, '', 1, 11),
(78, 6, '', 1, 12),
(79, 6, '', 1, 13),
(80, 6, '', 1, 14),
(81, 6, '', 1, 15),
(82, 6, '', 1, 16),
(83, 6, '', 1, 17),
(84, 6, '', 1, 18),
(85, 6, '', 1, 19),
(86, 6, '', 1, 20),
(87, 7, '', 1, 0),
(88, 7, '', 1, 1),
(89, 7, '', 1, 2),
(90, 7, '', 1, 3),
(91, 7, '', 1, 4),
(92, 7, '', 1, 5),
(93, 7, '', 1, 6),
(94, 7, '', 1, 7),
(95, 7, '', 1, 8),
(96, 7, '', 1, 9),
(97, 7, '', 1, 10),
(98, 7, '', 1, 11),
(99, 7, '', 1, 12),
(100, 7, '', 1, 13),
(101, 7, '', 1, 14),
(102, 7, '', 1, 15),
(103, 8, '', 1, 0),
(104, 8, '', 1, 1),
(105, 8, '', 1, 2),
(106, 8, '', 1, 3),
(107, 8, '', 1, 4),
(108, 8, '', 1, 5),
(109, 8, '', 1, 6),
(110, 8, '', 1, 7),
(111, 8, '', 1, 8),
(112, 8, '', 1, 9),
(113, 8, '', 1, 10),
(114, 8, '', 1, 11),
(115, 8, '', 1, 12),
(116, 8, '', 1, 13),
(117, 8, '', 1, 14),
(118, 8, '', 1, 15),
(119, 9, '', 1, 0),
(120, 9, '', 1, 1),
(121, 9, '', 1, 2),
(122, 9, '', 1, 3),
(123, 9, '', 1, 4),
(124, 9, '', 1, 5),
(125, 9, '', 1, 6),
(126, 9, '', 1, 7),
(127, 9, '', 1, 8),
(128, 9, '', 1, 9),
(129, 9, '', 1, 10),
(130, 9, '', 1, 11),
(131, 9, '', 1, 12),
(132, 9, '', 1, 13),
(133, 9, '', 1, 14),
(134, 9, '', 1, 15),
(135, 10, '', 1, 0),
(136, 10, '', 1, 1),
(137, 10, '', 1, 2),
(138, 10, '', 1, 3),
(139, 10, '', 1, 4),
(140, 10, '', 1, 5),
(141, 10, '', 1, 6),
(142, 10, '', 1, 7),
(143, 10, '', 1, 8),
(144, 10, '', 1, 9),
(145, 10, '', 1, 10),
(146, 10, '', 1, 11),
(147, 10, '', 1, 12),
(148, 10, '', 1, 13),
(149, 10, '', 1, 14),
(150, 10, '', 1, 15),
(151, 11, '', 1, 0),
(152, 11, '', 1, 1),
(153, 11, '', 1, 2),
(154, 11, '', 1, 3),
(155, 11, '', 1, 4),
(156, 11, '', 1, 5),
(157, 11, '', 1, 6),
(158, 11, '', 1, 7),
(159, 11, '', 1, 8),
(160, 11, '', 1, 9),
(161, 11, '', 1, 10),
(162, 11, '', 1, 11),
(163, 11, '', 1, 12),
(164, 11, '', 1, 13),
(165, 11, '', 1, 14),
(166, 11, '', 1, 15),
(167, 11, '', 1, 16),
(168, 11, '', 1, 17),
(169, 11, '', 1, 18),
(170, 11, '', 1, 19),
(171, 12, '', 1, 0),
(172, 12, '', 1, 1),
(173, 13, '', 1, 0),
(174, 13, '', 1, 1),
(175, 13, '', 1, 2),
(176, 13, '', 1, 3),
(177, 14, '', 1, 0),
(178, 14, '', 1, 1),
(179, 14, '', 1, 2),
(180, 14, '', 1, 3),
(181, 14, '', 1, 4),
(182, 15, '', 1, 0),
(183, 15, '', 1, 1),
(184, 15, '', 1, 2),
(185, 15, '', 1, 3),
(186, 15, '', 1, 4),
(187, 15, '', 1, 5),
(188, 15, '', 1, 6),
(189, 15, '', 1, 7),
(190, 15, '', 1, 8),
(191, 15, '', 1, 9),
(192, 15, '', 1, 10),
(193, 15, '', 1, 11),
(194, 15, '', 1, 12),
(195, 15, '', 1, 13),
(196, 15, '', 1, 14),
(197, 15, '', 1, 15),
(198, 15, '', 1, 16),
(199, 16, '', 1, 0),
(200, 16, '', 1, 1),
(201, 16, '', 1, 2),
(202, 16, '', 1, 3),
(203, 16, '', 1, 4),
(204, 16, '', 1, 5),
(205, 16, '', 1, 6),
(206, 16, '', 1, 7),
(207, 16, '', 1, 8),
(208, 16, '', 1, 9),
(209, 16, '', 1, 10),
(210, 16, '', 1, 11),
(211, 16, '', 1, 12),
(212, 16, '', 1, 13),
(213, 16, '', 1, 14),
(214, 16, '', 1, 15),
(215, 16, '', 1, 16),
(216, 17, '', 1, 0),
(217, 18, '', 1, 0),
(218, 18, '', 1, 1),
(219, 18, '', 1, 2),
(220, 18, '', 1, 3),
(221, 18, '', 1, 4),
(222, 18, '', 1, 5),
(223, 18, '', 1, 6),
(224, 18, '', 1, 7),
(225, 18, '', 1, 8),
(226, 18, '', 1, 9),
(227, 18, '', 1, 10),
(228, 18, '', 1, 11),
(229, 18, '', 1, 12),
(230, 18, '', 1, 13),
(231, 18, '', 1, 14),
(232, 18, '', 1, 15),
(233, 18, '', 1, 16),
(234, 19, '', 1, 0),
(235, 19, '', 1, 1),
(236, 19, '', 1, 2),
(237, 19, '', 1, 3),
(238, 19, '', 1, 4),
(239, 19, '', 1, 5),
(240, 19, '', 1, 6),
(241, 19, '', 1, 7),
(242, 19, '', 1, 8),
(243, 19, '', 1, 9),
(244, 19, '', 1, 10),
(245, 19, '', 1, 11),
(246, 19, '', 1, 12),
(247, 19, '', 1, 13),
(248, 19, '', 1, 14),
(249, 19, '', 1, 15),
(250, 19, '', 1, 16),
(251, 20, '', 1, 0),
(252, 20, '', 1, 1),
(253, 20, '', 1, 2),
(254, 20, '', 1, 3),
(255, 20, '', 1, 4),
(256, 20, '', 1, 5),
(257, 20, '', 1, 6),
(258, 20, '', 1, 7),
(259, 20, '', 1, 8),
(260, 20, '', 1, 9),
(261, 20, '', 1, 10),
(262, 20, '', 1, 11),
(263, 20, '', 1, 12),
(264, 20, '', 1, 13),
(265, 20, '', 1, 14),
(266, 20, '', 1, 15),
(267, 20, '', 1, 16),
(268, 21, '', 1, 0),
(269, 21, '', 1, 1),
(270, 21, '', 1, 2),
(271, 22, '', 1, 0),
(272, 22, '', 1, 1),
(273, 23, '', 1, 0),
(274, 23, '', 1, 1),
(275, 23, '', 1, 2),
(276, 23, '', 1, 3),
(277, 23, '', 1, 4),
(278, 23, '', 1, 5),
(279, 23, '', 1, 6),
(280, 23, '', 1, 7),
(281, 23, '', 1, 8),
(282, 23, '', 1, 9),
(283, 23, '', 1, 10),
(284, 24, '', 1, 0),
(285, 24, '', 1, 1),
(286, 24, '', 1, 2),
(287, 24, '', 1, 3),
(288, 24, '', 1, 4),
(289, 24, '', 1, 5),
(290, 24, '', 1, 6),
(291, 24, '', 1, 7),
(292, 24, '', 1, 8),
(293, 24, '', 1, 9),
(294, 24, '', 1, 10),
(295, 25, '', 1, 0),
(296, 25, '', 1, 1),
(297, 25, '', 1, 2),
(298, 25, '', 1, 3),
(299, 25, '', 1, 4),
(300, 25, '', 1, 5),
(301, 25, '', 1, 6),
(302, 25, '', 1, 7),
(303, 25, '', 1, 8),
(304, 25, '', 1, 9),
(305, 25, '', 1, 10),
(306, 26, '', 1, 0),
(307, 26, '', 1, 1),
(308, 26, '', 1, 2),
(309, 26, '', 1, 3),
(310, 26, '', 1, 4),
(311, 26, '', 1, 5),
(312, 26, '', 1, 6),
(313, 26, '', 1, 7),
(314, 26, '', 1, 8),
(315, 26, '', 1, 9),
(316, 26, '', 1, 10),
(317, 27, '', 1, 0),
(318, 27, '', 1, 1),
(319, 27, '', 1, 2),
(320, 27, '', 1, 3),
(321, 27, '', 1, 4),
(322, 27, '', 1, 5),
(323, 27, '', 1, 6),
(324, 27, '', 1, 7),
(325, 27, '', 1, 8),
(326, 27, '', 1, 9),
(327, 27, '', 1, 10),
(328, 28, '', 1, 0),
(329, 28, '', 1, 1),
(330, 28, '', 1, 2),
(331, 28, '', 1, 3),
(332, 28, '', 1, 4),
(333, 28, '', 1, 5),
(334, 28, '', 1, 6),
(335, 28, '', 1, 7),
(336, 28, '', 1, 8),
(337, 28, '', 1, 9),
(338, 28, '', 1, 10),
(339, 28, '', 1, 11),
(340, 28, '', 1, 12),
(341, 28, '', 1, 13),
(342, 28, '', 1, 14),
(343, 28, '', 1, 15),
(344, 28, '', 1, 16),
(345, 28, '', 1, 17),
(346, 29, '', 1, 0),
(347, 29, '', 1, 1),
(348, 29, '', 1, 2),
(349, 29, '', 1, 3),
(350, 29, '', 1, 4),
(351, 29, '', 1, 5),
(352, 29, '', 1, 6),
(353, 29, '', 1, 7),
(354, 29, '', 1, 8),
(355, 29, '', 1, 9),
(356, 29, '', 1, 10),
(357, 29, '', 1, 11),
(358, 29, '', 1, 12),
(359, 29, '', 1, 13),
(360, 29, '', 1, 14),
(361, 29, '', 1, 15),
(362, 29, '', 1, 16),
(363, 29, '', 1, 17),
(364, 30, '', 1, 0),
(365, 30, '', 1, 1),
(366, 30, '', 1, 2),
(367, 30, '', 1, 3),
(368, 30, '', 1, 4),
(369, 30, '', 1, 5),
(370, 30, '', 1, 6),
(371, 30, '', 1, 7),
(372, 30, '', 1, 8),
(373, 30, '', 1, 9),
(374, 30, '', 1, 10),
(375, 30, '', 1, 11),
(376, 30, '', 1, 12),
(377, 30, '', 1, 13),
(378, 30, '', 1, 14),
(379, 30, '', 1, 15),
(380, 30, '', 1, 16),
(381, 30, '', 1, 17),
(382, 31, '', 1, 0),
(383, 31, '', 1, 1),
(384, 31, '', 1, 2),
(385, 31, '', 1, 3),
(386, 31, '', 1, 4),
(387, 31, '', 1, 5),
(388, 31, '', 1, 6),
(389, 31, '', 1, 7),
(390, 31, '', 1, 8),
(391, 31, '', 1, 9),
(392, 31, '', 1, 10),
(393, 31, '', 1, 11),
(394, 31, '', 1, 12),
(395, 31, '', 1, 13),
(396, 31, '', 1, 14),
(397, 31, '', 1, 15),
(398, 31, '', 1, 16),
(399, 31, '', 1, 17),
(400, 32, '', 1, 0),
(401, 32, '', 1, 1),
(402, 32, '', 1, 2),
(403, 32, '', 1, 3),
(404, 32, '', 1, 4),
(405, 32, '', 1, 5),
(406, 32, '', 1, 6),
(407, 32, '', 1, 7),
(408, 32, '', 1, 8),
(409, 32, '', 1, 9),
(410, 32, '', 1, 10),
(411, 32, '', 1, 11),
(412, 32, '', 1, 12),
(413, 32, '', 1, 13),
(414, 32, '', 1, 14),
(415, 32, '', 1, 15),
(416, 32, '', 1, 16),
(417, 32, '', 1, 17),
(418, 33, '', 1, 0),
(419, 33, '', 1, 1),
(420, 33, '', 1, 2),
(421, 33, '', 1, 3),
(422, 33, '', 1, 4),
(423, 33, '', 1, 5),
(424, 33, '', 1, 6),
(425, 33, '', 1, 7),
(426, 33, '', 1, 8),
(427, 33, '', 1, 9),
(428, 33, '', 1, 10),
(429, 33, '', 1, 11),
(430, 33, '', 1, 12),
(431, 33, '', 1, 13),
(432, 33, '', 1, 14),
(433, 33, '', 1, 15),
(434, 33, '', 1, 16),
(435, 33, '', 1, 17),
(436, 34, '', 1, 0),
(437, 34, '', 1, 1),
(438, 34, '', 1, 2),
(439, 34, '', 1, 3),
(440, 34, '', 1, 4),
(441, 34, '', 1, 5),
(442, 34, '', 1, 6),
(443, 34, '', 1, 7),
(444, 34, '', 1, 8),
(445, 34, '', 1, 9),
(446, 34, '', 1, 10),
(447, 34, '', 1, 11),
(448, 34, '', 1, 12),
(449, 34, '', 1, 13),
(450, 34, '', 1, 14),
(451, 34, '', 1, 15),
(452, 34, '', 1, 16),
(453, 34, '', 1, 17),
(454, 35, '', 1, 0),
(455, 35, '', 1, 1),
(456, 35, '', 1, 2),
(457, 35, '', 1, 3),
(458, 35, '', 1, 4),
(459, 35, '', 1, 5),
(460, 35, '', 1, 6),
(461, 35, '', 1, 7),
(462, 35, '', 1, 8),
(463, 35, '', 1, 9),
(464, 35, '', 1, 10),
(465, 35, '', 1, 11),
(466, 35, '', 1, 12),
(467, 35, '', 1, 13),
(468, 35, '', 1, 14),
(469, 35, '', 1, 15),
(470, 35, '', 1, 16),
(471, 35, '', 1, 17);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_option_value_characteristic`
--

CREATE TABLE `oc_option_value_characteristic` (
  `option_value_characteristic_id` int(11) NOT NULL,
  `option_value_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `characteristic_id` int(11) NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `serialized` tinyint(1) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_option_value_description`
--

CREATE TABLE `oc_option_value_description` (
  `option_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_option_value_description`
--

INSERT INTO `oc_option_value_description` (`option_value_id`, `language_id`, `option_id`, `name`) VALUES
(1, 1, 1, 'Грязно розовый / One Size'),
(2, 1, 1, 'Черный / One Size'),
(3, 1, 1, 'Синий / One Size'),
(4, 1, 1, 'Охра / One Size'),
(5, 1, 1, 'Берюзовый / One Size'),
(6, 1, 1, 'Светло серый / One Size'),
(7, 1, 1, 'Бежевый / One Size'),
(8, 1, 1, 'Лиловый / One Size'),
(9, 1, 1, 'Голубой / One Size'),
(10, 1, 1, 'Мятный / One Size'),
(11, 1, 1, 'Темно зеленый / One Size'),
(12, 1, 1, 'Серый / One Size'),
(13, 1, 1, 'Фуксия / One Size'),
(14, 1, 2, 'Берюзовый / One Size'),
(15, 1, 2, 'Бежевый / One Size'),
(16, 1, 2, 'Фуксия / One Size'),
(17, 1, 2, 'Лиловый / One Size'),
(18, 1, 2, 'Грязно розовый / One Size'),
(19, 1, 2, 'Светло серый / One Size'),
(20, 1, 2, 'Охра / One Size'),
(21, 1, 2, 'Черный / One Size'),
(22, 1, 2, 'Мятный / One Size'),
(23, 1, 2, 'Темно зеленый / One Size'),
(24, 1, 2, 'Серый / One Size'),
(25, 1, 2, 'Синий / One Size'),
(26, 1, 2, 'Голубой / One Size'),
(27, 1, 3, 'Серый / One Size'),
(28, 1, 3, 'Синий / One Size'),
(29, 1, 3, 'Грязно розовый / One Size'),
(30, 1, 3, 'Черный / One Size'),
(31, 1, 3, 'Мятный / One Size'),
(32, 1, 3, 'Темно зеленый / One Size'),
(33, 1, 3, 'Светло серый / One Size'),
(34, 1, 3, 'Охра / One Size'),
(35, 1, 3, 'Лиловый / One Size'),
(36, 1, 3, 'Фуксия / One Size'),
(37, 1, 3, 'Бежевый / One Size'),
(38, 1, 3, 'Берюзовый / One Size'),
(39, 1, 3, 'Голубой / One Size'),
(40, 1, 4, 'Черный / One Size'),
(41, 1, 4, 'Берюзовый  / One Size'),
(42, 1, 4, 'Лиловый / One Size'),
(43, 1, 4, 'Светло серый / One Size'),
(44, 1, 4, 'Фуксия / One Size'),
(45, 1, 4, 'Охра / One Size'),
(46, 1, 4, 'Мятный / One Size'),
(47, 1, 4, 'Серый / One Size'),
(48, 1, 4, 'Голубой / One Size'),
(49, 1, 4, 'Синий / One Size'),
(50, 1, 4, 'Грязно розовый / One Size'),
(51, 1, 4, 'Бежевый / One Size'),
(52, 1, 4, 'Темно зеленый / One Size'),
(53, 1, 5, 'Светло серый / One Size'),
(54, 1, 5, 'Мятный / One Size'),
(55, 1, 5, 'Темно зеленый / One Size'),
(56, 1, 5, 'Охра / One Size'),
(57, 1, 5, 'Берюзовый / One Size'),
(58, 1, 5, 'Синий / One Size'),
(59, 1, 5, 'Бежевый / One Size'),
(60, 1, 5, 'Черный / One Size'),
(61, 1, 5, 'Серый / One Size'),
(62, 1, 5, 'Голубой / One Size'),
(63, 1, 5, 'Лиловый / One Size'),
(64, 1, 5, 'Фуксия / One Size'),
(65, 1, 5, 'Грязно розовый / One Size'),
(66, 1, 6, 'Лиловый / One Size'),
(67, 1, 6, 'Бордовый / One Size'),
(68, 1, 6, 'Розовый / One Size'),
(69, 1, 6, 'Серый / One Size'),
(70, 1, 6, 'Темно синий / One Size'),
(71, 1, 6, 'Берюзовый / One Size'),
(72, 1, 6, 'Темно зеленый / One Size'),
(73, 1, 6, 'Грязно розовый / One Size'),
(74, 1, 6, 'Нежно розовый / One Size'),
(75, 1, 6, 'Красный / One Size'),
(76, 1, 6, 'Фиолетовый / One Size'),
(77, 1, 6, 'Оранжевый / One Size'),
(78, 1, 6, 'Кирпичный / One Size'),
(79, 1, 6, 'Хаки / One Size'),
(80, 1, 6, 'Синий / One Size'),
(81, 1, 6, 'Мятный / One Size'),
(82, 1, 6, 'Черный / One Size'),
(83, 1, 6, 'Фуксия / One Size'),
(84, 1, 6, 'Голубой / One Size'),
(85, 1, 6, 'Золотистый / One Size'),
(86, 1, 6, 'Желтый / One Size'),
(87, 1, 7, 'Светло серый / One Size'),
(88, 1, 7, 'Розовый / One Size'),
(89, 1, 7, 'Мятный / One Size'),
(90, 1, 7, 'Лиловый / One Size'),
(91, 1, 7, 'Фиолетовый / One Size'),
(92, 1, 7, 'Серый / One Size'),
(93, 1, 7, 'Кирпичный / One Size'),
(94, 1, 7, 'Золотистый / One Size'),
(95, 1, 7, 'Нежно розовый / One Size'),
(96, 1, 7, 'Белый / One Size'),
(97, 1, 7, 'Черный / One Size'),
(98, 1, 7, 'Темно синий / One Size'),
(99, 1, 7, 'Фуксия / One Size'),
(100, 1, 7, 'Красный / One Size'),
(101, 1, 7, 'Бордовый / One Size'),
(102, 1, 7, 'Синий / One Size'),
(103, 1, 8, 'Фиолетовый / One Size'),
(104, 1, 8, 'Бордовый / One Size'),
(105, 1, 8, 'Темно синий / One Size'),
(106, 1, 8, 'Розовый / One Size'),
(107, 1, 8, 'Красный / One Size'),
(108, 1, 8, 'Светло серый / One Size'),
(109, 1, 8, 'Золотистый / One Size'),
(110, 1, 8, 'Синий / One Size'),
(111, 1, 8, 'Лиловый / One Size'),
(112, 1, 8, 'Нежно розовый / One Size'),
(113, 1, 8, 'Серый / One Size'),
(114, 1, 8, 'Мятный / One Size'),
(115, 1, 8, 'Черный / One Size'),
(116, 1, 8, 'Белый / One Size'),
(117, 1, 8, 'Фуксия / One Size'),
(118, 1, 8, 'Кирпичный / One Size'),
(119, 1, 9, 'Лиловый / One Size'),
(120, 1, 9, 'Темно синий / One Size'),
(121, 1, 9, 'Белый / One Size'),
(122, 1, 9, 'Красный / One Size'),
(123, 1, 9, 'Розовый / One Size'),
(124, 1, 9, 'Черный  / One Size'),
(125, 1, 9, 'Светло серый / One Size'),
(126, 1, 9, 'Фуксия / One Size'),
(127, 1, 9, 'Нежно розовый / One Size'),
(128, 1, 9, 'Синий / One Size'),
(129, 1, 9, 'Бордовый / One Size'),
(130, 1, 9, 'Мятный / One Size'),
(131, 1, 9, 'Кирпичный / One Size'),
(132, 1, 9, 'Золотистый  / One Size'),
(133, 1, 9, 'Фиолетовый / One Size'),
(134, 1, 9, 'Серый / One Size'),
(135, 1, 10, 'Бордовый / One Size'),
(136, 1, 10, 'Фиолетовый / One Size'),
(137, 1, 10, 'Лиловый / One Size'),
(138, 1, 10, 'Кирпичный / One Size'),
(139, 1, 10, 'Красный / One Size'),
(140, 1, 10, 'Светло серый / One Size'),
(141, 1, 10, 'Темно синий / One Size'),
(142, 1, 10, 'Мятный / One Size'),
(143, 1, 10, 'Синий / One Size'),
(144, 1, 10, 'Золотистый / One Size'),
(145, 1, 10, 'Нежно розовый / One Size'),
(146, 1, 10, 'Розовый / One Size'),
(147, 1, 10, 'Серый / One Size'),
(148, 1, 10, 'Черный / One Size'),
(149, 1, 10, 'Фуксия / One Size'),
(150, 1, 10, 'Белый / One Size'),
(151, 1, 11, 'Фиолетовый / One Size'),
(152, 1, 11, 'Кирпичный / One Size'),
(153, 1, 11, 'Лиловый / One Size'),
(154, 1, 11, 'Капучино / One Size'),
(155, 1, 11, 'Золотистый / One Size'),
(156, 1, 11, 'Красный / One Size'),
(157, 1, 11, 'Розовый / One Size'),
(158, 1, 11, 'Бордовый / One Size'),
(159, 1, 11, 'Белый / One Size'),
(160, 1, 11, 'Охра / One Size'),
(161, 1, 11, 'Персиковый / One Size'),
(162, 1, 11, 'Нежно розовый / One Size'),
(163, 1, 11, 'Синий / One Size'),
(164, 1, 11, 'Темно синий / One Size'),
(165, 1, 11, 'Черный / One Size'),
(166, 1, 11, 'Фуксия / One Size'),
(167, 1, 11, 'Светло серый / One Size'),
(168, 1, 11, 'Мятный / One Size'),
(169, 1, 11, 'Принт / One Size'),
(170, 1, 11, 'Серый / One Size'),
(171, 1, 12, 'Принт оранжевый / One Size'),
(172, 1, 12, 'Принт синий / One Size'),
(173, 1, 13, 'Лиловый / One Size'),
(174, 1, 13, 'Черный / One Size'),
(175, 1, 13, 'Розовый / One Size'),
(176, 1, 13, 'Мятный / One Size'),
(177, 1, 14, 'Белый / One Size'),
(178, 1, 14, 'Черный / One Size'),
(179, 1, 14, 'Мятный / One Size'),
(180, 1, 14, 'Персиковый / One Size'),
(181, 1, 14, 'Хаки / One Size'),
(182, 1, 15, 'Лиловый / One Size'),
(183, 1, 15, 'Серый / One Size'),
(184, 1, 15, 'Бордовый / One Size'),
(185, 1, 15, 'Белый / One Size'),
(186, 1, 15, 'Темно фуксия / One Size'),
(187, 1, 15, 'Фиолетовый / One Size'),
(188, 1, 15, 'Светло серый / One Size'),
(189, 1, 15, 'Темно синий / One Size'),
(190, 1, 15, 'Мятный / One Size'),
(191, 1, 15, 'Золотистый / One Size'),
(192, 1, 15, 'Розовый / One Size'),
(193, 1, 15, 'Фуксия / One Size'),
(194, 1, 15, 'Нежно розовый / One Size'),
(195, 1, 15, 'Кирпичный / One Size'),
(196, 1, 15, 'Синий / One Size'),
(197, 1, 15, 'Черный / One Size'),
(198, 1, 15, 'Красный / One Size'),
(199, 1, 16, 'Лиловый / One Size'),
(200, 1, 16, 'Темно синий / One Size'),
(201, 1, 16, 'Бордовый / One Size'),
(202, 1, 16, 'Красный / One Size'),
(203, 1, 16, 'Светло серый / One Size'),
(204, 1, 16, 'Мятный / One Size'),
(205, 1, 16, 'Темно фуксия / One Size'),
(206, 1, 16, 'Белый / One Size'),
(207, 1, 16, 'Нежно розовый / One Size'),
(208, 1, 16, 'Синий / One Size'),
(209, 1, 16, 'Розовый / One Size'),
(210, 1, 16, 'Черный / One Size'),
(211, 1, 16, 'Кирпичный / One Size'),
(212, 1, 16, 'Фуксия / One Size'),
(213, 1, 16, 'Фиолетовый / One Size'),
(214, 1, 16, 'Золотистый / One Size'),
(215, 1, 16, 'Серый / One Size'),
(216, 1, 17, ' / One Size'),
(217, 1, 18, 'Серый / One Size'),
(218, 1, 18, 'Кирпичный / One Size'),
(219, 1, 18, 'Мятный / One Size'),
(220, 1, 18, 'Фиолетовый / One Size'),
(221, 1, 18, 'Золотистый / One Size'),
(222, 1, 18, 'Черный / One Size'),
(223, 1, 18, 'Нежно розовый / One Size'),
(224, 1, 18, 'Темно синий / One Size'),
(225, 1, 18, 'Красный / One Size'),
(226, 1, 18, 'Светло серый / One Size'),
(227, 1, 18, 'Розовый / One Size'),
(228, 1, 18, 'Лиловый / One Size'),
(229, 1, 18, 'Темно фуксия / One Size'),
(230, 1, 18, 'Белый / One Size'),
(231, 1, 18, 'Бордовый / One Size'),
(232, 1, 18, 'Синий / One Size'),
(233, 1, 18, 'Фуксия / One Size'),
(234, 1, 19, 'Белый / One Size'),
(235, 1, 19, 'Золотистый / One Size'),
(236, 1, 19, 'Кирпичный / One Size'),
(237, 1, 19, 'Бордовый / One Size'),
(238, 1, 19, 'Нежно розовый / One Size'),
(239, 1, 19, 'Мятный / One Size'),
(240, 1, 19, 'Темно фуксия / One Size'),
(241, 1, 19, 'Красный / One Size'),
(242, 1, 19, 'Серый / One Size'),
(243, 1, 19, 'Розовый / One Size'),
(244, 1, 19, 'Темно синий / One Size'),
(245, 1, 19, 'Светло серый / One Size'),
(246, 1, 19, 'Фуксия / One Size'),
(247, 1, 19, 'Фиолетовый / One Size'),
(248, 1, 19, 'Синий / One Size'),
(249, 1, 19, 'Лиловый / One Size'),
(250, 1, 19, 'Черный / One Size'),
(251, 1, 20, 'Лиловый / One Size'),
(252, 1, 20, 'Темно синий / One Size'),
(253, 1, 20, 'Розовый / One Size'),
(254, 1, 20, 'Светло серый / One Size'),
(255, 1, 20, 'Золотистый / One Size'),
(256, 1, 20, 'Фиолетовый / One Size'),
(257, 1, 20, 'Синий / One Size'),
(258, 1, 20, 'Бордовый / One Size'),
(259, 1, 20, 'Мятный / One Size'),
(260, 1, 20, 'Черный / One Size'),
(261, 1, 20, 'Фуксия / One Size'),
(262, 1, 20, 'Темно фуксия / One Size'),
(263, 1, 20, 'Нежно розовый / One Size'),
(264, 1, 20, 'Серый / One Size'),
(265, 1, 20, 'Красный / One Size'),
(266, 1, 20, 'Белый / One Size'),
(267, 1, 20, 'Кирпичный / One Size'),
(268, 1, 21, ' / One Size'),
(269, 1, 21, 'Синий / One Size'),
(270, 1, 21, 'Коричневый / One Size'),
(271, 1, 22, 'Черный гуси / One Size'),
(272, 1, 22, 'Мятный / One Size'),
(273, 1, 23, 'Бежевый / One Size'),
(274, 1, 23, 'Молочный / One Size'),
(275, 1, 23, 'Бордовый / One Size'),
(276, 1, 23, 'Хаки / One Size'),
(277, 1, 23, 'Розовый / One Size'),
(278, 1, 23, 'Берюзовый / One Size'),
(279, 1, 23, 'Синий / One Size'),
(280, 1, 23, 'Черный / One Size'),
(281, 1, 23, 'Белый / One Size'),
(282, 1, 23, 'Голубой / One Size'),
(283, 1, 23, 'Оранжевый / One Size'),
(284, 1, 24, 'Черный / One Size'),
(285, 1, 24, 'Молочный / One Size'),
(286, 1, 24, 'Бордовый / One Size'),
(287, 1, 24, 'Берюзовый / One Size'),
(288, 1, 24, 'Бежевый / One Size'),
(289, 1, 24, 'Белый / One Size'),
(290, 1, 24, 'Синий / One Size'),
(291, 1, 24, 'Хаки / One Size'),
(292, 1, 24, 'Розовый / One Size'),
(293, 1, 24, 'Голубой / One Size'),
(294, 1, 24, 'Оранжевый / One Size'),
(295, 1, 25, 'Молочный / One Size'),
(296, 1, 25, 'Голубой / One Size'),
(297, 1, 25, 'Розовый / One Size'),
(298, 1, 25, 'Бордовый / One Size'),
(299, 1, 25, 'Белый / One Size'),
(300, 1, 25, 'Бежевый / One Size'),
(301, 1, 25, 'Хаки / One Size'),
(302, 1, 25, 'Оранжевый / One Size'),
(303, 1, 25, 'Черный / One Size'),
(304, 1, 25, 'Берюзовый / One Size'),
(305, 1, 25, 'Синий / One Size'),
(306, 1, 26, 'Молочный / One Size'),
(307, 1, 26, 'Белый / One Size'),
(308, 1, 26, 'Бордовый / One Size'),
(309, 1, 26, 'Оранжевый / One Size'),
(310, 1, 26, 'Бежевый / One Size'),
(311, 1, 26, 'Розовый / One Size'),
(312, 1, 26, 'Хаки / One Size'),
(313, 1, 26, 'Голубой / One Size'),
(314, 1, 26, 'Черный / One Size'),
(315, 1, 26, 'Берюзовый / One Size'),
(316, 1, 26, 'Синий / One Size'),
(317, 1, 27, 'Хаки / One Size'),
(318, 1, 27, 'Голубой / One Size'),
(319, 1, 27, 'Молочный / One Size'),
(320, 1, 27, 'Бежевый / One Size'),
(321, 1, 27, 'Берюзовый / One Size'),
(322, 1, 27, 'Розовый / One Size'),
(323, 1, 27, 'Синий / One Size'),
(324, 1, 27, 'Оранжевый / One Size'),
(325, 1, 27, 'Белый / One Size'),
(326, 1, 27, 'Бордовый / One Size'),
(327, 1, 27, 'Черный / One Size'),
(328, 1, 28, 'Золотой / One Size'),
(329, 1, 28, 'Зеленый / One Size'),
(330, 1, 28, 'Черный / One Size'),
(331, 1, 28, 'Коричневый / One Size'),
(332, 1, 28, 'Синий / One Size'),
(333, 1, 28, 'Темно синий / One Size'),
(334, 1, 28, 'Бордовый / One Size'),
(335, 1, 28, 'Голубой / One Size'),
(336, 1, 28, 'Хаки / One Size'),
(337, 1, 28, 'Нежно розовый / One Size'),
(338, 1, 28, 'Темно серый / One Size'),
(339, 1, 28, 'Капучино / One Size'),
(340, 1, 28, 'Серый / One Size'),
(341, 1, 28, 'Фиолетовый / One Size'),
(342, 1, 28, 'Вишневый / One Size'),
(343, 1, 28, 'Розовый / One Size'),
(344, 1, 28, 'Серо-голубой / One Size'),
(345, 1, 28, 'Лиловый / One Size'),
(346, 1, 29, 'Нежно розовый / One Size'),
(347, 1, 29, 'Золотой / One Size'),
(348, 1, 29, 'Темно серый / One Size'),
(349, 1, 29, 'Черный / One Size'),
(350, 1, 29, 'Синий / One Size'),
(351, 1, 29, 'Фиолетовый / One Size'),
(352, 1, 29, 'Зеленый / One Size'),
(353, 1, 29, 'Бордовый / One Size'),
(354, 1, 29, 'Капучино / One Size'),
(355, 1, 29, 'Серый / One Size'),
(356, 1, 29, 'Розовый / One Size'),
(357, 1, 29, 'Голубой / One Size'),
(358, 1, 29, 'Темно синий / One Size'),
(359, 1, 29, 'Лиловый / One Size'),
(360, 1, 29, 'Коричневый / One Size'),
(361, 1, 29, 'Вишневый / One Size'),
(362, 1, 29, 'Хаки / One Size'),
(363, 1, 29, 'Серо-голубой / One Size'),
(364, 1, 30, 'Серо-голубой / One Size'),
(365, 1, 30, 'Темно серый / One Size'),
(366, 1, 30, 'Синий / One Size'),
(367, 1, 30, 'Вишневый / One Size'),
(368, 1, 30, 'Серый / One Size'),
(369, 1, 30, 'Коричневый / One Size'),
(370, 1, 30, 'Темно синий / One Size'),
(371, 1, 30, 'Золотой / One Size'),
(372, 1, 30, 'Зеленый / One Size'),
(373, 1, 30, 'Черный / One Size'),
(374, 1, 30, 'Розовый / One Size'),
(375, 1, 30, 'Бордовый / One Size'),
(376, 1, 30, 'Хаки / One Size'),
(377, 1, 30, 'Лиловый / One Size'),
(378, 1, 30, 'Голубой / One Size'),
(379, 1, 30, 'Фиолетовый / One Size'),
(380, 1, 30, 'Капучино / One Size'),
(381, 1, 30, 'Нежно розовый / One Size'),
(382, 1, 31, 'Коричневый / One Size'),
(383, 1, 31, 'Нежно розовый / One Size'),
(384, 1, 31, 'Золотой / One Size'),
(385, 1, 31, 'Серо-голубой / One Size'),
(386, 1, 31, 'Черный / One Size'),
(387, 1, 31, 'Фиолетовый / One Size'),
(388, 1, 31, 'Вишневый / One Size'),
(389, 1, 31, 'Темно синий / One Size'),
(390, 1, 31, 'Синий / One Size'),
(391, 1, 31, 'Лиловый / One Size'),
(392, 1, 31, 'Капучино / One Size'),
(393, 1, 31, 'Серый / One Size'),
(394, 1, 31, 'Хаки / One Size'),
(395, 1, 31, 'Розовый / One Size'),
(396, 1, 31, 'Бордовый / One Size'),
(397, 1, 31, 'Зеленый / One Size'),
(398, 1, 31, 'Голубой / One Size'),
(399, 1, 31, 'Темно серый / One Size'),
(400, 1, 32, 'Черный / One Size'),
(401, 1, 32, 'Вишневый / One Size'),
(402, 1, 32, 'Розовый / One Size'),
(403, 1, 32, 'Серо-голубой / One Size'),
(404, 1, 32, 'Голубой / One Size'),
(405, 1, 32, 'Бордовый / One Size'),
(406, 1, 32, 'Лиловый / One Size'),
(407, 1, 32, 'Золотой / One Size'),
(408, 1, 32, 'Темно синий / One Size'),
(409, 1, 32, 'Серый / One Size'),
(410, 1, 32, 'Темно серый / One Size'),
(411, 1, 32, 'Фиолетовый / One Size'),
(412, 1, 32, 'Зеленый / One Size'),
(413, 1, 32, 'Капучино / One Size'),
(414, 1, 32, 'Синий / One Size'),
(415, 1, 32, 'Нежно розовый / One Size'),
(416, 1, 32, 'Коричневый / One Size'),
(417, 1, 32, 'Хаки / One Size'),
(418, 1, 33, 'Темно серый / One Size'),
(419, 1, 33, 'Фиолетовый / One Size'),
(420, 1, 33, 'Розовый / One Size'),
(421, 1, 33, 'Лиловый / One Size'),
(422, 1, 33, 'Серый / One Size'),
(423, 1, 33, 'Золотой / One Size'),
(424, 1, 33, 'Хаки / One Size'),
(425, 1, 33, 'Голубой / One Size'),
(426, 1, 33, 'Серо-голубой / One Size'),
(427, 1, 33, 'Синий / One Size'),
(428, 1, 33, 'Зеленый / One Size'),
(429, 1, 33, 'Капучино / One Size'),
(430, 1, 33, 'Бордовый / One Size'),
(431, 1, 33, 'Черный / One Size'),
(432, 1, 33, 'Вишневый / One Size'),
(433, 1, 33, 'Коричневый / One Size'),
(434, 1, 33, 'Темно синий / One Size'),
(435, 1, 33, 'Нежно розовый / One Size'),
(436, 1, 34, 'Голубой / One Size'),
(437, 1, 34, 'Хаки / One Size'),
(438, 1, 34, 'Бордовый / One Size'),
(439, 1, 34, 'Розовый / One Size'),
(440, 1, 34, 'Вишневый / One Size'),
(441, 1, 34, 'Золотой / One Size'),
(442, 1, 34, 'Черный / One Size'),
(443, 1, 34, 'Зеленый / One Size'),
(444, 1, 34, 'Темно серый / One Size'),
(445, 1, 34, 'Серо-голубой / One Size'),
(446, 1, 34, 'Капучино / One Size'),
(447, 1, 34, 'Синий / One Size'),
(448, 1, 34, 'Коричневый / One Size'),
(449, 1, 34, 'Серый / One Size'),
(450, 1, 34, 'Лиловый / One Size'),
(451, 1, 34, 'Нежно розовый / One Size'),
(452, 1, 34, 'Фиолетовый / One Size'),
(453, 1, 34, 'Темно синий / One Size'),
(454, 1, 35, 'Синий / One Size'),
(455, 1, 35, 'Коричневый / One Size'),
(456, 1, 35, 'Темно серый / One Size'),
(457, 1, 35, 'Зеленый / One Size'),
(458, 1, 35, 'Фиолетовый / One Size'),
(459, 1, 35, 'Вишневый / One Size'),
(460, 1, 35, 'Черный / One Size'),
(461, 1, 35, 'Капучино / One Size'),
(462, 1, 35, 'Голубой / One Size'),
(463, 1, 35, 'Серо-голубой / One Size'),
(464, 1, 35, 'Лиловый / One Size'),
(465, 1, 35, 'Хаки / One Size'),
(466, 1, 35, 'Розовый / One Size'),
(467, 1, 35, 'Серый / One Size'),
(468, 1, 35, 'Бордовый / One Size'),
(469, 1, 35, 'Нежно розовый / One Size'),
(470, 1, 35, 'Золотой / One Size'),
(471, 1, 35, 'Темно синий / One Size');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order`
--

CREATE TABLE `oc_order` (
  `order_id` int(11) NOT NULL,
  `invoice_no` int(11) NOT NULL DEFAULT 0,
  `invoice_prefix` varchar(26) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT 0,
  `store_name` varchar(64) NOT NULL,
  `store_url` varchar(255) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT 0,
  `customer_group_id` int(11) NOT NULL DEFAULT 0,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `custom_field` text NOT NULL,
  `payment_firstname` varchar(32) NOT NULL,
  `payment_lastname` varchar(32) NOT NULL,
  `payment_company` varchar(60) NOT NULL,
  `payment_address_1` varchar(128) NOT NULL,
  `payment_address_2` varchar(128) NOT NULL,
  `payment_city` varchar(128) NOT NULL,
  `payment_postcode` varchar(10) NOT NULL,
  `payment_country` varchar(128) NOT NULL,
  `payment_country_id` int(11) NOT NULL,
  `payment_zone` varchar(128) NOT NULL,
  `payment_zone_id` int(11) NOT NULL,
  `payment_address_format` text NOT NULL,
  `payment_custom_field` text NOT NULL,
  `payment_method` varchar(128) NOT NULL,
  `payment_code` varchar(128) NOT NULL,
  `shipping_firstname` varchar(32) NOT NULL,
  `shipping_lastname` varchar(32) NOT NULL,
  `shipping_company` varchar(40) NOT NULL,
  `shipping_address_1` varchar(128) NOT NULL,
  `shipping_address_2` varchar(128) NOT NULL,
  `shipping_city` varchar(128) NOT NULL,
  `shipping_postcode` varchar(10) NOT NULL,
  `shipping_country` varchar(128) NOT NULL,
  `shipping_country_id` int(11) NOT NULL,
  `shipping_zone` varchar(128) NOT NULL,
  `shipping_zone_id` int(11) NOT NULL,
  `shipping_address_format` text NOT NULL,
  `shipping_custom_field` text NOT NULL,
  `shipping_method` varchar(128) NOT NULL,
  `shipping_code` varchar(128) NOT NULL,
  `comment` text NOT NULL,
  `total` decimal(15,4) NOT NULL DEFAULT 0.0000,
  `order_status_id` int(11) NOT NULL DEFAULT 0,
  `affiliate_id` int(11) NOT NULL,
  `commission` decimal(15,4) NOT NULL,
  `marketing_id` int(11) NOT NULL,
  `tracking` varchar(64) NOT NULL,
  `language_id` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `currency_code` varchar(3) NOT NULL,
  `currency_value` decimal(15,8) NOT NULL DEFAULT 1.00000000,
  `ip` varchar(40) NOT NULL,
  `forwarded_ip` varchar(40) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `accept_language` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order_history`
--

CREATE TABLE `oc_order_history` (
  `order_history_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL DEFAULT 0,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order_option`
--

CREATE TABLE `oc_order_option` (
  `order_option_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_product_id` int(11) NOT NULL,
  `product_option_id` int(11) NOT NULL,
  `product_option_value_id` int(11) NOT NULL DEFAULT 0,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `type` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order_product`
--

CREATE TABLE `oc_order_product` (
  `order_product_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `model` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `price` decimal(15,4) NOT NULL DEFAULT 0.0000,
  `total` decimal(15,4) NOT NULL DEFAULT 0.0000,
  `tax` decimal(15,4) NOT NULL DEFAULT 0.0000,
  `reward` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order_recurring`
--

CREATE TABLE `oc_order_recurring` (
  `order_recurring_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `recurring_name` varchar(255) NOT NULL,
  `recurring_description` varchar(255) NOT NULL,
  `recurring_frequency` varchar(25) NOT NULL,
  `recurring_cycle` smallint(6) NOT NULL,
  `recurring_duration` smallint(6) NOT NULL,
  `recurring_price` decimal(10,4) NOT NULL,
  `trial` tinyint(1) NOT NULL,
  `trial_frequency` varchar(25) NOT NULL,
  `trial_cycle` smallint(6) NOT NULL,
  `trial_duration` smallint(6) NOT NULL,
  `trial_price` decimal(10,4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order_recurring_transaction`
--

CREATE TABLE `oc_order_recurring_transaction` (
  `order_recurring_transaction_id` int(11) NOT NULL,
  `order_recurring_id` int(11) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `amount` decimal(10,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order_shipment`
--

CREATE TABLE `oc_order_shipment` (
  `order_shipment_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `shipping_courier_id` varchar(255) NOT NULL DEFAULT '',
  `tracking_number` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order_status`
--

CREATE TABLE `oc_order_status` (
  `order_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_order_status`
--

INSERT INTO `oc_order_status` (`order_status_id`, `language_id`, `name`) VALUES
(1, 1, 'Ожидание'),
(1, 2, 'Pending'),
(2, 1, 'В обработке'),
(2, 2, 'Processing'),
(3, 1, 'Доставлено'),
(3, 2, 'Shipped'),
(5, 1, 'Сделка завершена'),
(5, 2, 'Complete'),
(7, 1, 'Отменено'),
(7, 2, 'Canceled'),
(8, 1, 'Возврат'),
(8, 2, 'Denied'),
(9, 1, 'Отмена и аннулирование'),
(9, 2, 'Canceled Reversal'),
(10, 1, 'Неудавшийся'),
(10, 2, 'Failed'),
(11, 1, 'Возмещенный'),
(11, 2, 'Refunded'),
(12, 1, 'Полностью измененный'),
(12, 2, 'Reversed'),
(13, 1, 'Полный возврат'),
(13, 2, 'Chargeback'),
(14, 1, 'Истекло'),
(14, 2, 'Expired'),
(15, 1, 'Обработано'),
(15, 2, 'Processed'),
(16, 1, 'Анулированный'),
(16, 2, 'Voided');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order_total`
--

CREATE TABLE `oc_order_total` (
  `order_total_id` int(10) NOT NULL,
  `order_id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `value` decimal(15,4) NOT NULL DEFAULT 0.0000,
  `sort_order` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order_to_sdek`
--

CREATE TABLE `oc_order_to_sdek` (
  `order_to_sdek_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `cityId` int(11) NOT NULL,
  `pvz_code` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order_voucher`
--

CREATE TABLE `oc_order_voucher` (
  `order_voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `code` varchar(10) NOT NULL,
  `from_name` varchar(64) NOT NULL,
  `from_email` varchar(96) NOT NULL,
  `to_name` varchar(64) NOT NULL,
  `to_email` varchar(96) NOT NULL,
  `voucher_theme_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `amount` decimal(15,4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product`
--

CREATE TABLE `oc_product` (
  `product_id` int(11) NOT NULL,
  `model` varchar(64) NOT NULL,
  `sku` varchar(64) NOT NULL,
  `upc` varchar(12) NOT NULL,
  `ean` varchar(14) NOT NULL,
  `jan` varchar(13) NOT NULL,
  `isbn` varchar(17) NOT NULL,
  `mpn` varchar(64) NOT NULL,
  `location` varchar(128) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT 0,
  `stock_status_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `shipping` tinyint(1) NOT NULL DEFAULT 1,
  `price` decimal(15,4) NOT NULL DEFAULT 0.0000,
  `points` int(8) NOT NULL DEFAULT 0,
  `tax_class_id` int(11) NOT NULL,
  `date_available` date NOT NULL DEFAULT '0000-00-00',
  `weight` decimal(15,8) NOT NULL DEFAULT 0.00000000,
  `weight_class_id` int(11) NOT NULL DEFAULT 0,
  `length` decimal(15,8) NOT NULL DEFAULT 0.00000000,
  `width` decimal(15,8) NOT NULL DEFAULT 0.00000000,
  `height` decimal(15,8) NOT NULL DEFAULT 0.00000000,
  `length_class_id` int(11) NOT NULL DEFAULT 0,
  `measure_name` varchar(255) NOT NULL DEFAULT '',
  `subtract` tinyint(1) NOT NULL DEFAULT 1,
  `minimum` int(11) NOT NULL DEFAULT 1,
  `sort_order` int(11) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `viewed` int(5) NOT NULL DEFAULT 0,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `noindex` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_product`
--

INSERT INTO `oc_product` (`product_id`, `model`, `sku`, `upc`, `ean`, `jan`, `isbn`, `mpn`, `location`, `quantity`, `stock_status_id`, `image`, `manufacturer_id`, `shipping`, `price`, `points`, `tax_class_id`, `date_available`, `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `measure_name`, `subtract`, `minimum`, `sort_order`, `status`, `viewed`, `date_added`, `date_modified`, `noindex`) VALUES
(50, '1001', '1001', '', '', '', '', '', '', 0, 5, '', 0, 1, '0.0000', 0, 0, '2021-09-13', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 'шт', 0, 1, 0, 1, 0, '2021-09-13 13:30:02', '2021-09-14 13:30:02', 1),
(51, '1002', '1002', '', '', '', '', '', '', 0, 5, '', 0, 1, '0.0000', 0, 0, '2021-09-13', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 'шт', 0, 1, 0, 1, 0, '2021-09-13 13:30:02', '2021-09-14 13:30:02', 1),
(52, '1003', '1003', '', '', '', '', '', '', 0, 5, '', 0, 1, '0.0000', 0, 0, '2021-09-13', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 'шт', 0, 1, 0, 1, 0, '2021-09-13 13:30:02', '2021-09-14 13:30:02', 1),
(53, '1004', '1004', '', '', '', '', '', '', 0, 5, '', 0, 1, '0.0000', 0, 0, '2021-09-13', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 'шт', 0, 1, 0, 1, 0, '2021-09-13 13:30:02', '2021-09-14 13:30:02', 1),
(54, '1005', '1005', '', '', '', '', '', '', 0, 5, '', 0, 1, '0.0000', 0, 0, '2021-09-13', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 'шт', 0, 1, 0, 1, 0, '2021-09-13 13:30:02', '2021-09-14 13:30:02', 1),
(55, '1006', '1006', '', '', '', '', '', '', 0, 5, '', 0, 1, '0.0000', 0, 0, '2021-09-13', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 'шт', 0, 1, 0, 1, 0, '2021-09-13 13:30:02', '2021-09-14 13:30:02', 1),
(56, '1007', '1007', '', '', '', '', '', '', 0, 5, '', 0, 1, '0.0000', 0, 0, '2021-09-13', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 'шт', 0, 1, 0, 1, 0, '2021-09-13 13:30:02', '2021-09-14 13:30:02', 1),
(57, '1008', '1008', '', '', '', '', '', '', 0, 5, '', 0, 1, '0.0000', 0, 0, '2021-09-13', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 'шт', 0, 1, 0, 1, 0, '2021-09-13 13:30:02', '2021-09-14 13:30:02', 1),
(58, '1009', '1009', '', '', '', '', '', '', 0, 5, '', 0, 1, '0.0000', 0, 0, '2021-09-13', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 'шт', 0, 1, 0, 1, 0, '2021-09-13 13:30:02', '2021-09-14 13:30:02', 1),
(59, '1010', '1010', '', '', '', '', '', '', 0, 5, '', 0, 1, '0.0000', 0, 0, '2021-09-13', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 'шт', 0, 1, 0, 1, 0, '2021-09-13 13:30:02', '2021-09-14 13:30:02', 1),
(60, '1011', '1011', '', '', '', '', '', '', 0, 5, '', 0, 1, '0.0000', 0, 0, '2021-09-13', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 'шт', 0, 1, 0, 1, 0, '2021-09-13 13:30:02', '2021-09-14 13:30:02', 1),
(61, '1012', '1012', '', '', '', '', '', '', 0, 5, '', 0, 1, '0.0000', 0, 0, '2021-09-13', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 'шт', 0, 1, 0, 1, 0, '2021-09-13 13:30:02', '2021-09-14 13:30:02', 1),
(62, '1013', '1013', '', '', '', '', '', '', 0, 5, '', 0, 1, '0.0000', 0, 0, '2021-09-13', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 'шт', 0, 1, 0, 1, 0, '2021-09-13 13:30:02', '2021-09-14 13:30:02', 1),
(63, '1014', '1014', '', '', '', '', '', '', 0, 5, '', 0, 1, '0.0000', 0, 0, '2021-09-13', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 'шт', 0, 1, 0, 1, 0, '2021-09-13 13:30:02', '2021-09-14 13:30:02', 1),
(64, '1015', '1015', '', '', '', '', '', '', 0, 5, '', 0, 1, '0.0000', 0, 0, '2021-09-13', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 'шт', 0, 1, 0, 1, 0, '2021-09-13 13:30:02', '2021-09-14 13:30:02', 1),
(65, '1016', '1016', '', '', '', '', '', '', 0, 5, '', 0, 1, '0.0000', 0, 0, '2021-09-13', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 'шт', 0, 1, 0, 1, 0, '2021-09-13 13:30:02', '2021-09-14 13:30:02', 1),
(66, '1017', '1017', '', '', '', '', '', '', 0, 5, '', 0, 1, '0.0000', 0, 0, '2021-09-13', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 'шт', 0, 1, 0, 1, 0, '2021-09-13 13:30:02', '2021-09-14 13:30:02', 1),
(67, '1018', '1018', '', '', '', '', '', '', 0, 5, '', 0, 1, '0.0000', 0, 0, '2021-09-13', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 'шт', 0, 1, 0, 1, 0, '2021-09-13 13:30:02', '2021-09-14 13:30:02', 1),
(68, '1019', '1019', '', '', '', '', '', '', 0, 5, '', 0, 1, '0.0000', 0, 0, '2021-09-13', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 'шт', 0, 1, 0, 1, 0, '2021-09-13 13:30:02', '2021-09-14 13:30:02', 1),
(69, '1020', '1020', '', '', '', '', '', '', 0, 5, '', 0, 1, '0.0000', 0, 0, '2021-09-13', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 'шт', 0, 1, 0, 1, 0, '2021-09-13 13:30:02', '2021-09-14 13:30:02', 1),
(70, '1021', '1021', '', '', '', '', '', '', 0, 5, '', 0, 1, '0.0000', 0, 0, '2021-09-13', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 'шт', 0, 1, 0, 1, 0, '2021-09-13 13:30:02', '2021-09-14 13:30:02', 1),
(71, '1022', '1022', '', '', '', '', '', '', 0, 5, '', 0, 1, '0.0000', 0, 0, '2021-09-13', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 'шт', 0, 1, 0, 1, 0, '2021-09-13 13:30:02', '2021-09-14 13:30:02', 1),
(72, '1023', '1023', '', '', '', '', '', '', 0, 5, '', 0, 1, '0.0000', 0, 0, '2021-09-13', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 'шт', 0, 1, 0, 1, 0, '2021-09-13 13:30:02', '2021-09-14 13:30:02', 1),
(73, '1024', '1024', '', '', '', '', '', '', 0, 5, '', 0, 1, '0.0000', 0, 0, '2021-09-13', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 'шт', 0, 1, 0, 1, 0, '2021-09-13 13:30:02', '2021-09-14 13:30:02', 1),
(74, '1025', '1025', '', '', '', '', '', '', 0, 5, '', 0, 1, '0.0000', 0, 0, '2021-09-13', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 'шт', 0, 1, 0, 1, 0, '2021-09-13 13:30:02', '2021-09-14 13:30:02', 1),
(75, '1026', '1026', '', '', '', '', '', '', 0, 5, '', 0, 1, '0.0000', 0, 0, '2021-09-13', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 'шт', 0, 1, 0, 1, 0, '2021-09-13 13:30:02', '2021-09-14 13:30:02', 1),
(76, '1027', '1027', '', '', '', '', '', '', 0, 5, '', 0, 1, '0.0000', 0, 0, '2021-09-13', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 'шт', 0, 1, 0, 1, 0, '2021-09-13 13:30:02', '2021-09-14 13:30:02', 1),
(77, '1028', '1028', '', '', '', '', '', '', 0, 5, '', 0, 1, '0.0000', 0, 0, '2021-09-13', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 'шт', 0, 1, 0, 1, 0, '2021-09-13 13:30:02', '2021-09-14 13:30:02', 1),
(78, '1029', '1029', '', '', '', '', '', '', 0, 5, '', 0, 1, '0.0000', 0, 0, '2021-09-13', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 'шт', 0, 1, 0, 1, 0, '2021-09-13 13:30:02', '2021-09-14 13:30:02', 1),
(79, '1030', '1030', '', '', '', '', '', '', 0, 5, '', 0, 1, '0.0000', 0, 0, '2021-09-13', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 'шт', 0, 1, 0, 1, 0, '2021-09-13 13:30:02', '2021-09-14 13:30:02', 1),
(80, '1031', '1031', '', '', '', '', '', '', 0, 5, '', 0, 1, '0.0000', 0, 0, '2021-09-13', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 'шт', 0, 1, 0, 1, 0, '2021-09-13 13:30:02', '2021-09-14 13:30:02', 1),
(81, '1032', '1032', '', '', '', '', '', '', 0, 5, '', 0, 1, '0.0000', 0, 0, '2021-09-13', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 'шт', 0, 1, 0, 1, 0, '2021-09-13 13:30:02', '2021-09-14 13:30:02', 1),
(82, '1033', '1033', '', '', '', '', '', '', 0, 5, '', 0, 1, '0.0000', 0, 0, '2021-09-13', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 'шт', 0, 1, 0, 1, 0, '2021-09-13 13:30:02', '2021-09-14 13:30:02', 1),
(83, '1034', '1034', '', '', '', '', '', '', 0, 5, '', 0, 1, '0.0000', 0, 0, '2021-09-13', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 'шт', 0, 1, 0, 1, 0, '2021-09-13 13:30:02', '2021-09-14 13:30:02', 1),
(84, '1035', '1035', '', '', '', '', '', '', 0, 5, '', 0, 1, '0.0000', 0, 0, '2021-09-13', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 'шт', 0, 1, 0, 1, 0, '2021-09-13 13:30:02', '2021-09-14 13:30:02', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_attribute`
--

CREATE TABLE `oc_product_attribute` (
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_city`
--

CREATE TABLE `oc_product_city` (
  `product_id` int(11) NOT NULL,
  `product_option_value_id` int(11) NOT NULL DEFAULT 0,
  `city_id` int(11) NOT NULL,
  `price` decimal(15,4) NOT NULL,
  `price_old` decimal(15,4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_description`
--

CREATE TABLE `oc_product_description` (
  `product_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `tag` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_h1` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_product_description`
--

INSERT INTO `oc_product_description` (`product_id`, `language_id`, `name`, `description`, `tag`, `meta_title`, `meta_description`, `meta_keyword`, `meta_h1`) VALUES
(50, 1, 'Логслив Arshaevy 1001', 'Футболка с длиными руками', 'Логслив Arshaevy 1001,1001', 'Купить Логслив Arshaevy 1001 в Аршаев по лучшей цене', 'Покупайте Логслив Arshaevy 1001 в магазине Аршаев по лучшей цене', 'Логслив Arshaevy 1001,1001,Аршаев', 'Логслив Arshaevy 1001'),
(51, 1, 'Футболка Arshaevy 1002', 'Футболка с короткими рукавами', 'Футболка Arshaevy 1002,1002', 'Купить Футболка Arshaevy 1002 в Аршаев по лучшей цене', 'Покупайте Футболка Arshaevy 1002 в магазине Аршаев по лучшей цене', 'Футболка Arshaevy 1002,1002,Аршаев', 'Футболка Arshaevy 1002'),
(52, 1, 'Лонг слив Платье  Arshaevy 1003', 'Футболка с длиными рукавами -платье ', 'Лонг слив Платье  Arshaevy 1003,1003', 'Купить Лонг слив Платье  Arshaevy 1003 в Аршаев по лучшей цене', 'Покупайте Лонг слив Платье  Arshaevy 1003 в магазине Аршаев по лучшей цене', 'Лонг слив Платье  Arshaevy 1003,1003,Аршаев', 'Лонг слив Платье  Arshaevy 1003'),
(53, 1, 'Лонг футболка Платье  Arshaevy 1004', 'Футболка с короткими рукавами-платье ', 'Лонг футболка Платье  Arshaevy 1004,1004', 'Купить Лонг футболка Платье  Arshaevy 1004 в Аршаев по лучшей цене', 'Покупайте Лонг футболка Платье  Arshaevy 1004 в магазине Аршаев по лучшей цене', 'Лонг футболка Платье  Arshaevy 1004,1004,Аршаев', 'Лонг футболка Платье  Arshaevy 1004'),
(54, 1, 'Бонька Arshaevy 1005', '', 'Бонька Arshaevy 1005,1005', 'Купить Бонька Arshaevy 1005 в Аршаев по лучшей цене', 'Покупайте Бонька Arshaevy 1005 в магазине Аршаев по лучшей цене', 'Бонька Arshaevy 1005,1005,Аршаев', 'Бонька Arshaevy 1005'),
(55, 1, 'Silvi Arshaevy 1006', '', 'Silvi Arshaevy 1006,1006', 'Купить Silvi Arshaevy 1006 в Аршаев по лучшей цене', 'Покупайте Silvi Arshaevy 1006 в магазине Аршаев по лучшей цене', 'Silvi Arshaevy 1006,1006,Аршаев', 'Silvi Arshaevy 1006'),
(56, 1, 'Khalifa Arshaevy 1007', '', 'Khalifa Arshaevy 1007,1007', 'Купить Khalifa Arshaevy 1007 в Аршаев по лучшей цене', 'Покупайте Khalifa Arshaevy 1007 в магазине Аршаев по лучшей цене', 'Khalifa Arshaevy 1007,1007,Аршаев', 'Khalifa Arshaevy 1007'),
(57, 1, 'Madlen Arshaevy 1008', '', 'Madlen Arshaevy 1008,1008', 'Купить Madlen Arshaevy 1008 в Аршаев по лучшей цене', 'Покупайте Madlen Arshaevy 1008 в магазине Аршаев по лучшей цене', 'Madlen Arshaevy 1008,1008,Аршаев', 'Madlen Arshaevy 1008'),
(58, 1, 'Miya Arshaevy 1009', '', 'Miya Arshaevy 1009,1009', 'Купить Miya Arshaevy 1009 в Аршаев по лучшей цене', 'Покупайте Miya Arshaevy 1009 в магазине Аршаев по лучшей цене', 'Miya Arshaevy 1009,1009,Аршаев', 'Miya Arshaevy 1009'),
(59, 1, 'Frida Arshaevy 1010', '', 'Frida Arshaevy 1010,1010', 'Купить Frida Arshaevy 1010 в Аршаев по лучшей цене', 'Покупайте Frida Arshaevy 1010 в магазине Аршаев по лучшей цене', 'Frida Arshaevy 1010,1010,Аршаев', 'Frida Arshaevy 1010'),
(60, 1, 'Amali Arshaevy 1011', '', 'Amali Arshaevy 1011,1011', 'Купить Amali Arshaevy 1011 в Аршаев по лучшей цене', 'Покупайте Amali Arshaevy 1011 в магазине Аршаев по лучшей цене', 'Amali Arshaevy 1011,1011,Аршаев', 'Amali Arshaevy 1011'),
(61, 1, 'Корсет платье Arshaevy 1012', '', 'Корсет платье Arshaevy 1012,1012', 'Купить Корсет платье Arshaevy 1012 в Аршаев по лучшей цене', 'Покупайте Корсет платье Arshaevy 1012 в магазине Аршаев по лучшей цене', 'Корсет платье Arshaevy 1012,1012,Аршаев', 'Корсет платье Arshaevy 1012'),
(62, 1, 'Ариель Arshaevy 1013', '', 'Ариель Arshaevy 1013,1013', 'Купить Ариель Arshaevy 1013 в Аршаев по лучшей цене', 'Покупайте Ариель Arshaevy 1013 в магазине Аршаев по лучшей цене', 'Ариель Arshaevy 1013,1013,Аршаев', 'Ариель Arshaevy 1013'),
(63, 1, 'Texas Arshaevy 1014', 'Платье (рубашка Платье)', 'Texas Arshaevy 1014,1014', 'Купить Texas Arshaevy 1014 в Аршаев по лучшей цене', 'Покупайте Texas Arshaevy 1014 в магазине Аршаев по лучшей цене', 'Texas Arshaevy 1014,1014,Аршаев', 'Texas Arshaevy 1014'),
(64, 1, 'Костюм Arshaevy 1015', '', 'Костюм Arshaevy 1015,1015', 'Купить Костюм Arshaevy 1015 в Аршаев по лучшей цене', 'Покупайте Костюм Arshaevy 1015 в магазине Аршаев по лучшей цене', 'Костюм Arshaevy 1015,1015,Аршаев', 'Костюм Arshaevy 1015'),
(65, 1, 'Костюм romb arshaevy 1016', '', 'Костюм romb arshaevy 1016,1016', 'Купить Костюм romb arshaevy 1016 в Аршаев по лучшей цене', 'Покупайте Костюм romb arshaevy 1016 в магазине Аршаев по лучшей цене', 'Костюм romb arshaevy 1016,1016,Аршаев', 'Костюм romb arshaevy 1016'),
(66, 1, 'Шерстяной костюм arshaevy 1017', '', 'Шерстяной костюм arshaevy 1017,1017', 'Купить Шерстяной костюм arshaevy 1017 в Аршаев по лучшей цене', 'Покупайте Шерстяной костюм arshaevy 1017 в магазине Аршаев по лучшей цене', 'Шерстяной костюм arshaevy 1017,1017,Аршаев', 'Шерстяной костюм arshaevy 1017'),
(67, 1, 'Костюм Arshaevy 1018', '', 'Костюм Arshaevy 1018,1018', 'Купить Костюм Arshaevy 1018 в Аршаев по лучшей цене', 'Покупайте Костюм Arshaevy 1018 в магазине Аршаев по лучшей цене', 'Костюм Arshaevy 1018,1018,Аршаев', 'Костюм Arshaevy 1018'),
(68, 1, 'Килоты arshaevy 1019', '', 'Килоты arshaevy 1019,1019', 'Купить Килоты arshaevy 1019 в Аршаев по лучшей цене', 'Покупайте Килоты arshaevy 1019 в магазине Аршаев по лучшей цене', 'Килоты arshaevy 1019,1019,Аршаев', 'Килоты arshaevy 1019'),
(69, 1, 'Юбка romb arshaevy 1020', '', 'Юбка romb arshaevy 1020,1020', 'Купить Юбка romb arshaevy 1020 в Аршаев по лучшей цене', 'Покупайте Юбка romb arshaevy 1020 в магазине Аршаев по лучшей цене', 'Юбка romb arshaevy 1020,1020,Аршаев', 'Юбка romb arshaevy 1020'),
(70, 1, 'Пальто LV Arshaevy 1021', '', 'Пальто LV Arshaevy 1021,1021', 'Купить Пальто LV Arshaevy 1021 в Аршаев по лучшей цене', 'Покупайте Пальто LV Arshaevy 1021 в магазине Аршаев по лучшей цене', 'Пальто LV Arshaevy 1021,1021,Аршаев', 'Пальто LV Arshaevy 1021'),
(71, 1, 'Пальто Неопренд Arshaevy 1022', '', 'Пальто Неопренд Arshaevy 1022,1022', 'Купить Пальто Неопренд Arshaevy 1022 в Аршаев по лучшей цене', 'Покупайте Пальто Неопренд Arshaevy 1022 в магазине Аршаев по лучшей цене', 'Пальто Неопренд Arshaevy 1022,1022,Аршаев', 'Пальто Неопренд Arshaevy 1022'),
(72, 1, 'Костюм Kendi Arshaevy 1023', '', 'Костюм Kendi Arshaevy 1023,1023', 'Купить Костюм Kendi Arshaevy 1023 в Аршаев по лучшей цене', 'Покупайте Костюм Kendi Arshaevy 1023 в магазине Аршаев по лучшей цене', 'Костюм Kendi Arshaevy 1023,1023,Аршаев', 'Костюм Kendi Arshaevy 1023'),
(73, 1, 'Костюм Kendi Arshaevy 1024', '', 'Костюм Kendi Arshaevy 1024,1024', 'Купить Костюм Kendi Arshaevy 1024 в Аршаев по лучшей цене', 'Покупайте Костюм Kendi Arshaevy 1024 в магазине Аршаев по лучшей цене', 'Костюм Kendi Arshaevy 1024,1024,Аршаев', 'Костюм Kendi Arshaevy 1024'),
(74, 1, 'Худи Arshaevy 1025', '', 'Худи Arshaevy 1025,1025', 'Купить Худи Arshaevy 1025 в Аршаев по лучшей цене', 'Покупайте Худи Arshaevy 1025 в магазине Аршаев по лучшей цене', 'Худи Arshaevy 1025,1025,Аршаев', 'Худи Arshaevy 1025'),
(75, 1, 'Худи Arshaevy 1026', '', 'Худи Arshaevy 1026,1026', 'Купить Худи Arshaevy 1026 в Аршаев по лучшей цене', 'Покупайте Худи Arshaevy 1026 в магазине Аршаев по лучшей цене', 'Худи Arshaevy 1026,1026,Аршаев', 'Худи Arshaevy 1026'),
(76, 1, 'Худи Arshaevy 1027', '', 'Худи Arshaevy 1027,1027', 'Купить Худи Arshaevy 1027 в Аршаев по лучшей цене', 'Покупайте Худи Arshaevy 1027 в магазине Аршаев по лучшей цене', 'Худи Arshaevy 1027,1027,Аршаев', 'Худи Arshaevy 1027'),
(77, 1, 'Буркини Arshaevy 1028', '', 'Буркини Arshaevy 1028,1028', 'Купить Буркини Arshaevy 1028 в Аршаев по лучшей цене', 'Покупайте Буркини Arshaevy 1028 в магазине Аршаев по лучшей цене', 'Буркини Arshaevy 1028,1028,Аршаев', 'Буркини Arshaevy 1028'),
(78, 1, 'Буркини Arshaevy 1029', '', 'Буркини Arshaevy 1029,1029', 'Купить Буркини Arshaevy 1029 в Аршаев по лучшей цене', 'Покупайте Буркини Arshaevy 1029 в магазине Аршаев по лучшей цене', 'Буркини Arshaevy 1029,1029,Аршаев', 'Буркини Arshaevy 1029'),
(79, 1, 'Буркини Arshaevy 1030', '', 'Буркини Arshaevy 1030,1030', 'Купить Буркини Arshaevy 1030 в Аршаев по лучшей цене', 'Покупайте Буркини Arshaevy 1030 в магазине Аршаев по лучшей цене', 'Буркини Arshaevy 1030,1030,Аршаев', 'Буркини Arshaevy 1030'),
(80, 1, 'Буркини Arshaevy 1031', '', 'Буркини Arshaevy 1031,1031', 'Купить Буркини Arshaevy 1031 в Аршаев по лучшей цене', 'Покупайте Буркини Arshaevy 1031 в магазине Аршаев по лучшей цене', 'Буркини Arshaevy 1031,1031,Аршаев', 'Буркини Arshaevy 1031'),
(81, 1, 'Буркини Arshaevy 1032', '', 'Буркини Arshaevy 1032,1032', 'Купить Буркини Arshaevy 1032 в Аршаев по лучшей цене', 'Покупайте Буркини Arshaevy 1032 в магазине Аршаев по лучшей цене', 'Буркини Arshaevy 1032,1032,Аршаев', 'Буркини Arshaevy 1032'),
(82, 1, 'Буркини Arshaevy комлект X 1033', '', 'Буркини Arshaevy комлект X 1033,1033', 'Купить Буркини Arshaevy комлект X 1033 в Аршаев по лучшей цене', 'Покупайте Буркини Arshaevy комлект X 1033 в магазине Аршаев по лучшей цене', 'Буркини Arshaevy комлект X 1033,1033,Аршаев', 'Буркини Arshaevy комлект X 1033'),
(83, 1, 'Буркини Arshaevy комлект X 1034', '', 'Буркини Arshaevy комлект X 1034,1034', 'Купить Буркини Arshaevy комлект X 1034 в Аршаев по лучшей цене', 'Покупайте Буркини Arshaevy комлект X 1034 в магазине Аршаев по лучшей цене', 'Буркини Arshaevy комлект X 1034,1034,Аршаев', 'Буркини Arshaevy комлект X 1034'),
(84, 1, 'Буркини Arshaevy бюджетная версия 1035', '', 'Буркини Arshaevy бюджетная версия 1035,1035', 'Купить Буркини Arshaevy бюджетная версия 1035 в Аршаев по лучшей цене', 'Покупайте Буркини Arshaevy бюджетная версия 1035 в магазине Аршаев по лучшей цене', 'Буркини Arshaevy бюджетная версия 1035,1035,Аршаев', 'Буркини Arshaevy бюджетная версия 1035');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_description_composition`
--

CREATE TABLE `oc_product_description_composition` (
  `product_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `composition` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `oc_product_description_composition`
--

INSERT INTO `oc_product_description_composition` (`product_id`, `language_id`, `composition`) VALUES
(50, 1, 'Хлопок 100%');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_discount`
--

CREATE TABLE `oc_product_discount` (
  `product_discount_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_option_value_id` int(11) NOT NULL DEFAULT 0,
  `customer_group_id` int(11) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT 0,
  `priority` int(5) NOT NULL DEFAULT 1,
  `price` decimal(15,4) NOT NULL DEFAULT 0.0000,
  `date_start` date NOT NULL DEFAULT '1970-01-01',
  `date_end` date NOT NULL DEFAULT '2099-01-01'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_filter`
--

CREATE TABLE `oc_product_filter` (
  `product_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_image`
--

CREATE TABLE `oc_product_image` (
  `product_image_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_option`
--

CREATE TABLE `oc_product_option` (
  `product_option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `value` text NOT NULL,
  `required` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_product_option`
--

INSERT INTO `oc_product_option` (`product_option_id`, `product_id`, `option_id`, `value`, `required`) VALUES
(1, 50, 1, '', 1),
(2, 51, 2, '', 1),
(3, 52, 3, '', 1),
(4, 53, 4, '', 1),
(5, 54, 5, '', 1),
(6, 55, 6, '', 1),
(7, 56, 7, '', 1),
(8, 57, 8, '', 1),
(9, 58, 9, '', 1),
(10, 59, 10, '', 1),
(11, 60, 11, '', 1),
(12, 61, 12, '', 1),
(13, 62, 13, '', 1),
(14, 63, 14, '', 1),
(15, 64, 15, '', 1),
(16, 65, 16, '', 1),
(17, 66, 17, '', 1),
(18, 67, 18, '', 1),
(19, 68, 19, '', 1),
(20, 69, 20, '', 1),
(21, 70, 21, '', 1),
(22, 71, 22, '', 1),
(23, 72, 23, '', 1),
(24, 73, 24, '', 1),
(25, 74, 25, '', 1),
(26, 75, 26, '', 1),
(27, 76, 27, '', 1),
(28, 77, 28, '', 1),
(29, 78, 29, '', 1),
(30, 79, 30, '', 1),
(31, 80, 31, '', 1),
(32, 81, 32, '', 1),
(33, 82, 33, '', 1),
(34, 83, 34, '', 1),
(35, 84, 35, '', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_option_value`
--

CREATE TABLE `oc_product_option_value` (
  `product_option_value_id` int(11) NOT NULL,
  `product_option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_value_id` int(11) NOT NULL,
  `quantity` int(3) NOT NULL,
  `subtract` tinyint(1) NOT NULL,
  `price` decimal(15,4) NOT NULL,
  `price_prefix` varchar(1) NOT NULL,
  `discount` decimal(6,2) NOT NULL DEFAULT 0.00,
  `price_old` decimal(15,4) NOT NULL DEFAULT 0.0000,
  `points` int(8) NOT NULL,
  `points_prefix` varchar(1) NOT NULL,
  `weight` decimal(15,8) NOT NULL,
  `weight_prefix` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_product_option_value`
--

INSERT INTO `oc_product_option_value` (`product_option_value_id`, `product_option_id`, `product_id`, `option_id`, `option_value_id`, `quantity`, `subtract`, `price`, `price_prefix`, `discount`, `price_old`, `points`, `points_prefix`, `weight`, `weight_prefix`) VALUES
(1, 1, 50, 1, 1, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(2, 1, 50, 1, 2, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(3, 1, 50, 1, 3, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(4, 1, 50, 1, 4, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(5, 1, 50, 1, 5, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(6, 1, 50, 1, 6, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(7, 1, 50, 1, 7, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(8, 1, 50, 1, 8, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(9, 1, 50, 1, 9, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(10, 1, 50, 1, 10, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(11, 1, 50, 1, 11, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(12, 1, 50, 1, 12, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(13, 1, 50, 1, 13, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(14, 2, 51, 2, 14, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(15, 2, 51, 2, 15, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(16, 2, 51, 2, 16, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(17, 2, 51, 2, 17, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(18, 2, 51, 2, 18, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(19, 2, 51, 2, 19, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(20, 2, 51, 2, 20, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(21, 2, 51, 2, 21, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(22, 2, 51, 2, 22, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(23, 2, 51, 2, 23, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(24, 2, 51, 2, 24, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(25, 2, 51, 2, 25, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(26, 2, 51, 2, 26, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(27, 3, 52, 3, 27, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(28, 3, 52, 3, 28, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(29, 3, 52, 3, 29, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(30, 3, 52, 3, 30, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(31, 3, 52, 3, 31, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(32, 3, 52, 3, 32, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(33, 3, 52, 3, 33, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(34, 3, 52, 3, 34, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(35, 3, 52, 3, 35, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(36, 3, 52, 3, 36, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(37, 3, 52, 3, 37, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(38, 3, 52, 3, 38, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(39, 3, 52, 3, 39, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(40, 4, 53, 4, 40, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(41, 4, 53, 4, 41, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(42, 4, 53, 4, 42, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(43, 4, 53, 4, 43, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(44, 4, 53, 4, 44, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(45, 4, 53, 4, 45, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(46, 4, 53, 4, 46, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(47, 4, 53, 4, 47, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(48, 4, 53, 4, 48, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(49, 4, 53, 4, 49, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(50, 4, 53, 4, 50, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(51, 4, 53, 4, 51, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(52, 4, 53, 4, 52, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(53, 5, 54, 5, 53, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(54, 5, 54, 5, 54, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(55, 5, 54, 5, 55, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(56, 5, 54, 5, 56, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(57, 5, 54, 5, 57, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(58, 5, 54, 5, 58, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(59, 5, 54, 5, 59, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(60, 5, 54, 5, 60, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(61, 5, 54, 5, 61, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(62, 5, 54, 5, 62, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(63, 5, 54, 5, 63, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(64, 5, 54, 5, 64, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(65, 5, 54, 5, 65, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(66, 6, 55, 6, 66, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(67, 6, 55, 6, 67, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(68, 6, 55, 6, 68, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(69, 6, 55, 6, 69, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(70, 6, 55, 6, 70, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(71, 6, 55, 6, 71, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(72, 6, 55, 6, 72, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(73, 6, 55, 6, 73, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(74, 6, 55, 6, 74, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(75, 6, 55, 6, 75, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(76, 6, 55, 6, 76, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(77, 6, 55, 6, 77, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(78, 6, 55, 6, 78, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(79, 6, 55, 6, 79, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(80, 6, 55, 6, 80, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(81, 6, 55, 6, 81, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(82, 6, 55, 6, 82, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(83, 6, 55, 6, 83, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(84, 6, 55, 6, 84, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(85, 6, 55, 6, 85, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(86, 6, 55, 6, 86, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(87, 7, 56, 7, 87, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(88, 7, 56, 7, 88, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(89, 7, 56, 7, 89, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(90, 7, 56, 7, 90, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(91, 7, 56, 7, 91, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(92, 7, 56, 7, 92, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(93, 7, 56, 7, 93, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(94, 7, 56, 7, 94, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(95, 7, 56, 7, 95, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(96, 7, 56, 7, 96, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(97, 7, 56, 7, 97, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(98, 7, 56, 7, 98, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(99, 7, 56, 7, 99, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(100, 7, 56, 7, 100, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(101, 7, 56, 7, 101, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(102, 7, 56, 7, 102, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(103, 8, 57, 8, 103, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(104, 8, 57, 8, 104, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(105, 8, 57, 8, 105, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(106, 8, 57, 8, 106, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(107, 8, 57, 8, 107, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(108, 8, 57, 8, 108, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(109, 8, 57, 8, 109, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(110, 8, 57, 8, 110, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(111, 8, 57, 8, 111, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(112, 8, 57, 8, 112, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(113, 8, 57, 8, 113, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(114, 8, 57, 8, 114, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(115, 8, 57, 8, 115, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(116, 8, 57, 8, 116, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(117, 8, 57, 8, 117, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(118, 8, 57, 8, 118, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(119, 9, 58, 9, 119, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(120, 9, 58, 9, 120, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(121, 9, 58, 9, 121, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(122, 9, 58, 9, 122, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(123, 9, 58, 9, 123, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(124, 9, 58, 9, 124, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(125, 9, 58, 9, 125, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(126, 9, 58, 9, 126, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(127, 9, 58, 9, 127, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(128, 9, 58, 9, 128, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(129, 9, 58, 9, 129, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(130, 9, 58, 9, 130, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(131, 9, 58, 9, 131, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(132, 9, 58, 9, 132, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(133, 9, 58, 9, 133, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(134, 9, 58, 9, 134, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(135, 10, 59, 10, 135, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(136, 10, 59, 10, 136, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(137, 10, 59, 10, 137, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(138, 10, 59, 10, 138, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(139, 10, 59, 10, 139, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(140, 10, 59, 10, 140, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(141, 10, 59, 10, 141, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(142, 10, 59, 10, 142, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(143, 10, 59, 10, 143, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(144, 10, 59, 10, 144, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(145, 10, 59, 10, 145, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(146, 10, 59, 10, 146, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(147, 10, 59, 10, 147, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(148, 10, 59, 10, 148, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(149, 10, 59, 10, 149, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(150, 10, 59, 10, 150, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(151, 11, 60, 11, 151, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(152, 11, 60, 11, 152, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(153, 11, 60, 11, 153, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(154, 11, 60, 11, 154, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(155, 11, 60, 11, 155, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(156, 11, 60, 11, 156, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(157, 11, 60, 11, 157, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(158, 11, 60, 11, 158, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(159, 11, 60, 11, 159, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(160, 11, 60, 11, 160, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(161, 11, 60, 11, 161, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(162, 11, 60, 11, 162, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(163, 11, 60, 11, 163, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(164, 11, 60, 11, 164, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(165, 11, 60, 11, 165, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(166, 11, 60, 11, 166, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(167, 11, 60, 11, 167, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(168, 11, 60, 11, 168, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(169, 11, 60, 11, 169, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(170, 11, 60, 11, 170, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(171, 12, 61, 12, 171, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(172, 12, 61, 12, 172, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(173, 13, 62, 13, 173, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(174, 13, 62, 13, 174, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(175, 13, 62, 13, 175, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(176, 13, 62, 13, 176, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(177, 14, 63, 14, 177, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(178, 14, 63, 14, 178, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(179, 14, 63, 14, 179, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(180, 14, 63, 14, 180, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(181, 14, 63, 14, 181, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(182, 15, 64, 15, 182, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(183, 15, 64, 15, 183, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(184, 15, 64, 15, 184, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(185, 15, 64, 15, 185, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(186, 15, 64, 15, 186, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(187, 15, 64, 15, 187, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(188, 15, 64, 15, 188, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(189, 15, 64, 15, 189, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(190, 15, 64, 15, 190, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(191, 15, 64, 15, 191, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(192, 15, 64, 15, 192, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(193, 15, 64, 15, 193, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(194, 15, 64, 15, 194, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(195, 15, 64, 15, 195, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(196, 15, 64, 15, 196, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(197, 15, 64, 15, 197, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(198, 15, 64, 15, 198, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(199, 16, 65, 16, 199, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(200, 16, 65, 16, 200, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(201, 16, 65, 16, 201, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(202, 16, 65, 16, 202, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(203, 16, 65, 16, 203, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(204, 16, 65, 16, 204, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(205, 16, 65, 16, 205, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(206, 16, 65, 16, 206, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(207, 16, 65, 16, 207, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(208, 16, 65, 16, 208, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(209, 16, 65, 16, 209, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(210, 16, 65, 16, 210, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(211, 16, 65, 16, 211, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(212, 16, 65, 16, 212, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(213, 16, 65, 16, 213, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(214, 16, 65, 16, 214, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(215, 16, 65, 16, 215, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(216, 17, 66, 17, 216, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(217, 18, 67, 18, 217, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(218, 18, 67, 18, 218, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(219, 18, 67, 18, 219, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(220, 18, 67, 18, 220, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(221, 18, 67, 18, 221, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(222, 18, 67, 18, 222, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(223, 18, 67, 18, 223, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(224, 18, 67, 18, 224, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(225, 18, 67, 18, 225, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(226, 18, 67, 18, 226, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(227, 18, 67, 18, 227, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(228, 18, 67, 18, 228, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(229, 18, 67, 18, 229, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(230, 18, 67, 18, 230, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(231, 18, 67, 18, 231, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(232, 18, 67, 18, 232, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(233, 18, 67, 18, 233, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(234, 19, 68, 19, 234, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(235, 19, 68, 19, 235, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(236, 19, 68, 19, 236, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(237, 19, 68, 19, 237, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(238, 19, 68, 19, 238, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(239, 19, 68, 19, 239, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(240, 19, 68, 19, 240, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(241, 19, 68, 19, 241, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(242, 19, 68, 19, 242, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(243, 19, 68, 19, 243, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(244, 19, 68, 19, 244, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(245, 19, 68, 19, 245, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(246, 19, 68, 19, 246, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(247, 19, 68, 19, 247, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(248, 19, 68, 19, 248, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(249, 19, 68, 19, 249, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(250, 19, 68, 19, 250, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(251, 20, 69, 20, 251, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(252, 20, 69, 20, 252, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(253, 20, 69, 20, 253, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(254, 20, 69, 20, 254, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(255, 20, 69, 20, 255, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(256, 20, 69, 20, 256, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(257, 20, 69, 20, 257, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(258, 20, 69, 20, 258, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(259, 20, 69, 20, 259, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(260, 20, 69, 20, 260, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(261, 20, 69, 20, 261, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(262, 20, 69, 20, 262, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(263, 20, 69, 20, 263, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(264, 20, 69, 20, 264, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(265, 20, 69, 20, 265, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(266, 20, 69, 20, 266, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(267, 20, 69, 20, 267, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(268, 21, 70, 21, 268, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(269, 21, 70, 21, 269, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(270, 21, 70, 21, 270, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(271, 22, 71, 22, 271, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(272, 22, 71, 22, 272, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(273, 23, 72, 23, 273, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(274, 23, 72, 23, 274, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(275, 23, 72, 23, 275, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(276, 23, 72, 23, 276, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(277, 23, 72, 23, 277, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(278, 23, 72, 23, 278, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(279, 23, 72, 23, 279, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(280, 23, 72, 23, 280, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(281, 23, 72, 23, 281, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(282, 23, 72, 23, 282, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(283, 23, 72, 23, 283, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(284, 24, 73, 24, 284, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(285, 24, 73, 24, 285, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(286, 24, 73, 24, 286, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(287, 24, 73, 24, 287, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(288, 24, 73, 24, 288, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(289, 24, 73, 24, 289, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(290, 24, 73, 24, 290, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(291, 24, 73, 24, 291, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(292, 24, 73, 24, 292, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(293, 24, 73, 24, 293, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(294, 24, 73, 24, 294, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(295, 25, 74, 25, 295, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(296, 25, 74, 25, 296, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(297, 25, 74, 25, 297, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(298, 25, 74, 25, 298, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(299, 25, 74, 25, 299, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(300, 25, 74, 25, 300, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(301, 25, 74, 25, 301, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(302, 25, 74, 25, 302, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(303, 25, 74, 25, 303, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(304, 25, 74, 25, 304, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(305, 25, 74, 25, 305, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(306, 26, 75, 26, 306, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(307, 26, 75, 26, 307, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(308, 26, 75, 26, 308, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(309, 26, 75, 26, 309, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(310, 26, 75, 26, 310, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(311, 26, 75, 26, 311, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(312, 26, 75, 26, 312, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(313, 26, 75, 26, 313, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(314, 26, 75, 26, 314, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(315, 26, 75, 26, 315, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(316, 26, 75, 26, 316, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(317, 27, 76, 27, 317, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(318, 27, 76, 27, 318, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(319, 27, 76, 27, 319, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(320, 27, 76, 27, 320, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(321, 27, 76, 27, 321, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(322, 27, 76, 27, 322, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(323, 27, 76, 27, 323, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(324, 27, 76, 27, 324, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(325, 27, 76, 27, 325, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(326, 27, 76, 27, 326, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(327, 27, 76, 27, 327, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(328, 28, 77, 28, 328, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(329, 28, 77, 28, 329, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(330, 28, 77, 28, 330, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(331, 28, 77, 28, 331, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(332, 28, 77, 28, 332, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(333, 28, 77, 28, 333, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(334, 28, 77, 28, 334, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(335, 28, 77, 28, 335, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(336, 28, 77, 28, 336, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(337, 28, 77, 28, 337, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(338, 28, 77, 28, 338, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(339, 28, 77, 28, 339, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(340, 28, 77, 28, 340, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(341, 28, 77, 28, 341, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(342, 28, 77, 28, 342, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(343, 28, 77, 28, 343, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(344, 28, 77, 28, 344, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(345, 28, 77, 28, 345, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(346, 29, 78, 29, 346, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(347, 29, 78, 29, 347, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(348, 29, 78, 29, 348, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(349, 29, 78, 29, 349, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(350, 29, 78, 29, 350, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(351, 29, 78, 29, 351, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(352, 29, 78, 29, 352, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(353, 29, 78, 29, 353, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(354, 29, 78, 29, 354, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(355, 29, 78, 29, 355, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(356, 29, 78, 29, 356, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(357, 29, 78, 29, 357, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(358, 29, 78, 29, 358, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(359, 29, 78, 29, 359, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(360, 29, 78, 29, 360, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(361, 29, 78, 29, 361, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(362, 29, 78, 29, 362, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(363, 29, 78, 29, 363, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(364, 30, 79, 30, 364, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(365, 30, 79, 30, 365, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(366, 30, 79, 30, 366, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(367, 30, 79, 30, 367, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(368, 30, 79, 30, 368, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(369, 30, 79, 30, 369, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(370, 30, 79, 30, 370, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(371, 30, 79, 30, 371, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(372, 30, 79, 30, 372, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(373, 30, 79, 30, 373, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(374, 30, 79, 30, 374, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(375, 30, 79, 30, 375, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(376, 30, 79, 30, 376, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(377, 30, 79, 30, 377, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(378, 30, 79, 30, 378, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(379, 30, 79, 30, 379, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(380, 30, 79, 30, 380, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(381, 30, 79, 30, 381, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(382, 31, 80, 31, 382, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(383, 31, 80, 31, 383, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(384, 31, 80, 31, 384, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(385, 31, 80, 31, 385, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(386, 31, 80, 31, 386, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(387, 31, 80, 31, 387, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(388, 31, 80, 31, 388, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(389, 31, 80, 31, 389, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(390, 31, 80, 31, 390, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(391, 31, 80, 31, 391, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(392, 31, 80, 31, 392, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(393, 31, 80, 31, 393, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(394, 31, 80, 31, 394, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(395, 31, 80, 31, 395, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(396, 31, 80, 31, 396, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(397, 31, 80, 31, 397, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(398, 31, 80, 31, 398, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(399, 31, 80, 31, 399, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(400, 32, 81, 32, 400, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(401, 32, 81, 32, 401, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(402, 32, 81, 32, 402, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(403, 32, 81, 32, 403, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(404, 32, 81, 32, 404, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(405, 32, 81, 32, 405, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(406, 32, 81, 32, 406, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(407, 32, 81, 32, 407, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(408, 32, 81, 32, 408, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(409, 32, 81, 32, 409, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(410, 32, 81, 32, 410, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(411, 32, 81, 32, 411, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(412, 32, 81, 32, 412, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(413, 32, 81, 32, 413, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(414, 32, 81, 32, 414, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(415, 32, 81, 32, 415, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(416, 32, 81, 32, 416, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(417, 32, 81, 32, 417, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(418, 33, 82, 33, 418, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(419, 33, 82, 33, 419, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(420, 33, 82, 33, 420, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(421, 33, 82, 33, 421, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(422, 33, 82, 33, 422, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(423, 33, 82, 33, 423, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(424, 33, 82, 33, 424, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(425, 33, 82, 33, 425, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(426, 33, 82, 33, 426, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(427, 33, 82, 33, 427, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(428, 33, 82, 33, 428, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(429, 33, 82, 33, 429, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(430, 33, 82, 33, 430, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(431, 33, 82, 33, 431, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(432, 33, 82, 33, 432, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(433, 33, 82, 33, 433, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(434, 33, 82, 33, 434, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(435, 33, 82, 33, 435, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(436, 34, 83, 34, 436, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(437, 34, 83, 34, 437, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(438, 34, 83, 34, 438, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(439, 34, 83, 34, 439, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(440, 34, 83, 34, 440, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(441, 34, 83, 34, 441, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(442, 34, 83, 34, 442, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(443, 34, 83, 34, 443, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(444, 34, 83, 34, 444, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(445, 34, 83, 34, 445, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(446, 34, 83, 34, 446, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(447, 34, 83, 34, 447, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(448, 34, 83, 34, 448, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(449, 34, 83, 34, 449, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(450, 34, 83, 34, 450, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(451, 34, 83, 34, 451, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(452, 34, 83, 34, 452, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(453, 34, 83, 34, 453, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(454, 35, 84, 35, 454, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(455, 35, 84, 35, 455, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(456, 35, 84, 35, 456, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(457, 35, 84, 35, 457, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(458, 35, 84, 35, 458, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(459, 35, 84, 35, 459, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(460, 35, 84, 35, 460, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(461, 35, 84, 35, 461, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(462, 35, 84, 35, 462, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(463, 35, 84, 35, 463, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(464, 35, 84, 35, 464, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(465, 35, 84, 35, 465, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(466, 35, 84, 35, 466, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(467, 35, 84, 35, 467, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(468, 35, 84, 35, 468, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(469, 35, 84, 35, 469, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(470, 35, 84, 35, 470, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+'),
(471, 35, 84, 35, 471, 0, 0, '0.0000', '=', '0.00', '0.0000', 0, '+', '0.00000000', '+');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_recurring`
--

CREATE TABLE `oc_product_recurring` (
  `product_id` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_related`
--

CREATE TABLE `oc_product_related` (
  `product_id` int(11) NOT NULL,
  `related_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_related_article`
--

CREATE TABLE `oc_product_related_article` (
  `article_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_related_mn`
--

CREATE TABLE `oc_product_related_mn` (
  `product_id` int(11) NOT NULL,
  `manufacturer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_related_wb`
--

CREATE TABLE `oc_product_related_wb` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_reward`
--

CREATE TABLE `oc_product_reward` (
  `product_reward_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL DEFAULT 0,
  `customer_group_id` int(11) NOT NULL DEFAULT 0,
  `points` int(8) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_special`
--

CREATE TABLE `oc_product_special` (
  `product_special_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT 1,
  `price` decimal(15,4) NOT NULL DEFAULT 0.0000,
  `discount` decimal(6,2) NOT NULL DEFAULT 0.00,
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_to_category`
--

CREATE TABLE `oc_product_to_category` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `main_category` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_product_to_category`
--

INSERT INTO `oc_product_to_category` (`product_id`, `category_id`, `main_category`) VALUES
(50, 50, 1),
(51, 50, 1),
(52, 50, 1),
(53, 50, 1),
(54, 50, 1),
(55, 50, 1),
(56, 50, 1),
(57, 50, 1),
(58, 50, 1),
(59, 50, 1),
(60, 50, 1),
(61, 50, 1),
(62, 50, 1),
(63, 50, 1),
(64, 50, 1),
(65, 50, 1),
(66, 50, 1),
(67, 50, 1),
(68, 50, 1),
(69, 50, 1),
(70, 50, 1),
(71, 50, 1),
(72, 50, 1),
(73, 50, 1),
(74, 50, 1),
(75, 50, 1),
(76, 50, 1),
(77, 50, 1),
(78, 50, 1),
(79, 50, 1),
(80, 50, 1),
(81, 50, 1),
(82, 50, 1),
(83, 50, 1),
(84, 50, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_to_download`
--

CREATE TABLE `oc_product_to_download` (
  `product_id` int(11) NOT NULL,
  `download_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_to_layout`
--

CREATE TABLE `oc_product_to_layout` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_to_store`
--

CREATE TABLE `oc_product_to_store` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_product_to_store`
--

INSERT INTO `oc_product_to_store` (`product_id`, `store_id`) VALUES
(50, 0),
(51, 0),
(52, 0),
(53, 0),
(54, 0),
(55, 0),
(56, 0),
(57, 0),
(58, 0),
(59, 0),
(60, 0),
(61, 0),
(62, 0),
(63, 0),
(64, 0),
(65, 0),
(66, 0),
(67, 0),
(68, 0),
(69, 0),
(70, 0),
(71, 0),
(72, 0),
(73, 0),
(74, 0),
(75, 0),
(76, 0),
(77, 0),
(78, 0),
(79, 0),
(80, 0),
(81, 0),
(82, 0),
(83, 0),
(84, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_recurring`
--

CREATE TABLE `oc_recurring` (
  `recurring_id` int(11) NOT NULL,
  `price` decimal(10,4) NOT NULL,
  `frequency` enum('day','week','semi_month','month','year') NOT NULL,
  `duration` int(10) UNSIGNED NOT NULL,
  `cycle` int(10) UNSIGNED NOT NULL,
  `trial_status` tinyint(4) NOT NULL,
  `trial_price` decimal(10,4) NOT NULL,
  `trial_frequency` enum('day','week','semi_month','month','year') NOT NULL,
  `trial_duration` int(10) UNSIGNED NOT NULL,
  `trial_cycle` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL,
  `sort_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_recurring_description`
--

CREATE TABLE `oc_recurring_description` (
  `recurring_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_return`
--

CREATE TABLE `oc_return` (
  `return_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `product` varchar(255) NOT NULL,
  `model` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `opened` tinyint(1) NOT NULL,
  `return_reason_id` int(11) NOT NULL,
  `return_action_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `comment` text DEFAULT NULL,
  `date_ordered` date NOT NULL DEFAULT '0000-00-00',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_return_action`
--

CREATE TABLE `oc_return_action` (
  `return_action_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT 0,
  `name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_return_action`
--

INSERT INTO `oc_return_action` (`return_action_id`, `language_id`, `name`) VALUES
(1, 1, 'Возмещенный'),
(1, 2, 'Refunded'),
(2, 1, 'Возврат средств'),
(2, 2, 'Credit Issued'),
(3, 1, 'Отправлена замена'),
(3, 2, 'Replacement Sent');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_return_history`
--

CREATE TABLE `oc_return_history` (
  `return_history_id` int(11) NOT NULL,
  `return_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_return_reason`
--

CREATE TABLE `oc_return_reason` (
  `return_reason_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT 0,
  `name` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_return_reason`
--

INSERT INTO `oc_return_reason` (`return_reason_id`, `language_id`, `name`) VALUES
(1, 1, 'Получен неисправным (сломанным)'),
(1, 2, 'Dead On Arrival'),
(2, 1, 'Получен не тот (ошибочный) товар'),
(2, 2, 'Received Wrong Item'),
(3, 1, 'Заказан по ошибке'),
(3, 2, 'Order Error'),
(4, 1, 'Неисправен, пожалуйста укажите/приложите подробности'),
(4, 2, 'Faulty, please supply details'),
(5, 1, 'Другое (другая причина), пожалуйста укажите/приложите подробности'),
(5, 2, 'Other, please supply details');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_return_status`
--

CREATE TABLE `oc_return_status` (
  `return_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT 0,
  `name` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_return_status`
--

INSERT INTO `oc_return_status` (`return_status_id`, `language_id`, `name`) VALUES
(1, 1, 'В ожидании'),
(1, 2, 'Pending'),
(2, 1, 'Ожидание товара'),
(2, 2, 'Awaiting Products'),
(3, 1, 'Выполнен'),
(3, 2, 'Complete');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_review`
--

CREATE TABLE `oc_review` (
  `review_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `author` varchar(64) NOT NULL,
  `text` text NOT NULL,
  `rating` int(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_review_article`
--

CREATE TABLE `oc_review_article` (
  `review_article_id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `author` varchar(64) NOT NULL DEFAULT '',
  `text` text NOT NULL,
  `rating` int(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_seo_url`
--

CREATE TABLE `oc_seo_url` (
  `seo_url_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `query` varchar(255) NOT NULL,
  `keyword` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_seo_url`
--

INSERT INTO `oc_seo_url` (`seo_url_id`, `store_id`, `language_id`, `query`, `keyword`) VALUES
(1, 0, 1, 'common/home', ''),
(2, 0, 2, 'common/home', 'en'),
(3, 0, 1, 'information_id=5', 'policy'),
(4, 0, 1, 'extension/quickcheckout/checkout', 'checkout'),
(5, 0, 1, 'account/register', 'register'),
(6, 0, 1, 'information/contact', 'contacts'),
(7, 0, 1, 'product/search', 'search'),
(8, 0, 1, 'checkout/cart', 'cart'),
(9, 0, 1, 'account/login', 'login'),
(10, 0, 1, 'account/account', 'account'),
(11, 0, 1, 'product/stocks', 'stocks'),
(12, 0, 1, 'category_id=50', 'catalogue'),
(13, 0, 1, 'category_id=51', 'obuv'),
(14, 0, 1, 'category_id=52', 'odezhda'),
(15, 0, 1, 'category_id=53', 'trikotazh'),
(16, 0, 1, 'product_id=50', 'logsliv-arshaevy-1001-50'),
(17, 0, 1, 'product_id=51', 'futbolka-arshaevy-1002-51'),
(18, 0, 1, 'product_id=52', 'long-sliv-plate-arshaevy-1003-52'),
(19, 0, 1, 'product_id=53', 'long-futbolka-plate-arshaevy-1004-53'),
(20, 0, 1, 'product_id=54', 'bonka-arshaevy-1005-54'),
(21, 0, 1, 'product_id=55', 'silvi-arshaevy-1006-55'),
(22, 0, 1, 'product_id=56', 'khalifa-arshaevy-1007-56'),
(23, 0, 1, 'product_id=57', 'madlen-arshaevy-1008-57'),
(24, 0, 1, 'product_id=58', 'miya-arshaevy-1009-58'),
(25, 0, 1, 'product_id=59', 'frida-arshaevy-1010-59'),
(26, 0, 1, 'product_id=60', 'amali-arshaevy-1011-60'),
(27, 0, 1, 'product_id=61', 'korset-plate-arshaevy-1012-61'),
(28, 0, 1, 'product_id=62', 'ariel-arshaevy-1013-62'),
(29, 0, 1, 'product_id=63', 'texas-arshaevy-1014-63'),
(30, 0, 1, 'product_id=64', 'kostyum-arshaevy-1015-64'),
(31, 0, 1, 'product_id=65', 'kostyum-romb-arshaevy-1016-65'),
(32, 0, 1, 'product_id=66', 'sherstyanoj-kostyum-arshaevy-1017-66'),
(33, 0, 1, 'product_id=67', 'kostyum-arshaevy-1018-67'),
(34, 0, 1, 'product_id=68', 'kiloty-arshaevy-1019-68'),
(35, 0, 1, 'product_id=69', 'yubka-romb-arshaevy-1020-69'),
(36, 0, 1, 'product_id=70', 'palto-lv-arshaevy-1021-70'),
(37, 0, 1, 'product_id=71', 'palto-neoprend-arshaevy-1022-71'),
(38, 0, 1, 'product_id=72', 'kostyum-kendi-arshaevy-1023-72'),
(39, 0, 1, 'product_id=73', 'kostyum-kendi-arshaevy-1024-73'),
(40, 0, 1, 'product_id=74', 'hudi-arshaevy-1025-74'),
(41, 0, 1, 'product_id=75', 'hudi-arshaevy-1026-75'),
(42, 0, 1, 'product_id=76', 'hudi-arshaevy-1027-76'),
(43, 0, 1, 'product_id=77', 'burkini-arshaevy-1028-77'),
(44, 0, 1, 'product_id=78', 'burkini-arshaevy-1029-78'),
(45, 0, 1, 'product_id=79', 'burkini-arshaevy-1030-79'),
(46, 0, 1, 'product_id=80', 'burkini-arshaevy-1031-80'),
(47, 0, 1, 'product_id=81', 'burkini-arshaevy-1032-81'),
(48, 0, 1, 'product_id=82', 'burkini-arshaevy-komlekt-x-1033-82'),
(49, 0, 1, 'product_id=83', 'burkini-arshaevy-komlekt-x-1034-83'),
(50, 0, 1, 'product_id=84', 'burkini-arshaevy-byudzhetnaya-versiya-1035-84');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_session`
--

CREATE TABLE `oc_session` (
  `session_id` varchar(32) NOT NULL,
  `data` longtext NOT NULL,
  `expire` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_session`
--

INSERT INTO `oc_session` (`session_id`, `data`, `expire`) VALUES
('2d8bf9dcc9ab6cd05f44fd91c6', '{\"api_id\":\"1\"}', '2021-09-17 03:24:48'),
('55ec826c1670c5e5ed26528542', '{\"user_id\":\"1\",\"user_token\":\"urLhwNeb3jKHxcw3XFCctLx9ZsuSuCwZ\",\"api_token\":\"c6a4b191a1d978d04e6839a72c\",\"language\":\"ru-ru\",\"currency\":\"RUB\"}', '2021-09-19 05:12:20'),
('79536b4ed27e71eee941a5edff', '{\"language\":\"ru-ru\",\"currency\":\"RUB\",\"user_id\":\"1\",\"user_token\":\"dlZd59oFegDnpIR3not923tkVbI85iHB\",\"api_token\":\"f5cd9dfcdfcec408706e6d3c98\",\"success\":\"\\u041d\\u0430\\u0441\\u0442\\u0440\\u043e\\u0439\\u043a\\u0438 \\u0443\\u0441\\u043f\\u0435\\u0448\\u043d\\u043e \\u0438\\u0437\\u043c\\u0435\\u043d\\u0435\\u043d\\u044b!\",\"account\":\"guest\",\"last_order_id\":163,\"guest\":{\"customer_group_id\":\"1\"}}', '2021-09-16 06:12:13'),
('88ecd3b080ffd999d338a19ea6', '{\"language\":\"ru-ru\",\"currency\":\"RUB\",\"user_id\":\"1\",\"user_token\":\"DjAAH5qiuxFjKmCuktml9AUHrkdirxkW\",\"api_token\":\"2d8bf9dcc9ab6cd05f44fd91c6\",\"success\":\"\\u041d\\u0430\\u0441\\u0442\\u0440\\u043e\\u0439\\u043a\\u0438 \\u0443\\u0441\\u043f\\u0435\\u0448\\u043d\\u043e \\u0438\\u0437\\u043c\\u0435\\u043d\\u0435\\u043d\\u044b!\"}', '2021-09-17 03:30:09'),
('a0a80afa6e2f5a592730f56771', '{\"language\":\"ru-ru\",\"currency\":\"RUB\"}', '2021-09-16 06:10:40'),
('c6a4b191a1d978d04e6839a72c', '{\"api_id\":\"1\"}', '2021-09-19 05:05:35'),
('c7ebf2ba2417ce941852509fc2', '{\"language\":\"ru-ru\",\"currency\":\"RUB\"}', '2021-09-17 03:26:21'),
('f5cd9dfcdfcec408706e6d3c98', '{\"api_id\":\"1\",\"language\":\"ru-ru\",\"currency\":\"RUB\"}', '2021-09-16 06:11:53');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_setting`
--

CREATE TABLE `oc_setting` (
  `setting_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT 0,
  `code` varchar(128) NOT NULL,
  `key` varchar(128) NOT NULL,
  `value` text NOT NULL,
  `serialized` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_setting`
--

INSERT INTO `oc_setting` (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES
(4, 0, 'voucher', 'total_voucher_sort_order', '8', 0),
(5, 0, 'voucher', 'total_voucher_status', '1', 0),
(98, 0, 'payment_cod', 'payment_cod_sort_order', '5', 0),
(99, 0, 'payment_cod', 'payment_cod_total', '0.01', 0),
(100, 0, 'payment_cod', 'payment_cod_order_status_id', '1', 0),
(101, 0, 'payment_cod', 'payment_cod_geo_zone_id', '0', 0),
(102, 0, 'payment_cod', 'payment_cod_status', '1', 0),
(111, 0, 'total_tax', 'total_tax_status', '1', 0),
(114, 0, 'total_tax', 'total_tax_sort_order', '5', 0),
(115, 0, 'total_credit', 'total_credit_sort_order', '7', 0),
(116, 0, 'total_credit', 'total_credit_status', '1', 0),
(151, 0, 'dashboard_activity', 'dashboard_activity_status', '1', 0),
(152, 0, 'dashboard_activity', 'dashboard_activity_sort_order', '7', 0),
(153, 0, 'dashboard_sale', 'dashboard_sale_status', '1', 0),
(154, 0, 'dashboard_sale', 'dashboard_sale_width', '3', 0),
(155, 0, 'dashboard_chart', 'dashboard_chart_status', '1', 0),
(156, 0, 'dashboard_chart', 'dashboard_chart_width', '6', 0),
(157, 0, 'dashboard_customer', 'dashboard_customer_status', '1', 0),
(158, 0, 'dashboard_customer', 'dashboard_customer_width', '3', 0),
(159, 0, 'dashboard_map', 'dashboard_map_status', '1', 0),
(160, 0, 'dashboard_map', 'dashboard_map_width', '6', 0),
(161, 0, 'dashboard_online', 'dashboard_online_status', '1', 0),
(162, 0, 'dashboard_online', 'dashboard_online_width', '3', 0),
(163, 0, 'dashboard_order', 'dashboard_order_sort_order', '1', 0),
(164, 0, 'dashboard_order', 'dashboard_order_status', '1', 0),
(165, 0, 'dashboard_order', 'dashboard_order_width', '3', 0),
(166, 0, 'dashboard_sale', 'dashboard_sale_sort_order', '2', 0),
(167, 0, 'dashboard_customer', 'dashboard_customer_sort_order', '3', 0),
(168, 0, 'dashboard_online', 'dashboard_online_sort_order', '4', 0),
(169, 0, 'dashboard_map', 'dashboard_map_sort_order', '5', 0),
(170, 0, 'dashboard_chart', 'dashboard_chart_sort_order', '6', 0),
(171, 0, 'dashboard_recent', 'dashboard_recent_status', '1', 0),
(172, 0, 'dashboard_recent', 'dashboard_recent_sort_order', '8', 0),
(173, 0, 'dashboard_activity', 'dashboard_activity_width', '4', 0),
(174, 0, 'dashboard_recent', 'dashboard_recent_width', '8', 0),
(175, 0, 'report_customer_activity', 'report_customer_activity_status', '1', 0),
(176, 0, 'report_customer_activity', 'report_customer_activity_sort_order', '1', 0),
(177, 0, 'report_customer_order', 'report_customer_order_status', '1', 0),
(178, 0, 'report_customer_order', 'report_customer_order_sort_order', '2', 0),
(179, 0, 'report_customer_reward', 'report_customer_reward_status', '1', 0),
(180, 0, 'report_customer_reward', 'report_customer_reward_sort_order', '3', 0),
(181, 0, 'report_customer_search', 'report_customer_search_sort_order', '3', 0),
(182, 0, 'report_customer_search', 'report_customer_search_status', '1', 0),
(183, 0, 'report_customer_transaction', 'report_customer_transaction_status', '1', 0),
(184, 0, 'report_customer_transaction', 'report_customer_transaction_status_sort_order', '4', 0),
(185, 0, 'report_sale_tax', 'report_sale_tax_status', '1', 0),
(186, 0, 'report_sale_tax', 'report_sale_tax_sort_order', '5', 0),
(187, 0, 'report_sale_shipping', 'report_sale_shipping_status', '1', 0),
(188, 0, 'report_sale_shipping', 'report_sale_shipping_sort_order', '6', 0),
(189, 0, 'report_sale_return', 'report_sale_return_status', '1', 0),
(190, 0, 'report_sale_return', 'report_sale_return_sort_order', '7', 0),
(191, 0, 'report_sale_order', 'report_sale_order_status', '1', 0),
(192, 0, 'report_sale_order', 'report_sale_order_sort_order', '8', 0),
(193, 0, 'report_sale_coupon', 'report_sale_coupon_status', '1', 0),
(194, 0, 'report_sale_coupon', 'report_sale_coupon_sort_order', '9', 0),
(195, 0, 'report_product_viewed', 'report_product_viewed_status', '1', 0),
(196, 0, 'report_product_viewed', 'report_product_viewed_sort_order', '10', 0),
(197, 0, 'report_product_purchased', 'report_product_purchased_status', '1', 0),
(198, 0, 'report_product_purchased', 'report_product_purchased_sort_order', '11', 0),
(199, 0, 'report_marketing', 'report_marketing_status', '1', 0),
(200, 0, 'report_marketing', 'report_marketing_sort_order', '12', 0),
(203, 0, 'configblog', 'configblog_name', 'Блог', 0),
(204, 0, 'configblog', 'configblog_html_h1', 'Блог для интернет магазина на OpenCart', 0),
(205, 0, 'configblog', 'configblog_meta_title', 'Блог для интернет магазина на OpenCart', 0),
(206, 0, 'configblog', 'configblog_meta_description', 'Блог для интернет магазина на OpenCart', 0),
(207, 0, 'configblog', 'configblog_meta_keyword', 'Блог для интернет магазина на OpenCart', 0),
(208, 0, 'configblog', 'configblog_article_count', '1', 0),
(209, 0, 'configblog', 'configblog_article_limit', '20', 0),
(210, 0, 'configblog', 'configblog_article_description_length', '200', 0),
(211, 0, 'configblog', 'configblog_limit_admin', '20', 0),
(212, 0, 'configblog', 'configblog_blog_menu', '1', 0),
(213, 0, 'configblog', 'configblog_article_download', '1', 0),
(214, 0, 'configblog', 'configblog_review_status', '1', 0),
(215, 0, 'configblog', 'configblog_review_guest', '1', 0),
(216, 0, 'configblog', 'configblog_review_mail', '1', 0),
(217, 0, 'configblog', 'configblog_image_category_width', '50', 0),
(218, 0, 'configblog', 'configblog_image_category_height', '50', 0),
(219, 0, 'configblog', 'configblog_image_article_width', '150', 0),
(220, 0, 'configblog', 'configblog_image_article_height', '150', 0),
(221, 0, 'configblog', 'configblog_image_related_width', '200', 0),
(222, 0, 'configblog', 'configblog_image_related_height', '200', 0),
(321, 0, 'developer', 'developer_theme', '0', 0),
(322, 0, 'developer', 'developer_sass', '1', 0),
(418, 0, 'opencart', 'opencart_username', 'demonized', 0),
(419, 0, 'opencart', 'opencart_secret', 'KcQpnhGLfh3N8HEI5G5zDC0tCxGDLXI57VR8v2DWb5SB9xyhZ3BhrKwyngphxlZUa0JBq61uLeh1J6G7scXrBgHXFCbXg30qe6rgcpbb9VMXtufSq0bnItYrED2GaJmhEvVDaucFEb17j0upKNuWDW4FY3j1YbVtLgkpqJwdUCaS1CnJX3j4JI2gn5dZVYMhxqpaPxz3OMUTqBINzQjcI0JAQ8Iuuj1FBBoqrGdGrbeBAxQ2xahaPAyOgiTsa7JeoAAIHhu4Yjyr6rym150bIcC2nNUboDViOol5GWx7TRp34ZlbMnAvhtMtRGtWzb7lDT1qNNWLywWJVMiGijS0Z5J4RTD1wqYwOywSAVtFGBFwhFa3oER220CQpDAIPdkLknhUWXfFru0prcX9zA3Cvh4Q80G4PjNsjoTlwndic0Zh8smVVkSwUnTpXIiIbpodCTG9XGaiGT7pR3dkPVuXszifWsI6fMpEbnMaCCMH3KgolwnuhIo7xuU8gp5D6SZT', 0),
(531, 0, 'module_extendedsearch', 'module_extendedsearch_status', '1', 0),
(532, 0, 'module_extendedsearch', 'module_extendedsearch_model', '1', 0),
(533, 0, 'module_extendedsearch', 'module_extendedsearch_sku', '1', 0),
(534, 0, 'module_extendedsearch', 'module_extendedsearch_upc', '1', 0),
(535, 0, 'module_extendedsearch', 'module_extendedsearch_ean', '1', 0),
(536, 0, 'module_extendedsearch', 'module_extendedsearch_jan', '1', 0),
(537, 0, 'module_extendedsearch', 'module_extendedsearch_isbn', '1', 0),
(538, 0, 'module_extendedsearch', 'module_extendedsearch_mpn', '1', 0),
(539, 0, 'module_extendedsearch', 'module_extendedsearch_location', '1', 0),
(540, 0, 'module_extendedsearch', 'module_extendedsearch_attr', '1', 0),
(541, 0, 'd_quickcheckout', 'd_quickcheckout_setting_cycle', '{\"1\":\"1  \"}', 1),
(542, 0, 'd_quickcheckout', 'd_quickcheckout_status', '1', 0),
(543, 0, 'd_quickcheckout', 'd_quickcheckout_trigger', '#button-confirm, .button, .btn, .button_oc, input[type=submit]', 0),
(544, 0, 'd_quickcheckout', 'd_quickcheckout_debug', '0', 0),
(545, 0, 'd_quickcheckout', 'd_quickcheckout_setting', '{\"name\":\"05\\/16\\/2019 05:45:04 am\",\"general\":{\"clear_session\":\"0\",\"login_refresh\":\"0\",\"analytics_event\":\"0\",\"update_mini_cart\":\"1\",\"compress\":\"1\",\"min_order\":{\"value\":\"0\",\"text\":{\"1\":\"You must have a sum more then %s to make an order       \",\"2\":\"The minimum order is %s      \"}},\"min_quantity\":{\"value\":\"0\",\"text\":{\"1\":\"You must have a quantity more then %s to make an order    \",\"2\":\"The minimum quantity is %s   \"}},\"config\":\"d_quickcheckout\"}}', 1),
(546, 0, 'd_quickcheckout', 'd_quickcheckout_debug_file', 'd_quickcheckout.log', 0),
(1180, 0, 'total_total', 'total_total_status', '1', 0),
(1181, 0, 'total_total', 'total_total_sort_order', '9', 0),
(1189, 0, 'total_sub_total', 'total_sub_total_status', '1', 0),
(1190, 0, 'total_shipping', 'total_shipping_estimator', '1', 0),
(1191, 0, 'total_shipping', 'total_shipping_status', '1', 0),
(1192, 0, 'total_shipping', 'total_shipping_sort_order', '3', 0),
(1193, 0, 'module_quickcheckout', 'module_quickcheckout_status', '1', 0),
(6066, 0, 'module_account', 'module_account_status', '1', 0),
(6792, 0, 'analytics_metrika', 'analytics_metrika_code', '&lt;!-- Yandex.Metrika counter --&gt;\r\n&lt;script type=&quot;text/javascript&quot; &gt;\r\n   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};\r\n   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})\r\n   (window, document, &quot;script&quot;, &quot;https://mc.yandex.ru/metrika/tag.js&quot;, &quot;ym&quot;);\r\n\r\n   ym(55343608, &quot;init&quot;, {\r\n        clickmap:true,\r\n        trackLinks:true,\r\n        accurateTrackBounce:true\r\n   });\r\n&lt;/script&gt;\r\n&lt;noscript&gt;&lt;div&gt;&lt;img src=&quot;https://mc.yandex.ru/watch/55343608&quot; style=&quot;position:absolute; left:-9999px;&quot; alt=&quot;&quot; /&gt;&lt;/div&gt;&lt;/noscript&gt;\r\n&lt;!-- /Yandex.Metrika counter --&gt;', 0),
(6793, 0, 'analytics_metrika', 'analytics_metrika_status', '1', 0),
(6794, 0, 'feed_yandex_sitemap', 'feed_yandex_sitemap_status', '1', 0),
(7389, 0, 'module_manager', 'module_manager_buttons', '[\"history\",\"invoice\",\"shipping\",\"delete\",\"create\",\"minimize\",\"toggle\",\"filter\",\"clear\",\"edit_customer\",\"view_order\",\"edit_order\"]', 1),
(7390, 0, 'module_manager', 'module_manager_columns', '[\"select\",\"order_id\",\"order_status_id\",\"customer\",\"recipient\",\"date_added\",\"date_modified\",\"products\",\"payment\",\"shipping\",\"subtotal\",\"total\",\"actions\"]', 1),
(7391, 0, 'module_manager', 'module_manager_statuses', '{\"16\":{\"checked\":\"1\",\"color\":\"\"},\"2\":{\"checked\":\"1\",\"color\":\"\"},\"8\":{\"checked\":\"1\",\"color\":\"\"},\"11\":{\"checked\":\"1\",\"color\":\"\"},\"3\":{\"checked\":\"1\",\"color\":\"\"},\"14\":{\"checked\":\"1\",\"color\":\"\"},\"10\":{\"checked\":\"1\",\"color\":\"\"},\"15\":{\"checked\":\"1\",\"color\":\"\"},\"1\":{\"checked\":\"1\",\"color\":\"\"},\"9\":{\"checked\":\"1\",\"color\":\"\"},\"7\":{\"checked\":\"1\",\"color\":\"\"},\"12\":{\"checked\":\"1\",\"color\":\"\"},\"13\":{\"checked\":\"1\",\"color\":\"\"},\"5\":{\"checked\":\"1\",\"color\":\"\"},\"0\":{\"checked\":\"1\",\"color\":\"\"}}', 1),
(7392, 0, 'module_manager', 'module_manager_payments', '{\"cod\":{\"color\":\"\"},\"free_checkout\":{\"color\":\"\"}}', 1),
(7393, 0, 'module_manager', 'module_manager_shippings', '{\"free\":{\"color\":\"\"}}', 1),
(7394, 0, 'module_manager', 'module_manager_mode', 'full', 0),
(7395, 0, 'module_manager', 'module_manager_notice', '', 0),
(7396, 0, 'module_manager', 'module_manager_hide_dashboard', '', 0),
(7397, 0, 'module_manager', 'module_manager_filters', '', 0),
(7398, 0, 'module_manager', 'module_manager_notify', '1', 0),
(7399, 0, 'module_manager', 'module_manager_default_limit', '10', 0),
(7400, 0, 'module_manager', 'module_manager_default_links', '', 0),
(7401, 0, 'module_manager', 'module_manager_default_sort', 'o.order_id', 0),
(7402, 0, 'module_manager', 'module_manager_default_order', 'DESC', 0),
(7403, 0, 'module_manager', 'module_manager_name_format', 'firstname', 0),
(7404, 0, 'module_manager', 'module_manager_address_format', '&lt;b&gt;{name}&lt;/b&gt;\r\nМагазин: {store}\r\n{company}\r\n&lt;a href=\'skype:+{telephone}?call\'&gt;{telephone}&lt;/a&gt;\r\n&lt;a href=\'mailto:{email}\'&gt;{email}&lt;/a&gt;\r\n{address}\r\n{country}, {city}, {zone}\r\n{postcode}\'\'\'\'\'', 0),
(7405, 0, 'module_manager', 'module_manager_date_format', '', 0),
(7406, 0, 'module_manager', 'module_manager_addips', '', 0),
(7407, 0, 'module_manager', 'module_manager_status', '1', 0),
(9417, 0, 'module_template_switcher', 'module_template_switcher_status', '1', 0),
(9734, 0, 'module_ocfilter', 'module_ocfilter_status', '1', 0),
(9735, 0, 'module_ocfilter', 'module_ocfilter_sub_category', '0', 0),
(9736, 0, 'module_ocfilter', 'module_ocfilter_sitemap_status', '1', 0),
(9737, 0, 'module_ocfilter', 'module_ocfilter_sitemap_link', 'http://neko.neko.ru/index.php?route=extension/feed/ocfilter_sitemap', 0),
(9738, 0, 'module_ocfilter', 'module_ocfilter_search_button', '1', 0),
(9739, 0, 'module_ocfilter', 'module_ocfilter_show_selected', '1', 0),
(9740, 0, 'module_ocfilter', 'module_ocfilter_show_price', '1', 0),
(9741, 0, 'module_ocfilter', 'module_ocfilter_show_counter', '1', 0),
(9742, 0, 'module_ocfilter', 'module_ocfilter_manufacturer', '0', 0),
(9743, 0, 'module_ocfilter', 'module_ocfilter_manufacturer_type', 'checkbox', 0),
(9744, 0, 'module_ocfilter', 'module_ocfilter_stock_status', '1', 0),
(9745, 0, 'module_ocfilter', 'module_ocfilter_stock_status_method', 'quantity', 0),
(9746, 0, 'module_ocfilter', 'module_ocfilter_stock_status_type', 'checkbox', 0),
(9747, 0, 'module_ocfilter', 'module_ocfilter_stock_out_value', '1', 0),
(9748, 0, 'module_ocfilter', 'module_ocfilter_manual_price', '1', 0),
(9749, 0, 'module_ocfilter', 'module_ocfilter_consider_discount', '0', 0),
(9750, 0, 'module_ocfilter', 'module_ocfilter_consider_special', '0', 0),
(9751, 0, 'module_ocfilter', 'module_ocfilter_consider_option', '0', 0),
(9752, 0, 'module_ocfilter', 'module_ocfilter_show_options_limit', '0', 0),
(9753, 0, 'module_ocfilter', 'module_ocfilter_show_values_limit', '10', 0),
(9754, 0, 'module_ocfilter', 'module_ocfilter_hide_empty_values', '1', 0),
(9755, 0, 'module_ocfilter', 'module_ocfilter_copy_type', 'checkbox', 0),
(9756, 0, 'module_ocfilter', 'module_ocfilter_copy_status', '1', 0),
(9757, 0, 'module_ocfilter', 'module_ocfilter_copy_attribute', '0', 0),
(9758, 0, 'module_ocfilter', 'module_ocfilter_copy_filter', '1', 0),
(9759, 0, 'module_ocfilter', 'module_ocfilter_copy_option', '0', 0),
(9760, 0, 'module_ocfilter', 'module_ocfilter_copy_truncate', '1', 0),
(9761, 0, 'module_ocfilter', 'module_ocfilter_copy_category', '1', 0),
(10031, 0, 'payment_rbs', 'payment_rbs_status', '1', 0),
(10032, 0, 'payment_rbs', 'payment_rbs_merchantLogin', '', 0),
(10033, 0, 'payment_rbs', 'payment_rbs_merchantPassword', '', 0),
(10034, 0, 'payment_rbs', 'payment_rbs_mode', 'test', 0),
(10035, 0, 'payment_rbs', 'payment_rbs_stage', 'one', 0),
(10036, 0, 'payment_rbs', 'payment_rbs_order_status_id', '15', 0),
(10037, 0, 'payment_rbs', 'payment_rbs_sort_order', '', 0),
(10038, 0, 'payment_rbs', 'payment_rbs_logging', '1', 0),
(10039, 0, 'payment_rbs', 'payment_rbs_currency', '0', 0),
(10040, 0, 'payment_rbs', 'payment_rbs_ofd_status', '1', 0),
(10041, 0, 'payment_rbs', 'payment_rbs_taxSystem', '3', 0),
(10042, 0, 'payment_rbs', 'payment_rbs_taxType', '0', 0),
(10043, 0, 'payment_rbs', 'payment_rbs_ffdVersion', 'v10', 0),
(10044, 0, 'payment_rbs', 'payment_rbs_paymentMethodType', '1', 0),
(10045, 0, 'payment_rbs', 'payment_rbs_paymentObjectType', '1', 0),
(11387, 0, 'shipping_xshippingpro', 'shipping_xshippingpro_status', '1', 0),
(11388, 0, 'shipping_xshippingpro', 'shipping_xshippingpro_sort_order', '', 0),
(11389, 0, 'shipping_xshippingpro', 'shipping_xshippingpro_map_api', '', 0),
(11390, 0, 'shipping_xshippingpro', 'shipping_xshippingpro_group', 'no_group', 0),
(11391, 0, 'shipping_xshippingpro', 'shipping_xshippingpro_group_limit', '1', 0),
(11392, 0, 'shipping_xshippingpro', 'shipping_xshippingpro_sorting', '1', 0),
(11393, 0, 'shipping_xshippingpro', 'shipping_xshippingpro_heading', '{\"1\":\"\\u041e\\u043f\\u0446\\u0438\\u0438 \\u0434\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0438\"}', 1),
(11394, 0, 'shipping_xshippingpro', 'shipping_xshippingpro_desc_mail', '', 0),
(11395, 0, 'shipping_xshippingpro', 'shipping_xshippingpro_debug', '0', 0),
(11396, 0, 'shipping_xshippingpro', 'shipping_xshippingpro_sub_group', '{\"1\":\"no_group\",\"2\":\"no_group\",\"3\":\"no_group\",\"4\":\"no_group\",\"5\":\"no_group\",\"6\":\"no_group\",\"7\":\"no_group\",\"8\":\"no_group\",\"9\":\"no_group\",\"10\":\"no_group\"}', 1),
(11397, 0, 'shipping_xshippingpro', 'shipping_xshippingpro_sub_group_limit', '{\"1\":\"1\",\"2\":\"1\",\"3\":\"1\",\"4\":\"1\",\"5\":\"1\",\"6\":\"1\",\"7\":\"1\",\"8\":\"1\",\"9\":\"1\",\"10\":\"1\"}', 1),
(11398, 0, 'shipping_xshippingpro', 'shipping_xshippingpro_sub_group_name', '{\"1\":\"\",\"2\":\"\",\"3\":\"\",\"4\":\"\",\"5\":\"\",\"6\":\"\",\"7\":\"\",\"8\":\"\",\"9\":\"\",\"10\":\"\"}', 1),
(11399, 0, 'shipping_xshippingpro', 'shipping_xshippingpro_sub_group_desc', '{\"1\":\"\",\"2\":\"\",\"3\":\"\",\"4\":\"\",\"5\":\"\",\"6\":\"\",\"7\":\"\",\"8\":\"\",\"9\":\"\",\"10\":\"\"}', 1),
(11400, 0, 'shipping_xshippingpro', 'shipping_xshippingpro_estimator', '{\"type\":\"method\",\"selector\":\"#product\",\"css\":\"\"}', 1),
(11401, 0, 'quickcheckout', 'quickcheckout_status', '1', 0),
(11402, 0, 'quickcheckout', 'quickcheckout_minimum_order', '0', 0),
(11403, 0, 'quickcheckout', 'quickcheckout_debug', '0', 0),
(11404, 0, 'quickcheckout', 'quickcheckout_confirmation_page', '1', 0),
(11405, 0, 'quickcheckout', 'quickcheckout_save_data', '1', 0),
(11406, 0, 'quickcheckout', 'quickcheckout_edit_cart', '1', 0),
(11407, 0, 'quickcheckout', 'quickcheckout_highlight_error', '1', 0),
(11408, 0, 'quickcheckout', 'quickcheckout_text_error', '1', 0),
(11409, 0, 'quickcheckout', 'quickcheckout_auto_submit', '0', 0),
(11410, 0, 'quickcheckout', 'quickcheckout_payment_target', '#button-confirm, .button, .btn', 0),
(11411, 0, 'quickcheckout', 'quickcheckout_skip_cart', '1', 0),
(11412, 0, 'quickcheckout', 'quickcheckout_force_bootstrap', '0', 0),
(11413, 0, 'quickcheckout', 'quickcheckout_proceed_button_text', '{\"1\":\"\\u041f\\u043e\\u0434\\u0442\\u0432\\u0435\\u0440\\u0434\\u0438\\u0442\\u044c\"}', 1),
(11414, 0, 'quickcheckout', 'quickcheckout_keyword', '{\"1\":\"checkout\"}', 1),
(11415, 0, 'quickcheckout', 'quickcheckout_layout', '4', 0),
(11416, 0, 'quickcheckout', 'quickcheckout_responsive', '1', 0),
(11417, 0, 'quickcheckout', 'quickcheckout_column', '{\"1\":\"8\",\"2\":\"4\",\"3\":\"0\",\"4\":\"4\"}', 1),
(11418, 0, 'quickcheckout', 'quickcheckout_step', '{\"cart\":{\"column\":\"1\",\"row\":\"0\"},\"login\":{\"column\":\"2\",\"row\":\"0\"},\"shipping_method\":{\"column\":\"4\",\"row\":\"0\"},\"payment_address\":{\"column\":\"4\",\"row\":\"1\"},\"shipping_address\":{\"column\":\"4\",\"row\":\"2\"},\"payment_method\":{\"column\":\"4\",\"row\":\"3\"},\"coupons\":{\"column\":\"4\",\"row\":\"4\"},\"confirm\":{\"column\":\"4\",\"row\":\"6\"}}', 1),
(11419, 0, 'quickcheckout', 'quickcheckout_login_module', '0', 0),
(11420, 0, 'quickcheckout', 'quickcheckout_cart', '1', 0),
(11421, 0, 'quickcheckout', 'quickcheckout_show_shipping_address', '0', 0),
(11422, 0, 'quickcheckout', 'quickcheckout_slide_effect', '0', 0),
(11423, 0, 'quickcheckout', 'quickcheckout_load_screen', '0', 0),
(11424, 0, 'quickcheckout', 'quickcheckout_loading_display', '0', 0),
(11425, 0, 'quickcheckout', 'quickcheckout_custom_css', '', 0),
(11426, 0, 'quickcheckout', 'quickcheckout_field_firstname', '{\"display\":\"on\",\"default\":{\"1\":\"\"},\"placeholder\":{\"1\":\"\\u0418\\u043c\\u044f \\u0438 \\u0444\\u0430\\u043c\\u0438\\u043b\\u0438\\u044f\"},\"sort_order\":\"3\"}', 1),
(11427, 0, 'quickcheckout', 'quickcheckout_field_lastname', '{\"default\":{\"1\":\"\"},\"placeholder\":{\"1\":\"\"},\"sort_order\":\"2\"}', 1),
(11428, 0, 'quickcheckout', 'quickcheckout_field_email', '{\"display\":\"on\",\"required\":\"on\",\"default\":{\"1\":\"\"},\"placeholder\":{\"1\":\"\\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0447\\u0442\\u0430\"},\"sort_order\":\"2\"}', 1),
(11429, 0, 'quickcheckout', 'quickcheckout_field_telephone', '{\"display\":\"on\",\"required\":\"on\",\"default\":{\"1\":\"\"},\"placeholder\":{\"1\":\"\\u041c\\u043e\\u0431\\u0438\\u043b\\u044c\\u043d\\u044b\\u0439 \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\"},\"sort_order\":\"4\"}', 1),
(11430, 0, 'quickcheckout', 'quickcheckout_field_company', '{\"default\":{\"1\":\"\"},\"placeholder\":{\"1\":\"\\u0418\\u041d\\u041d \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438\"},\"sort_order\":\"5\"}', 1),
(11431, 0, 'quickcheckout', 'quickcheckout_field_customer_group', '{\"display\":\"on\",\"sort_order\":\"0\"}', 1),
(11432, 0, 'quickcheckout', 'quickcheckout_field_address_1', '{\"display\":\"on\",\"default\":{\"1\":\"\"},\"placeholder\":{\"1\":\"\\u0410\\u0434\\u0440\\u0435\\u0441 \\u0434\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0438\"},\"sort_order\":\"1\"}', 1),
(11433, 0, 'quickcheckout', 'quickcheckout_field_address_2', '{\"default\":{\"1\":\"\"},\"placeholder\":{\"1\":\"\"},\"sort_order\":\"9\"}', 1),
(11434, 0, 'quickcheckout', 'quickcheckout_field_city', '{\"default\":{\"1\":\"\"},\"placeholder\":{\"1\":\"\"},\"sort_order\":\"10\"}', 1),
(11435, 0, 'quickcheckout', 'quickcheckout_field_postcode', '{\"default\":{\"1\":\"\"},\"placeholder\":{\"1\":\"\"},\"sort_order\":\"11\"}', 1),
(11436, 0, 'quickcheckout', 'quickcheckout_field_country', '{\"default\":\"176\",\"sort_order\":\"12\"}', 1),
(11437, 0, 'quickcheckout', 'quickcheckout_field_zone', '{\"required\":\"on\",\"default\":\"2748\",\"sort_order\":\"0\"}', 1),
(11438, 0, 'quickcheckout', 'quickcheckout_field_newsletter', '{\"sort_order\":\"\"}', 1),
(11439, 0, 'quickcheckout', 'quickcheckout_field_register', '{\"sort_order\":\"\"}', 1),
(11440, 0, 'quickcheckout', 'quickcheckout_field_shipping', '{\"required\":\"on\",\"default\":\"on\",\"sort_order\":\"\"}', 1),
(11441, 0, 'quickcheckout', 'quickcheckout_field_rules', '{\"display\":\"on\",\"required\":\"on\",\"sort_order\":\"\"}', 1),
(11442, 0, 'quickcheckout', 'quickcheckout_field_comment', '{\"default\":{\"1\":\"\"},\"placeholder\":{\"1\":\"\"}}', 1),
(11443, 0, 'quickcheckout', 'quickcheckout_coupon', '0', 0),
(11444, 0, 'quickcheckout', 'quickcheckout_voucher', '0', 0),
(11445, 0, 'quickcheckout', 'quickcheckout_reward', '0', 0),
(11446, 0, 'quickcheckout', 'quickcheckout_html_header', '{\"1\":\"\"}', 1),
(11447, 0, 'quickcheckout', 'quickcheckout_html_footer', '{\"1\":\"\"}', 1),
(11448, 0, 'quickcheckout', 'quickcheckout_payment_module', '1', 0),
(11449, 0, 'quickcheckout', 'quickcheckout_payment_reload', '0', 0),
(11450, 0, 'quickcheckout', 'quickcheckout_payment', '1', 0),
(11451, 0, 'quickcheckout', 'quickcheckout_payment_default', 'rbs', 0),
(11452, 0, 'quickcheckout', 'quickcheckout_shipping_reload', '0', 0),
(11453, 0, 'quickcheckout', 'quickcheckout_payment_logo', '{\"cod\":\"\",\"rbs\":\"\"}', 1),
(11454, 0, 'quickcheckout', 'quickcheckout_shipping_module', '0', 0),
(11455, 0, 'quickcheckout', 'quickcheckout_shipping_title_display', '1', 0),
(11456, 0, 'quickcheckout', 'quickcheckout_shipping', '1', 0),
(11457, 0, 'quickcheckout', 'quickcheckout_shipping_default', 'xshippingpro', 0),
(11458, 0, 'quickcheckout', 'quickcheckout_shipping_logo', '{\"flat\":\"\",\"free\":\"\",\"xshippingpro\":\"\"}', 1),
(11459, 0, 'quickcheckout', 'quickcheckout_survey', '0', 0),
(11460, 0, 'quickcheckout', 'quickcheckout_survey_required', '0', 0),
(11461, 0, 'quickcheckout', 'quickcheckout_survey_text', '{\"1\":\"\"}', 1),
(11462, 0, 'quickcheckout', 'quickcheckout_survey_type', '0', 0),
(11463, 0, 'quickcheckout', 'quickcheckout_delivery', '0', 0),
(11464, 0, 'quickcheckout', 'quickcheckout_delivery_time', '0', 0),
(11465, 0, 'quickcheckout', 'quickcheckout_delivery_required', '0', 0),
(11466, 0, 'quickcheckout', 'quickcheckout_delivery_unavailable', '&quot;2017-10-31&quot;, &quot;2017-08-11&quot;, &quot;2017-12-25&quot;', 0),
(11467, 0, 'quickcheckout', 'quickcheckout_delivery_min', '1', 0),
(11468, 0, 'quickcheckout', 'quickcheckout_delivery_max', '30', 0),
(11469, 0, 'quickcheckout', 'quickcheckout_delivery_min_hour', '09', 0),
(11470, 0, 'quickcheckout', 'quickcheckout_delivery_max_hour', '17', 0),
(11471, 0, 'quickcheckout', 'quickcheckout_delivery_days_of_week', '', 0),
(11472, 0, 'theme_default', 'theme_default_directory', 'default', 0),
(11473, 0, 'theme_default', 'theme_default_status', '1', 0),
(11474, 0, 'theme_default', 'theme_default_product_limit', '15', 0),
(11475, 0, 'theme_default', 'theme_default_product_description_length', '100', 0),
(11476, 0, 'theme_default', 'theme_default_image_category_width', '80', 0),
(11477, 0, 'theme_default', 'theme_default_image_category_height', '80', 0),
(11478, 0, 'theme_default', 'theme_default_image_manufacturer_width', '80', 0),
(11479, 0, 'theme_default', 'theme_default_image_manufacturer_height', '80', 0),
(11480, 0, 'theme_default', 'theme_default_image_thumb_width', '1000', 0),
(11481, 0, 'theme_default', 'theme_default_image_thumb_height', '1000', 0),
(11482, 0, 'theme_default', 'theme_default_image_popup_width', '1000', 0),
(11483, 0, 'theme_default', 'theme_default_image_popup_height', '1000', 0),
(11484, 0, 'theme_default', 'theme_default_image_product_width', '228', 0),
(11485, 0, 'theme_default', 'theme_default_image_product_height', '228', 0),
(11486, 0, 'theme_default', 'theme_default_image_additional_width', '1000', 0),
(11487, 0, 'theme_default', 'theme_default_image_additional_height', '1000', 0),
(11488, 0, 'theme_default', 'theme_default_image_related_width', '200', 0),
(11489, 0, 'theme_default', 'theme_default_image_related_height', '200', 0),
(11490, 0, 'theme_default', 'theme_default_image_compare_width', '90', 0),
(11491, 0, 'theme_default', 'theme_default_image_compare_height', '90', 0),
(11492, 0, 'theme_default', 'theme_default_image_wishlist_width', '64', 0),
(11493, 0, 'theme_default', 'theme_default_image_wishlist_height', '64', 0),
(11494, 0, 'theme_default', 'theme_default_image_cart_width', '200', 0),
(11495, 0, 'theme_default', 'theme_default_image_cart_height', '200', 0),
(11496, 0, 'theme_default', 'theme_default_image_location_width', '268', 0),
(11497, 0, 'theme_default', 'theme_default_image_location_height', '50', 0),
(17466, 0, 'unixml', 'unixml_domby_status', '0', 0),
(17467, 0, 'unixml', 'unixml_domby_name', '', 0),
(17468, 0, 'unixml', 'unixml_domby_language', '1', 0),
(17469, 0, 'unixml', 'unixml_domby_currency', '3', 0),
(17470, 0, 'unixml', 'unixml_domby_field_id', 'p.product_id', 0),
(17471, 0, 'unixml', 'unixml_domby_markup', '', 0),
(17472, 0, 'unixml', 'unixml_domby_custom_xml', '', 0),
(17473, 0, 'unixml', 'unixml_domby_utm', '', 0),
(17474, 0, 'unixml', 'unixml_domby_delivery_cost', '', 0),
(17475, 0, 'unixml', 'unixml_domby_delivery_time', '', 0),
(17476, 0, 'unixml', 'unixml_domby_delivery_jump', '', 0),
(17477, 0, 'unixml', 'unixml_domby_fields', '', 0),
(17478, 0, 'unixml', 'unixml_domby_field_price', 'p.price', 0),
(17479, 0, 'unixml', 'unixml_domby_step', '10000', 0),
(17480, 0, 'unixml', 'unixml_domby_seopro', '0', 0),
(17481, 0, 'unixml', 'unixml_domby_log', '', 0),
(17482, 0, 'unixml', 'unixml_domby_quantity', '0', 0),
(17483, 0, 'unixml', 'unixml_domby_stock', '8', 0),
(17484, 0, 'unixml', 'unixml_domby_option_multiplier_status', '0', 0),
(17485, 0, 'unixml', 'unixml_domby_genname', '', 0),
(17486, 0, 'unixml', 'unixml_domby_products_mode', '', 0),
(17487, 0, 'unixml', 'unixml_domby_product', '', 0),
(17488, 0, 'unixml', 'unixml_domby_andor', '0', 0),
(17489, 0, 'unixml', 'unixml_domby_image', '0', 0),
(17490, 0, 'unixml', 'unixml_domby_images', '0', 0),
(17491, 0, 'unixml', 'unixml_domby_attribute_status', '0', 0),
(17492, 0, 'unixml', 'unixml_domby_gendesc', '', 0),
(17493, 0, 'unixml', 'unixml_domby_clear_desc', '', 0),
(17494, 0, 'unixml', 'unixml_domby_gendesc_mode', '', 0),
(17495, 0, 'unixml', 'unixml_domby_secret', '', 0),
(17496, 0, 'unixml', 'unixml_rozetka_status', '0', 0),
(17497, 0, 'unixml', 'unixml_rozetka_name', '', 0),
(17498, 0, 'unixml', 'unixml_rozetka_language', '1', 0),
(17499, 0, 'unixml', 'unixml_rozetka_currency', '3', 0),
(17500, 0, 'unixml', 'unixml_rozetka_field_id', 'p.product_id', 0),
(17501, 0, 'unixml', 'unixml_rozetka_markup', '', 0),
(17502, 0, 'unixml', 'unixml_rozetka_custom_xml', '', 0),
(17503, 0, 'unixml', 'unixml_rozetka_utm', '', 0),
(17504, 0, 'unixml', 'unixml_rozetka_delivery_cost', '', 0),
(17505, 0, 'unixml', 'unixml_rozetka_delivery_time', '', 0),
(17506, 0, 'unixml', 'unixml_rozetka_delivery_jump', '', 0),
(17507, 0, 'unixml', 'unixml_rozetka_fields', '', 0),
(17508, 0, 'unixml', 'unixml_rozetka_field_price', 'p.price', 0),
(17509, 0, 'unixml', 'unixml_rozetka_step', '10000', 0),
(17510, 0, 'unixml', 'unixml_rozetka_seopro', '0', 0),
(17511, 0, 'unixml', 'unixml_rozetka_log', '', 0),
(17512, 0, 'unixml', 'unixml_rozetka_quantity', '0', 0),
(17513, 0, 'unixml', 'unixml_rozetka_stock', '8', 0),
(17514, 0, 'unixml', 'unixml_rozetka_option_multiplier_status', '0', 0),
(17515, 0, 'unixml', 'unixml_rozetka_genname', '', 0),
(17516, 0, 'unixml', 'unixml_rozetka_products_mode', '', 0),
(17517, 0, 'unixml', 'unixml_rozetka_product', '', 0),
(17518, 0, 'unixml', 'unixml_rozetka_andor', '0', 0),
(17519, 0, 'unixml', 'unixml_rozetka_image', '0', 0),
(17520, 0, 'unixml', 'unixml_rozetka_images', '0', 0),
(17521, 0, 'unixml', 'unixml_rozetka_attribute_status', '0', 0),
(17522, 0, 'unixml', 'unixml_rozetka_gendesc', '', 0),
(17523, 0, 'unixml', 'unixml_rozetka_clear_desc', '', 0),
(17524, 0, 'unixml', 'unixml_rozetka_gendesc_mode', '', 0),
(17525, 0, 'unixml', 'unixml_rozetka_secret', '', 0),
(17526, 0, 'unixml', 'unixml_hotline_status', '0', 0),
(17527, 0, 'unixml', 'unixml_hotline_name', '', 0),
(17528, 0, 'unixml', 'unixml_hotline_language', '1', 0),
(17529, 0, 'unixml', 'unixml_hotline_currency', '3', 0),
(17530, 0, 'unixml', 'unixml_hotline_field_id', 'p.product_id', 0),
(17531, 0, 'unixml', 'unixml_hotline_markup', '', 0),
(17532, 0, 'unixml', 'unixml_hotline_custom_xml', '', 0),
(17533, 0, 'unixml', 'unixml_hotline_utm', '', 0),
(17534, 0, 'unixml', 'unixml_hotline_delivery_cost', '', 0),
(17535, 0, 'unixml', 'unixml_hotline_delivery_time', '', 0),
(17536, 0, 'unixml', 'unixml_hotline_delivery_jump', '', 0),
(17537, 0, 'unixml', 'unixml_hotline_fields', '', 0),
(17538, 0, 'unixml', 'unixml_hotline_field_price', 'p.price', 0),
(17539, 0, 'unixml', 'unixml_hotline_step', '10000', 0),
(17540, 0, 'unixml', 'unixml_hotline_seopro', '0', 0),
(17541, 0, 'unixml', 'unixml_hotline_log', '', 0),
(17542, 0, 'unixml', 'unixml_hotline_quantity', '0', 0),
(17543, 0, 'unixml', 'unixml_hotline_stock', '8', 0),
(17544, 0, 'unixml', 'unixml_hotline_option_multiplier_status', '0', 0),
(17545, 0, 'unixml', 'unixml_hotline_genname', '', 0),
(17546, 0, 'unixml', 'unixml_hotline_products_mode', '', 0),
(17547, 0, 'unixml', 'unixml_hotline_product', '', 0),
(17548, 0, 'unixml', 'unixml_hotline_andor', '0', 0),
(17549, 0, 'unixml', 'unixml_hotline_image', '0', 0),
(17550, 0, 'unixml', 'unixml_hotline_images', '0', 0),
(17551, 0, 'unixml', 'unixml_hotline_attribute_status', '0', 0),
(17552, 0, 'unixml', 'unixml_hotline_gendesc', '', 0),
(17553, 0, 'unixml', 'unixml_hotline_clear_desc', '', 0),
(17554, 0, 'unixml', 'unixml_hotline_gendesc_mode', '', 0),
(17555, 0, 'unixml', 'unixml_hotline_secret', '', 0),
(17556, 0, 'unixml', 'unixml_price_status', '0', 0),
(17557, 0, 'unixml', 'unixml_price_name', '', 0),
(17558, 0, 'unixml', 'unixml_price_language', '1', 0),
(17559, 0, 'unixml', 'unixml_price_currency', '3', 0),
(17560, 0, 'unixml', 'unixml_price_field_id', 'p.product_id', 0),
(17561, 0, 'unixml', 'unixml_price_markup', '', 0),
(17562, 0, 'unixml', 'unixml_price_custom_xml', '', 0),
(17563, 0, 'unixml', 'unixml_price_utm', '', 0),
(17564, 0, 'unixml', 'unixml_price_delivery_cost', '', 0),
(17565, 0, 'unixml', 'unixml_price_delivery_time', '', 0),
(17566, 0, 'unixml', 'unixml_price_delivery_jump', '', 0),
(17567, 0, 'unixml', 'unixml_price_fields', '', 0),
(17568, 0, 'unixml', 'unixml_price_field_price', 'p.price', 0),
(17569, 0, 'unixml', 'unixml_price_step', '10000', 0),
(17570, 0, 'unixml', 'unixml_price_seopro', '0', 0),
(17571, 0, 'unixml', 'unixml_price_log', '', 0),
(17572, 0, 'unixml', 'unixml_price_quantity', '0', 0),
(17573, 0, 'unixml', 'unixml_price_stock', '8', 0),
(17574, 0, 'unixml', 'unixml_price_option_multiplier_status', '0', 0),
(17575, 0, 'unixml', 'unixml_price_genname', '', 0),
(17576, 0, 'unixml', 'unixml_price_products_mode', '', 0),
(17577, 0, 'unixml', 'unixml_price_product', '', 0),
(17578, 0, 'unixml', 'unixml_price_andor', '0', 0),
(17579, 0, 'unixml', 'unixml_price_image', '0', 0),
(17580, 0, 'unixml', 'unixml_price_images', '0', 0),
(17581, 0, 'unixml', 'unixml_price_attribute_status', '0', 0),
(17582, 0, 'unixml', 'unixml_price_gendesc', '', 0),
(17583, 0, 'unixml', 'unixml_price_clear_desc', '', 0),
(17584, 0, 'unixml', 'unixml_price_gendesc_mode', '', 0),
(17585, 0, 'unixml', 'unixml_price_secret', '', 0),
(17586, 0, 'unixml', 'unixml_nadavi_status', '0', 0),
(17587, 0, 'unixml', 'unixml_nadavi_name', '', 0),
(17588, 0, 'unixml', 'unixml_nadavi_language', '1', 0),
(17589, 0, 'unixml', 'unixml_nadavi_currency', '3', 0),
(17590, 0, 'unixml', 'unixml_nadavi_field_id', 'p.product_id', 0),
(17591, 0, 'unixml', 'unixml_nadavi_markup', '', 0),
(17592, 0, 'unixml', 'unixml_nadavi_custom_xml', '', 0),
(17593, 0, 'unixml', 'unixml_nadavi_utm', '', 0),
(17594, 0, 'unixml', 'unixml_nadavi_delivery_cost', '', 0),
(17595, 0, 'unixml', 'unixml_nadavi_delivery_time', '', 0),
(17596, 0, 'unixml', 'unixml_nadavi_delivery_jump', '', 0),
(17597, 0, 'unixml', 'unixml_nadavi_fields', '', 0),
(17598, 0, 'unixml', 'unixml_nadavi_field_price', 'p.price', 0),
(17599, 0, 'unixml', 'unixml_nadavi_step', '10000', 0),
(17600, 0, 'unixml', 'unixml_nadavi_seopro', '0', 0),
(17601, 0, 'unixml', 'unixml_nadavi_log', '', 0),
(17602, 0, 'unixml', 'unixml_nadavi_quantity', '0', 0),
(17603, 0, 'unixml', 'unixml_nadavi_stock', '8', 0),
(17604, 0, 'unixml', 'unixml_nadavi_option_multiplier_status', '0', 0),
(17605, 0, 'unixml', 'unixml_nadavi_genname', '', 0),
(17606, 0, 'unixml', 'unixml_nadavi_products_mode', '', 0),
(17607, 0, 'unixml', 'unixml_nadavi_product', '', 0),
(17608, 0, 'unixml', 'unixml_nadavi_andor', '0', 0),
(17609, 0, 'unixml', 'unixml_nadavi_image', '0', 0),
(17610, 0, 'unixml', 'unixml_nadavi_images', '0', 0),
(17611, 0, 'unixml', 'unixml_nadavi_attribute_status', '0', 0),
(17612, 0, 'unixml', 'unixml_nadavi_gendesc', '', 0),
(17613, 0, 'unixml', 'unixml_nadavi_clear_desc', '', 0),
(17614, 0, 'unixml', 'unixml_nadavi_gendesc_mode', '', 0),
(17615, 0, 'unixml', 'unixml_nadavi_secret', '', 0),
(17616, 0, 'unixml', 'unixml_google_status', '0', 0),
(17617, 0, 'unixml', 'unixml_google_name', '', 0),
(17618, 0, 'unixml', 'unixml_google_language', '1', 0),
(17619, 0, 'unixml', 'unixml_google_currency', '3', 0),
(17620, 0, 'unixml', 'unixml_google_field_id', 'p.product_id', 0),
(17621, 0, 'unixml', 'unixml_google_markup', '', 0),
(17622, 0, 'unixml', 'unixml_google_custom_xml', '', 0),
(17623, 0, 'unixml', 'unixml_google_utm', '', 0),
(17624, 0, 'unixml', 'unixml_google_delivery_cost', '', 0),
(17625, 0, 'unixml', 'unixml_google_delivery_time', '', 0),
(17626, 0, 'unixml', 'unixml_google_delivery_jump', '', 0),
(17627, 0, 'unixml', 'unixml_google_fields', '', 0),
(17628, 0, 'unixml', 'unixml_google_field_price', 'p.price', 0),
(17629, 0, 'unixml', 'unixml_google_step', '10000', 0),
(17630, 0, 'unixml', 'unixml_google_seopro', '0', 0),
(17631, 0, 'unixml', 'unixml_google_log', '', 0),
(17632, 0, 'unixml', 'unixml_google_quantity', '0', 0),
(17633, 0, 'unixml', 'unixml_google_stock', '8', 0),
(17634, 0, 'unixml', 'unixml_google_option_multiplier_status', '0', 0),
(17635, 0, 'unixml', 'unixml_google_genname', '', 0),
(17636, 0, 'unixml', 'unixml_google_products_mode', '', 0),
(17637, 0, 'unixml', 'unixml_google_product', '', 0),
(17638, 0, 'unixml', 'unixml_google_andor', '0', 0),
(17639, 0, 'unixml', 'unixml_google_image', '0', 0),
(17640, 0, 'unixml', 'unixml_google_images', '0', 0),
(17641, 0, 'unixml', 'unixml_google_attribute_status', '0', 0),
(17642, 0, 'unixml', 'unixml_google_gendesc', '', 0),
(17643, 0, 'unixml', 'unixml_google_clear_desc', '', 0),
(17644, 0, 'unixml', 'unixml_google_gendesc_mode', '', 0),
(17645, 0, 'unixml', 'unixml_google_secret', '', 0),
(17646, 0, 'unixml', 'unixml_facebook_status', '0', 0),
(17647, 0, 'unixml', 'unixml_facebook_name', '', 0),
(17648, 0, 'unixml', 'unixml_facebook_language', '1', 0),
(17649, 0, 'unixml', 'unixml_facebook_currency', '3', 0),
(17650, 0, 'unixml', 'unixml_facebook_field_id', 'p.product_id', 0),
(17651, 0, 'unixml', 'unixml_facebook_markup', '', 0),
(17652, 0, 'unixml', 'unixml_facebook_custom_xml', '', 0),
(17653, 0, 'unixml', 'unixml_facebook_utm', '', 0),
(17654, 0, 'unixml', 'unixml_facebook_delivery_cost', '', 0),
(17655, 0, 'unixml', 'unixml_facebook_delivery_time', '', 0),
(17656, 0, 'unixml', 'unixml_facebook_delivery_jump', '', 0),
(17657, 0, 'unixml', 'unixml_facebook_fields', '', 0),
(17658, 0, 'unixml', 'unixml_facebook_field_price', 'p.price', 0),
(17659, 0, 'unixml', 'unixml_facebook_step', '10000', 0),
(17660, 0, 'unixml', 'unixml_facebook_seopro', '0', 0),
(17661, 0, 'unixml', 'unixml_facebook_log', '', 0),
(17662, 0, 'unixml', 'unixml_facebook_quantity', '0', 0),
(17663, 0, 'unixml', 'unixml_facebook_stock', '8', 0),
(17664, 0, 'unixml', 'unixml_facebook_option_multiplier_status', '0', 0),
(17665, 0, 'unixml', 'unixml_facebook_genname', '', 0),
(17666, 0, 'unixml', 'unixml_facebook_products_mode', '', 0),
(17667, 0, 'unixml', 'unixml_facebook_product', '', 0),
(17668, 0, 'unixml', 'unixml_facebook_andor', '0', 0),
(17669, 0, 'unixml', 'unixml_facebook_image', '0', 0),
(17670, 0, 'unixml', 'unixml_facebook_images', '0', 0),
(17671, 0, 'unixml', 'unixml_facebook_attribute_status', '0', 0),
(17672, 0, 'unixml', 'unixml_facebook_gendesc', '', 0),
(17673, 0, 'unixml', 'unixml_facebook_clear_desc', '', 0),
(17674, 0, 'unixml', 'unixml_facebook_gendesc_mode', '', 0),
(17675, 0, 'unixml', 'unixml_facebook_secret', '', 0),
(17676, 0, 'unixml', 'unixml_prom_status', '0', 0),
(17677, 0, 'unixml', 'unixml_prom_name', '', 0),
(17678, 0, 'unixml', 'unixml_prom_language', '1', 0),
(17679, 0, 'unixml', 'unixml_prom_currency', '3', 0),
(17680, 0, 'unixml', 'unixml_prom_field_id', 'p.product_id', 0),
(17681, 0, 'unixml', 'unixml_prom_markup', '', 0),
(17682, 0, 'unixml', 'unixml_prom_custom_xml', '', 0),
(17683, 0, 'unixml', 'unixml_prom_utm', '', 0),
(17684, 0, 'unixml', 'unixml_prom_delivery_cost', '', 0),
(17685, 0, 'unixml', 'unixml_prom_delivery_time', '', 0),
(17686, 0, 'unixml', 'unixml_prom_delivery_jump', '', 0),
(17687, 0, 'unixml', 'unixml_prom_fields', '', 0),
(17688, 0, 'unixml', 'unixml_prom_field_price', 'p.price', 0),
(17689, 0, 'unixml', 'unixml_prom_step', '10000', 0),
(17690, 0, 'unixml', 'unixml_prom_seopro', '0', 0),
(17691, 0, 'unixml', 'unixml_prom_log', '', 0),
(17692, 0, 'unixml', 'unixml_prom_quantity', '0', 0),
(17693, 0, 'unixml', 'unixml_prom_stock', '8', 0),
(17694, 0, 'unixml', 'unixml_prom_option_multiplier_status', '0', 0),
(17695, 0, 'unixml', 'unixml_prom_genname', '', 0),
(17696, 0, 'unixml', 'unixml_prom_products_mode', '', 0),
(17697, 0, 'unixml', 'unixml_prom_product', '', 0),
(17698, 0, 'unixml', 'unixml_prom_andor', '0', 0),
(17699, 0, 'unixml', 'unixml_prom_image', '0', 0),
(17700, 0, 'unixml', 'unixml_prom_images', '0', 0),
(17701, 0, 'unixml', 'unixml_prom_attribute_status', '0', 0),
(17702, 0, 'unixml', 'unixml_prom_gendesc', '', 0),
(17703, 0, 'unixml', 'unixml_prom_clear_desc', '', 0),
(17704, 0, 'unixml', 'unixml_prom_gendesc_mode', '', 0),
(17705, 0, 'unixml', 'unixml_prom_secret', '', 0),
(17706, 0, 'unixml', 'unixml_yandex_status', '0', 0),
(17707, 0, 'unixml', 'unixml_yandex_name', '', 0),
(17708, 0, 'unixml', 'unixml_yandex_language', '1', 0),
(17709, 0, 'unixml', 'unixml_yandex_currency', '3', 0),
(17710, 0, 'unixml', 'unixml_yandex_field_id', 'p.product_id', 0),
(17711, 0, 'unixml', 'unixml_yandex_markup', '', 0),
(17712, 0, 'unixml', 'unixml_yandex_custom_xml', '', 0),
(17713, 0, 'unixml', 'unixml_yandex_utm', '', 0),
(17714, 0, 'unixml', 'unixml_yandex_delivery_cost', '', 0),
(17715, 0, 'unixml', 'unixml_yandex_delivery_time', '', 0),
(17716, 0, 'unixml', 'unixml_yandex_delivery_jump', '', 0),
(17717, 0, 'unixml', 'unixml_yandex_fields', '', 0),
(17718, 0, 'unixml', 'unixml_yandex_field_price', 'p.price', 0),
(17719, 0, 'unixml', 'unixml_yandex_step', '10000', 0),
(17720, 0, 'unixml', 'unixml_yandex_seopro', '0', 0),
(17721, 0, 'unixml', 'unixml_yandex_log', '', 0),
(17722, 0, 'unixml', 'unixml_yandex_quantity', '0', 0),
(17723, 0, 'unixml', 'unixml_yandex_stock', '8', 0),
(17724, 0, 'unixml', 'unixml_yandex_option_multiplier_status', '0', 0),
(17725, 0, 'unixml', 'unixml_yandex_genname', '', 0),
(17726, 0, 'unixml', 'unixml_yandex_products_mode', '', 0),
(17727, 0, 'unixml', 'unixml_yandex_product', '', 0),
(17728, 0, 'unixml', 'unixml_yandex_andor', '0', 0),
(17729, 0, 'unixml', 'unixml_yandex_image', '0', 0),
(17730, 0, 'unixml', 'unixml_yandex_images', '0', 0),
(17731, 0, 'unixml', 'unixml_yandex_attribute_status', '0', 0),
(17732, 0, 'unixml', 'unixml_yandex_gendesc', '', 0),
(17733, 0, 'unixml', 'unixml_yandex_clear_desc', '', 0),
(17734, 0, 'unixml', 'unixml_yandex_gendesc_mode', '', 0),
(17735, 0, 'unixml', 'unixml_yandex_secret', '', 0),
(17736, 0, 'unixml', 'unixml_turbo_status', '0', 0),
(17737, 0, 'unixml', 'unixml_turbo_name', '', 0),
(17738, 0, 'unixml', 'unixml_turbo_language', '1', 0),
(17739, 0, 'unixml', 'unixml_turbo_currency', '3', 0),
(17740, 0, 'unixml', 'unixml_turbo_field_id', 'p.product_id', 0),
(17741, 0, 'unixml', 'unixml_turbo_markup', '', 0),
(17742, 0, 'unixml', 'unixml_turbo_custom_xml', '', 0),
(17743, 0, 'unixml', 'unixml_turbo_utm', '', 0),
(17744, 0, 'unixml', 'unixml_turbo_delivery_cost', '', 0),
(17745, 0, 'unixml', 'unixml_turbo_delivery_time', '', 0),
(17746, 0, 'unixml', 'unixml_turbo_delivery_jump', '', 0),
(17747, 0, 'unixml', 'unixml_turbo_fields', '', 0),
(17748, 0, 'unixml', 'unixml_turbo_field_price', 'p.price', 0),
(17749, 0, 'unixml', 'unixml_turbo_step', '10000', 0),
(17750, 0, 'unixml', 'unixml_turbo_seopro', '0', 0),
(17751, 0, 'unixml', 'unixml_turbo_log', '', 0),
(17752, 0, 'unixml', 'unixml_turbo_quantity', '0', 0),
(17753, 0, 'unixml', 'unixml_turbo_stock', '8', 0),
(17754, 0, 'unixml', 'unixml_turbo_option_multiplier_status', '0', 0),
(17755, 0, 'unixml', 'unixml_turbo_genname', '', 0),
(17756, 0, 'unixml', 'unixml_turbo_products_mode', '', 0),
(17757, 0, 'unixml', 'unixml_turbo_product', '', 0),
(17758, 0, 'unixml', 'unixml_turbo_andor', '0', 0),
(17759, 0, 'unixml', 'unixml_turbo_image', '0', 0),
(17760, 0, 'unixml', 'unixml_turbo_images', '0', 0),
(17761, 0, 'unixml', 'unixml_turbo_attribute_status', '0', 0),
(17762, 0, 'unixml', 'unixml_turbo_gendesc', '', 0),
(17763, 0, 'unixml', 'unixml_turbo_clear_desc', '', 0),
(17764, 0, 'unixml', 'unixml_turbo_gendesc_mode', '', 0),
(17765, 0, 'unixml', 'unixml_turbo_secret', '', 0),
(17766, 0, 'unixml', 'unixml_cdek_status', '0', 0),
(17767, 0, 'unixml', 'unixml_cdek_name', '', 0),
(17768, 0, 'unixml', 'unixml_cdek_language', '1', 0),
(17769, 0, 'unixml', 'unixml_cdek_currency', '3', 0),
(17770, 0, 'unixml', 'unixml_cdek_field_id', 'p.product_id', 0),
(17771, 0, 'unixml', 'unixml_cdek_markup', '', 0),
(17772, 0, 'unixml', 'unixml_cdek_custom_xml', '', 0),
(17773, 0, 'unixml', 'unixml_cdek_utm', '', 0),
(17774, 0, 'unixml', 'unixml_cdek_delivery_cost', '', 0),
(17775, 0, 'unixml', 'unixml_cdek_delivery_time', '', 0),
(17776, 0, 'unixml', 'unixml_cdek_delivery_jump', '', 0),
(17777, 0, 'unixml', 'unixml_cdek_fields', '', 0),
(17778, 0, 'unixml', 'unixml_cdek_field_price', 'p.price', 0),
(17779, 0, 'unixml', 'unixml_cdek_step', '10000', 0),
(17780, 0, 'unixml', 'unixml_cdek_seopro', '0', 0),
(17781, 0, 'unixml', 'unixml_cdek_log', '', 0),
(17782, 0, 'unixml', 'unixml_cdek_quantity', '0', 0),
(17783, 0, 'unixml', 'unixml_cdek_stock', '8', 0),
(17784, 0, 'unixml', 'unixml_cdek_option_multiplier_status', '0', 0),
(17785, 0, 'unixml', 'unixml_cdek_genname', '', 0),
(17786, 0, 'unixml', 'unixml_cdek_products_mode', '', 0),
(17787, 0, 'unixml', 'unixml_cdek_product', '', 0),
(17788, 0, 'unixml', 'unixml_cdek_andor', '0', 0),
(17789, 0, 'unixml', 'unixml_cdek_image', '0', 0),
(17790, 0, 'unixml', 'unixml_cdek_images', '0', 0),
(17791, 0, 'unixml', 'unixml_cdek_attribute_status', '0', 0),
(17792, 0, 'unixml', 'unixml_cdek_gendesc', '', 0),
(17793, 0, 'unixml', 'unixml_cdek_clear_desc', '', 0),
(17794, 0, 'unixml', 'unixml_cdek_gendesc_mode', '', 0),
(17795, 0, 'unixml', 'unixml_cdek_secret', '', 0),
(17796, 0, 'unixml', 'unixml_goods_status', '0', 0),
(17797, 0, 'unixml', 'unixml_goods_name', '', 0),
(17798, 0, 'unixml', 'unixml_goods_language', '1', 0),
(17799, 0, 'unixml', 'unixml_goods_currency', '3', 0),
(17800, 0, 'unixml', 'unixml_goods_field_id', 'p.product_id', 0),
(17801, 0, 'unixml', 'unixml_goods_markup', '', 0),
(17802, 0, 'unixml', 'unixml_goods_custom_xml', '', 0),
(17803, 0, 'unixml', 'unixml_goods_utm', '', 0),
(17804, 0, 'unixml', 'unixml_goods_delivery_cost', '', 0),
(17805, 0, 'unixml', 'unixml_goods_delivery_time', '', 0),
(17806, 0, 'unixml', 'unixml_goods_delivery_jump', '', 0),
(17807, 0, 'unixml', 'unixml_goods_fields', '', 0),
(17808, 0, 'unixml', 'unixml_goods_field_price', 'p.price', 0),
(17809, 0, 'unixml', 'unixml_goods_step', '10000', 0),
(17810, 0, 'unixml', 'unixml_goods_seopro', '0', 0),
(17811, 0, 'unixml', 'unixml_goods_log', '', 0),
(17812, 0, 'unixml', 'unixml_goods_quantity', '0', 0),
(17813, 0, 'unixml', 'unixml_goods_stock', '8', 0),
(17814, 0, 'unixml', 'unixml_goods_option_multiplier_status', '0', 0),
(17815, 0, 'unixml', 'unixml_goods_genname', '', 0),
(17816, 0, 'unixml', 'unixml_goods_products_mode', '', 0),
(17817, 0, 'unixml', 'unixml_goods_product', '', 0),
(17818, 0, 'unixml', 'unixml_goods_andor', '0', 0),
(17819, 0, 'unixml', 'unixml_goods_image', '0', 0),
(17820, 0, 'unixml', 'unixml_goods_images', '0', 0),
(17821, 0, 'unixml', 'unixml_goods_attribute_status', '0', 0),
(17822, 0, 'unixml', 'unixml_goods_gendesc', '', 0),
(17823, 0, 'unixml', 'unixml_goods_clear_desc', '', 0),
(17824, 0, 'unixml', 'unixml_goods_gendesc_mode', '', 0),
(17825, 0, 'unixml', 'unixml_goods_secret', '', 0),
(17826, 0, 'unixml', 'unixml_mobilluck_status', '0', 0),
(17827, 0, 'unixml', 'unixml_mobilluck_name', '', 0),
(17828, 0, 'unixml', 'unixml_mobilluck_language', '1', 0),
(17829, 0, 'unixml', 'unixml_mobilluck_currency', '3', 0),
(17830, 0, 'unixml', 'unixml_mobilluck_field_id', 'p.product_id', 0),
(17831, 0, 'unixml', 'unixml_mobilluck_markup', '', 0),
(17832, 0, 'unixml', 'unixml_mobilluck_custom_xml', '', 0),
(17833, 0, 'unixml', 'unixml_mobilluck_utm', '', 0),
(17834, 0, 'unixml', 'unixml_mobilluck_delivery_cost', '', 0),
(17835, 0, 'unixml', 'unixml_mobilluck_delivery_time', '', 0),
(17836, 0, 'unixml', 'unixml_mobilluck_delivery_jump', '', 0),
(17837, 0, 'unixml', 'unixml_mobilluck_fields', '', 0),
(17838, 0, 'unixml', 'unixml_mobilluck_field_price', 'p.price', 0),
(17839, 0, 'unixml', 'unixml_mobilluck_step', '10000', 0),
(17840, 0, 'unixml', 'unixml_mobilluck_seopro', '0', 0),
(17841, 0, 'unixml', 'unixml_mobilluck_log', '', 0),
(17842, 0, 'unixml', 'unixml_mobilluck_quantity', '0', 0),
(17843, 0, 'unixml', 'unixml_mobilluck_stock', '8', 0),
(17844, 0, 'unixml', 'unixml_mobilluck_option_multiplier_status', '0', 0),
(17845, 0, 'unixml', 'unixml_mobilluck_genname', '', 0),
(17846, 0, 'unixml', 'unixml_mobilluck_products_mode', '', 0),
(17847, 0, 'unixml', 'unixml_mobilluck_product', '', 0),
(17848, 0, 'unixml', 'unixml_mobilluck_andor', '0', 0),
(17849, 0, 'unixml', 'unixml_mobilluck_image', '0', 0),
(17850, 0, 'unixml', 'unixml_mobilluck_images', '0', 0),
(17851, 0, 'unixml', 'unixml_mobilluck_attribute_status', '0', 0),
(17852, 0, 'unixml', 'unixml_mobilluck_gendesc', '', 0),
(17853, 0, 'unixml', 'unixml_mobilluck_clear_desc', '', 0),
(17854, 0, 'unixml', 'unixml_mobilluck_gendesc_mode', '', 0),
(17855, 0, 'unixml', 'unixml_mobilluck_secret', '', 0),
(17856, 0, 'unixml', 'unixml_allo_status', '0', 0),
(17857, 0, 'unixml', 'unixml_allo_name', '', 0),
(17858, 0, 'unixml', 'unixml_allo_language', '1', 0),
(17859, 0, 'unixml', 'unixml_allo_currency', '3', 0),
(17860, 0, 'unixml', 'unixml_allo_field_id', 'p.product_id', 0),
(17861, 0, 'unixml', 'unixml_allo_markup', '', 0),
(17862, 0, 'unixml', 'unixml_allo_custom_xml', '', 0),
(17863, 0, 'unixml', 'unixml_allo_utm', '', 0),
(17864, 0, 'unixml', 'unixml_allo_delivery_cost', '', 0),
(17865, 0, 'unixml', 'unixml_allo_delivery_time', '', 0),
(17866, 0, 'unixml', 'unixml_allo_delivery_jump', '', 0),
(17867, 0, 'unixml', 'unixml_allo_fields', '', 0),
(17868, 0, 'unixml', 'unixml_allo_field_price', 'p.price', 0),
(17869, 0, 'unixml', 'unixml_allo_step', '10000', 0),
(17870, 0, 'unixml', 'unixml_allo_seopro', '0', 0),
(17871, 0, 'unixml', 'unixml_allo_log', '', 0),
(17872, 0, 'unixml', 'unixml_allo_quantity', '0', 0),
(17873, 0, 'unixml', 'unixml_allo_stock', '8', 0),
(17874, 0, 'unixml', 'unixml_allo_option_multiplier_status', '0', 0),
(17875, 0, 'unixml', 'unixml_allo_genname', '', 0),
(17876, 0, 'unixml', 'unixml_allo_products_mode', '', 0),
(17877, 0, 'unixml', 'unixml_allo_product', '', 0),
(17878, 0, 'unixml', 'unixml_allo_andor', '0', 0),
(17879, 0, 'unixml', 'unixml_allo_image', '0', 0),
(17880, 0, 'unixml', 'unixml_allo_images', '0', 0),
(17881, 0, 'unixml', 'unixml_allo_attribute_status', '0', 0),
(17882, 0, 'unixml', 'unixml_allo_gendesc', '', 0),
(17883, 0, 'unixml', 'unixml_allo_clear_desc', '', 0),
(17884, 0, 'unixml', 'unixml_allo_gendesc_mode', '', 0),
(17885, 0, 'unixml', 'unixml_allo_secret', '', 0),
(17886, 0, 'unixml', 'unixml_fotos_status', '0', 0),
(17887, 0, 'unixml', 'unixml_fotos_name', '', 0),
(17888, 0, 'unixml', 'unixml_fotos_language', '1', 0),
(17889, 0, 'unixml', 'unixml_fotos_currency', '3', 0),
(17890, 0, 'unixml', 'unixml_fotos_field_id', 'p.product_id', 0),
(17891, 0, 'unixml', 'unixml_fotos_markup', '', 0),
(17892, 0, 'unixml', 'unixml_fotos_custom_xml', '', 0),
(17893, 0, 'unixml', 'unixml_fotos_utm', '', 0),
(17894, 0, 'unixml', 'unixml_fotos_delivery_cost', '', 0),
(17895, 0, 'unixml', 'unixml_fotos_delivery_time', '', 0),
(17896, 0, 'unixml', 'unixml_fotos_delivery_jump', '', 0),
(17897, 0, 'unixml', 'unixml_fotos_fields', '', 0),
(17898, 0, 'unixml', 'unixml_fotos_field_price', 'p.price', 0),
(17899, 0, 'unixml', 'unixml_fotos_step', '10000', 0),
(17900, 0, 'unixml', 'unixml_fotos_seopro', '0', 0),
(17901, 0, 'unixml', 'unixml_fotos_log', '', 0),
(17902, 0, 'unixml', 'unixml_fotos_quantity', '0', 0),
(17903, 0, 'unixml', 'unixml_fotos_stock', '8', 0),
(17904, 0, 'unixml', 'unixml_fotos_option_multiplier_status', '0', 0),
(17905, 0, 'unixml', 'unixml_fotos_genname', '', 0),
(17906, 0, 'unixml', 'unixml_fotos_products_mode', '', 0),
(17907, 0, 'unixml', 'unixml_fotos_product', '', 0),
(17908, 0, 'unixml', 'unixml_fotos_andor', '0', 0),
(17909, 0, 'unixml', 'unixml_fotos_image', '0', 0),
(17910, 0, 'unixml', 'unixml_fotos_images', '0', 0),
(17911, 0, 'unixml', 'unixml_fotos_attribute_status', '0', 0),
(17912, 0, 'unixml', 'unixml_fotos_gendesc', '', 0);
INSERT INTO `oc_setting` (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES
(17913, 0, 'unixml', 'unixml_fotos_clear_desc', '', 0),
(17914, 0, 'unixml', 'unixml_fotos_gendesc_mode', '', 0),
(17915, 0, 'unixml', 'unixml_fotos_secret', '', 0),
(17916, 0, 'unixml', 'unixml_privat_status', '0', 0),
(17917, 0, 'unixml', 'unixml_privat_name', '', 0),
(17918, 0, 'unixml', 'unixml_privat_language', '1', 0),
(17919, 0, 'unixml', 'unixml_privat_currency', '3', 0),
(17920, 0, 'unixml', 'unixml_privat_field_id', 'p.product_id', 0),
(17921, 0, 'unixml', 'unixml_privat_markup', '', 0),
(17922, 0, 'unixml', 'unixml_privat_custom_xml', '', 0),
(17923, 0, 'unixml', 'unixml_privat_utm', '', 0),
(17924, 0, 'unixml', 'unixml_privat_delivery_cost', '', 0),
(17925, 0, 'unixml', 'unixml_privat_delivery_time', '', 0),
(17926, 0, 'unixml', 'unixml_privat_delivery_jump', '', 0),
(17927, 0, 'unixml', 'unixml_privat_fields', '', 0),
(17928, 0, 'unixml', 'unixml_privat_field_price', 'p.price', 0),
(17929, 0, 'unixml', 'unixml_privat_step', '10000', 0),
(17930, 0, 'unixml', 'unixml_privat_seopro', '0', 0),
(17931, 0, 'unixml', 'unixml_privat_log', '', 0),
(17932, 0, 'unixml', 'unixml_privat_quantity', '0', 0),
(17933, 0, 'unixml', 'unixml_privat_stock', '8', 0),
(17934, 0, 'unixml', 'unixml_privat_option_multiplier_status', '0', 0),
(17935, 0, 'unixml', 'unixml_privat_genname', '', 0),
(17936, 0, 'unixml', 'unixml_privat_products_mode', '', 0),
(17937, 0, 'unixml', 'unixml_privat_product', '', 0),
(17938, 0, 'unixml', 'unixml_privat_andor', '0', 0),
(17939, 0, 'unixml', 'unixml_privat_image', '0', 0),
(17940, 0, 'unixml', 'unixml_privat_images', '0', 0),
(17941, 0, 'unixml', 'unixml_privat_attribute_status', '0', 0),
(17942, 0, 'unixml', 'unixml_privat_gendesc', '', 0),
(17943, 0, 'unixml', 'unixml_privat_clear_desc', '', 0),
(17944, 0, 'unixml', 'unixml_privat_gendesc_mode', '', 0),
(17945, 0, 'unixml', 'unixml_privat_secret', '', 0),
(17946, 0, 'unixml', 'unixml_joom_status', '0', 0),
(17947, 0, 'unixml', 'unixml_joom_name', '', 0),
(17948, 0, 'unixml', 'unixml_joom_language', '1', 0),
(17949, 0, 'unixml', 'unixml_joom_currency', '3', 0),
(17950, 0, 'unixml', 'unixml_joom_field_id', 'p.product_id', 0),
(17951, 0, 'unixml', 'unixml_joom_markup', '', 0),
(17952, 0, 'unixml', 'unixml_joom_custom_xml', '', 0),
(17953, 0, 'unixml', 'unixml_joom_utm', '', 0),
(17954, 0, 'unixml', 'unixml_joom_delivery_cost', '', 0),
(17955, 0, 'unixml', 'unixml_joom_delivery_time', '', 0),
(17956, 0, 'unixml', 'unixml_joom_delivery_jump', '', 0),
(17957, 0, 'unixml', 'unixml_joom_fields', '', 0),
(17958, 0, 'unixml', 'unixml_joom_field_price', 'p.price', 0),
(17959, 0, 'unixml', 'unixml_joom_step', '10000', 0),
(17960, 0, 'unixml', 'unixml_joom_seopro', '0', 0),
(17961, 0, 'unixml', 'unixml_joom_log', '', 0),
(17962, 0, 'unixml', 'unixml_joom_quantity', '0', 0),
(17963, 0, 'unixml', 'unixml_joom_stock', '8', 0),
(17964, 0, 'unixml', 'unixml_joom_option_multiplier_status', '0', 0),
(17965, 0, 'unixml', 'unixml_joom_genname', '', 0),
(17966, 0, 'unixml', 'unixml_joom_products_mode', '', 0),
(17967, 0, 'unixml', 'unixml_joom_product', '', 0),
(17968, 0, 'unixml', 'unixml_joom_andor', '0', 0),
(17969, 0, 'unixml', 'unixml_joom_image', '0', 0),
(17970, 0, 'unixml', 'unixml_joom_images', '0', 0),
(17971, 0, 'unixml', 'unixml_joom_attribute_status', '0', 0),
(17972, 0, 'unixml', 'unixml_joom_gendesc', '', 0),
(17973, 0, 'unixml', 'unixml_joom_clear_desc', '', 0),
(17974, 0, 'unixml', 'unixml_joom_gendesc_mode', '', 0),
(17975, 0, 'unixml', 'unixml_joom_secret', '', 0),
(17976, 0, 'unixml', 'unixml_olx_status', '0', 0),
(17977, 0, 'unixml', 'unixml_olx_name', '', 0),
(17978, 0, 'unixml', 'unixml_olx_language', '1', 0),
(17979, 0, 'unixml', 'unixml_olx_currency', '3', 0),
(17980, 0, 'unixml', 'unixml_olx_field_id', 'p.product_id', 0),
(17981, 0, 'unixml', 'unixml_olx_markup', '', 0),
(17982, 0, 'unixml', 'unixml_olx_custom_xml', '', 0),
(17983, 0, 'unixml', 'unixml_olx_utm', '', 0),
(17984, 0, 'unixml', 'unixml_olx_delivery_cost', '', 0),
(17985, 0, 'unixml', 'unixml_olx_delivery_time', '', 0),
(17986, 0, 'unixml', 'unixml_olx_delivery_jump', '', 0),
(17987, 0, 'unixml', 'unixml_olx_fields', '', 0),
(17988, 0, 'unixml', 'unixml_olx_field_price', 'p.price', 0),
(17989, 0, 'unixml', 'unixml_olx_step', '10000', 0),
(17990, 0, 'unixml', 'unixml_olx_seopro', '0', 0),
(17991, 0, 'unixml', 'unixml_olx_log', '', 0),
(17992, 0, 'unixml', 'unixml_olx_quantity', '0', 0),
(17993, 0, 'unixml', 'unixml_olx_stock', '8', 0),
(17994, 0, 'unixml', 'unixml_olx_option_multiplier_status', '0', 0),
(17995, 0, 'unixml', 'unixml_olx_genname', '', 0),
(17996, 0, 'unixml', 'unixml_olx_products_mode', '', 0),
(17997, 0, 'unixml', 'unixml_olx_product', '', 0),
(17998, 0, 'unixml', 'unixml_olx_andor', '0', 0),
(17999, 0, 'unixml', 'unixml_olx_image', '0', 0),
(18000, 0, 'unixml', 'unixml_olx_images', '0', 0),
(18001, 0, 'unixml', 'unixml_olx_attribute_status', '0', 0),
(18002, 0, 'unixml', 'unixml_olx_gendesc', '', 0),
(18003, 0, 'unixml', 'unixml_olx_clear_desc', '', 0),
(18004, 0, 'unixml', 'unixml_olx_gendesc_mode', '', 0),
(18005, 0, 'unixml', 'unixml_olx_secret', '', 0),
(18006, 0, 'unixml', 'unixml_beru_status', '0', 0),
(18007, 0, 'unixml', 'unixml_beru_name', '', 0),
(18008, 0, 'unixml', 'unixml_beru_language', '1', 0),
(18009, 0, 'unixml', 'unixml_beru_currency', '3', 0),
(18010, 0, 'unixml', 'unixml_beru_field_id', 'p.product_id', 0),
(18011, 0, 'unixml', 'unixml_beru_markup', '', 0),
(18012, 0, 'unixml', 'unixml_beru_custom_xml', '', 0),
(18013, 0, 'unixml', 'unixml_beru_utm', '', 0),
(18014, 0, 'unixml', 'unixml_beru_delivery_cost', '', 0),
(18015, 0, 'unixml', 'unixml_beru_delivery_time', '', 0),
(18016, 0, 'unixml', 'unixml_beru_delivery_jump', '', 0),
(18017, 0, 'unixml', 'unixml_beru_fields', '', 0),
(18018, 0, 'unixml', 'unixml_beru_field_price', 'p.price', 0),
(18019, 0, 'unixml', 'unixml_beru_step', '10000', 0),
(18020, 0, 'unixml', 'unixml_beru_seopro', '0', 0),
(18021, 0, 'unixml', 'unixml_beru_log', '', 0),
(18022, 0, 'unixml', 'unixml_beru_quantity', '0', 0),
(18023, 0, 'unixml', 'unixml_beru_stock', '8', 0),
(18024, 0, 'unixml', 'unixml_beru_option_multiplier_status', '0', 0),
(18025, 0, 'unixml', 'unixml_beru_genname', '', 0),
(18026, 0, 'unixml', 'unixml_beru_products_mode', '', 0),
(18027, 0, 'unixml', 'unixml_beru_product', '', 0),
(18028, 0, 'unixml', 'unixml_beru_andor', '0', 0),
(18029, 0, 'unixml', 'unixml_beru_image', '0', 0),
(18030, 0, 'unixml', 'unixml_beru_images', '0', 0),
(18031, 0, 'unixml', 'unixml_beru_attribute_status', '0', 0),
(18032, 0, 'unixml', 'unixml_beru_gendesc', '', 0),
(18033, 0, 'unixml', 'unixml_beru_clear_desc', '', 0),
(18034, 0, 'unixml', 'unixml_beru_gendesc_mode', '', 0),
(18035, 0, 'unixml', 'unixml_beru_secret', '', 0),
(18036, 0, 'unixml', 'unixml_kidstaff_status', '0', 0),
(18037, 0, 'unixml', 'unixml_kidstaff_name', '', 0),
(18038, 0, 'unixml', 'unixml_kidstaff_language', '1', 0),
(18039, 0, 'unixml', 'unixml_kidstaff_currency', '3', 0),
(18040, 0, 'unixml', 'unixml_kidstaff_field_id', 'p.product_id', 0),
(18041, 0, 'unixml', 'unixml_kidstaff_markup', '', 0),
(18042, 0, 'unixml', 'unixml_kidstaff_custom_xml', '', 0),
(18043, 0, 'unixml', 'unixml_kidstaff_utm', '', 0),
(18044, 0, 'unixml', 'unixml_kidstaff_delivery_cost', '', 0),
(18045, 0, 'unixml', 'unixml_kidstaff_delivery_time', '', 0),
(18046, 0, 'unixml', 'unixml_kidstaff_delivery_jump', '', 0),
(18047, 0, 'unixml', 'unixml_kidstaff_fields', '', 0),
(18048, 0, 'unixml', 'unixml_kidstaff_field_price', 'p.price', 0),
(18049, 0, 'unixml', 'unixml_kidstaff_step', '10000', 0),
(18050, 0, 'unixml', 'unixml_kidstaff_seopro', '0', 0),
(18051, 0, 'unixml', 'unixml_kidstaff_log', '', 0),
(18052, 0, 'unixml', 'unixml_kidstaff_quantity', '0', 0),
(18053, 0, 'unixml', 'unixml_kidstaff_stock', '8', 0),
(18054, 0, 'unixml', 'unixml_kidstaff_option_multiplier_status', '0', 0),
(18055, 0, 'unixml', 'unixml_kidstaff_genname', '', 0),
(18056, 0, 'unixml', 'unixml_kidstaff_products_mode', '', 0),
(18057, 0, 'unixml', 'unixml_kidstaff_product', '', 0),
(18058, 0, 'unixml', 'unixml_kidstaff_andor', '0', 0),
(18059, 0, 'unixml', 'unixml_kidstaff_image', '0', 0),
(18060, 0, 'unixml', 'unixml_kidstaff_images', '0', 0),
(18061, 0, 'unixml', 'unixml_kidstaff_attribute_status', '0', 0),
(18062, 0, 'unixml', 'unixml_kidstaff_gendesc', '', 0),
(18063, 0, 'unixml', 'unixml_kidstaff_clear_desc', '', 0),
(18064, 0, 'unixml', 'unixml_kidstaff_gendesc_mode', '', 0),
(18065, 0, 'unixml', 'unixml_kidstaff_secret', '', 0),
(18066, 0, 'unixml', 'unixml_bigl_status', '0', 0),
(18067, 0, 'unixml', 'unixml_bigl_name', '', 0),
(18068, 0, 'unixml', 'unixml_bigl_language', '1', 0),
(18069, 0, 'unixml', 'unixml_bigl_currency', '3', 0),
(18070, 0, 'unixml', 'unixml_bigl_field_id', 'p.product_id', 0),
(18071, 0, 'unixml', 'unixml_bigl_markup', '', 0),
(18072, 0, 'unixml', 'unixml_bigl_custom_xml', '', 0),
(18073, 0, 'unixml', 'unixml_bigl_utm', '', 0),
(18074, 0, 'unixml', 'unixml_bigl_delivery_cost', '', 0),
(18075, 0, 'unixml', 'unixml_bigl_delivery_time', '', 0),
(18076, 0, 'unixml', 'unixml_bigl_delivery_jump', '', 0),
(18077, 0, 'unixml', 'unixml_bigl_fields', '', 0),
(18078, 0, 'unixml', 'unixml_bigl_field_price', 'p.price', 0),
(18079, 0, 'unixml', 'unixml_bigl_step', '10000', 0),
(18080, 0, 'unixml', 'unixml_bigl_seopro', '0', 0),
(18081, 0, 'unixml', 'unixml_bigl_log', '', 0),
(18082, 0, 'unixml', 'unixml_bigl_quantity', '0', 0),
(18083, 0, 'unixml', 'unixml_bigl_stock', '8', 0),
(18084, 0, 'unixml', 'unixml_bigl_option_multiplier_status', '0', 0),
(18085, 0, 'unixml', 'unixml_bigl_genname', '', 0),
(18086, 0, 'unixml', 'unixml_bigl_products_mode', '', 0),
(18087, 0, 'unixml', 'unixml_bigl_product', '', 0),
(18088, 0, 'unixml', 'unixml_bigl_andor', '0', 0),
(18089, 0, 'unixml', 'unixml_bigl_image', '0', 0),
(18090, 0, 'unixml', 'unixml_bigl_images', '0', 0),
(18091, 0, 'unixml', 'unixml_bigl_attribute_status', '0', 0),
(18092, 0, 'unixml', 'unixml_bigl_gendesc', '', 0),
(18093, 0, 'unixml', 'unixml_bigl_clear_desc', '', 0),
(18094, 0, 'unixml', 'unixml_bigl_gendesc_mode', '', 0),
(18095, 0, 'unixml', 'unixml_bigl_secret', '', 0),
(18096, 0, 'unixml', 'unixml_froot_status', '0', 0),
(18097, 0, 'unixml', 'unixml_froot_name', '', 0),
(18098, 0, 'unixml', 'unixml_froot_language', '1', 0),
(18099, 0, 'unixml', 'unixml_froot_currency', '3', 0),
(18100, 0, 'unixml', 'unixml_froot_field_id', 'p.product_id', 0),
(18101, 0, 'unixml', 'unixml_froot_markup', '', 0),
(18102, 0, 'unixml', 'unixml_froot_custom_xml', '', 0),
(18103, 0, 'unixml', 'unixml_froot_utm', '', 0),
(18104, 0, 'unixml', 'unixml_froot_delivery_cost', '', 0),
(18105, 0, 'unixml', 'unixml_froot_delivery_time', '', 0),
(18106, 0, 'unixml', 'unixml_froot_delivery_jump', '', 0),
(18107, 0, 'unixml', 'unixml_froot_fields', '', 0),
(18108, 0, 'unixml', 'unixml_froot_field_price', 'p.price', 0),
(18109, 0, 'unixml', 'unixml_froot_step', '10000', 0),
(18110, 0, 'unixml', 'unixml_froot_seopro', '0', 0),
(18111, 0, 'unixml', 'unixml_froot_log', '', 0),
(18112, 0, 'unixml', 'unixml_froot_quantity', '0', 0),
(18113, 0, 'unixml', 'unixml_froot_stock', '8', 0),
(18114, 0, 'unixml', 'unixml_froot_option_multiplier_status', '0', 0),
(18115, 0, 'unixml', 'unixml_froot_genname', '', 0),
(18116, 0, 'unixml', 'unixml_froot_products_mode', '', 0),
(18117, 0, 'unixml', 'unixml_froot_product', '', 0),
(18118, 0, 'unixml', 'unixml_froot_andor', '0', 0),
(18119, 0, 'unixml', 'unixml_froot_image', '0', 0),
(18120, 0, 'unixml', 'unixml_froot_images', '0', 0),
(18121, 0, 'unixml', 'unixml_froot_attribute_status', '0', 0),
(18122, 0, 'unixml', 'unixml_froot_gendesc', '', 0),
(18123, 0, 'unixml', 'unixml_froot_clear_desc', '', 0),
(18124, 0, 'unixml', 'unixml_froot_gendesc_mode', '', 0),
(18125, 0, 'unixml', 'unixml_froot_secret', '', 0),
(18126, 0, 'unixml', 'unixml_regmarkets_status', '0', 0),
(18127, 0, 'unixml', 'unixml_regmarkets_name', '', 0),
(18128, 0, 'unixml', 'unixml_regmarkets_language', '1', 0),
(18129, 0, 'unixml', 'unixml_regmarkets_currency', '3', 0),
(18130, 0, 'unixml', 'unixml_regmarkets_field_id', 'p.product_id', 0),
(18131, 0, 'unixml', 'unixml_regmarkets_markup', '', 0),
(18132, 0, 'unixml', 'unixml_regmarkets_custom_xml', '', 0),
(18133, 0, 'unixml', 'unixml_regmarkets_utm', '', 0),
(18134, 0, 'unixml', 'unixml_regmarkets_delivery_cost', '', 0),
(18135, 0, 'unixml', 'unixml_regmarkets_delivery_time', '', 0),
(18136, 0, 'unixml', 'unixml_regmarkets_delivery_jump', '', 0),
(18137, 0, 'unixml', 'unixml_regmarkets_fields', '', 0),
(18138, 0, 'unixml', 'unixml_regmarkets_field_price', 'p.price', 0),
(18139, 0, 'unixml', 'unixml_regmarkets_step', '10000', 0),
(18140, 0, 'unixml', 'unixml_regmarkets_seopro', '0', 0),
(18141, 0, 'unixml', 'unixml_regmarkets_log', '', 0),
(18142, 0, 'unixml', 'unixml_regmarkets_quantity', '0', 0),
(18143, 0, 'unixml', 'unixml_regmarkets_stock', '8', 0),
(18144, 0, 'unixml', 'unixml_regmarkets_option_multiplier_status', '0', 0),
(18145, 0, 'unixml', 'unixml_regmarkets_genname', '', 0),
(18146, 0, 'unixml', 'unixml_regmarkets_products_mode', '', 0),
(18147, 0, 'unixml', 'unixml_regmarkets_product', '', 0),
(18148, 0, 'unixml', 'unixml_regmarkets_andor', '0', 0),
(18149, 0, 'unixml', 'unixml_regmarkets_image', '0', 0),
(18150, 0, 'unixml', 'unixml_regmarkets_images', '0', 0),
(18151, 0, 'unixml', 'unixml_regmarkets_attribute_status', '0', 0),
(18152, 0, 'unixml', 'unixml_regmarkets_gendesc', '', 0),
(18153, 0, 'unixml', 'unixml_regmarkets_clear_desc', '', 0),
(18154, 0, 'unixml', 'unixml_regmarkets_gendesc_mode', '', 0),
(18155, 0, 'unixml', 'unixml_regmarkets_secret', '', 0),
(18156, 0, 'unixml', 'unixml_besplatka_status', '0', 0),
(18157, 0, 'unixml', 'unixml_besplatka_name', '', 0),
(18158, 0, 'unixml', 'unixml_besplatka_language', '1', 0),
(18159, 0, 'unixml', 'unixml_besplatka_currency', '3', 0),
(18160, 0, 'unixml', 'unixml_besplatka_field_id', 'p.product_id', 0),
(18161, 0, 'unixml', 'unixml_besplatka_markup', '', 0),
(18162, 0, 'unixml', 'unixml_besplatka_custom_xml', '', 0),
(18163, 0, 'unixml', 'unixml_besplatka_utm', '', 0),
(18164, 0, 'unixml', 'unixml_besplatka_delivery_cost', '', 0),
(18165, 0, 'unixml', 'unixml_besplatka_delivery_time', '', 0),
(18166, 0, 'unixml', 'unixml_besplatka_delivery_jump', '', 0),
(18167, 0, 'unixml', 'unixml_besplatka_fields', '', 0),
(18168, 0, 'unixml', 'unixml_besplatka_field_price', 'p.price', 0),
(18169, 0, 'unixml', 'unixml_besplatka_step', '10000', 0),
(18170, 0, 'unixml', 'unixml_besplatka_seopro', '0', 0),
(18171, 0, 'unixml', 'unixml_besplatka_log', '', 0),
(18172, 0, 'unixml', 'unixml_besplatka_quantity', '0', 0),
(18173, 0, 'unixml', 'unixml_besplatka_stock', '8', 0),
(18174, 0, 'unixml', 'unixml_besplatka_option_multiplier_status', '0', 0),
(18175, 0, 'unixml', 'unixml_besplatka_genname', '', 0),
(18176, 0, 'unixml', 'unixml_besplatka_products_mode', '', 0),
(18177, 0, 'unixml', 'unixml_besplatka_product', '', 0),
(18178, 0, 'unixml', 'unixml_besplatka_andor', '0', 0),
(18179, 0, 'unixml', 'unixml_besplatka_image', '0', 0),
(18180, 0, 'unixml', 'unixml_besplatka_images', '0', 0),
(18181, 0, 'unixml', 'unixml_besplatka_attribute_status', '0', 0),
(18182, 0, 'unixml', 'unixml_besplatka_gendesc', '', 0),
(18183, 0, 'unixml', 'unixml_besplatka_clear_desc', '', 0),
(18184, 0, 'unixml', 'unixml_besplatka_gendesc_mode', '', 0),
(18185, 0, 'unixml', 'unixml_besplatka_secret', '', 0),
(18186, 0, 'unixml', 'unixml_skidochnik_status', '0', 0),
(18187, 0, 'unixml', 'unixml_skidochnik_name', '', 0),
(18188, 0, 'unixml', 'unixml_skidochnik_language', '1', 0),
(18189, 0, 'unixml', 'unixml_skidochnik_currency', '3', 0),
(18190, 0, 'unixml', 'unixml_skidochnik_field_id', 'p.product_id', 0),
(18191, 0, 'unixml', 'unixml_skidochnik_markup', '', 0),
(18192, 0, 'unixml', 'unixml_skidochnik_custom_xml', '', 0),
(18193, 0, 'unixml', 'unixml_skidochnik_utm', '', 0),
(18194, 0, 'unixml', 'unixml_skidochnik_delivery_cost', '', 0),
(18195, 0, 'unixml', 'unixml_skidochnik_delivery_time', '', 0),
(18196, 0, 'unixml', 'unixml_skidochnik_delivery_jump', '', 0),
(18197, 0, 'unixml', 'unixml_skidochnik_fields', '', 0),
(18198, 0, 'unixml', 'unixml_skidochnik_field_price', 'p.price', 0),
(18199, 0, 'unixml', 'unixml_skidochnik_step', '10000', 0),
(18200, 0, 'unixml', 'unixml_skidochnik_seopro', '0', 0),
(18201, 0, 'unixml', 'unixml_skidochnik_log', '', 0),
(18202, 0, 'unixml', 'unixml_skidochnik_quantity', '0', 0),
(18203, 0, 'unixml', 'unixml_skidochnik_stock', '8', 0),
(18204, 0, 'unixml', 'unixml_skidochnik_option_multiplier_status', '0', 0),
(18205, 0, 'unixml', 'unixml_skidochnik_genname', '', 0),
(18206, 0, 'unixml', 'unixml_skidochnik_products_mode', '', 0),
(18207, 0, 'unixml', 'unixml_skidochnik_product', '', 0),
(18208, 0, 'unixml', 'unixml_skidochnik_andor', '0', 0),
(18209, 0, 'unixml', 'unixml_skidochnik_image', '0', 0),
(18210, 0, 'unixml', 'unixml_skidochnik_images', '0', 0),
(18211, 0, 'unixml', 'unixml_skidochnik_attribute_status', '0', 0),
(18212, 0, 'unixml', 'unixml_skidochnik_gendesc', '', 0),
(18213, 0, 'unixml', 'unixml_skidochnik_clear_desc', '', 0),
(18214, 0, 'unixml', 'unixml_skidochnik_gendesc_mode', '', 0),
(18215, 0, 'unixml', 'unixml_skidochnik_secret', '', 0),
(18216, 0, 'unixml', 'unixml_metamarket_status', '0', 0),
(18217, 0, 'unixml', 'unixml_metamarket_name', '', 0),
(18218, 0, 'unixml', 'unixml_metamarket_language', '1', 0),
(18219, 0, 'unixml', 'unixml_metamarket_currency', '3', 0),
(18220, 0, 'unixml', 'unixml_metamarket_field_id', 'p.product_id', 0),
(18221, 0, 'unixml', 'unixml_metamarket_markup', '', 0),
(18222, 0, 'unixml', 'unixml_metamarket_custom_xml', '', 0),
(18223, 0, 'unixml', 'unixml_metamarket_utm', '', 0),
(18224, 0, 'unixml', 'unixml_metamarket_delivery_cost', '', 0),
(18225, 0, 'unixml', 'unixml_metamarket_delivery_time', '', 0),
(18226, 0, 'unixml', 'unixml_metamarket_delivery_jump', '', 0),
(18227, 0, 'unixml', 'unixml_metamarket_fields', '', 0),
(18228, 0, 'unixml', 'unixml_metamarket_field_price', 'p.price', 0),
(18229, 0, 'unixml', 'unixml_metamarket_step', '10000', 0),
(18230, 0, 'unixml', 'unixml_metamarket_seopro', '0', 0),
(18231, 0, 'unixml', 'unixml_metamarket_log', '', 0),
(18232, 0, 'unixml', 'unixml_metamarket_quantity', '0', 0),
(18233, 0, 'unixml', 'unixml_metamarket_stock', '8', 0),
(18234, 0, 'unixml', 'unixml_metamarket_option_multiplier_status', '0', 0),
(18235, 0, 'unixml', 'unixml_metamarket_genname', '', 0),
(18236, 0, 'unixml', 'unixml_metamarket_products_mode', '', 0),
(18237, 0, 'unixml', 'unixml_metamarket_product', '', 0),
(18238, 0, 'unixml', 'unixml_metamarket_andor', '0', 0),
(18239, 0, 'unixml', 'unixml_metamarket_image', '0', 0),
(18240, 0, 'unixml', 'unixml_metamarket_images', '0', 0),
(18241, 0, 'unixml', 'unixml_metamarket_attribute_status', '0', 0),
(18242, 0, 'unixml', 'unixml_metamarket_gendesc', '', 0),
(18243, 0, 'unixml', 'unixml_metamarket_clear_desc', '', 0),
(18244, 0, 'unixml', 'unixml_metamarket_gendesc_mode', '', 0),
(18245, 0, 'unixml', 'unixml_metamarket_secret', '', 0),
(18246, 0, 'unixml', 'unixml_vcene_status', '0', 0),
(18247, 0, 'unixml', 'unixml_vcene_name', '', 0),
(18248, 0, 'unixml', 'unixml_vcene_language', '1', 0),
(18249, 0, 'unixml', 'unixml_vcene_currency', '3', 0),
(18250, 0, 'unixml', 'unixml_vcene_field_id', 'p.product_id', 0),
(18251, 0, 'unixml', 'unixml_vcene_markup', '', 0),
(18252, 0, 'unixml', 'unixml_vcene_custom_xml', '', 0),
(18253, 0, 'unixml', 'unixml_vcene_utm', '', 0),
(18254, 0, 'unixml', 'unixml_vcene_delivery_cost', '', 0),
(18255, 0, 'unixml', 'unixml_vcene_delivery_time', '', 0),
(18256, 0, 'unixml', 'unixml_vcene_delivery_jump', '', 0),
(18257, 0, 'unixml', 'unixml_vcene_fields', '', 0),
(18258, 0, 'unixml', 'unixml_vcene_field_price', 'p.price', 0),
(18259, 0, 'unixml', 'unixml_vcene_step', '10000', 0),
(18260, 0, 'unixml', 'unixml_vcene_seopro', '0', 0),
(18261, 0, 'unixml', 'unixml_vcene_log', '', 0),
(18262, 0, 'unixml', 'unixml_vcene_quantity', '0', 0),
(18263, 0, 'unixml', 'unixml_vcene_stock', '8', 0),
(18264, 0, 'unixml', 'unixml_vcene_option_multiplier_status', '0', 0),
(18265, 0, 'unixml', 'unixml_vcene_genname', '', 0),
(18266, 0, 'unixml', 'unixml_vcene_products_mode', '', 0),
(18267, 0, 'unixml', 'unixml_vcene_product', '', 0),
(18268, 0, 'unixml', 'unixml_vcene_andor', '0', 0),
(18269, 0, 'unixml', 'unixml_vcene_image', '0', 0),
(18270, 0, 'unixml', 'unixml_vcene_images', '0', 0),
(18271, 0, 'unixml', 'unixml_vcene_attribute_status', '0', 0),
(18272, 0, 'unixml', 'unixml_vcene_gendesc', '', 0),
(18273, 0, 'unixml', 'unixml_vcene_clear_desc', '', 0),
(18274, 0, 'unixml', 'unixml_vcene_gendesc_mode', '', 0),
(18275, 0, 'unixml', 'unixml_vcene_secret', '', 0),
(18276, 0, 'unixml', 'unixml_obyava_status', '0', 0),
(18277, 0, 'unixml', 'unixml_obyava_name', '', 0),
(18278, 0, 'unixml', 'unixml_obyava_language', '1', 0),
(18279, 0, 'unixml', 'unixml_obyava_currency', '3', 0),
(18280, 0, 'unixml', 'unixml_obyava_field_id', 'p.product_id', 0),
(18281, 0, 'unixml', 'unixml_obyava_markup', '', 0),
(18282, 0, 'unixml', 'unixml_obyava_custom_xml', '', 0),
(18283, 0, 'unixml', 'unixml_obyava_utm', '', 0),
(18284, 0, 'unixml', 'unixml_obyava_delivery_cost', '', 0),
(18285, 0, 'unixml', 'unixml_obyava_delivery_time', '', 0),
(18286, 0, 'unixml', 'unixml_obyava_delivery_jump', '', 0),
(18287, 0, 'unixml', 'unixml_obyava_fields', '', 0),
(18288, 0, 'unixml', 'unixml_obyava_field_price', 'p.price', 0),
(18289, 0, 'unixml', 'unixml_obyava_step', '10000', 0),
(18290, 0, 'unixml', 'unixml_obyava_seopro', '0', 0),
(18291, 0, 'unixml', 'unixml_obyava_log', '', 0),
(18292, 0, 'unixml', 'unixml_obyava_quantity', '0', 0),
(18293, 0, 'unixml', 'unixml_obyava_stock', '8', 0),
(18294, 0, 'unixml', 'unixml_obyava_option_multiplier_status', '0', 0),
(18295, 0, 'unixml', 'unixml_obyava_genname', '', 0),
(18296, 0, 'unixml', 'unixml_obyava_products_mode', '', 0),
(18297, 0, 'unixml', 'unixml_obyava_product', '', 0),
(18298, 0, 'unixml', 'unixml_obyava_andor', '0', 0),
(18299, 0, 'unixml', 'unixml_obyava_image', '0', 0),
(18300, 0, 'unixml', 'unixml_obyava_images', '0', 0),
(18301, 0, 'unixml', 'unixml_obyava_attribute_status', '0', 0),
(18302, 0, 'unixml', 'unixml_obyava_gendesc', '', 0),
(18303, 0, 'unixml', 'unixml_obyava_clear_desc', '', 0),
(18304, 0, 'unixml', 'unixml_obyava_gendesc_mode', '', 0),
(18305, 0, 'unixml', 'unixml_obyava_secret', '', 0),
(18306, 0, 'unixml', 'unixml_ekatalog_status', '0', 0),
(18307, 0, 'unixml', 'unixml_ekatalog_name', '', 0),
(18308, 0, 'unixml', 'unixml_ekatalog_language', '1', 0),
(18309, 0, 'unixml', 'unixml_ekatalog_currency', '3', 0),
(18310, 0, 'unixml', 'unixml_ekatalog_field_id', 'p.product_id', 0),
(18311, 0, 'unixml', 'unixml_ekatalog_markup', '', 0),
(18312, 0, 'unixml', 'unixml_ekatalog_custom_xml', '', 0),
(18313, 0, 'unixml', 'unixml_ekatalog_utm', '', 0),
(18314, 0, 'unixml', 'unixml_ekatalog_delivery_cost', '', 0),
(18315, 0, 'unixml', 'unixml_ekatalog_delivery_time', '', 0),
(18316, 0, 'unixml', 'unixml_ekatalog_delivery_jump', '', 0),
(18317, 0, 'unixml', 'unixml_ekatalog_fields', '', 0),
(18318, 0, 'unixml', 'unixml_ekatalog_field_price', 'p.price', 0),
(18319, 0, 'unixml', 'unixml_ekatalog_step', '10000', 0),
(18320, 0, 'unixml', 'unixml_ekatalog_seopro', '0', 0),
(18321, 0, 'unixml', 'unixml_ekatalog_log', '', 0),
(18322, 0, 'unixml', 'unixml_ekatalog_quantity', '0', 0),
(18323, 0, 'unixml', 'unixml_ekatalog_stock', '8', 0),
(18324, 0, 'unixml', 'unixml_ekatalog_option_multiplier_status', '0', 0),
(18325, 0, 'unixml', 'unixml_ekatalog_genname', '', 0),
(18326, 0, 'unixml', 'unixml_ekatalog_products_mode', '', 0),
(18327, 0, 'unixml', 'unixml_ekatalog_product', '', 0),
(18328, 0, 'unixml', 'unixml_ekatalog_andor', '0', 0),
(18329, 0, 'unixml', 'unixml_ekatalog_image', '0', 0),
(18330, 0, 'unixml', 'unixml_ekatalog_images', '0', 0),
(18331, 0, 'unixml', 'unixml_ekatalog_attribute_status', '0', 0),
(18332, 0, 'unixml', 'unixml_ekatalog_gendesc', '', 0),
(18333, 0, 'unixml', 'unixml_ekatalog_clear_desc', '', 0),
(18334, 0, 'unixml', 'unixml_ekatalog_gendesc_mode', '', 0),
(18335, 0, 'unixml', 'unixml_ekatalog_secret', '', 0),
(18336, 0, 'unixml', 'unixml_salidzini_status', '0', 0),
(18337, 0, 'unixml', 'unixml_salidzini_name', '', 0),
(18338, 0, 'unixml', 'unixml_salidzini_language', '1', 0),
(18339, 0, 'unixml', 'unixml_salidzini_currency', '3', 0),
(18340, 0, 'unixml', 'unixml_salidzini_field_id', 'p.product_id', 0),
(18341, 0, 'unixml', 'unixml_salidzini_markup', '', 0),
(18342, 0, 'unixml', 'unixml_salidzini_custom_xml', '', 0),
(18343, 0, 'unixml', 'unixml_salidzini_utm', '', 0),
(18344, 0, 'unixml', 'unixml_salidzini_delivery_cost', '', 0),
(18345, 0, 'unixml', 'unixml_salidzini_delivery_time', '', 0),
(18346, 0, 'unixml', 'unixml_salidzini_delivery_jump', '', 0),
(18347, 0, 'unixml', 'unixml_salidzini_fields', '', 0),
(18348, 0, 'unixml', 'unixml_salidzini_field_price', 'p.price', 0),
(18349, 0, 'unixml', 'unixml_salidzini_step', '10000', 0),
(18350, 0, 'unixml', 'unixml_salidzini_seopro', '0', 0),
(18351, 0, 'unixml', 'unixml_salidzini_log', '', 0),
(18352, 0, 'unixml', 'unixml_salidzini_quantity', '0', 0),
(18353, 0, 'unixml', 'unixml_salidzini_stock', '8', 0),
(18354, 0, 'unixml', 'unixml_salidzini_option_multiplier_status', '0', 0),
(18355, 0, 'unixml', 'unixml_salidzini_genname', '', 0),
(18356, 0, 'unixml', 'unixml_salidzini_products_mode', '', 0),
(18357, 0, 'unixml', 'unixml_salidzini_product', '', 0),
(18358, 0, 'unixml', 'unixml_salidzini_andor', '0', 0),
(18359, 0, 'unixml', 'unixml_salidzini_image', '0', 0),
(18360, 0, 'unixml', 'unixml_salidzini_images', '0', 0),
(18361, 0, 'unixml', 'unixml_salidzini_attribute_status', '0', 0),
(18362, 0, 'unixml', 'unixml_salidzini_gendesc', '', 0),
(18363, 0, 'unixml', 'unixml_salidzini_clear_desc', '', 0),
(18364, 0, 'unixml', 'unixml_salidzini_gendesc_mode', '', 0),
(18365, 0, 'unixml', 'unixml_salidzini_secret', '', 0),
(18366, 0, 'unixml', 'unixml_tiu_status', '0', 0),
(18367, 0, 'unixml', 'unixml_tiu_name', '', 0),
(18368, 0, 'unixml', 'unixml_tiu_language', '1', 0),
(18369, 0, 'unixml', 'unixml_tiu_currency', '3', 0),
(18370, 0, 'unixml', 'unixml_tiu_field_id', 'p.product_id', 0),
(18371, 0, 'unixml', 'unixml_tiu_markup', '', 0),
(18372, 0, 'unixml', 'unixml_tiu_custom_xml', '', 0),
(18373, 0, 'unixml', 'unixml_tiu_utm', '', 0),
(18374, 0, 'unixml', 'unixml_tiu_delivery_cost', '', 0),
(18375, 0, 'unixml', 'unixml_tiu_delivery_time', '', 0),
(18376, 0, 'unixml', 'unixml_tiu_delivery_jump', '', 0),
(18377, 0, 'unixml', 'unixml_tiu_fields', '', 0),
(18378, 0, 'unixml', 'unixml_tiu_field_price', 'p.price', 0),
(18379, 0, 'unixml', 'unixml_tiu_step', '10000', 0),
(18380, 0, 'unixml', 'unixml_tiu_seopro', '0', 0),
(18381, 0, 'unixml', 'unixml_tiu_log', '', 0),
(18382, 0, 'unixml', 'unixml_tiu_quantity', '0', 0),
(18383, 0, 'unixml', 'unixml_tiu_stock', '8', 0),
(18384, 0, 'unixml', 'unixml_tiu_option_multiplier_status', '0', 0),
(18385, 0, 'unixml', 'unixml_tiu_genname', '', 0),
(18386, 0, 'unixml', 'unixml_tiu_products_mode', '', 0),
(18387, 0, 'unixml', 'unixml_tiu_product', '', 0),
(18388, 0, 'unixml', 'unixml_tiu_andor', '0', 0),
(18389, 0, 'unixml', 'unixml_tiu_image', '0', 0),
(18390, 0, 'unixml', 'unixml_tiu_images', '0', 0),
(18391, 0, 'unixml', 'unixml_tiu_attribute_status', '0', 0),
(18392, 0, 'unixml', 'unixml_tiu_gendesc', '', 0),
(18393, 0, 'unixml', 'unixml_tiu_clear_desc', '', 0),
(18394, 0, 'unixml', 'unixml_tiu_gendesc_mode', '', 0),
(18395, 0, 'unixml', 'unixml_tiu_secret', '', 0),
(18396, 0, 'unixml', 'unixml_priceru_status', '0', 0),
(18397, 0, 'unixml', 'unixml_priceru_name', '', 0),
(18398, 0, 'unixml', 'unixml_priceru_language', '1', 0),
(18399, 0, 'unixml', 'unixml_priceru_currency', '3', 0),
(18400, 0, 'unixml', 'unixml_priceru_field_id', 'p.product_id', 0),
(18401, 0, 'unixml', 'unixml_priceru_markup', '', 0),
(18402, 0, 'unixml', 'unixml_priceru_custom_xml', '', 0),
(18403, 0, 'unixml', 'unixml_priceru_utm', '', 0),
(18404, 0, 'unixml', 'unixml_priceru_delivery_cost', '', 0),
(18405, 0, 'unixml', 'unixml_priceru_delivery_time', '', 0),
(18406, 0, 'unixml', 'unixml_priceru_delivery_jump', '', 0),
(18407, 0, 'unixml', 'unixml_priceru_fields', '', 0),
(18408, 0, 'unixml', 'unixml_priceru_field_price', 'p.price', 0),
(18409, 0, 'unixml', 'unixml_priceru_step', '10000', 0),
(18410, 0, 'unixml', 'unixml_priceru_seopro', '0', 0),
(18411, 0, 'unixml', 'unixml_priceru_log', '', 0),
(18412, 0, 'unixml', 'unixml_priceru_quantity', '0', 0),
(18413, 0, 'unixml', 'unixml_priceru_stock', '8', 0),
(18414, 0, 'unixml', 'unixml_priceru_option_multiplier_status', '0', 0),
(18415, 0, 'unixml', 'unixml_priceru_genname', '', 0),
(18416, 0, 'unixml', 'unixml_priceru_products_mode', '', 0),
(18417, 0, 'unixml', 'unixml_priceru_product', '', 0),
(18418, 0, 'unixml', 'unixml_priceru_andor', '0', 0),
(18419, 0, 'unixml', 'unixml_priceru_image', '0', 0),
(18420, 0, 'unixml', 'unixml_priceru_images', '0', 0),
(18421, 0, 'unixml', 'unixml_priceru_attribute_status', '0', 0),
(18422, 0, 'unixml', 'unixml_priceru_gendesc', '', 0),
(18423, 0, 'unixml', 'unixml_priceru_clear_desc', '', 0),
(18424, 0, 'unixml', 'unixml_priceru_gendesc_mode', '', 0),
(18425, 0, 'unixml', 'unixml_priceru_secret', '', 0),
(18426, 0, 'unixml', 'unixml_tomasby_status', '0', 0),
(18427, 0, 'unixml', 'unixml_tomasby_name', '', 0),
(18428, 0, 'unixml', 'unixml_tomasby_language', '1', 0),
(18429, 0, 'unixml', 'unixml_tomasby_currency', '3', 0),
(18430, 0, 'unixml', 'unixml_tomasby_field_id', 'p.product_id', 0),
(18431, 0, 'unixml', 'unixml_tomasby_markup', '', 0),
(18432, 0, 'unixml', 'unixml_tomasby_custom_xml', '', 0),
(18433, 0, 'unixml', 'unixml_tomasby_utm', '', 0),
(18434, 0, 'unixml', 'unixml_tomasby_delivery_cost', '', 0),
(18435, 0, 'unixml', 'unixml_tomasby_delivery_time', '', 0),
(18436, 0, 'unixml', 'unixml_tomasby_delivery_jump', '', 0),
(18437, 0, 'unixml', 'unixml_tomasby_fields', '', 0),
(18438, 0, 'unixml', 'unixml_tomasby_field_price', 'p.price', 0),
(18439, 0, 'unixml', 'unixml_tomasby_step', '10000', 0),
(18440, 0, 'unixml', 'unixml_tomasby_seopro', '0', 0),
(18441, 0, 'unixml', 'unixml_tomasby_log', '', 0),
(18442, 0, 'unixml', 'unixml_tomasby_quantity', '0', 0),
(18443, 0, 'unixml', 'unixml_tomasby_stock', '8', 0),
(18444, 0, 'unixml', 'unixml_tomasby_option_multiplier_status', '0', 0),
(18445, 0, 'unixml', 'unixml_tomasby_genname', '', 0),
(18446, 0, 'unixml', 'unixml_tomasby_products_mode', '', 0),
(18447, 0, 'unixml', 'unixml_tomasby_product', '', 0),
(18448, 0, 'unixml', 'unixml_tomasby_andor', '0', 0),
(18449, 0, 'unixml', 'unixml_tomasby_image', '0', 0),
(18450, 0, 'unixml', 'unixml_tomasby_images', '0', 0),
(18451, 0, 'unixml', 'unixml_tomasby_attribute_status', '0', 0),
(18452, 0, 'unixml', 'unixml_tomasby_gendesc', '', 0),
(18453, 0, 'unixml', 'unixml_tomasby_clear_desc', '', 0),
(18454, 0, 'unixml', 'unixml_tomasby_gendesc_mode', '', 0),
(18455, 0, 'unixml', 'unixml_tomasby_secret', '', 0),
(18456, 0, 'unixml', 'unixml_kaspi_status', '0', 0),
(18457, 0, 'unixml', 'unixml_kaspi_name', '', 0),
(18458, 0, 'unixml', 'unixml_kaspi_language', '1', 0),
(18459, 0, 'unixml', 'unixml_kaspi_currency', '3', 0),
(18460, 0, 'unixml', 'unixml_kaspi_field_id', 'p.product_id', 0),
(18461, 0, 'unixml', 'unixml_kaspi_markup', '', 0),
(18462, 0, 'unixml', 'unixml_kaspi_custom_xml', '', 0),
(18463, 0, 'unixml', 'unixml_kaspi_utm', '', 0),
(18464, 0, 'unixml', 'unixml_kaspi_delivery_cost', '', 0),
(18465, 0, 'unixml', 'unixml_kaspi_delivery_time', '', 0),
(18466, 0, 'unixml', 'unixml_kaspi_delivery_jump', '', 0),
(18467, 0, 'unixml', 'unixml_kaspi_fields', '', 0),
(18468, 0, 'unixml', 'unixml_kaspi_field_price', 'p.price', 0),
(18469, 0, 'unixml', 'unixml_kaspi_step', '10000', 0),
(18470, 0, 'unixml', 'unixml_kaspi_seopro', '0', 0),
(18471, 0, 'unixml', 'unixml_kaspi_log', '', 0),
(18472, 0, 'unixml', 'unixml_kaspi_quantity', '0', 0),
(18473, 0, 'unixml', 'unixml_kaspi_stock', '8', 0),
(18474, 0, 'unixml', 'unixml_kaspi_option_multiplier_status', '0', 0),
(18475, 0, 'unixml', 'unixml_kaspi_genname', '', 0),
(18476, 0, 'unixml', 'unixml_kaspi_products_mode', '', 0),
(18477, 0, 'unixml', 'unixml_kaspi_product', '', 0),
(18478, 0, 'unixml', 'unixml_kaspi_andor', '0', 0),
(18479, 0, 'unixml', 'unixml_kaspi_image', '0', 0),
(18480, 0, 'unixml', 'unixml_kaspi_images', '0', 0),
(18481, 0, 'unixml', 'unixml_kaspi_attribute_status', '0', 0),
(18482, 0, 'unixml', 'unixml_kaspi_gendesc', '', 0),
(18483, 0, 'unixml', 'unixml_kaspi_clear_desc', '', 0),
(18484, 0, 'unixml', 'unixml_kaspi_gendesc_mode', '', 0),
(18485, 0, 'unixml', 'unixml_kaspi_secret', '', 0),
(18486, 0, 'unixml', 'unixml_autoru_status', '0', 0),
(18487, 0, 'unixml', 'unixml_autoru_name', '', 0),
(18488, 0, 'unixml', 'unixml_autoru_language', '1', 0),
(18489, 0, 'unixml', 'unixml_autoru_currency', '3', 0),
(18490, 0, 'unixml', 'unixml_autoru_field_id', 'p.product_id', 0),
(18491, 0, 'unixml', 'unixml_autoru_markup', '', 0),
(18492, 0, 'unixml', 'unixml_autoru_custom_xml', '', 0),
(18493, 0, 'unixml', 'unixml_autoru_utm', '', 0),
(18494, 0, 'unixml', 'unixml_autoru_delivery_cost', '', 0),
(18495, 0, 'unixml', 'unixml_autoru_delivery_time', '', 0),
(18496, 0, 'unixml', 'unixml_autoru_delivery_jump', '', 0),
(18497, 0, 'unixml', 'unixml_autoru_fields', '', 0),
(18498, 0, 'unixml', 'unixml_autoru_field_price', 'p.price', 0),
(18499, 0, 'unixml', 'unixml_autoru_step', '10000', 0),
(18500, 0, 'unixml', 'unixml_autoru_seopro', '0', 0),
(18501, 0, 'unixml', 'unixml_autoru_log', '', 0),
(18502, 0, 'unixml', 'unixml_autoru_quantity', '0', 0),
(18503, 0, 'unixml', 'unixml_autoru_stock', '8', 0),
(18504, 0, 'unixml', 'unixml_autoru_option_multiplier_status', '0', 0),
(18505, 0, 'unixml', 'unixml_autoru_genname', '', 0),
(18506, 0, 'unixml', 'unixml_autoru_products_mode', '', 0),
(18507, 0, 'unixml', 'unixml_autoru_product', '', 0),
(18508, 0, 'unixml', 'unixml_autoru_andor', '0', 0),
(18509, 0, 'unixml', 'unixml_autoru_image', '0', 0),
(18510, 0, 'unixml', 'unixml_autoru_images', '0', 0),
(18511, 0, 'unixml', 'unixml_autoru_attribute_status', '0', 0),
(18512, 0, 'unixml', 'unixml_autoru_gendesc', '', 0),
(18513, 0, 'unixml', 'unixml_autoru_clear_desc', '', 0),
(18514, 0, 'unixml', 'unixml_autoru_gendesc_mode', '', 0),
(18515, 0, 'unixml', 'unixml_autoru_secret', '', 0),
(18516, 0, 'unixml', 'unixml_drom_status', '0', 0),
(18517, 0, 'unixml', 'unixml_drom_name', '', 0),
(18518, 0, 'unixml', 'unixml_drom_language', '1', 0),
(18519, 0, 'unixml', 'unixml_drom_currency', '3', 0),
(18520, 0, 'unixml', 'unixml_drom_field_id', 'p.product_id', 0),
(18521, 0, 'unixml', 'unixml_drom_markup', '', 0),
(18522, 0, 'unixml', 'unixml_drom_custom_xml', '', 0),
(18523, 0, 'unixml', 'unixml_drom_utm', '', 0),
(18524, 0, 'unixml', 'unixml_drom_delivery_cost', '', 0),
(18525, 0, 'unixml', 'unixml_drom_delivery_time', '', 0),
(18526, 0, 'unixml', 'unixml_drom_delivery_jump', '', 0),
(18527, 0, 'unixml', 'unixml_drom_fields', '', 0),
(18528, 0, 'unixml', 'unixml_drom_field_price', 'p.price', 0),
(18529, 0, 'unixml', 'unixml_drom_step', '10000', 0),
(18530, 0, 'unixml', 'unixml_drom_seopro', '0', 0),
(18531, 0, 'unixml', 'unixml_drom_log', '', 0),
(18532, 0, 'unixml', 'unixml_drom_quantity', '0', 0),
(18533, 0, 'unixml', 'unixml_drom_stock', '8', 0),
(18534, 0, 'unixml', 'unixml_drom_option_multiplier_status', '0', 0),
(18535, 0, 'unixml', 'unixml_drom_genname', '', 0),
(18536, 0, 'unixml', 'unixml_drom_products_mode', '', 0),
(18537, 0, 'unixml', 'unixml_drom_product', '', 0),
(18538, 0, 'unixml', 'unixml_drom_andor', '0', 0),
(18539, 0, 'unixml', 'unixml_drom_image', '0', 0),
(18540, 0, 'unixml', 'unixml_drom_images', '0', 0),
(18541, 0, 'unixml', 'unixml_drom_attribute_status', '0', 0),
(18542, 0, 'unixml', 'unixml_drom_gendesc', '', 0),
(18543, 0, 'unixml', 'unixml_drom_clear_desc', '', 0),
(18544, 0, 'unixml', 'unixml_drom_gendesc_mode', '', 0),
(18545, 0, 'unixml', 'unixml_drom_secret', '', 0),
(18546, 0, 'unixml', 'unixml_hide', '[\"google\",\"yandex\"]', 1),
(18547, 0, 'unixml', 'unixml_active_tab', '#tab-info', 0),
(18548, 0, 'total_cdek', 'total_cdek_status', '1', 0),
(18549, 0, 'total_cdek', 'total_cdek_sort_order', '3', 0),
(18977, 0, 'config', 'config_meta_title', 'Аршаев', 0),
(18978, 0, 'config', 'config_meta_description', 'Аршаев', 0),
(18979, 0, 'config', 'config_meta_keyword', 'Аршаев', 0),
(18980, 0, 'config', 'config_theme', 'default', 0),
(18981, 0, 'config', 'config_layout_id', '4', 0),
(18982, 0, 'config', 'config_name', 'Аршаев', 0),
(18983, 0, 'config', 'config_owner', 'Аршаев', 0),
(18984, 0, 'config', 'config_address', 'Аршаев', 0),
(18985, 0, 'config', 'config_geocode', '', 0),
(18986, 0, 'config', 'config_email', 'ecos@4ait.ru', 0),
(18987, 0, 'config', 'config_telephone', '+7 (4212) 12-34-56', 0),
(18988, 0, 'config', 'config_telephone2', '', 0),
(18989, 0, 'config', 'config_fax', '', 0),
(18990, 0, 'config', 'config_image', '', 0),
(18991, 0, 'config', 'config_open', 'c 10:00 до 05:00', 0),
(18992, 0, 'config', 'config_comment', '', 0),
(18993, 0, 'config', 'config_schema_address', '', 0),
(18994, 0, 'config', 'config_country_id', '176', 0),
(18995, 0, 'config', 'config_zone_id', '', 0),
(18996, 0, 'config', 'config_language', 'ru-ru', 0),
(18997, 0, 'config', 'config_admin_language', 'ru-ru', 0),
(18998, 0, 'config', 'config_currency', 'RUB', 0),
(18999, 0, 'config', 'config_currency_auto', '1', 0),
(19000, 0, 'config', 'config_length_class_id', '1', 0),
(19001, 0, 'config', 'config_weight_class_id', '1', 0),
(19002, 0, 'config', 'config_product_count', '0', 0),
(19003, 0, 'config', 'config_limit_admin', '20', 0),
(19004, 0, 'config', 'config_autocomplete_limit', '20', 0),
(19005, 0, 'config', 'config_review_status', '0', 0),
(19006, 0, 'config', 'config_review_guest', '0', 0),
(19007, 0, 'config', 'config_voucher_min', '1', 0),
(19008, 0, 'config', 'config_voucher_max', '1000', 0),
(19009, 0, 'config', 'config_tax', '0', 0),
(19010, 0, 'config', 'config_tax_default', 'shipping', 0),
(19011, 0, 'config', 'config_tax_customer', 'shipping', 0),
(19012, 0, 'config', 'config_customer_online', '1', 0),
(19013, 0, 'config', 'config_customer_activity', '1', 0),
(19014, 0, 'config', 'config_customer_search', '0', 0),
(19015, 0, 'config', 'config_customer_group_id', '1', 0),
(19016, 0, 'config', 'config_customer_group_display', '[\"1\",\"4\"]', 1),
(19017, 0, 'config', 'config_customer_price', '0', 0),
(19018, 0, 'config', 'config_login_attempts', '5', 0),
(19019, 0, 'config', 'config_account_id', '0', 0),
(19020, 0, 'config', 'config_invoice_prefix', 'INV-2019-00', 0),
(19021, 0, 'config', 'config_cart_weight', '0', 0),
(19022, 0, 'config', 'config_checkout_guest', '1', 0),
(19023, 0, 'config', 'config_checkout_id', '5', 0),
(19024, 0, 'config', 'config_order_status_id', '1', 0),
(19025, 0, 'config', 'config_processing_status', '[\"2\",\"3\",\"1\",\"12\",\"5\"]', 1),
(19026, 0, 'config', 'config_complete_status', '[\"3\",\"5\"]', 1),
(19027, 0, 'config', 'config_fraud_status_id', '16', 0),
(19028, 0, 'config', 'config_api_id', '1', 0),
(19029, 0, 'config', 'config_stock_display', '0', 0),
(19030, 0, 'config', 'config_stock_warning', '0', 0),
(19031, 0, 'config', 'config_stock_checkout', '1', 0),
(19032, 0, 'config', 'config_affiliate_group_id', '1', 0),
(19033, 0, 'config', 'config_affiliate_approval', '0', 0),
(19034, 0, 'config', 'config_affiliate_auto', '0', 0),
(19035, 0, 'config', 'config_affiliate_commission', '5', 0),
(19036, 0, 'config', 'config_affiliate_id', '0', 0),
(19037, 0, 'config', 'config_return_id', '0', 0),
(19038, 0, 'config', 'config_return_status_id', '2', 0),
(19039, 0, 'config', 'config_captcha', '', 0),
(19040, 0, 'config', 'config_captcha_page', '[\"guest\",\"review\",\"return\",\"contact\"]', 1),
(19041, 0, 'config', 'config_logo', 'catalog/Group 9.svg', 0),
(19042, 0, 'config', 'config_logo_footer', 'catalog/Group 91.svg', 0),
(19043, 0, 'config', 'config_icon', '', 0),
(19044, 0, 'config', 'config_mail_engine', 'mail', 0),
(19045, 0, 'config', 'config_mail_parameter', '-O DeliveryMode=b', 0),
(19046, 0, 'config', 'config_mail_smtp_hostname', 'ssl://smtp.yandex.ru', 0),
(19047, 0, 'config', 'config_mail_smtp_username', 'postservice@4ait.ru', 0),
(19048, 0, 'config', 'config_mail_smtp_password', 'PaSs2019dv1', 0),
(19049, 0, 'config', 'config_mail_smtp_port', '25', 0),
(19050, 0, 'config', 'config_mail_smtp_timeout', '5', 0),
(19051, 0, 'config', 'config_mail_alert', '[\"order\"]', 1),
(19052, 0, 'config', 'config_mail_alert_email', '', 0),
(19053, 0, 'config', 'config_maintenance', '0', 0),
(19054, 0, 'config', 'config_seo_url', '1', 0),
(19055, 0, 'config', 'config_robots', 'abot\r\ndbot\r\nebot\r\nhbot\r\nkbot\r\nlbot\r\nmbot\r\nnbot\r\nobot\r\npbot\r\nrbot\r\nsbot\r\ntbot\r\nvbot\r\nybot\r\nzbot\r\nbot.\r\nbot/\r\n_bot\r\n.bot\r\n/bot\r\n-bot\r\n:bot\r\n(bot\r\ncrawl\r\nslurp\r\nspider\r\nseek\r\naccoona\r\nacoon\r\nadressendeutschland\r\nah-ha.com\r\nahoy\r\naltavista\r\nananzi\r\nanthill\r\nappie\r\narachnophilia\r\narale\r\naraneo\r\naranha\r\narchitext\r\naretha\r\narks\r\nasterias\r\natlocal\r\natn\r\natomz\r\naugurfind\r\nbackrub\r\nbannana_bot\r\nbaypup\r\nbdfetch\r\nbig brother\r\nbiglotron\r\nbjaaland\r\nblackwidow\r\nblaiz\r\nblog\r\nblo.\r\nbloodhound\r\nboitho\r\nbooch\r\nbradley\r\nbutterfly\r\ncalif\r\ncassandra\r\nccubee\r\ncfetch\r\ncharlotte\r\nchurl\r\ncienciaficcion\r\ncmc\r\ncollective\r\ncomagent\r\ncombine\r\ncomputingsite\r\ncsci\r\ncurl\r\ncusco\r\ndaumoa\r\ndeepindex\r\ndelorie\r\ndepspid\r\ndeweb\r\ndie blinde kuh\r\ndigger\r\nditto\r\ndmoz\r\ndocomo\r\ndownload express\r\ndtaagent\r\ndwcp\r\nebiness\r\nebingbong\r\ne-collector\r\nejupiter\r\nemacs-w3 search engine\r\nesther\r\nevliya celebi\r\nezresult\r\nfalcon\r\nfelix ide\r\nferret\r\nfetchrover\r\nfido\r\nfindlinks\r\nfireball\r\nfish search\r\nfouineur\r\nfunnelweb\r\ngazz\r\ngcreep\r\ngenieknows\r\ngetterroboplus\r\ngeturl\r\nglx\r\ngoforit\r\ngolem\r\ngrabber\r\ngrapnel\r\ngralon\r\ngriffon\r\ngromit\r\ngrub\r\ngulliver\r\nhamahakki\r\nharvest\r\nhavindex\r\nhelix\r\nheritrix\r\nhku www octopus\r\nhomerweb\r\nhtdig\r\nhtml index\r\nhtml_analyzer\r\nhtmlgobble\r\nhubater\r\nhyper-decontextualizer\r\nia_archiver\r\nibm_planetwide\r\nichiro\r\niconsurf\r\niltrovatore\r\nimage.kapsi.net\r\nimagelock\r\nincywincy\r\nindexer\r\ninfobee\r\ninformant\r\ningrid\r\ninktomisearch.com\r\ninspector web\r\nintelliagent\r\ninternet shinchakubin\r\nip3000\r\niron33\r\nisraeli-search\r\nivia\r\njack\r\njakarta\r\njavabee\r\njetbot\r\njumpstation\r\nkatipo\r\nkdd-explorer\r\nkilroy\r\nknowledge\r\nkototoi\r\nkretrieve\r\nlabelgrabber\r\nlachesis\r\nlarbin\r\nlegs\r\nlibwww\r\nlinkalarm\r\nlink validator\r\nlinkscan\r\nlockon\r\nlwp\r\nlycos\r\nmagpie\r\nmantraagent\r\nmapoftheinternet\r\nmarvin/\r\nmattie\r\nmediafox\r\nmediapartners\r\nmercator\r\nmerzscope\r\nmicrosoft url control\r\nminirank\r\nmiva\r\nmj12\r\nmnogosearch\r\nmoget\r\nmonster\r\nmoose\r\nmotor\r\nmultitext\r\nmuncher\r\nmuscatferret\r\nmwd.search\r\nmyweb\r\nnajdi\r\nnameprotect\r\nnationaldirectory\r\nnazilla\r\nncsa beta\r\nnec-meshexplorer\r\nnederland.zoek\r\nnetcarta webmap engine\r\nnetmechanic\r\nnetresearchserver\r\nnetscoop\r\nnewscan-online\r\nnhse\r\nnokia6682/\r\nnomad\r\nnoyona\r\nnutch\r\nnzexplorer\r\nobjectssearch\r\noccam\r\nomni\r\nopen text\r\nopenfind\r\nopenintelligencedata\r\norb search\r\nosis-project\r\npack rat\r\npageboy\r\npagebull\r\npage_verifier\r\npanscient\r\nparasite\r\npartnersite\r\npatric\r\npear.\r\npegasus\r\nperegrinator\r\npgp key agent\r\nphantom\r\nphpdig\r\npicosearch\r\npiltdownman\r\npimptrain\r\npinpoint\r\npioneer\r\npiranha\r\nplumtreewebaccessor\r\npogodak\r\npoirot\r\npompos\r\npoppelsdorf\r\npoppi\r\npopular iconoclast\r\npsycheclone\r\npublisher\r\npython\r\nrambler\r\nraven search\r\nroach\r\nroad runner\r\nroadhouse\r\nrobbie\r\nrobofox\r\nrobozilla\r\nrules\r\nsalty\r\nsbider\r\nscooter\r\nscoutjet\r\nscrubby\r\nsearch.\r\nsearchprocess\r\nsemanticdiscovery\r\nsenrigan\r\nsg-scout\r\nshai\'hulud\r\nshark\r\nshopwiki\r\nsidewinder\r\nsift\r\nsilk\r\nsimmany\r\nsite searcher\r\nsite valet\r\nsitetech-rover\r\nskymob.com\r\nsleek\r\nsmartwit\r\nsna-\r\nsnappy\r\nsnooper\r\nsohu\r\nspeedfind\r\nsphere\r\nsphider\r\nspinner\r\nspyder\r\nsteeler/\r\nsuke\r\nsuntek\r\nsupersnooper\r\nsurfnomore\r\nsven\r\nsygol\r\nszukacz\r\ntach black widow\r\ntarantula\r\ntempleton\r\n/teoma\r\nt-h-u-n-d-e-r-s-t-o-n-e\r\ntheophrastus\r\ntitan\r\ntitin\r\ntkwww\r\ntoutatis\r\nt-rex\r\ntutorgig\r\ntwiceler\r\ntwisted\r\nucsd\r\nudmsearch\r\nurl check\r\nupdated\r\nvagabondo\r\nvalkyrie\r\nverticrawl\r\nvictoria\r\nvision-search\r\nvolcano\r\nvoyager/\r\nvoyager-hc\r\nw3c_validator\r\nw3m2\r\nw3mir\r\nwalker\r\nwallpaper\r\nwanderer\r\nwauuu\r\nwavefire\r\nweb core\r\nweb hopper\r\nweb wombat\r\nwebbandit\r\nwebcatcher\r\nwebcopy\r\nwebfoot\r\nweblayers\r\nweblinker\r\nweblog monitor\r\nwebmirror\r\nwebmonkey\r\nwebquest\r\nwebreaper\r\nwebsitepulse\r\nwebsnarf\r\nwebstolperer\r\nwebvac\r\nwebwalk\r\nwebwatch\r\nwebwombat\r\nwebzinger\r\nwhizbang\r\nwhowhere\r\nwild ferret\r\nworldlight\r\nwwwc\r\nwwwster\r\nxenu\r\nxget\r\nxift\r\nxirq\r\nyandex\r\nyanga\r\nyeti\r\nyodao\r\nzao\r\nzippp\r\nzyborg', 0),
(19056, 0, 'config', 'config_compression', '5', 0),
(19057, 0, 'config', 'config_secure', '1', 0),
(19058, 0, 'config', 'config_password', '1', 0),
(19059, 0, 'config', 'config_shared', '0', 0),
(19060, 0, 'config', 'config_encryption', 'qR6NRhiKJ3Vp016e5sDTTcjJ1r2XSOjaBtkzlCdY2pJKEKuWFAMrsNDGUic3Kmt6CUeI1hQ3cHInhDJ8tguyIvQlLCG3qJ1jG3gl6LrsclfHif9ganwNs1WCp1wuG2eBBGx9wZPfMHnqVaziZ8HHeGe43LM4Pj2GgeIoA8sraG1oekSa8U359W4V1ZrffA2uqn4S5ZkGRiqcS4IbDJT73nbdQw7yHeFLXz0VA3EIR9DvjoJ3qkjrXRyfJeyn2FRUpqN4ffUeSBHx2tOZOHZ2K4E4KJun5qJHkdV7SiFvu7ricD9EY8AMAcjkK0KBbjvM6FeAaTCq0UAyBnA0gxgfcHuU7NYmnpw1IdsckfgMjp1cqBMQdOp3D22Yh6bcytBBSR0hFqxSuEjUkNudD97LNB3b2ZlcaGDdqJV1uULIS2HbcG25kGJPZQs9jUJqnhuCdMfyRC5tvn2GZjbK0sRNP7gav3YJrkvWDQfj9JJ37vFpw0I81ltm28PsUpIuGbA6PzsCAdA5kV3EbwJEq7r6LE0ufvhmyEnHMLObaeZWISkx4NEejsh3onuUrBpEAHeSopREWr4ZPzUW5wPfW3dkCI8bJNwnH6y9f2fAGchymXleacCZYEoFkFnPQDOifvMbLfNYbmHKO3ow6UuV87TfD6hlruxiCayCJvCK70BCdZpSaro2RD7OuYfqDtWt8wAOIgaDUTT4NIbOniE3U2mWs48rQkmIRo9v12Aev89v4cxSGpjn4ol4OfdMn72zCdvmTqrdhUxIqUr8CgyYuzswqHwBIWlCOPIswX95Wr5YGhrZT8Ywh6Rzxvq3yEdi4W0yTVWvqmCLXZjM1ZjRQHCNchINtn0vg6enVP6CmwxcHgXsNleDRWO034fq50EbYXCp0bqA6sXyKSQ1iLZJPeKBKuABT01GAEjK560hmFCgGk5RnsiXsgwOQIqID9kaoDxarCZZWnI74CwdEeQhimUueqy4mSdRgZ0oBT7zWGuUbmb48vA8PUaVizqKYxVV8iE2', 0),
(19061, 0, 'config', 'config_file_max_size', '300000', 0),
(19062, 0, 'config', 'config_file_ext_allowed', 'zip\r\ntxt\r\npng\r\njpe\r\njpeg\r\njpg\r\ngif\r\nbmp\r\nico\r\ntiff\r\ntif\r\nsvg\r\nsvgz\r\nzip\r\nrar\r\nmsi\r\ncab\r\nmp3\r\nqt\r\nmov\r\npdf\r\npsd\r\nai\r\neps\r\nps\r\ndoc', 0),
(19063, 0, 'config', 'config_file_mime_allowed', 'text/plain\r\nimage/png\r\nimage/jpeg\r\nimage/gif\r\nimage/bmp\r\nimage/tiff\r\nimage/svg+xml\r\napplication/zip\r\n&quot;application/zip&quot;\r\napplication/x-zip\r\n&quot;application/x-zip&quot;\r\napplication/x-zip-compressed\r\n&quot;application/x-zip-compressed&quot;\r\napplication/rar\r\n&quot;application/rar&quot;\r\napplication/x-rar\r\n&quot;application/x-rar&quot;\r\napplication/x-rar-compressed\r\n&quot;application/x-rar-compressed&quot;\r\napplication/octet-stream\r\n&quot;application/octet-stream&quot;\r\naudio/mpeg\r\nvideo/quicktime\r\napplication/pdf', 0),
(19064, 0, 'config', 'config_error_display', '1', 0),
(19065, 0, 'config', 'config_error_log', '1', 0),
(19066, 0, 'config', 'config_error_filename', 'error.log', 0),
(19067, 0, 'config', 'config_seo_pro', '1', 0),
(19068, 0, 'config', 'config_seo_url_include_path', '1', 0),
(19069, 0, 'config', 'config_seo_url_short_category_path', '0', 0),
(19070, 0, 'config', 'config_seo_url_cache', '0', 0),
(19071, 0, 'config', 'config_seopro_addslash', '1', 0),
(19072, 0, 'config', 'config_seopro_lowercase', '1', 0),
(19073, 0, 'config', 'config_page_postfix', '', 0),
(19074, 0, 'config', 'config_valide_param_flag', '1', 0),
(19075, 0, 'config', 'config_valide_params', 'tracking\r\nutm_source\r\nutm_campaign\r\nutm_medium\r\ntype\r\nsource\r\nblock\r\nposition\r\nkeyword\r\nyclid\r\ngclid', 0),
(19076, 0, 'config', 'config_watermark_extension', '1', 0),
(19077, 0, 'config', 'config_watermark_progressive_jpeg', '0', 0),
(19078, 0, 'config', 'config_watermark_status', '0', 0),
(19079, 0, 'config', 'config_watermark_hide_real_path', '0', 0),
(19080, 0, 'config', 'config_watermark_resize_first', '1', 0),
(19081, 0, 'config', 'config_watermark_image', 'catalog/opencart-logo.png', 0),
(19082, 0, 'config', 'config_watermark_zoom', '0.6', 0),
(19083, 0, 'config', 'config_watermark_pos_x_center', '1', 0),
(19084, 0, 'config', 'config_watermark_pos_x', '0', 0),
(19085, 0, 'config', 'config_watermark_pos_y_center', '1', 0),
(19086, 0, 'config', 'config_watermark_pos_y', '0', 0),
(19087, 0, 'config', 'config_watermark_opacity', '1.0', 0),
(19088, 0, 'config', 'config_watermark_category_image', '0', 0),
(19089, 0, 'config', 'config_watermark_product_thumb', '0', 0),
(19090, 0, 'config', 'config_watermark_product_popup', '1', 0),
(19091, 0, 'config', 'config_watermark_product_list', '0', 0),
(19092, 0, 'config', 'config_watermark_product_additional', '0', 0),
(19093, 0, 'config', 'config_watermark_product_related', '0', 0),
(19094, 0, 'config', 'config_watermark_product_in_compare', '0', 0),
(19095, 0, 'config', 'config_watermark_product_in_wish_list', '0', 0),
(19096, 0, 'config', 'config_watermark_product_in_cart', '0', 0),
(19097, 0, 'config', 'config_watermark_size_x', '187', 0),
(19098, 0, 'config', 'config_watermark_size_y', '50', 0),
(19343, 0, 'module_trade_import', 'module_trade_import_server', 'https://arshaevy.4ait.ru/', 0),
(19344, 0, 'module_trade_import', 'module_trade_import_code', 'https://arshaevy.4ait.ru/api/v1/auth', 0),
(19345, 0, 'module_trade_import', 'module_trade_import_nomenclature', 'https://arshaevy.4ait.ru/api/v1/products', 0),
(19346, 0, 'module_trade_import', 'module_trade_import_token', '4c12fdc0d74931015955334c06fc67802b161fcd', 0),
(19347, 0, 'module_trade_import', 'module_trade_import_enable_old_api', '0', 0),
(19348, 0, 'module_trade_import', 'module_trade_import_old_api_address', '', 0),
(19349, 0, 'module_trade_import', 'module_trade_import_old_api_token', '', 0),
(19350, 0, 'module_trade_import', 'module_trade_import_enable_order', '1', 0),
(19351, 0, 'module_trade_import', 'module_trade_import_order_address', 'https://arshaevy.4ait.ru/api/v1/orders', 0),
(19352, 0, 'module_trade_import', 'module_trade_import_order_token', '', 0),
(19353, 0, 'module_trade_import', 'module_trade_import_order_cashbox_id', '', 0),
(19354, 0, 'module_trade_import', 'module_trade_import_order_employee_id', '', 0),
(19355, 0, 'module_trade_import', 'module_trade_import_order_payment_type_id', '', 0),
(19356, 0, 'module_trade_import', 'module_trade_import_order_storage_id', '', 0),
(19357, 0, 'module_trade_import', 'module_trade_import_delivery_uuid', '', 0),
(19358, 0, 'module_trade_import', 'module_trade_import_delivery_storage_id', '', 0),
(19359, 0, 'module_trade_import', 'module_trade_import_price', '5bc6ee0b-b6bf-4597-ab84-6d6656963722', 0),
(19360, 0, 'module_trade_import', 'module_trade_import_top_category', '', 0);
INSERT INTO `oc_setting` (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES
(19361, 0, 'module_trade_import', 'module_trade_import_price_city', '', 0),
(19362, 0, 'module_trade_import', 'module_trade_import_price_map', '', 0),
(19363, 0, 'module_trade_import', 'module_trade_import_parent_id', '50', 0),
(19364, 0, 'module_trade_import', 'module_trade_import_ignore_category', '', 0),
(19365, 0, 'module_trade_import', 'module_trade_import_ignore_filter', '', 0),
(19366, 0, 'module_trade_import', 'module_trade_import_ignore_property', '', 0),
(19367, 0, 'module_trade_import', 'module_trade_import_add_properties_to_filters', '', 0),
(19368, 0, 'module_trade_import', 'module_trade_import_banner_id', '', 0),
(19369, 0, 'module_trade_import', 'module_trade_import_properties_as_description', '', 0),
(19370, 0, 'module_trade_import', 'module_trade_import_default_weight', '', 0),
(19371, 0, 'module_trade_import', 'module_trade_import_default_size', '', 0),
(19372, 0, 'module_trade_import', 'module_trade_import_enable_sync', '0', 0),
(19373, 0, 'module_trade_import', 'module_trade_import_sync_period', '10_minutes', 0),
(19374, 0, 'module_trade_import', 'module_trade_import_sync_time', '', 0),
(19375, 0, 'module_trade_import', 'module_trade_import_time_zone', 'Asia/Vladivostok', 0),
(19376, 0, 'module_trade_import', 'module_trade_import_local_json', '0', 0),
(19377, 0, 'module_trade_import', 'module_trade_import_save_json', '1', 0),
(19378, 0, 'module_trade_import', 'module_trade_import_add_category', '1', 0),
(19379, 0, 'module_trade_import', 'module_trade_import_delete_category', '1', 0),
(19380, 0, 'module_trade_import', 'module_trade_import_hide_category', '0', 0),
(19381, 0, 'module_trade_import', 'module_trade_import_sub_filters', '0', 0),
(19382, 0, 'module_trade_import', 'module_trade_import_add_product', '1', 0),
(19383, 0, 'module_trade_import', 'module_trade_import_delete_product', '1', 0),
(19384, 0, 'module_trade_import', 'module_trade_import_hide_product', '0', 0),
(19385, 0, 'module_trade_import', 'module_trade_import_hide_empty_product', '0', 0),
(19386, 0, 'module_trade_import', 'module_trade_import_round_price', 'off', 0),
(19387, 0, 'module_trade_import', 'module_trade_import_add_separate_products', '0', 0),
(19388, 0, 'module_trade_import', 'module_trade_import_ocfilter', '1', 0),
(19389, 0, 'module_trade_import', 'module_trade_import_keep_category_names', '0', 0),
(19390, 0, 'module_trade_import', 'module_trade_import_keep_category_description', '0', 0),
(19391, 0, 'module_trade_import', 'module_trade_import_keep_category_meta', '0', 0),
(19392, 0, 'module_trade_import', 'module_trade_import_keep_product_names', '0', 0),
(19393, 0, 'module_trade_import', 'module_trade_import_keep_product_description', '0', 0),
(19394, 0, 'module_trade_import', 'module_trade_import_keep_product_meta', '0', 0),
(19395, 0, 'module_trade_import', 'module_trade_import_short_url', '1', 0),
(19396, 0, 'module_trade_import', 'module_trade_import_full_path_url', '0', 0),
(19397, 0, 'module_trade_import', 'module_trade_import_full_sync', '1', 0),
(19398, 0, 'module_trade_import', 'module_trade_import_names_as_uuid', '0', 0),
(19399, 0, 'module_trade_import', 'module_trade_import_product_image_jpeg', '0', 0),
(19400, 0, 'module_trade_import', 'module_trade_import_banner_image_jpeg', '0', 0),
(19401, 0, 'module_trade_import', 'module_trade_import_image_ignore_same_size', '0', 0),
(19402, 0, 'module_trade_import', 'module_trade_import_add_one_product', '', 0),
(19403, 0, 'module_trade_import', 'module_trade_import_sync_schedule', '2021-09-14 13:40:00', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_shipping_courier`
--

CREATE TABLE `oc_shipping_courier` (
  `shipping_courier_id` int(11) NOT NULL,
  `shipping_courier_code` varchar(255) NOT NULL DEFAULT '',
  `shipping_courier_name` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_shipping_courier`
--

INSERT INTO `oc_shipping_courier` (`shipping_courier_id`, `shipping_courier_code`, `shipping_courier_name`) VALUES
(1, 'dhl', 'DHL'),
(2, 'fedex', 'Fedex'),
(3, 'ups', 'UPS'),
(4, 'royal-mail', 'Royal Mail'),
(5, 'usps', 'United States Postal Service'),
(6, 'auspost', 'Australia Post'),
(7, 'citylink', 'Citylink');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_statistics`
--

CREATE TABLE `oc_statistics` (
  `statistics_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `value` decimal(15,4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_statistics`
--

INSERT INTO `oc_statistics` (`statistics_id`, `code`, `value`) VALUES
(1, 'order_sale', '99347.2400'),
(2, 'order_processing', '0.0000'),
(3, 'order_complete', '0.0000'),
(4, 'order_other', '0.0000'),
(5, 'returns', '0.0000'),
(6, 'product', '0.0000'),
(7, 'review', '0.0000');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_stocks`
--

CREATE TABLE `oc_stocks` (
  `stocks_id` int(11) NOT NULL,
  `start_at` date NOT NULL,
  `end_at` date NOT NULL,
  `discount` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_stocks_description`
--

CREATE TABLE `oc_stocks_description` (
  `stocks_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `requirements` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_keyword` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_h1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_stocks_product`
--

CREATE TABLE `oc_stocks_product` (
  `stocks_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_stock_status`
--

CREATE TABLE `oc_stock_status` (
  `stock_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_stock_status`
--

INSERT INTO `oc_stock_status` (`stock_status_id`, `language_id`, `name`) VALUES
(5, 1, 'Нет в наличии'),
(5, 2, 'Out Of Stock'),
(6, 1, 'Ожидание 2-3 дня'),
(6, 2, '2-3 Days'),
(7, 1, 'В наличии'),
(7, 2, 'In Stock'),
(8, 1, 'Предзаказ'),
(8, 2, 'Pre-Order');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_store`
--

CREATE TABLE `oc_store` (
  `store_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `url` varchar(255) NOT NULL,
  `ssl` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_tax_class`
--

CREATE TABLE `oc_tax_class` (
  `tax_class_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_tax_class`
--

INSERT INTO `oc_tax_class` (`tax_class_id`, `title`, `description`, `date_added`, `date_modified`) VALUES
(9, 'Taxable Goods', 'Taxed goods', '2009-01-06 23:21:53', '2011-09-23 14:07:50'),
(10, 'Downloadable Products', 'Downloadable', '2011-09-21 22:19:39', '2011-09-22 10:27:36');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_tax_rate`
--

CREATE TABLE `oc_tax_rate` (
  `tax_rate_id` int(11) NOT NULL,
  `geo_zone_id` int(11) NOT NULL DEFAULT 0,
  `name` varchar(32) NOT NULL,
  `rate` decimal(15,4) NOT NULL DEFAULT 0.0000,
  `type` char(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_tax_rate`
--

INSERT INTO `oc_tax_rate` (`tax_rate_id`, `geo_zone_id`, `name`, `rate`, `type`, `date_added`, `date_modified`) VALUES
(86, 3, 'VAT (20%)', '20.0000', 'P', '2011-03-09 21:17:10', '2011-09-22 22:24:29'),
(87, 3, 'Eco Tax (-2.00)', '2.0000', 'F', '2011-09-21 21:49:23', '2011-09-23 00:40:19');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_tax_rate_to_customer_group`
--

CREATE TABLE `oc_tax_rate_to_customer_group` (
  `tax_rate_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_tax_rate_to_customer_group`
--

INSERT INTO `oc_tax_rate_to_customer_group` (`tax_rate_id`, `customer_group_id`) VALUES
(86, 1),
(87, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_tax_rule`
--

CREATE TABLE `oc_tax_rule` (
  `tax_rule_id` int(11) NOT NULL,
  `tax_class_id` int(11) NOT NULL,
  `tax_rate_id` int(11) NOT NULL,
  `based` varchar(10) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_tax_rule`
--

INSERT INTO `oc_tax_rule` (`tax_rule_id`, `tax_class_id`, `tax_rate_id`, `based`, `priority`) VALUES
(120, 10, 87, 'store', 0),
(121, 10, 86, 'payment', 1),
(127, 9, 87, 'shipping', 2),
(128, 9, 86, 'shipping', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_theme`
--

CREATE TABLE `oc_theme` (
  `theme_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `theme` varchar(64) NOT NULL,
  `route` varchar(64) NOT NULL,
  `code` mediumtext NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_trade_checks`
--

CREATE TABLE `oc_trade_checks` (
  `check_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_data` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `response` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_trade_images`
--

CREATE TABLE `oc_trade_images` (
  `id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hash` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_trade_import`
--

CREATE TABLE `oc_trade_import` (
  `operation_id` int(11) NOT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  `json_timestamp` timestamp NULL DEFAULT NULL,
  `success` tinyint(1) DEFAULT NULL,
  `response` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `oc_trade_import`
--

INSERT INTO `oc_trade_import` (`operation_id`, `timestamp`, `json_timestamp`, `success`, `response`) VALUES
(1, '2021-09-14 03:30:03', '2021-09-13 17:30:03', 1, '');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_trade_import_category_codes`
--

CREATE TABLE `oc_trade_import_category_codes` (
  `category_id` int(11) NOT NULL,
  `group_uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `oc_trade_import_category_codes`
--

INSERT INTO `oc_trade_import_category_codes` (`category_id`, `group_uuid`) VALUES
(51, '3747115a-9bbf-4037-874c-9bf8100ddf9e'),
(52, '624b6619-136a-4b52-8416-c464d09af598'),
(53, 'd21534ee-7b5e-49e8-8bf3-64ce891029b1');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_trade_import_filter_group_codes`
--

CREATE TABLE `oc_trade_import_filter_group_codes` (
  `filter_group_id` int(11) NOT NULL,
  `filter_uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_trade_import_option_characteristic_codes`
--

CREATE TABLE `oc_trade_import_option_characteristic_codes` (
  `characteristic_id` int(11) NOT NULL,
  `property_uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_trade_import_option_codes`
--

CREATE TABLE `oc_trade_import_option_codes` (
  `option_id` int(11) NOT NULL,
  `nomenclature_uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `oc_trade_import_option_codes`
--

INSERT INTO `oc_trade_import_option_codes` (`option_id`, `nomenclature_uuid`) VALUES
(1, 'af8f5019-2385-4054-9eed-96c08079bd74'),
(2, '497d4414-c05f-4e70-a4b9-60f4e5fe6f76'),
(3, 'e504d8d8-b516-4eec-8064-97a8256b9584'),
(4, 'fd0a89b6-3fbb-4d3b-b518-622530c6fcb4'),
(5, '26b00395-e9ed-4dde-9fbd-3766c044eb38'),
(6, 'fb440671-4818-453e-813e-6d2f39f51047'),
(7, 'b80a1ad0-4154-4bcc-81a4-420aad7ed35a'),
(8, '63b58b3b-f1ec-4e6f-b245-ebe49f135e7b'),
(9, 'd33e5c6b-0549-4186-b8a6-1f4059be4529'),
(10, '5c2401c5-8332-478d-9f74-021e37af8f77'),
(11, '7866f70b-2ca0-43d2-a24d-d469dda21a08'),
(12, '7383aca9-2deb-4f4d-a371-d71cf99cb0fe'),
(13, 'a17d74a8-7e90-44bd-b3c2-644815a9c7cb'),
(14, '9efb5ecd-a54d-41af-a381-6ba7753266d3'),
(15, '71fee567-679c-4704-bee3-613c8cb0c2f5'),
(16, '72d8ebdb-dd6b-4dc1-9391-7d2d60a6d422'),
(17, 'f77b72ca-c4bb-4655-a845-8e4526d83940'),
(18, '4c3774a5-fe81-4b99-a183-d6d0a472b98d'),
(19, 'c97654d2-d8a9-4f24-b60c-b231f54f412f'),
(20, 'a78e8715-96c2-416a-833c-854f6c8d1972'),
(21, '79e7509e-ef2f-4e5e-8b80-dd673d62bb66'),
(22, 'c179a270-ce8a-4d3c-9e80-e2f07056dda0'),
(23, '9d3e5fec-ac71-43d4-a466-a3fb590df6e0'),
(24, 'd270b222-2ffe-448c-961d-c46b1cd03231'),
(25, 'c5ab5265-8a8d-43fd-a282-52f9dd19f91b'),
(26, '78c68093-5fa1-4c77-b13c-b52a008f90ee'),
(27, 'fc1adafa-51a8-4bb4-9d8a-0261eb556168'),
(28, 'b04805fc-08b3-43f5-bc15-4c174abd556d'),
(29, '0c2d0cb3-f60a-4f2f-a717-7ea1a61e14cf'),
(30, 'dba5eb21-2947-4361-9405-82f81ec9453a'),
(31, '43fd5c6a-1252-46b4-9c8e-fee43010253e'),
(32, 'f8c9bbe4-2610-4e27-a7e0-93c7eb522087'),
(33, '52f53776-d370-4f6d-94e3-56b1a5b349e0'),
(34, 'e93425d8-211d-4211-ac23-1397a6df1539'),
(35, '5ee99a8b-8156-4102-bae8-aee2bf4d006c');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_trade_import_option_value_codes`
--

CREATE TABLE `oc_trade_import_option_value_codes` (
  `option_value_id` int(11) NOT NULL,
  `characteristic_uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nomenclature_uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `oc_trade_import_option_value_codes`
--

INSERT INTO `oc_trade_import_option_value_codes` (`option_value_id`, `characteristic_uuid`, `nomenclature_uuid`) VALUES
(376, '00a90306-94f5-4db5-be6b-c7b2621aacb3', 'dba5eb21-2947-4361-9405-82f81ec9453a'),
(269, '011b9104-70ff-4e71-b815-eff9050ad4e9', '79e7509e-ef2f-4e5e-8b80-dd673d62bb66'),
(141, '01a48db4-b3cd-45a8-a876-feb70c800479', '5c2401c5-8332-478d-9f74-021e37af8f77'),
(136, '02128a6c-51eb-4b88-b58a-8595e8a1502c', '5c2401c5-8332-478d-9f74-021e37af8f77'),
(212, '02220289-d48f-4c34-9837-3e30255d2dc8', '72d8ebdb-dd6b-4dc1-9391-7d2d60a6d422'),
(16, '027651e3-1114-4413-ac14-4c7f9ba840d8', '497d4414-c05f-4e70-a4b9-60f4e5fe6f76'),
(220, '02a099e5-5da6-4f3b-ade8-89ba567bf254', '4c3774a5-fe81-4b99-a183-d6d0a472b98d'),
(216, '034a5ddf-18b6-4e6e-bb79-22ec62baccde', 'f77b72ca-c4bb-4655-a845-8e4526d83940'),
(322, '03df2bcb-a16f-4102-a54b-4cf5e2d75fdd', 'fc1adafa-51a8-4bb4-9d8a-0261eb556168'),
(230, '05e23225-578b-4980-a1aa-49874e4ca9b8', '4c3774a5-fe81-4b99-a183-d6d0a472b98d'),
(385, '06237225-0712-4532-bc26-5af114afb9b4', '43fd5c6a-1252-46b4-9c8e-fee43010253e'),
(168, '0670d62a-adf9-42a1-af47-5f9d9c756c79', '7866f70b-2ca0-43d2-a24d-d469dda21a08'),
(206, '06a53bf5-6068-48e9-a0db-4be6f1a344e0', '72d8ebdb-dd6b-4dc1-9391-7d2d60a6d422'),
(28, '06df624e-62f1-4226-a92e-5da5260027d6', 'e504d8d8-b516-4eec-8064-97a8256b9584'),
(424, '06ffc5be-c7d5-42ae-9d8c-a587f005ef00', '52f53776-d370-4f6d-94e3-56b1a5b349e0'),
(6, '07a5e01a-b318-4042-a4bf-d0ca4ebd669c', 'af8f5019-2385-4054-9eed-96c08079bd74'),
(465, '07e24982-e922-40c3-b1e7-719121c16394', '5ee99a8b-8156-4102-bae8-aee2bf4d006c'),
(250, '09037ea5-1e44-4975-86c9-3c4df80c5871', 'c97654d2-d8a9-4f24-b60c-b231f54f412f'),
(293, '09156ff6-4636-4af6-b8b2-3e87696e62a4', 'd270b222-2ffe-448c-961d-c46b1cd03231'),
(355, '096df2ea-5c5a-4260-8bbc-cb4b8c7f6476', '0c2d0cb3-f60a-4f2f-a717-7ea1a61e14cf'),
(454, '09ca1e67-628c-4c61-8f65-cb29bc31b3e7', '5ee99a8b-8156-4102-bae8-aee2bf4d006c'),
(445, '09f1a8a5-e512-47c4-834a-4800791f698e', 'e93425d8-211d-4211-ac23-1397a6df1539'),
(348, '0a1baf28-1e03-4145-8f95-c460f63189c9', '0c2d0cb3-f60a-4f2f-a717-7ea1a61e14cf'),
(288, '0ac8af2c-5940-42b1-b680-575901484ce2', 'd270b222-2ffe-448c-961d-c46b1cd03231'),
(406, '0b6404a0-ede5-41fb-aa45-cacffaba0cad', 'f8c9bbe4-2610-4e27-a7e0-93c7eb522087'),
(466, '0c9f735c-3a65-4872-997f-c518a4ec9e8a', '5ee99a8b-8156-4102-bae8-aee2bf4d006c'),
(63, '0cfe2b89-ba4e-4058-834e-9e772479c2df', '26b00395-e9ed-4dde-9fbd-3766c044eb38'),
(32, '0d278d20-3398-4be7-beec-d4032512cd6a', 'e504d8d8-b516-4eec-8064-97a8256b9584'),
(73, '0dcd9aee-19b1-4dad-8720-f5b516992345', 'fb440671-4818-453e-813e-6d2f39f51047'),
(134, '0e6fc298-4679-40e0-a2ea-a3a6e52e5e27', 'd33e5c6b-0549-4186-b8a6-1f4059be4529'),
(177, '0e8e47f9-757f-443a-939f-ef3f6880506a', '9efb5ecd-a54d-41af-a381-6ba7753266d3'),
(442, '0fc12cf6-fa1e-4f47-a3e7-485e4d449473', 'e93425d8-211d-4211-ac23-1397a6df1539'),
(45, '1049723f-3350-4811-8a49-59d41a0eae5c', 'fd0a89b6-3fbb-4d3b-b518-622530c6fcb4'),
(100, '10904526-7705-4147-9dec-1159cc01e5f1', 'b80a1ad0-4154-4bcc-81a4-420aad7ed35a'),
(7, '10cebfcb-8260-46e0-a5db-7b46989e77b6', 'af8f5019-2385-4054-9eed-96c08079bd74'),
(118, '11bece5e-7ce4-474a-a922-66e3bfebfde8', '63b58b3b-f1ec-4e6f-b245-ebe49f135e7b'),
(312, '1232f64a-ec1c-45ea-8847-098f131751fc', '78c68093-5fa1-4c77-b13c-b52a008f90ee'),
(90, '12893555-d0e4-460e-ac29-f3107b7a5c26', 'b80a1ad0-4154-4bcc-81a4-420aad7ed35a'),
(339, '12ed38ac-43d4-4686-8a4b-01164203b7fe', 'b04805fc-08b3-43f5-bc15-4c174abd556d'),
(102, '131a3d21-afba-4b8d-9da0-f213233ef37a', 'b80a1ad0-4154-4bcc-81a4-420aad7ed35a'),
(294, '135c9e69-92fa-41d5-9872-ad55cf83ee3e', 'd270b222-2ffe-448c-961d-c46b1cd03231'),
(78, '136f144d-7d61-40b5-be63-7db3d7de3441', 'fb440671-4818-453e-813e-6d2f39f51047'),
(415, '142931b9-f8f0-43a3-8b55-da02c7817773', 'f8c9bbe4-2610-4e27-a7e0-93c7eb522087'),
(244, '147ce665-be5d-4510-bee0-74aac239ba9e', 'c97654d2-d8a9-4f24-b60c-b231f54f412f'),
(280, '14b7ce6a-55ba-4655-8db8-0645cb85df7a', '9d3e5fec-ac71-43d4-a466-a3fb590df6e0'),
(4, '159788a5-31a7-4462-9fc3-1e5b7bc07a94', 'af8f5019-2385-4054-9eed-96c08079bd74'),
(271, '15c6b7e9-9f00-484f-a8f8-1b9c45b5a7fa', 'c179a270-ce8a-4d3c-9e80-e2f07056dda0'),
(19, '15dc85f0-bc72-4a80-b56d-845c50ec54d0', '497d4414-c05f-4e70-a4b9-60f4e5fe6f76'),
(222, '16421925-20d4-4193-b9f6-6a626a6a938c', '4c3774a5-fe81-4b99-a183-d6d0a472b98d'),
(471, '1723b332-f6b3-4e2e-a372-2284bc5c05ab', '5ee99a8b-8156-4102-bae8-aee2bf4d006c'),
(25, '179a0d59-bea6-479e-9484-f836b5d698dd', '497d4414-c05f-4e70-a4b9-60f4e5fe6f76'),
(429, '18680c1e-eefc-41da-9fd4-ff7212238d1e', '52f53776-d370-4f6d-94e3-56b1a5b349e0'),
(50, '18f2cf3d-9389-42af-a5fa-688c893b3c3a', 'fd0a89b6-3fbb-4d3b-b518-622530c6fcb4'),
(268, '1931607d-cb2b-4ed1-9ff5-c310a84e726b', '79e7509e-ef2f-4e5e-8b80-dd673d62bb66'),
(151, '19aaeee9-bdb4-45fc-948c-8b0255fc4654', '7866f70b-2ca0-43d2-a24d-d469dda21a08'),
(91, '1adb0f09-d4a3-4481-bc7d-0aab64c30cff', 'b80a1ad0-4154-4bcc-81a4-420aad7ed35a'),
(160, '1b10a266-5c5f-45fa-8e6a-b75c4ae29133', '7866f70b-2ca0-43d2-a24d-d469dda21a08'),
(321, '1cfadfaa-176f-4b47-951b-16ea208e79d4', 'fc1adafa-51a8-4bb4-9d8a-0261eb556168'),
(239, '1d55e69b-7969-4d50-9460-46cd50b0485c', 'c97654d2-d8a9-4f24-b60c-b231f54f412f'),
(455, '1d600dfa-deeb-4ea1-b9b0-a1f7c0eb2e9e', '5ee99a8b-8156-4102-bae8-aee2bf4d006c'),
(49, '1dff0fb9-7bb7-4165-ab05-cf2283f65c2a', 'fd0a89b6-3fbb-4d3b-b518-622530c6fcb4'),
(198, '1e1b5066-9aff-4bc8-8eca-c96176481d89', '71fee567-679c-4704-bee3-613c8cb0c2f5'),
(56, '1e2ebcd1-5cf3-4d35-869c-38d3fb4bab03', '26b00395-e9ed-4dde-9fbd-3766c044eb38'),
(368, '1f412e6b-1ac7-449c-a4c5-08a37abc5254', 'dba5eb21-2947-4361-9405-82f81ec9453a'),
(446, '202498cc-28ca-475d-8e6f-7067fc86dd01', 'e93425d8-211d-4211-ac23-1397a6df1539'),
(178, '205ad39d-95b4-4740-a901-cc9dd5a26f97', '9efb5ecd-a54d-41af-a381-6ba7753266d3'),
(24, '2173624e-e00c-4eb3-8617-ac24afaa825f', '497d4414-c05f-4e70-a4b9-60f4e5fe6f76'),
(360, '219a478f-c08b-4ba7-b7c8-f6f3cc93ca20', '0c2d0cb3-f60a-4f2f-a717-7ea1a61e14cf'),
(139, '21bdb84e-b5c4-4e70-bc22-9642f0be9e4a', '5c2401c5-8332-478d-9f74-021e37af8f77'),
(89, '2205c249-a532-480e-adc3-ddbad4d1aa93', 'b80a1ad0-4154-4bcc-81a4-420aad7ed35a'),
(35, '22aa0939-f5cd-4f7e-baa5-62d2417ea711', 'e504d8d8-b516-4eec-8064-97a8256b9584'),
(188, '231a4473-9993-45a5-8459-b71dc4342739', '71fee567-679c-4704-bee3-613c8cb0c2f5'),
(337, '234fbce2-bebb-47e9-835f-f1783c990a3c', 'b04805fc-08b3-43f5-bc15-4c174abd556d'),
(384, '2393916f-b214-4c0e-83ef-eb0034354f7b', '43fd5c6a-1252-46b4-9c8e-fee43010253e'),
(46, '23cbbb32-8725-4708-b17a-4567654b958e', 'fd0a89b6-3fbb-4d3b-b518-622530c6fcb4'),
(310, '240693dc-d3ba-4f82-a440-db152f5e508f', '78c68093-5fa1-4c77-b13c-b52a008f90ee'),
(17, '24918612-3f9c-4c38-b108-a5bc1f967076', '497d4414-c05f-4e70-a4b9-60f4e5fe6f76'),
(191, '253f680b-fa88-4f96-b8ec-b83b70e4db56', '71fee567-679c-4704-bee3-613c8cb0c2f5'),
(350, '27916a3f-fce6-4c80-bc08-319203cba468', '0c2d0cb3-f60a-4f2f-a717-7ea1a61e14cf'),
(314, '27e73bf0-0770-4104-9a14-20cfde969e46', '78c68093-5fa1-4c77-b13c-b52a008f90ee'),
(130, '281c0db8-6a8c-4b85-bf20-be42e86f080d', 'd33e5c6b-0549-4186-b8a6-1f4059be4529'),
(209, '2976ae1a-0332-46b4-990e-396b9769fbf8', '72d8ebdb-dd6b-4dc1-9391-7d2d60a6d422'),
(210, '2989636a-aa8a-4746-88ac-5800c7a3057c', '72d8ebdb-dd6b-4dc1-9391-7d2d60a6d422'),
(283, '2a40f404-0d5d-4860-a412-258a1cedd661', '9d3e5fec-ac71-43d4-a466-a3fb590df6e0'),
(143, '2ca2fe7d-1ca3-421a-bbe4-c87bfab1f642', '5c2401c5-8332-478d-9f74-021e37af8f77'),
(440, '2d29662a-e6a6-4d9b-afb6-0de4b9e157e7', 'e93425d8-211d-4211-ac23-1397a6df1539'),
(158, '2d8e84b2-5ee7-4f73-99b8-390afd7024fc', '7866f70b-2ca0-43d2-a24d-d469dda21a08'),
(161, '2dd585c0-b235-4bec-ab92-b32a206d8fe4', '7866f70b-2ca0-43d2-a24d-d469dda21a08'),
(153, '2dfd7899-dddf-4110-926e-ee8a2f4e9982', '7866f70b-2ca0-43d2-a24d-d469dda21a08'),
(426, '2e99434a-52f6-42de-a0d7-772fcbf91898', '52f53776-d370-4f6d-94e3-56b1a5b349e0'),
(450, '2ebaaa9e-0129-4a5a-8035-63e65348b74a', 'e93425d8-211d-4211-ac23-1397a6df1539'),
(318, '2f40aaa2-fa91-42d0-aea0-f40baa588241', 'fc1adafa-51a8-4bb4-9d8a-0261eb556168'),
(234, '2f5cd6b1-27aa-40dd-8c64-aa72391d560b', 'c97654d2-d8a9-4f24-b60c-b231f54f412f'),
(114, '2f990909-7418-416f-a802-40df2fda184c', '63b58b3b-f1ec-4e6f-b245-ebe49f135e7b'),
(327, '2fd4d7c7-79d6-4f38-b5ef-260d37a45cad', 'fc1adafa-51a8-4bb4-9d8a-0261eb556168'),
(397, '3028c81d-cbac-4572-9662-78f7f321185b', '43fd5c6a-1252-46b4-9c8e-fee43010253e'),
(148, '30f6d56e-f880-4e00-9849-6b12bc4cc57d', '5c2401c5-8332-478d-9f74-021e37af8f77'),
(353, '31424a93-120a-4784-a241-44a534f2d594', '0c2d0cb3-f60a-4f2f-a717-7ea1a61e14cf'),
(92, '315171e8-5c07-4aa3-8b4c-301060032db0', 'b80a1ad0-4154-4bcc-81a4-420aad7ed35a'),
(236, '31b44b90-0811-4422-a130-dd68581c875b', 'c97654d2-d8a9-4f24-b60c-b231f54f412f'),
(332, '31b833a9-ad51-45f7-8aab-0ea572b402c4', 'b04805fc-08b3-43f5-bc15-4c174abd556d'),
(329, '31ce4022-3735-40f7-b793-22737e05a25f', 'b04805fc-08b3-43f5-bc15-4c174abd556d'),
(185, '324a220d-dae4-47fc-b37a-0059c6c12bd4', '71fee567-679c-4704-bee3-613c8cb0c2f5'),
(133, '330d49d2-a675-4418-b740-feaa3c30ccc5', 'd33e5c6b-0549-4186-b8a6-1f4059be4529'),
(335, '333d6783-70c2-45dd-83f4-4261d20bee75', 'b04805fc-08b3-43f5-bc15-4c174abd556d'),
(238, '334689b6-5e47-4649-af43-2dc85c0a94a0', 'c97654d2-d8a9-4f24-b60c-b231f54f412f'),
(127, '33addffc-798e-45f7-a0db-5a9edd408950', 'd33e5c6b-0549-4186-b8a6-1f4059be4529'),
(296, '33eb65ce-5c97-4651-a533-7e9d21430705', 'c5ab5265-8a8d-43fd-a282-52f9dd19f91b'),
(137, '3436fde4-5d36-4bb1-8bb6-9263cced2ef7', '5c2401c5-8332-478d-9f74-021e37af8f77'),
(313, '347e7992-f570-48df-be19-d9903dd62a0d', '78c68093-5fa1-4c77-b13c-b52a008f90ee'),
(20, '34b82a7c-9b82-4574-9157-ed997c60ac34', '497d4414-c05f-4e70-a4b9-60f4e5fe6f76'),
(458, '354fdf79-d5b1-48f8-9bff-4fc117e409d1', '5ee99a8b-8156-4102-bae8-aee2bf4d006c'),
(308, '36f5846b-b619-4f4f-9435-1800d2babe5d', '78c68093-5fa1-4c77-b13c-b52a008f90ee'),
(77, '37e0d875-65ec-4dc7-8aef-b7b839bddf3c', 'fb440671-4818-453e-813e-6d2f39f51047'),
(432, '3837dc0d-eb04-45f5-8515-f8ecd638d1c5', '52f53776-d370-4f6d-94e3-56b1a5b349e0'),
(1, '38c3115c-4b69-4b8e-85ef-07f67b80a7bf', 'af8f5019-2385-4054-9eed-96c08079bd74'),
(132, '38cf18b3-0255-4d48-aab2-12d05ff7799c', 'd33e5c6b-0549-4186-b8a6-1f4059be4529'),
(343, '38f35532-4ea6-4471-9bbb-12d79c0cfb5c', 'b04805fc-08b3-43f5-bc15-4c174abd556d'),
(33, '39007af3-b359-486f-9b12-a8a1a8f6b7a5', 'e504d8d8-b516-4eec-8064-97a8256b9584'),
(398, '39187cf4-a96d-4abf-8ef8-4e8677413e7f', '43fd5c6a-1252-46b4-9c8e-fee43010253e'),
(159, '39766d93-571e-4ac8-937c-853d528c6660', '7866f70b-2ca0-43d2-a24d-d469dda21a08'),
(140, '39fa9e39-d383-4d43-b9e2-8f9e6f2a0821', '5c2401c5-8332-478d-9f74-021e37af8f77'),
(192, '3a32d798-d4a8-4b2f-88b9-b80a50d19cf5', '71fee567-679c-4704-bee3-613c8cb0c2f5'),
(278, '3a8a6d1f-bdc1-42f9-ac32-15ffc9256415', '9d3e5fec-ac71-43d4-a466-a3fb590df6e0'),
(290, '3af773f6-e811-4a00-bb14-08193d3eef40', 'd270b222-2ffe-448c-961d-c46b1cd03231'),
(103, '3b04cb00-450b-43a2-8bc1-e51b55c1c2fb', '63b58b3b-f1ec-4e6f-b245-ebe49f135e7b'),
(315, '3c86f166-11d3-4b92-92ba-1bf2834a2e3b', '78c68093-5fa1-4c77-b13c-b52a008f90ee'),
(229, '3ce50848-8730-48ee-8c40-383e5fa5d870', '4c3774a5-fe81-4b99-a183-d6d0a472b98d'),
(121, '3d311deb-4a90-4406-8001-666c82e0a9f8', 'd33e5c6b-0549-4186-b8a6-1f4059be4529'),
(364, '3d8af70f-e219-4309-83e9-f7e5fd0eed66', 'dba5eb21-2947-4361-9405-82f81ec9453a'),
(266, '3ede55ed-a2b5-40f6-bd39-23f666e77a95', 'a78e8715-96c2-416a-833c-854f6c8d1972'),
(468, '3eeb494d-ad49-4363-a8dd-311af235f710', '5ee99a8b-8156-4102-bae8-aee2bf4d006c'),
(88, '3f10b6e6-1d4f-4c09-b0fb-d6d8aa75d94a', 'b80a1ad0-4154-4bcc-81a4-420aad7ed35a'),
(47, '3fc013d7-4041-4812-a97c-5aef494643cf', 'fd0a89b6-3fbb-4d3b-b518-622530c6fcb4'),
(101, '40871b6f-f29d-4a80-aa6f-51cc9ba85150', 'b80a1ad0-4154-4bcc-81a4-420aad7ed35a'),
(27, '40c9890d-c5b2-4d11-b6e0-7cd00aeefb20', 'e504d8d8-b516-4eec-8064-97a8256b9584'),
(202, '4121f146-6ef6-45f0-92ab-72a2287ccfec', '72d8ebdb-dd6b-4dc1-9391-7d2d60a6d422'),
(197, '4130a6e3-30db-40d2-874d-f6fc5b150729', '71fee567-679c-4704-bee3-613c8cb0c2f5'),
(58, '413b2bc4-3199-4612-843a-37c49660f4a0', '26b00395-e9ed-4dde-9fbd-3766c044eb38'),
(307, '41d0868c-e058-41bf-97c2-57be57ea7f6a', '78c68093-5fa1-4c77-b13c-b52a008f90ee'),
(306, '429dc6ec-440e-4b0b-974c-4a98090c95cb', '78c68093-5fa1-4c77-b13c-b52a008f90ee'),
(461, '42a35b9a-326a-438c-ac04-0e860e4c0ef0', '5ee99a8b-8156-4102-bae8-aee2bf4d006c'),
(399, '42a56f76-fd94-44d2-a63e-7df63bc031ff', '43fd5c6a-1252-46b4-9c8e-fee43010253e'),
(207, '42d6f71f-de66-474d-9fe0-dd0ad2e3cb3f', '72d8ebdb-dd6b-4dc1-9391-7d2d60a6d422'),
(413, '42e64395-a740-4bb1-9803-edbfb147d109', 'f8c9bbe4-2610-4e27-a7e0-93c7eb522087'),
(325, '431cc56d-7c37-431d-b44c-09c7e5469316', 'fc1adafa-51a8-4bb4-9d8a-0261eb556168'),
(365, '432a980e-4bc2-4368-acce-f2f1ae271fbe', 'dba5eb21-2947-4361-9405-82f81ec9453a'),
(36, '434c25d8-760e-4991-bdd4-756a6fa35720', 'e504d8d8-b516-4eec-8064-97a8256b9584'),
(421, '43524e37-8040-4bc7-9f24-3182257a9f3a', '52f53776-d370-4f6d-94e3-56b1a5b349e0'),
(344, '441e2208-8b34-4b6d-85d8-52b6a9f62bd1', 'b04805fc-08b3-43f5-bc15-4c174abd556d'),
(435, '44a05a54-2f65-4b31-a58c-a79be4defa4f', '52f53776-d370-4f6d-94e3-56b1a5b349e0'),
(163, '454bc763-8be4-436d-8299-ead1a56657f7', '7866f70b-2ca0-43d2-a24d-d469dda21a08'),
(331, '45abcad3-511b-457f-9f32-821cbff4147b', 'b04805fc-08b3-43f5-bc15-4c174abd556d'),
(470, '4674d33c-04a9-40c7-9426-cf30861d0aee', '5ee99a8b-8156-4102-bae8-aee2bf4d006c'),
(195, '46c529f1-4350-43e6-9bbe-65eb1e39c70d', '71fee567-679c-4704-bee3-613c8cb0c2f5'),
(381, '474b8c3e-66f6-4b12-8a00-7d8aa4e6bd5f', 'dba5eb21-2947-4361-9405-82f81ec9453a'),
(42, '475446aa-4363-4b24-89d7-10abe422eb24', 'fd0a89b6-3fbb-4d3b-b518-622530c6fcb4'),
(124, '47b9de31-718a-4212-b32d-7f3f94296ab6', 'd33e5c6b-0549-4186-b8a6-1f4059be4529'),
(378, '47bfd1aa-27cb-4cfe-b079-6185c5a0a3c1', 'dba5eb21-2947-4361-9405-82f81ec9453a'),
(15, '481e5bb5-500d-45a7-aaf6-b7a085f92c3f', '497d4414-c05f-4e70-a4b9-60f4e5fe6f76'),
(167, '490636de-dacb-437f-916b-c40495f1195a', '7866f70b-2ca0-43d2-a24d-d469dda21a08'),
(370, '4c05fc6b-8d03-4933-9769-d8b742db2ffe', 'dba5eb21-2947-4361-9405-82f81ec9453a'),
(150, '4c23917e-2251-4569-8985-729dc9629d72', '5c2401c5-8332-478d-9f74-021e37af8f77'),
(214, '4ca4925f-8112-4d0f-a07e-2fe04bae5e79', '72d8ebdb-dd6b-4dc1-9391-7d2d60a6d422'),
(377, '4d9f0e1f-94f5-479b-b73e-51433ded71b1', 'dba5eb21-2947-4361-9405-82f81ec9453a'),
(115, '4daa467c-2561-4896-b729-df1ecfc50ccf', '63b58b3b-f1ec-4e6f-b245-ebe49f135e7b'),
(183, '4e568a1a-5e7a-4d75-a8ca-758a933bb119', '71fee567-679c-4704-bee3-613c8cb0c2f5'),
(228, '4ea26c04-ef41-4e34-9509-36ad10dbd857', '4c3774a5-fe81-4b99-a183-d6d0a472b98d'),
(144, '4ee6da65-6442-490e-a2bc-89bedaf2dae8', '5c2401c5-8332-478d-9f74-021e37af8f77'),
(342, '4f2b9110-f395-4c33-bde8-fa1599a6d771', 'b04805fc-08b3-43f5-bc15-4c174abd556d'),
(412, '4f324811-422b-45d2-a171-171c3a28bccd', 'f8c9bbe4-2610-4e27-a7e0-93c7eb522087'),
(65, '5006f447-c37b-4f6d-8c58-cfd59385beb1', '26b00395-e9ed-4dde-9fbd-3766c044eb38'),
(18, '502edd55-2674-4590-8921-dd1c73e4a191', '497d4414-c05f-4e70-a4b9-60f4e5fe6f76'),
(334, '503f7519-902e-4f0c-a1e1-033ed6590071', 'b04805fc-08b3-43f5-bc15-4c174abd556d'),
(459, '508497fe-4ae5-4642-95e2-ed42e35350bd', '5ee99a8b-8156-4102-bae8-aee2bf4d006c'),
(231, '508e4ebb-c661-4542-90eb-0cec5cfcfa99', '4c3774a5-fe81-4b99-a183-d6d0a472b98d'),
(64, '5160710d-d1f5-4ccf-9ffd-31aa55aa06c2', '26b00395-e9ed-4dde-9fbd-3766c044eb38'),
(439, '51846291-eb47-4f9f-a3e2-12342374cc86', 'e93425d8-211d-4211-ac23-1397a6df1539'),
(443, '521f3e36-08b5-4425-ad5c-2ddf9adffc0c', 'e93425d8-211d-4211-ac23-1397a6df1539'),
(252, '53397628-a451-4035-8b39-0d6a8869a22c', 'a78e8715-96c2-416a-833c-854f6c8d1972'),
(333, '53f72249-6aa1-444f-adbf-59d1442921bc', 'b04805fc-08b3-43f5-bc15-4c174abd556d'),
(393, '55edbf35-4bb0-41ac-b09f-564982d7ab3e', '43fd5c6a-1252-46b4-9c8e-fee43010253e'),
(61, '565998a9-b1aa-4e59-ae99-7a57ee1eb3c7', '26b00395-e9ed-4dde-9fbd-3766c044eb38'),
(23, '565a22f4-d90a-40bf-a184-2a23285fa95f', '497d4414-c05f-4e70-a4b9-60f4e5fe6f76'),
(284, '567eeabe-0c15-4c4d-bcf3-e120f5c6c67b', 'd270b222-2ffe-448c-961d-c46b1cd03231'),
(372, '56ad480d-4f18-444a-9ade-a9ec6145f9e4', 'dba5eb21-2947-4361-9405-82f81ec9453a'),
(76, '579eb77e-20a9-47ef-9566-b214bb8640fd', 'fb440671-4818-453e-813e-6d2f39f51047'),
(340, '5a97731b-d3de-4d0d-ae91-aeed9901c671', 'b04805fc-08b3-43f5-bc15-4c174abd556d'),
(10, '5ae3d15c-1cba-417c-9648-0a517a17889f', 'af8f5019-2385-4054-9eed-96c08079bd74'),
(260, '5b9b89f6-bdfd-415a-9dd7-b49e4bb5819d', 'a78e8715-96c2-416a-833c-854f6c8d1972'),
(138, '5c7b58db-2d95-4dc1-aa11-86ce452c830b', '5c2401c5-8332-478d-9f74-021e37af8f77'),
(464, '5d1d8d2c-7a15-43b0-bdfc-54062ef77cd4', '5ee99a8b-8156-4102-bae8-aee2bf4d006c'),
(326, '5d7813d2-f737-4884-9d7c-17c3d395105b', 'fc1adafa-51a8-4bb4-9d8a-0261eb556168'),
(452, '5e255cd5-dde5-4c73-a57b-b71930488d91', 'e93425d8-211d-4211-ac23-1397a6df1539'),
(82, '5eb85940-9266-44e1-b41d-ab206e73b9b0', 'fb440671-4818-453e-813e-6d2f39f51047'),
(444, '6020c416-3315-489d-87e2-0dad54541865', 'e93425d8-211d-4211-ac23-1397a6df1539'),
(467, '602c3151-170d-41df-ad28-475ae7d8ae27', '5ee99a8b-8156-4102-bae8-aee2bf4d006c'),
(94, '60be3dc3-b3a8-4065-9838-c0eb3bfa02b3', 'b80a1ad0-4154-4bcc-81a4-420aad7ed35a'),
(201, '619bd265-e725-46d4-91c8-f956535cecd9', '72d8ebdb-dd6b-4dc1-9391-7d2d60a6d422'),
(8, '61cfe847-aacd-489a-8023-f5b2370395a6', 'af8f5019-2385-4054-9eed-96c08079bd74'),
(110, '62f9f259-0aac-4e20-bad4-217aa651b0a0', '63b58b3b-f1ec-4e6f-b245-ebe49f135e7b'),
(175, '63015bab-da51-408d-b7a3-e8e3b1489dad', 'a17d74a8-7e90-44bd-b3c2-644815a9c7cb'),
(223, '636d2d04-fc4b-473b-a38c-90dcb11a2091', '4c3774a5-fe81-4b99-a183-d6d0a472b98d'),
(375, '651258a2-ab09-4c80-8bd5-23ea8db9de80', 'dba5eb21-2947-4361-9405-82f81ec9453a'),
(414, '6548b7e3-d0a0-4841-b694-3a58119c354c', 'f8c9bbe4-2610-4e27-a7e0-93c7eb522087'),
(420, '65effeba-2b4c-400a-8421-a867cda03dd9', '52f53776-d370-4f6d-94e3-56b1a5b349e0'),
(11, '67282270-ba6a-44e4-a4c4-446cf0298508', 'af8f5019-2385-4054-9eed-96c08079bd74'),
(328, '67a85251-3343-4a0e-b21f-027763cceb44', 'b04805fc-08b3-43f5-bc15-4c174abd556d'),
(338, '6928c66e-d8b9-43ee-88f5-2f7477d797fd', 'b04805fc-08b3-43f5-bc15-4c174abd556d'),
(309, '6b5d9ccf-b333-4dd4-a9cc-92861fc51ad8', '78c68093-5fa1-4c77-b13c-b52a008f90ee'),
(291, '6bea0cfa-612a-453d-95b3-fc7b644f7707', 'd270b222-2ffe-448c-961d-c46b1cd03231'),
(366, '6cebe8a7-0f66-444f-94ef-5c62434e926a', 'dba5eb21-2947-4361-9405-82f81ec9453a'),
(297, '6cf076b1-a940-4f6b-91d3-2568fb0b7dd0', 'c5ab5265-8a8d-43fd-a282-52f9dd19f91b'),
(48, '6d010ebc-a36e-427d-8995-0b37f573831d', 'fd0a89b6-3fbb-4d3b-b518-622530c6fcb4'),
(279, '6dba872d-22a4-43f7-85b4-77e6ab748ee8', '9d3e5fec-ac71-43d4-a466-a3fb590df6e0'),
(383, '6e92accb-1413-4342-b6cd-a3b6ca8bbfa6', '43fd5c6a-1252-46b4-9c8e-fee43010253e'),
(205, '6eb51146-95bb-4059-b573-147fb0d09281', '72d8ebdb-dd6b-4dc1-9391-7d2d60a6d422'),
(388, '6f038dc5-6e0a-45b1-9e15-cbd018fa71d8', '43fd5c6a-1252-46b4-9c8e-fee43010253e'),
(108, '6fdbcfe8-97b3-4d52-a2ef-d2ea1ece5dba', '63b58b3b-f1ec-4e6f-b245-ebe49f135e7b'),
(39, '706dcf2a-2560-4625-a0dd-0c94234d71c9', 'e504d8d8-b516-4eec-8064-97a8256b9584'),
(402, '718bbbb0-6b45-46cc-9714-7d78ed3f47fb', 'f8c9bbe4-2610-4e27-a7e0-93c7eb522087'),
(417, '720f6ab5-9493-4ca3-b7c1-5cab873541c6', 'f8c9bbe4-2610-4e27-a7e0-93c7eb522087'),
(392, '7219d1c2-9740-4c55-aa98-d949e6c95713', '43fd5c6a-1252-46b4-9c8e-fee43010253e'),
(272, '7238ceaf-0d30-4bd7-82d8-173c96aacf07', 'c179a270-ce8a-4d3c-9e80-e2f07056dda0'),
(86, '72c7c20e-aeb6-43ab-a9ce-73fff38978b8', 'fb440671-4818-453e-813e-6d2f39f51047'),
(305, '736f505f-5aa3-4d54-bf8c-db73f5f06d15', 'c5ab5265-8a8d-43fd-a282-52f9dd19f91b'),
(62, '73a8e8af-e4ef-4565-8699-80aad7ac973d', '26b00395-e9ed-4dde-9fbd-3766c044eb38'),
(261, '7413eda9-7a4c-4157-bd71-6f8c792722c3', 'a78e8715-96c2-416a-833c-854f6c8d1972'),
(112, '74776964-282e-40a0-82ee-1f7461b211ad', '63b58b3b-f1ec-4e6f-b245-ebe49f135e7b'),
(240, '74b526ff-bd5f-4c9e-95a7-342843891b80', 'c97654d2-d8a9-4f24-b60c-b231f54f412f'),
(97, '74b5e41e-8960-4e14-82a8-e0eac7176e9e', 'b80a1ad0-4154-4bcc-81a4-420aad7ed35a'),
(347, '7512d0ae-599a-4587-a226-c99605f6002b', '0c2d0cb3-f60a-4f2f-a717-7ea1a61e14cf'),
(180, '758c5cb7-c4ef-4913-a1ff-eaa09436c802', '9efb5ecd-a54d-41af-a381-6ba7753266d3'),
(107, '75ca7e7c-4d0f-425c-a820-6df6c4d1dad5', '63b58b3b-f1ec-4e6f-b245-ebe49f135e7b'),
(243, '763ac566-88c6-4c0c-ae80-a01f9585618b', 'c97654d2-d8a9-4f24-b60c-b231f54f412f'),
(463, '7679e628-2cf8-4e2e-a183-2a464f1357ae', '5ee99a8b-8156-4102-bae8-aee2bf4d006c'),
(396, '78057970-64bc-4503-adb3-22c23db8cfb9', '43fd5c6a-1252-46b4-9c8e-fee43010253e'),
(26, '784cc49a-c16a-41d8-a7b1-cea960a5d8a7', '497d4414-c05f-4e70-a4b9-60f4e5fe6f76'),
(66, '7910d299-1bf1-4aaf-beee-f6106f37945a', 'fb440671-4818-453e-813e-6d2f39f51047'),
(176, '795ce132-2dc5-4a49-a672-6ac98061351c', 'a17d74a8-7e90-44bd-b3c2-644815a9c7cb'),
(14, '7a10644d-3fbd-4fdf-acd8-84b03240d9f6', '497d4414-c05f-4e70-a4b9-60f4e5fe6f76'),
(438, '7a8a7441-ee2a-4607-82f5-bda97d2783f3', 'e93425d8-211d-4211-ac23-1397a6df1539'),
(41, '7ca117f0-b092-4c49-a95e-f6841df11e3f', 'fd0a89b6-3fbb-4d3b-b518-622530c6fcb4'),
(289, '7ca4854f-3512-41c0-b08a-86178294de7c', 'd270b222-2ffe-448c-961d-c46b1cd03231'),
(262, '7cd28897-3982-4c83-b34e-2ad29410f484', 'a78e8715-96c2-416a-833c-854f6c8d1972'),
(226, '7d785503-d768-4bc7-ac1e-780f889642d2', '4c3774a5-fe81-4b99-a183-d6d0a472b98d'),
(317, '7dc58ff4-cb07-45a2-b645-88ba83bbd54b', 'fc1adafa-51a8-4bb4-9d8a-0261eb556168'),
(204, '7dcefdb6-7db2-4144-af03-246cac096f8d', '72d8ebdb-dd6b-4dc1-9391-7d2d60a6d422'),
(109, '7ddd2ba1-4353-4853-8d22-0f8357e04940', '63b58b3b-f1ec-4e6f-b245-ebe49f135e7b'),
(462, '7ef8b08f-2c1d-4ae3-aafc-a99b14e2262a', '5ee99a8b-8156-4102-bae8-aee2bf4d006c'),
(217, '7f8c0f1b-e98c-4535-8b26-9c75400583e0', '4c3774a5-fe81-4b99-a183-d6d0a472b98d'),
(275, '7f91dd6b-ff4f-4459-92a9-baa2adc7689c', '9d3e5fec-ac71-43d4-a466-a3fb590df6e0'),
(119, '80271f75-4987-4c41-90ee-56b722b353e1', 'd33e5c6b-0549-4186-b8a6-1f4059be4529'),
(126, '802d84c3-0108-413f-9838-643f1001a336', 'd33e5c6b-0549-4186-b8a6-1f4059be4529'),
(286, '811c7598-19e6-4b50-ba5f-9653250d91a6', 'd270b222-2ffe-448c-961d-c46b1cd03231'),
(418, '81ef7359-1a78-4dcd-9741-3b58d96f21d0', '52f53776-d370-4f6d-94e3-56b1a5b349e0'),
(373, '83695cd3-ccae-42c7-a70f-ae50806a43c5', 'dba5eb21-2947-4361-9405-82f81ec9453a'),
(441, '83af3810-a51e-46d1-85f0-4dfa5226b591', 'e93425d8-211d-4211-ac23-1397a6df1539'),
(128, '83cfdedf-f0fd-4f85-9b4a-b5f49c4970f7', 'd33e5c6b-0549-4186-b8a6-1f4059be4529'),
(449, '83e983f1-d2e6-4d54-b945-3efcf3f1d8d9', 'e93425d8-211d-4211-ac23-1397a6df1539'),
(433, '843574b6-372f-4a2f-9ec9-ce9952fae9ec', '52f53776-d370-4f6d-94e3-56b1a5b349e0'),
(154, '848a7069-4b11-4d66-9f2c-8cfccc0c76d3', '7866f70b-2ca0-43d2-a24d-d469dda21a08'),
(242, '84a18075-d20b-4095-b14d-23f2f0e40edc', 'c97654d2-d8a9-4f24-b60c-b231f54f412f'),
(404, '84c04f70-7140-4ca1-ab4f-dae01bcb4833', 'f8c9bbe4-2610-4e27-a7e0-93c7eb522087'),
(256, '852721b0-e1a4-485c-a64b-18faf1559386', 'a78e8715-96c2-416a-833c-854f6c8d1972'),
(187, '862a2073-c542-472e-97ee-0543cb3f69df', '71fee567-679c-4704-bee3-613c8cb0c2f5'),
(410, '8699bf3a-af87-4947-92f9-f1eb4a60341b', 'f8c9bbe4-2610-4e27-a7e0-93c7eb522087'),
(447, '86a4f96c-24d5-4a63-8aa3-862c4a47b72e', 'e93425d8-211d-4211-ac23-1397a6df1539'),
(391, '86a72dca-0a8c-4706-a674-d5997a421b89', '43fd5c6a-1252-46b4-9c8e-fee43010253e'),
(295, '88d0d639-d746-4cee-a008-c825f75598e2', 'c5ab5265-8a8d-43fd-a282-52f9dd19f91b'),
(265, '8901214e-4b7a-4f5f-9881-f9852c090608', 'a78e8715-96c2-416a-833c-854f6c8d1972'),
(259, '89124656-538c-486a-a426-719b3549163d', 'a78e8715-96c2-416a-833c-854f6c8d1972'),
(106, '89663cb8-b765-4bb9-97aa-eb7c6b914b4f', '63b58b3b-f1ec-4e6f-b245-ebe49f135e7b'),
(96, '89af007f-b2f2-42df-bae4-c477a5a191ab', 'b80a1ad0-4154-4bcc-81a4-420aad7ed35a'),
(453, '89e11e49-8eac-4dc7-b47c-b8ddf47648d0', 'e93425d8-211d-4211-ac23-1397a6df1539'),
(411, '8a0f3a2d-e350-49ca-a0c0-1dee3a2c8de4', 'f8c9bbe4-2610-4e27-a7e0-93c7eb522087'),
(21, '8b8e6ff4-ce78-4b26-9524-4f4a498637a3', '497d4414-c05f-4e70-a4b9-60f4e5fe6f76'),
(386, '8c0631ad-1699-4691-b5f1-5dce44652aa3', '43fd5c6a-1252-46b4-9c8e-fee43010253e'),
(123, '8c469340-8066-4cc6-93f8-8d232d82ad48', 'd33e5c6b-0549-4186-b8a6-1f4059be4529'),
(451, '8cd7c693-82c2-48f4-8bcd-d780c9515cbb', 'e93425d8-211d-4211-ac23-1397a6df1539'),
(53, '8dc3aeac-456e-4860-bf96-fe48536a925f', '26b00395-e9ed-4dde-9fbd-3766c044eb38'),
(401, '8e8743ff-64ec-421e-97a2-09184dc8c95f', 'f8c9bbe4-2610-4e27-a7e0-93c7eb522087'),
(253, '8ed32ccf-0578-46cb-ac15-a6a667d3ed6e', 'a78e8715-96c2-416a-833c-854f6c8d1972'),
(125, '8f963dce-e216-439b-a9f7-d995dfefc36d', 'd33e5c6b-0549-4186-b8a6-1f4059be4529'),
(249, '8f9e32b7-cb43-4430-bcb5-4ae2c1bfe4c4', 'c97654d2-d8a9-4f24-b60c-b231f54f412f'),
(215, '9000cd2c-a6d7-4c0c-946a-81ac80bd0cc0', '72d8ebdb-dd6b-4dc1-9391-7d2d60a6d422'),
(428, '92d3e99f-c651-45e2-9ed8-f9b100d75d90', '52f53776-d370-4f6d-94e3-56b1a5b349e0'),
(189, '939a3e42-8ebf-40a7-96fe-e0a23bf4a9e4', '71fee567-679c-4704-bee3-613c8cb0c2f5'),
(54, '944a433d-c26a-4015-845d-f5d570ba3ced', '26b00395-e9ed-4dde-9fbd-3766c044eb38'),
(81, '953faf4e-b770-42c1-8891-6ed55d3875aa', 'fb440671-4818-453e-813e-6d2f39f51047'),
(390, '969728be-5072-4498-8196-7d3f35c622b7', '43fd5c6a-1252-46b4-9c8e-fee43010253e'),
(164, '97777d0b-0d1a-40f0-87a6-bac2855ab021', '7866f70b-2ca0-43d2-a24d-d469dda21a08'),
(319, '99066103-e460-415d-a2de-22e5bb0b2a27', 'fc1adafa-51a8-4bb4-9d8a-0261eb556168'),
(324, '99c60509-fe3d-4aa7-9dc9-7aa68b34a4e4', 'fc1adafa-51a8-4bb4-9d8a-0261eb556168'),
(285, '9a3ef8a9-9cbc-4682-8b99-21507f65d12c', 'd270b222-2ffe-448c-961d-c46b1cd03231'),
(67, '9a6381a4-5c37-4ceb-90ba-6a074d1034d1', 'fb440671-4818-453e-813e-6d2f39f51047'),
(146, '9aa34e95-5850-4e71-8daa-b6c7a81f92b3', '5c2401c5-8332-478d-9f74-021e37af8f77'),
(80, '9abfb776-d2cd-42d2-9562-bfc66ff7ad0f', 'fb440671-4818-453e-813e-6d2f39f51047'),
(172, '9b028b52-baa0-4f3c-a4b0-ad69a03e8a97', '7383aca9-2deb-4f4d-a371-d71cf99cb0fe'),
(71, '9b4f3e7f-eafe-47bc-8f45-04fcc0bec99f', 'fb440671-4818-453e-813e-6d2f39f51047'),
(227, '9ca54490-91c2-40a8-8fec-1857cba2327b', '4c3774a5-fe81-4b99-a183-d6d0a472b98d'),
(194, '9ce17758-4c3d-491b-b776-b3075e3b8d37', '71fee567-679c-4704-bee3-613c8cb0c2f5'),
(156, '9d0f9063-50ba-4205-a5e5-fa0a1eb865c5', '7866f70b-2ca0-43d2-a24d-d469dda21a08'),
(190, '9db9907b-d1bd-405e-9eca-de802a672de0', '71fee567-679c-4704-bee3-613c8cb0c2f5'),
(267, '9e2b0300-154e-49e5-8923-9f8c5de0f2ad', 'a78e8715-96c2-416a-833c-854f6c8d1972'),
(60, '9e4b87fc-ddb5-421c-a661-ea9de5064cef', '26b00395-e9ed-4dde-9fbd-3766c044eb38'),
(22, '9effdd30-fbfc-4b1a-bce7-36bbe7b93e9f', '497d4414-c05f-4e70-a4b9-60f4e5fe6f76'),
(246, '9f43e33a-fcec-4edb-9762-6c8ba2908f83', 'c97654d2-d8a9-4f24-b60c-b231f54f412f'),
(131, 'a0aed580-13aa-403e-b085-18afd78da030', 'd33e5c6b-0549-4186-b8a6-1f4059be4529'),
(400, 'a0bee9f9-216e-4e1f-9969-45436f435e7b', 'f8c9bbe4-2610-4e27-a7e0-93c7eb522087'),
(354, 'a0cbf3ef-5571-46d0-9d95-1bb10a109426', '0c2d0cb3-f60a-4f2f-a717-7ea1a61e14cf'),
(369, 'a2047ed2-ad49-47e8-a081-92ffdaa09237', 'dba5eb21-2947-4361-9405-82f81ec9453a'),
(456, 'a324eb2b-4ef4-4c89-9e05-a74dd2633437', '5ee99a8b-8156-4102-bae8-aee2bf4d006c'),
(200, 'a5736269-8cc9-48fd-a956-dfd198915ea8', '72d8ebdb-dd6b-4dc1-9391-7d2d60a6d422'),
(211, 'a5c99f0f-09ba-4465-84a8-a5b25f7a49e9', '72d8ebdb-dd6b-4dc1-9391-7d2d60a6d422'),
(255, 'a60598be-3137-4849-8f8e-8c959a08a996', 'a78e8715-96c2-416a-833c-854f6c8d1972'),
(277, 'a612f902-254b-4eb4-b42c-50eee45fbeeb', '9d3e5fec-ac71-43d4-a466-a3fb590df6e0'),
(349, 'a65daf91-d7aa-4684-b6fd-1e8bf85eae23', '0c2d0cb3-f60a-4f2f-a717-7ea1a61e14cf'),
(430, 'a676fc01-077f-4dd7-a58f-15e8eb57bbc0', '52f53776-d370-4f6d-94e3-56b1a5b349e0'),
(469, 'a67b0ad8-cf6e-4c88-8636-c683f060b1cf', '5ee99a8b-8156-4102-bae8-aee2bf4d006c'),
(282, 'a757d5ab-b4cf-4f90-91f7-c53cf9a1a6d7', '9d3e5fec-ac71-43d4-a466-a3fb590df6e0'),
(300, 'a7f9ce81-eedd-4c55-89b4-e0c4ba8d0ff7', 'c5ab5265-8a8d-43fd-a282-52f9dd19f91b'),
(341, 'a8ff6993-737b-42ee-a412-88dd7b022c0f', 'b04805fc-08b3-43f5-bc15-4c174abd556d'),
(304, 'a9b30f92-1d0f-4fa6-af1b-ca99100df5a8', 'c5ab5265-8a8d-43fd-a282-52f9dd19f91b'),
(273, 'aaf6d8c1-6317-49b1-85cf-490fe9a8ad0c', '9d3e5fec-ac71-43d4-a466-a3fb590df6e0'),
(44, 'abf4f4d3-335d-4cda-a306-b34011ea6b4a', 'fd0a89b6-3fbb-4d3b-b518-622530c6fcb4'),
(346, 'acbcb5db-64f1-41ec-8527-d4f9817cab5c', '0c2d0cb3-f60a-4f2f-a717-7ea1a61e14cf'),
(419, 'adf24d54-dbad-4dfe-bccc-d38cc5538611', '52f53776-d370-4f6d-94e3-56b1a5b349e0'),
(135, 'aec1cf18-2e66-404d-8bda-ade2d4921d0f', '5c2401c5-8332-478d-9f74-021e37af8f77'),
(422, 'af242704-3971-45bb-b107-bf6488e58f63', '52f53776-d370-4f6d-94e3-56b1a5b349e0'),
(199, 'af835a66-b4c5-4624-9cd3-59465378c1f6', '72d8ebdb-dd6b-4dc1-9391-7d2d60a6d422'),
(254, 'b010f856-37a6-41dc-8e5d-71435141af0e', 'a78e8715-96c2-416a-833c-854f6c8d1972'),
(336, 'b0eb5da3-7713-4a51-b401-7a00ca5bd9e6', 'b04805fc-08b3-43f5-bc15-4c174abd556d'),
(357, 'b16999a7-2fa3-4160-bef1-4e41added0da', '0c2d0cb3-f60a-4f2f-a717-7ea1a61e14cf'),
(367, 'b1a4635c-2c56-44c3-95e3-a73747ff419b', 'dba5eb21-2947-4361-9405-82f81ec9453a'),
(233, 'b1fe5a9d-974c-41f4-bd3b-86dd4572bdaa', '4c3774a5-fe81-4b99-a183-d6d0a472b98d'),
(152, 'b2c9685b-48cb-4597-be26-7469f437902c', '7866f70b-2ca0-43d2-a24d-d469dda21a08'),
(95, 'b2f99473-15fa-43ed-bc8c-4de65c3a872d', 'b80a1ad0-4154-4bcc-81a4-420aad7ed35a'),
(55, 'b305c279-dbf5-454f-a80a-49feb097022c', '26b00395-e9ed-4dde-9fbd-3766c044eb38'),
(363, 'b30cec6e-9244-4ca5-b33d-d65963fd6362', '0c2d0cb3-f60a-4f2f-a717-7ea1a61e14cf'),
(425, 'b33966f9-4dd5-4f10-9d78-c1160082aa20', '52f53776-d370-4f6d-94e3-56b1a5b349e0'),
(52, 'b372db31-3871-458a-9acc-d1630b191285', 'fd0a89b6-3fbb-4d3b-b518-622530c6fcb4'),
(351, 'b44508a9-99c0-44e0-9636-8704cc9fe2ec', '0c2d0cb3-f60a-4f2f-a717-7ea1a61e14cf'),
(359, 'b458f155-65b8-4948-8946-e7af10f20cab', '0c2d0cb3-f60a-4f2f-a717-7ea1a61e14cf'),
(299, 'b5773763-9634-43b8-a7cb-b866602bcffe', 'c5ab5265-8a8d-43fd-a282-52f9dd19f91b'),
(248, 'b728bf8f-4aaa-4c00-bf37-fd0a1057c265', 'c97654d2-d8a9-4f24-b60c-b231f54f412f'),
(221, 'b95bfa8d-7930-4799-b19e-5fcad80d8002', '4c3774a5-fe81-4b99-a183-d6d0a472b98d'),
(170, 'ba4f0233-0e70-4db1-baa2-416ecb738437', '7866f70b-2ca0-43d2-a24d-d469dda21a08'),
(5, 'ba5f861c-4fb2-4cbf-a8e8-838e7e610672', 'af8f5019-2385-4054-9eed-96c08079bd74'),
(155, 'ba65087e-27f1-4216-a528-2d62ec664b85', '7866f70b-2ca0-43d2-a24d-d469dda21a08'),
(34, 'bae40b44-e9e3-4be3-9502-33a92150b0d5', 'e504d8d8-b516-4eec-8064-97a8256b9584'),
(302, 'bb89fcdc-981f-4c30-a863-b2e8c974a835', 'c5ab5265-8a8d-43fd-a282-52f9dd19f91b'),
(173, 'bbbed669-404f-456f-bcaa-c3f311b6c753', 'a17d74a8-7e90-44bd-b3c2-644815a9c7cb'),
(345, 'bbf0f9dd-47cd-49f8-85f5-be542e20ca00', 'b04805fc-08b3-43f5-bc15-4c174abd556d'),
(237, 'bc3682f9-3daf-4e3f-98a8-8b43f5c8e3e5', 'c97654d2-d8a9-4f24-b60c-b231f54f412f'),
(51, 'bc5db579-c568-450a-b630-c8a08689539f', 'fd0a89b6-3fbb-4d3b-b518-622530c6fcb4'),
(147, 'bc82b238-9e50-4d50-beef-6cc369eedaae', '5c2401c5-8332-478d-9f74-021e37af8f77'),
(120, 'bde559cd-f840-42d6-be00-dbe5a6f9d74c', 'd33e5c6b-0549-4186-b8a6-1f4059be4529'),
(181, 'bdfd187d-0db2-4120-86ee-45457e0421ac', '9efb5ecd-a54d-41af-a381-6ba7753266d3'),
(12, 'be6a76bd-0b93-4fc0-b158-ec642bcfc288', 'af8f5019-2385-4054-9eed-96c08079bd74'),
(448, 'bf0750b6-8756-445c-8c95-1cc999784721', 'e93425d8-211d-4211-ac23-1397a6df1539'),
(57, 'bf1b5dbd-d09d-4236-ac00-da54a80f8880', '26b00395-e9ed-4dde-9fbd-3766c044eb38'),
(257, 'c03c6017-eb27-4362-ba17-ea7163dfa0a7', 'a78e8715-96c2-416a-833c-854f6c8d1972'),
(316, 'c0dd4972-f6e9-4753-afb4-93815da6b105', '78c68093-5fa1-4c77-b13c-b52a008f90ee'),
(13, 'c262ea07-c9c2-4810-8327-175c637c7ef2', 'af8f5019-2385-4054-9eed-96c08079bd74'),
(213, 'c274e8f9-40c8-42e4-a8ac-fc4b87ac9967', '72d8ebdb-dd6b-4dc1-9391-7d2d60a6d422'),
(79, 'c289e3a4-b38b-4888-abd9-bf69f05ea4f3', 'fb440671-4818-453e-813e-6d2f39f51047'),
(171, 'c2ae67ff-3d4d-4af9-a80b-7d50beab468f', '7383aca9-2deb-4f4d-a371-d71cf99cb0fe'),
(311, 'c2d37619-ed3b-471b-93c5-1462f44bea20', '78c68093-5fa1-4c77-b13c-b52a008f90ee'),
(40, 'c3cfb9ac-cbe6-495e-84de-7f9c400974d1', 'fd0a89b6-3fbb-4d3b-b518-622530c6fcb4'),
(247, 'c3d49e96-72d2-4c7e-8637-7f408f9de78f', 'c97654d2-d8a9-4f24-b60c-b231f54f412f'),
(193, 'c40ddfac-17d1-4651-87d9-b30068182405', '71fee567-679c-4704-bee3-613c8cb0c2f5'),
(37, 'c442c653-84f0-442e-987d-b255d99eeef4', 'e504d8d8-b516-4eec-8064-97a8256b9584'),
(111, 'c479afe7-3228-4252-9304-6604d832611b', '63b58b3b-f1ec-4e6f-b245-ebe49f135e7b'),
(374, 'c52c8970-b2f2-4e7e-b25e-caa79e4153d6', 'dba5eb21-2947-4361-9405-82f81ec9453a'),
(165, 'c5769f58-03c8-4d27-9e08-0c6b52e9e7c5', '7866f70b-2ca0-43d2-a24d-d469dda21a08'),
(169, 'c624d7ca-3467-4c2f-a3d5-88556f75f24e', '7866f70b-2ca0-43d2-a24d-d469dda21a08'),
(31, 'c667de9d-adfd-4212-ae35-5700cfbd3163', 'e504d8d8-b516-4eec-8064-97a8256b9584'),
(203, 'c71017f7-69af-4113-8cd8-f4410e84880e', '72d8ebdb-dd6b-4dc1-9391-7d2d60a6d422'),
(74, 'c74b7380-a933-440d-b29b-5281bb7ad1b2', 'fb440671-4818-453e-813e-6d2f39f51047'),
(219, 'c7a8d0ab-3ff1-4ebc-9d1f-19fdc056faf1', '4c3774a5-fe81-4b99-a183-d6d0a472b98d'),
(457, 'c86b52ea-f28d-498a-aac1-d2f39bd28302', '5ee99a8b-8156-4102-bae8-aee2bf4d006c'),
(208, 'ca0c459c-9427-4f9e-92e3-8dc5638eb9db', '72d8ebdb-dd6b-4dc1-9391-7d2d60a6d422'),
(142, 'ca58c9b9-1918-4a5c-b943-26ab692434a2', '5c2401c5-8332-478d-9f74-021e37af8f77'),
(38, 'cb26992c-b587-4df4-84d2-713fdaa6dc18', 'e504d8d8-b516-4eec-8064-97a8256b9584'),
(149, 'cc08818c-b3a7-47b3-aff7-8c9794511ceb', '5c2401c5-8332-478d-9f74-021e37af8f77'),
(162, 'cc7c516f-736b-4bac-a1c9-dc0ddfd1358d', '7866f70b-2ca0-43d2-a24d-d469dda21a08'),
(301, 'ccb3c563-f3f8-40c1-9162-6b0057429612', 'c5ab5265-8a8d-43fd-a282-52f9dd19f91b'),
(362, 'cd1b767e-f02f-49a0-846e-8d51255f57c0', '0c2d0cb3-f60a-4f2f-a717-7ea1a61e14cf'),
(225, 'ce05903f-148e-47f2-a5c2-2c54fd2eada0', '4c3774a5-fe81-4b99-a183-d6d0a472b98d'),
(405, 'cefcb51b-887a-4e99-bbf9-4d3423f81542', 'f8c9bbe4-2610-4e27-a7e0-93c7eb522087'),
(75, 'cf590156-5b6c-4bec-b891-80081fca85b2', 'fb440671-4818-453e-813e-6d2f39f51047'),
(274, 'cf5ccf3a-3e42-48b5-a34b-297da9c06d6b', '9d3e5fec-ac71-43d4-a466-a3fb590df6e0'),
(409, 'd0175ee2-2a0e-4f39-a78f-e3965b45bf2b', 'f8c9bbe4-2610-4e27-a7e0-93c7eb522087'),
(196, 'd0459f10-296c-4009-a6a8-c18a92072899', '71fee567-679c-4704-bee3-613c8cb0c2f5'),
(276, 'd0baf845-adcd-4609-8379-a1ddebc9f9bc', '9d3e5fec-ac71-43d4-a466-a3fb590df6e0'),
(9, 'd1281177-a250-43cb-bf45-36e9abbacd32', 'af8f5019-2385-4054-9eed-96c08079bd74'),
(251, 'd18a5479-fde8-4162-9c21-c51e1abebd98', 'a78e8715-96c2-416a-833c-854f6c8d1972'),
(232, 'd1fa2942-29e1-4395-8948-1bf1b38c961c', '4c3774a5-fe81-4b99-a183-d6d0a472b98d'),
(323, 'd27cf8bc-7d1e-4a25-9534-432200958edf', 'fc1adafa-51a8-4bb4-9d8a-0261eb556168'),
(104, 'd2b5dcaa-4a15-40d8-88c6-6efbfd6623ce', '63b58b3b-f1ec-4e6f-b245-ebe49f135e7b'),
(174, 'd2ff9b3f-0451-45f8-a0a2-5239e70d966f', 'a17d74a8-7e90-44bd-b3c2-644815a9c7cb'),
(394, 'd336b33c-6525-4324-87d6-37138e74da15', '43fd5c6a-1252-46b4-9c8e-fee43010253e'),
(264, 'd4a055f8-843e-4bf0-8d53-a35ef5827b72', 'a78e8715-96c2-416a-833c-854f6c8d1972'),
(403, 'd4ac5336-3459-4b9c-9289-9de389b15a59', 'f8c9bbe4-2610-4e27-a7e0-93c7eb522087'),
(387, 'd623c5ac-ffd7-4621-81fc-8aefa3430908', '43fd5c6a-1252-46b4-9c8e-fee43010253e'),
(157, 'd65cf692-0625-4270-83c0-04baea6d8049', '7866f70b-2ca0-43d2-a24d-d469dda21a08'),
(281, 'd6ac8c68-ba08-4abc-90ac-008b69daeef2', '9d3e5fec-ac71-43d4-a466-a3fb590df6e0'),
(129, 'd6ae7363-dbaa-46a2-9d31-2d98f3f5718d', 'd33e5c6b-0549-4186-b8a6-1f4059be4529'),
(98, 'd75b1262-88db-4e4e-9052-fc89af7c8a06', 'b80a1ad0-4154-4bcc-81a4-420aad7ed35a'),
(99, 'd8002169-dee8-4d7f-b288-11c401f792bb', 'b80a1ad0-4154-4bcc-81a4-420aad7ed35a'),
(182, 'd82398a6-e4e5-4d49-9645-f6ba127a1fbb', '71fee567-679c-4704-bee3-613c8cb0c2f5'),
(241, 'd8c6cd9f-8a87-4716-a5d5-59928056707d', 'c97654d2-d8a9-4f24-b60c-b231f54f412f'),
(113, 'd91378e6-b17e-49bc-b86d-fcbb48ab5889', '63b58b3b-f1ec-4e6f-b245-ebe49f135e7b'),
(69, 'd9c0ccfd-80b4-441e-8162-3c087140a8dd', 'fb440671-4818-453e-813e-6d2f39f51047'),
(3, 'd9ce9a10-fc26-45b3-b475-a41fb730b8f9', 'af8f5019-2385-4054-9eed-96c08079bd74'),
(235, 'db87aa66-a43e-4aa5-a04c-0a3235223a2a', 'c97654d2-d8a9-4f24-b60c-b231f54f412f'),
(218, 'dbd6eed4-28c3-404b-8852-7c7b780e8498', '4c3774a5-fe81-4b99-a183-d6d0a472b98d'),
(408, 'dc471270-ffbb-4cae-bfc3-3ac90acb0794', 'f8c9bbe4-2610-4e27-a7e0-93c7eb522087'),
(356, 'dca321ad-bc5f-47db-ac66-c7c4f3819500', '0c2d0cb3-f60a-4f2f-a717-7ea1a61e14cf'),
(2, 'dcc72b97-2c3c-4c00-97e8-8270c4e2aa11', 'af8f5019-2385-4054-9eed-96c08079bd74'),
(93, 'ddbeae4c-9f5e-4b9d-9ef1-ed2304ee3903', 'b80a1ad0-4154-4bcc-81a4-420aad7ed35a'),
(122, 'ddc0b1db-ff9b-47c9-9fbf-808f74ad6d59', 'd33e5c6b-0549-4186-b8a6-1f4059be4529'),
(371, 'df40c53b-f66e-4ba8-8ccb-2b4c3abe50de', 'dba5eb21-2947-4361-9405-82f81ec9453a'),
(184, 'dfe006d9-e0aa-42a2-b37d-a5bbe7e6c327', '71fee567-679c-4704-bee3-613c8cb0c2f5'),
(186, 'e03c9c66-0a8e-4ea5-9ae9-4befc453195e', '71fee567-679c-4704-bee3-613c8cb0c2f5'),
(84, 'e0ba553d-2981-4ed3-8aa5-9bee7642c64d', 'fb440671-4818-453e-813e-6d2f39f51047'),
(105, 'e0fb594b-2dbf-444b-9553-68d027966fc0', '63b58b3b-f1ec-4e6f-b245-ebe49f135e7b'),
(427, 'e11e332d-1433-4bf0-8ae9-99d480b29f81', '52f53776-d370-4f6d-94e3-56b1a5b349e0'),
(117, 'e1406894-d555-4d59-9b28-2cdd0b60f569', '63b58b3b-f1ec-4e6f-b245-ebe49f135e7b'),
(382, 'e1437391-0652-4b6e-bbe8-0fa4b969e7dc', '43fd5c6a-1252-46b4-9c8e-fee43010253e'),
(298, 'e1b9041a-62bf-414a-b7fa-8066f91d6f4d', 'c5ab5265-8a8d-43fd-a282-52f9dd19f91b'),
(30, 'e2078f92-e5e7-42bc-aaba-2ce8b033dec4', 'e504d8d8-b516-4eec-8064-97a8256b9584'),
(416, 'e2cc9903-0867-48a8-a26e-9c75ab56e297', 'f8c9bbe4-2610-4e27-a7e0-93c7eb522087'),
(85, 'e45ca7e4-0b1d-47e2-8ce6-618bdd598fa7', 'fb440671-4818-453e-813e-6d2f39f51047'),
(145, 'e4dc5ae7-c596-443a-abd7-642e46720f8f', '5c2401c5-8332-478d-9f74-021e37af8f77'),
(59, 'e54cd380-37c3-44ba-a88c-f788d3c900f9', '26b00395-e9ed-4dde-9fbd-3766c044eb38'),
(379, 'e5dff1a7-dd18-4dc4-8905-10605f3a289d', 'dba5eb21-2947-4361-9405-82f81ec9453a'),
(292, 'e64a31d2-4344-424c-96d1-5c25d174c86f', 'd270b222-2ffe-448c-961d-c46b1cd03231'),
(434, 'e6b033cb-a67b-4aab-be79-dc5dce608ba5', '52f53776-d370-4f6d-94e3-56b1a5b349e0'),
(70, 'e6e2b0ce-c59e-440f-bd12-ce96dc5423bf', 'fb440671-4818-453e-813e-6d2f39f51047'),
(87, 'e6e2b636-0225-4e4d-a79b-4b0938dbc7c1', 'b80a1ad0-4154-4bcc-81a4-420aad7ed35a'),
(320, 'e96cee0a-c332-448f-bdc1-b42067339c5f', 'fc1adafa-51a8-4bb4-9d8a-0261eb556168'),
(43, 'e9804104-7766-4390-8744-4a3c9fb5b22d', 'fd0a89b6-3fbb-4d3b-b518-622530c6fcb4'),
(361, 'ea1a105e-f272-49c4-b383-8622db2a2c18', '0c2d0cb3-f60a-4f2f-a717-7ea1a61e14cf'),
(358, 'ea71af28-aee6-4b37-bcd8-f25a98f4f01b', '0c2d0cb3-f60a-4f2f-a717-7ea1a61e14cf'),
(72, 'ea898bb5-9972-46f1-989d-2cb77a121714', 'fb440671-4818-453e-813e-6d2f39f51047'),
(423, 'ed034fa7-aca3-4906-a8ea-c7c38fa7913e', '52f53776-d370-4f6d-94e3-56b1a5b349e0'),
(431, 'ed1a2bcf-cd12-4584-a686-ff7569f94529', '52f53776-d370-4f6d-94e3-56b1a5b349e0'),
(116, 'edbb64c3-0209-4ad5-93c5-c8a3e8d66af4', '63b58b3b-f1ec-4e6f-b245-ebe49f135e7b'),
(263, 'ee38aac5-b5a2-4140-bc12-0f47779cccb5', 'a78e8715-96c2-416a-833c-854f6c8d1972'),
(83, 'f0a2e3d9-41f6-460e-a1d2-71d78ec403f1', 'fb440671-4818-453e-813e-6d2f39f51047'),
(179, 'f285afb1-8b25-4acb-a029-295ce2ef49bf', '9efb5ecd-a54d-41af-a381-6ba7753266d3'),
(68, 'f300e385-11df-4350-9f27-fc8fb1451988', 'fb440671-4818-453e-813e-6d2f39f51047'),
(258, 'f30adcf0-c326-4ebf-930b-494f2a2737c8', 'a78e8715-96c2-416a-833c-854f6c8d1972'),
(245, 'f3d0ceec-c7c8-4087-94f3-ca47035da59f', 'c97654d2-d8a9-4f24-b60c-b231f54f412f'),
(395, 'f537eee0-ba61-4535-a744-4b18df490ab8', '43fd5c6a-1252-46b4-9c8e-fee43010253e'),
(436, 'f59193b0-5596-4762-834b-dd0aa1c2f19a', 'e93425d8-211d-4211-ac23-1397a6df1539'),
(460, 'f727fd16-95ee-4d62-b0ab-dc62d4f2809f', '5ee99a8b-8156-4102-bae8-aee2bf4d006c'),
(437, 'f799f361-2c23-4ce3-97db-ed7ede2ba12a', 'e93425d8-211d-4211-ac23-1397a6df1539'),
(380, 'f82a1b0c-9f28-4d59-aed2-a11e039e73ee', 'dba5eb21-2947-4361-9405-82f81ec9453a'),
(303, 'f9538439-b89a-45fd-9d39-987a5884ad99', 'c5ab5265-8a8d-43fd-a282-52f9dd19f91b'),
(270, 'f9a0bcbd-b303-40e8-a6d6-672613267496', '79e7509e-ef2f-4e5e-8b80-dd673d62bb66'),
(330, 'fa060762-b443-4b00-9cd0-7d8534c59ca5', 'b04805fc-08b3-43f5-bc15-4c174abd556d'),
(352, 'fb15c0b7-e9ff-40b1-83cf-131ce560540a', '0c2d0cb3-f60a-4f2f-a717-7ea1a61e14cf'),
(29, 'fb51b969-870c-414b-bb71-799f97c52253', 'e504d8d8-b516-4eec-8064-97a8256b9584'),
(389, 'fe3c0351-39d7-4c4b-88e7-bbabfc97bde3', '43fd5c6a-1252-46b4-9c8e-fee43010253e'),
(224, 'ff74840e-c2e4-4d75-a72c-0bf34a261531', '4c3774a5-fe81-4b99-a183-d6d0a472b98d'),
(166, 'ff791c3a-86c8-4beb-ac7d-93c7b6dd1eef', '7866f70b-2ca0-43d2-a24d-d469dda21a08'),
(287, 'ffc80d83-be90-44e9-9ade-a2cf94420c31', 'd270b222-2ffe-448c-961d-c46b1cd03231'),
(407, 'ffe75b19-a4ee-4fb7-a0fc-91fb1cf85d11', 'f8c9bbe4-2610-4e27-a7e0-93c7eb522087');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_trade_import_product_codes`
--

CREATE TABLE `oc_trade_import_product_codes` (
  `product_id` int(11) NOT NULL,
  `nomenclature_uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `oc_trade_import_product_codes`
--

INSERT INTO `oc_trade_import_product_codes` (`product_id`, `nomenclature_uuid`, `group_uuid`) VALUES
(78, '0c2d0cb3-f60a-4f2f-a717-7ea1a61e14cf', ''),
(54, '26b00395-e9ed-4dde-9fbd-3766c044eb38', ''),
(80, '43fd5c6a-1252-46b4-9c8e-fee43010253e', ''),
(51, '497d4414-c05f-4e70-a4b9-60f4e5fe6f76', ''),
(67, '4c3774a5-fe81-4b99-a183-d6d0a472b98d', ''),
(82, '52f53776-d370-4f6d-94e3-56b1a5b349e0', ''),
(59, '5c2401c5-8332-478d-9f74-021e37af8f77', ''),
(84, '5ee99a8b-8156-4102-bae8-aee2bf4d006c', ''),
(57, '63b58b3b-f1ec-4e6f-b245-ebe49f135e7b', ''),
(64, '71fee567-679c-4704-bee3-613c8cb0c2f5', ''),
(65, '72d8ebdb-dd6b-4dc1-9391-7d2d60a6d422', ''),
(61, '7383aca9-2deb-4f4d-a371-d71cf99cb0fe', ''),
(60, '7866f70b-2ca0-43d2-a24d-d469dda21a08', ''),
(75, '78c68093-5fa1-4c77-b13c-b52a008f90ee', ''),
(70, '79e7509e-ef2f-4e5e-8b80-dd673d62bb66', ''),
(72, '9d3e5fec-ac71-43d4-a466-a3fb590df6e0', ''),
(63, '9efb5ecd-a54d-41af-a381-6ba7753266d3', ''),
(62, 'a17d74a8-7e90-44bd-b3c2-644815a9c7cb', ''),
(69, 'a78e8715-96c2-416a-833c-854f6c8d1972', ''),
(50, 'af8f5019-2385-4054-9eed-96c08079bd74', ''),
(77, 'b04805fc-08b3-43f5-bc15-4c174abd556d', ''),
(56, 'b80a1ad0-4154-4bcc-81a4-420aad7ed35a', ''),
(71, 'c179a270-ce8a-4d3c-9e80-e2f07056dda0', ''),
(74, 'c5ab5265-8a8d-43fd-a282-52f9dd19f91b', ''),
(68, 'c97654d2-d8a9-4f24-b60c-b231f54f412f', ''),
(73, 'd270b222-2ffe-448c-961d-c46b1cd03231', ''),
(58, 'd33e5c6b-0549-4186-b8a6-1f4059be4529', ''),
(79, 'dba5eb21-2947-4361-9405-82f81ec9453a', ''),
(52, 'e504d8d8-b516-4eec-8064-97a8256b9584', ''),
(83, 'e93425d8-211d-4211-ac23-1397a6df1539', ''),
(66, 'f77b72ca-c4bb-4655-a845-8e4526d83940', ''),
(81, 'f8c9bbe4-2610-4e27-a7e0-93c7eb522087', ''),
(55, 'fb440671-4818-453e-813e-6d2f39f51047', ''),
(76, 'fc1adafa-51a8-4bb4-9d8a-0261eb556168', ''),
(53, 'fd0a89b6-3fbb-4d3b-b518-622530c6fcb4', '');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_trade_import_services`
--

CREATE TABLE `oc_trade_import_services` (
  `id` int(11) NOT NULL,
  `service_uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_trade_import_stocks_banner`
--

CREATE TABLE `oc_trade_import_stocks_banner` (
  `banner_image_id` int(11) NOT NULL,
  `banner_id` int(11) NOT NULL,
  `stocks_id` int(11) NOT NULL,
  `stocks_uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_trade_import_stocks_codes`
--

CREATE TABLE `oc_trade_import_stocks_codes` (
  `stocks_id` int(11) NOT NULL,
  `stocks_uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_trade_import_warehouse_codes`
--

CREATE TABLE `oc_trade_import_warehouse_codes` (
  `warehouse_id` int(11) NOT NULL,
  `storage_uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_trade_orders`
--

CREATE TABLE `oc_trade_orders` (
  `operation_id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `order_data` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `response` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_translation`
--

CREATE TABLE `oc_translation` (
  `translation_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `route` varchar(64) NOT NULL,
  `key` varchar(64) NOT NULL,
  `value` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_unixml_additional_params`
--

CREATE TABLE `oc_unixml_additional_params` (
  `item_id` int(11) NOT NULL,
  `feed` varchar(64) NOT NULL,
  `param_name` varchar(255) NOT NULL,
  `param_text` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_unixml_attributes`
--

CREATE TABLE `oc_unixml_attributes` (
  `item_id` int(11) NOT NULL,
  `feed` varchar(64) NOT NULL,
  `attribute_id` varchar(255) NOT NULL,
  `xml_name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_unixml_category_match`
--

CREATE TABLE `oc_unixml_category_match` (
  `item_id` int(11) NOT NULL,
  `feed` varchar(64) NOT NULL,
  `category_id` varchar(255) NOT NULL,
  `xml_name` varchar(255) NOT NULL,
  `markup` varchar(255) NOT NULL,
  `custom` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_unixml_markup`
--

CREATE TABLE `oc_unixml_markup` (
  `item_id` int(11) NOT NULL,
  `feed` varchar(64) NOT NULL,
  `name` varchar(255) NOT NULL,
  `products` varchar(20000) DEFAULT NULL,
  `markup` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_unixml_replace_name`
--

CREATE TABLE `oc_unixml_replace_name` (
  `item_id` int(11) NOT NULL,
  `feed` varchar(64) NOT NULL,
  `name_from` varchar(255) NOT NULL,
  `name_to` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_upload`
--

CREATE TABLE `oc_upload` (
  `upload_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_user`
--

CREATE TABLE `oc_user` (
  `user_id` int(11) NOT NULL,
  `user_group_id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `image` varchar(255) NOT NULL,
  `code` varchar(40) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_user`
--

INSERT INTO `oc_user` (`user_id`, `user_group_id`, `username`, `password`, `salt`, `firstname`, `lastname`, `email`, `image`, `code`, `ip`, `status`, `date_added`) VALUES
(1, 1, 'admin', 'ba28daf3ce4691a19dca3ae2d93cf613739bef8f', 'OdKRjcQVP', 'John', 'Doe', 'demonized@4ait.ru', '', '', '127.0.0.1', 1, '2019-05-16 14:35:22');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_user_group`
--

CREATE TABLE `oc_user_group` (
  `user_group_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `permission` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_user_group`
--

INSERT INTO `oc_user_group` (`user_group_id`, `name`, `permission`) VALUES
(1, 'Администратор', '{\"access\":[\"blog\\/article\",\"blog\\/category\",\"blog\\/review\",\"blog\\/setting\",\"catalog\\/attribute\",\"catalog\\/attribute_group\",\"catalog\\/category\",\"catalog\\/city\",\"catalog\\/download\",\"catalog\\/filter\",\"catalog\\/information\",\"catalog\\/manufacturer\",\"catalog\\/option\",\"catalog\\/product\",\"catalog\\/recurring\",\"catalog\\/review\",\"catalog\\/stocks\",\"catalog\\/warehouse\",\"common\\/developer\",\"common\\/filemanager\",\"common\\/profile\",\"common\\/security\",\"customer\\/custom_field\",\"customer\\/customer\",\"customer\\/customer_approval\",\"customer\\/customer_group\",\"dashboard\\/manager\",\"design\\/banner\",\"design\\/layout\",\"design\\/seo_url\",\"design\\/theme\",\"design\\/translation\",\"event\\/compatibility\",\"event\\/language\",\"event\\/statistics\",\"event\\/theme\",\"extension\\/analytics\\/google\",\"extension\\/analytics\\/metrika\",\"extension\\/captcha\\/basic\",\"extension\\/captcha\\/google\",\"extension\\/dashboard\\/activity\",\"extension\\/dashboard\\/chart\",\"extension\\/dashboard\\/customer\",\"extension\\/dashboard\\/manager\",\"extension\\/dashboard\\/map\",\"extension\\/dashboard\\/online\",\"extension\\/dashboard\\/order\",\"extension\\/dashboard\\/recent\",\"extension\\/dashboard\\/sale\",\"extension\\/export_import\",\"extension\\/extension\\/analytics\",\"extension\\/extension\\/captcha\",\"extension\\/extension\\/dashboard\",\"extension\\/extension\\/feed\",\"extension\\/extension\\/fraud\",\"extension\\/extension\\/menu\",\"extension\\/extension\\/module\",\"extension\\/extension\\/payment\",\"extension\\/extension\\/report\",\"extension\\/extension\\/shipping\",\"extension\\/extension\\/theme\",\"extension\\/extension\\/total\",\"extension\\/feed\\/google_base\",\"extension\\/feed\\/google_sitemap\",\"extension\\/feed\\/openbaypro\",\"extension\\/feed\\/unisender\",\"extension\\/feed\\/unixml\",\"extension\\/feed\\/yandex_market\",\"extension\\/feed\\/yandex_sitemap\",\"extension\\/feed\\/yandex_turbo\",\"extension\\/fraud\\/fraudlabspro\",\"extension\\/fraud\\/ip\",\"extension\\/fraud\\/maxmind\",\"extension\\/module\\/account\",\"extension\\/module\\/amazon_login\",\"extension\\/module\\/amazon_pay\",\"extension\\/module\\/banner\",\"extension\\/module\\/bestseller\",\"extension\\/module\\/blog_category\",\"extension\\/module\\/blog_featured\",\"extension\\/module\\/blog_latest\",\"extension\\/module\\/carousel\",\"extension\\/module\\/category\",\"extension\\/module\\/cdek_integrator\",\"extension\\/module\\/divido_calculator\",\"extension\\/module\\/ebay_listing\",\"extension\\/module\\/extendedsearch\",\"extension\\/module\\/featured\",\"extension\\/module\\/featured_article\",\"extension\\/module\\/featured_product\",\"extension\\/module\\/filter\",\"extension\\/module\\/google_hangouts\",\"extension\\/module\\/html\",\"extension\\/module\\/information\",\"extension\\/module\\/klarna_checkout_module\",\"extension\\/module\\/latest\",\"extension\\/module\\/laybuy_layout\",\"extension\\/module\\/manager\",\"extension\\/module\\/ocfilter\",\"extension\\/module\\/pilibaba_button\",\"extension\\/module\\/pp_braintree_button\",\"extension\\/module\\/pp_button\",\"extension\\/module\\/pp_login\",\"extension\\/module\\/quickcheckout\",\"extension\\/module\\/sagepay_direct_cards\",\"extension\\/module\\/sagepay_server_cards\",\"extension\\/module\\/slideshow\",\"extension\\/module\\/special\",\"extension\\/module\\/store\",\"extension\\/module\\/template_switcher\",\"extension\\/module\\/trade_import\",\"extension\\/module\\/vqmod_manager\",\"extension\\/openbay\\/amazon\",\"extension\\/openbay\\/amazon_listing\",\"extension\\/openbay\\/amazon_product\",\"extension\\/openbay\\/amazonus\",\"extension\\/openbay\\/amazonus_listing\",\"extension\\/openbay\\/amazonus_product\",\"extension\\/openbay\\/ebay\",\"extension\\/openbay\\/ebay_profile\",\"extension\\/openbay\\/ebay_template\",\"extension\\/openbay\\/etsy\",\"extension\\/openbay\\/etsy_product\",\"extension\\/openbay\\/etsy_shipping\",\"extension\\/openbay\\/etsy_shop\",\"extension\\/openbay\\/fba\",\"extension\\/payment\\/alipay\",\"extension\\/payment\\/alipay_cross\",\"extension\\/payment\\/amazon_login_pay\",\"extension\\/payment\\/authorizenet_aim\",\"extension\\/payment\\/authorizenet_sim\",\"extension\\/payment\\/bank_transfer\",\"extension\\/payment\\/bluepay_hosted\",\"extension\\/payment\\/bluepay_redirect\",\"extension\\/payment\\/cardconnect\",\"extension\\/payment\\/cardinity\",\"extension\\/payment\\/cheque\",\"extension\\/payment\\/cod\",\"extension\\/payment\\/cod_cdek\",\"extension\\/payment\\/divido\",\"extension\\/payment\\/eway\",\"extension\\/payment\\/firstdata\",\"extension\\/payment\\/firstdata_remote\",\"extension\\/payment\\/free_checkout\",\"extension\\/payment\\/g2apay\",\"extension\\/payment\\/globalpay\",\"extension\\/payment\\/globalpay_remote\",\"extension\\/payment\\/klarna_account\",\"extension\\/payment\\/klarna_checkout\",\"extension\\/payment\\/klarna_invoice\",\"extension\\/payment\\/laybuy\",\"extension\\/payment\\/liqpay\",\"extension\\/payment\\/nochex\",\"extension\\/payment\\/ocstore_w1\",\"extension\\/payment\\/paykeeper\",\"extension\\/payment\\/paymate\",\"extension\\/payment\\/paypoint\",\"extension\\/payment\\/payza\",\"extension\\/payment\\/perpetual_payments\",\"extension\\/payment\\/pilibaba\",\"extension\\/payment\\/pp_braintree\",\"extension\\/payment\\/pp_express\",\"extension\\/payment\\/pp_payflow\",\"extension\\/payment\\/pp_payflow_iframe\",\"extension\\/payment\\/pp_pro\",\"extension\\/payment\\/pp_pro_iframe\",\"extension\\/payment\\/pp_standard\",\"extension\\/payment\\/rbs\",\"extension\\/payment\\/realex\",\"extension\\/payment\\/realex_remote\",\"extension\\/payment\\/sagepay_direct\",\"extension\\/payment\\/sagepay_server\",\"extension\\/payment\\/sagepay_us\",\"extension\\/payment\\/securetrading_pp\",\"extension\\/payment\\/securetrading_ws\",\"extension\\/payment\\/skrill\",\"extension\\/payment\\/squareup\",\"extension\\/payment\\/tinkoff\",\"extension\\/payment\\/twocheckout\",\"extension\\/payment\\/web_payment_software\",\"extension\\/payment\\/webmoney_wmb\",\"extension\\/payment\\/webmoney_wme\",\"extension\\/payment\\/webmoney_wmk\",\"extension\\/payment\\/webmoney_wmr\",\"extension\\/payment\\/webmoney_wmu\",\"extension\\/payment\\/webmoney_wmv\",\"extension\\/payment\\/webmoney_wmz\",\"extension\\/payment\\/wechat_pay\",\"extension\\/payment\\/worldpay\",\"extension\\/report\\/customer_activity\",\"extension\\/report\\/customer_order\",\"extension\\/report\\/customer_reward\",\"extension\\/report\\/customer_search\",\"extension\\/report\\/customer_transaction\",\"extension\\/report\\/marketing\",\"extension\\/report\\/product_purchased\",\"extension\\/report\\/product_viewed\",\"extension\\/report\\/sale_coupon\",\"extension\\/report\\/sale_order\",\"extension\\/report\\/sale_return\",\"extension\\/report\\/sale_shipping\",\"extension\\/report\\/sale_tax\",\"extension\\/shipping\\/auspost\",\"extension\\/shipping\\/cdek\",\"extension\\/shipping\\/citylink\",\"extension\\/shipping\\/ec_ship\",\"extension\\/shipping\\/fedex\",\"extension\\/shipping\\/flat\",\"extension\\/shipping\\/free\",\"extension\\/shipping\\/item\",\"extension\\/shipping\\/parcelforce_48\",\"extension\\/shipping\\/pickup\",\"extension\\/shipping\\/royal_mail\",\"extension\\/shipping\\/ups\",\"extension\\/shipping\\/usps\",\"extension\\/shipping\\/weight\",\"extension\\/shipping\\/xshippingpro\",\"extension\\/theme\\/default\",\"extension\\/total\\/cdek\",\"extension\\/total\\/coupon\",\"extension\\/total\\/credit\",\"extension\\/total\\/handling\",\"extension\\/total\\/klarna_fee\",\"extension\\/total\\/low_order_fee\",\"extension\\/total\\/reward\",\"extension\\/total\\/shipping\",\"extension\\/total\\/sub_total\",\"extension\\/total\\/tax\",\"extension\\/total\\/total\",\"extension\\/total\\/voucher\",\"localisation\\/country\",\"localisation\\/currency\",\"localisation\\/geo_zone\",\"localisation\\/language\",\"localisation\\/length_class\",\"localisation\\/location\",\"localisation\\/order_status\",\"localisation\\/return_action\",\"localisation\\/return_reason\",\"localisation\\/return_status\",\"localisation\\/stock_status\",\"localisation\\/tax_class\",\"localisation\\/tax_rate\",\"localisation\\/weight_class\",\"localisation\\/zone\",\"mail\\/affiliate\",\"mail\\/customer\",\"mail\\/forgotten\",\"mail\\/return\",\"mail\\/reward\",\"mail\\/transaction\",\"marketing\\/contact\",\"marketing\\/coupon\",\"marketing\\/marketing\",\"marketplace\\/api\",\"marketplace\\/event\",\"marketplace\\/extension\",\"marketplace\\/install\",\"marketplace\\/installer\",\"marketplace\\/marketplace\",\"marketplace\\/modification\",\"marketplace\\/openbay\",\"marketplace\\/opencartforum\",\"report\\/online\",\"report\\/report\",\"report\\/statistics\",\"sale\\/order\",\"sale\\/recurring\",\"sale\\/return\",\"sale\\/voucher\",\"sale\\/voucher_theme\",\"search\\/search\",\"setting\\/ecos\",\"setting\\/setting\",\"setting\\/store\",\"startup\\/error\",\"startup\\/event\",\"startup\\/login\",\"startup\\/permission\",\"startup\\/router\",\"startup\\/sass\",\"startup\\/startup\",\"tool\\/backup\",\"tool\\/cdektool\",\"tool\\/log\",\"tool\\/upload\",\"user\\/api\",\"user\\/user\",\"user\\/user_permission\",\"module\\/cdek_integrator\",\"payment\\/cod_cdek\"],\"modify\":[\"blog\\/article\",\"blog\\/category\",\"blog\\/review\",\"blog\\/setting\",\"catalog\\/attribute\",\"catalog\\/attribute_group\",\"catalog\\/category\",\"catalog\\/city\",\"catalog\\/download\",\"catalog\\/filter\",\"catalog\\/information\",\"catalog\\/manufacturer\",\"catalog\\/option\",\"catalog\\/product\",\"catalog\\/recurring\",\"catalog\\/review\",\"catalog\\/stocks\",\"catalog\\/warehouse\",\"common\\/developer\",\"common\\/filemanager\",\"common\\/profile\",\"common\\/security\",\"customer\\/custom_field\",\"customer\\/customer\",\"customer\\/customer_approval\",\"customer\\/customer_group\",\"dashboard\\/manager\",\"design\\/banner\",\"design\\/layout\",\"design\\/seo_url\",\"design\\/theme\",\"design\\/translation\",\"event\\/compatibility\",\"event\\/language\",\"event\\/statistics\",\"event\\/theme\",\"extension\\/analytics\\/google\",\"extension\\/analytics\\/metrika\",\"extension\\/captcha\\/basic\",\"extension\\/captcha\\/google\",\"extension\\/dashboard\\/activity\",\"extension\\/dashboard\\/chart\",\"extension\\/dashboard\\/customer\",\"extension\\/dashboard\\/manager\",\"extension\\/dashboard\\/map\",\"extension\\/dashboard\\/online\",\"extension\\/dashboard\\/order\",\"extension\\/dashboard\\/recent\",\"extension\\/dashboard\\/sale\",\"extension\\/export_import\",\"extension\\/extension\\/analytics\",\"extension\\/extension\\/captcha\",\"extension\\/extension\\/dashboard\",\"extension\\/extension\\/feed\",\"extension\\/extension\\/fraud\",\"extension\\/extension\\/menu\",\"extension\\/extension\\/module\",\"extension\\/extension\\/payment\",\"extension\\/extension\\/report\",\"extension\\/extension\\/shipping\",\"extension\\/extension\\/theme\",\"extension\\/extension\\/total\",\"extension\\/feed\\/google_base\",\"extension\\/feed\\/google_sitemap\",\"extension\\/feed\\/openbaypro\",\"extension\\/feed\\/unisender\",\"extension\\/feed\\/unixml\",\"extension\\/feed\\/yandex_market\",\"extension\\/feed\\/yandex_sitemap\",\"extension\\/feed\\/yandex_turbo\",\"extension\\/fraud\\/fraudlabspro\",\"extension\\/fraud\\/ip\",\"extension\\/fraud\\/maxmind\",\"extension\\/module\\/account\",\"extension\\/module\\/amazon_login\",\"extension\\/module\\/amazon_pay\",\"extension\\/module\\/banner\",\"extension\\/module\\/bestseller\",\"extension\\/module\\/blog_category\",\"extension\\/module\\/blog_featured\",\"extension\\/module\\/blog_latest\",\"extension\\/module\\/carousel\",\"extension\\/module\\/category\",\"extension\\/module\\/cdek_integrator\",\"extension\\/module\\/divido_calculator\",\"extension\\/module\\/ebay_listing\",\"extension\\/module\\/extendedsearch\",\"extension\\/module\\/featured\",\"extension\\/module\\/featured_article\",\"extension\\/module\\/featured_product\",\"extension\\/module\\/filter\",\"extension\\/module\\/google_hangouts\",\"extension\\/module\\/html\",\"extension\\/module\\/information\",\"extension\\/module\\/klarna_checkout_module\",\"extension\\/module\\/latest\",\"extension\\/module\\/laybuy_layout\",\"extension\\/module\\/manager\",\"extension\\/module\\/ocfilter\",\"extension\\/module\\/pilibaba_button\",\"extension\\/module\\/pp_braintree_button\",\"extension\\/module\\/pp_button\",\"extension\\/module\\/pp_login\",\"extension\\/module\\/quickcheckout\",\"extension\\/module\\/sagepay_direct_cards\",\"extension\\/module\\/sagepay_server_cards\",\"extension\\/module\\/slideshow\",\"extension\\/module\\/special\",\"extension\\/module\\/store\",\"extension\\/module\\/template_switcher\",\"extension\\/module\\/trade_import\",\"extension\\/module\\/vqmod_manager\",\"extension\\/openbay\\/amazon\",\"extension\\/openbay\\/amazon_listing\",\"extension\\/openbay\\/amazon_product\",\"extension\\/openbay\\/amazonus\",\"extension\\/openbay\\/amazonus_listing\",\"extension\\/openbay\\/amazonus_product\",\"extension\\/openbay\\/ebay\",\"extension\\/openbay\\/ebay_profile\",\"extension\\/openbay\\/ebay_template\",\"extension\\/openbay\\/etsy\",\"extension\\/openbay\\/etsy_product\",\"extension\\/openbay\\/etsy_shipping\",\"extension\\/openbay\\/etsy_shop\",\"extension\\/openbay\\/fba\",\"extension\\/payment\\/alipay\",\"extension\\/payment\\/alipay_cross\",\"extension\\/payment\\/amazon_login_pay\",\"extension\\/payment\\/authorizenet_aim\",\"extension\\/payment\\/authorizenet_sim\",\"extension\\/payment\\/bank_transfer\",\"extension\\/payment\\/bluepay_hosted\",\"extension\\/payment\\/bluepay_redirect\",\"extension\\/payment\\/cardconnect\",\"extension\\/payment\\/cardinity\",\"extension\\/payment\\/cheque\",\"extension\\/payment\\/cod\",\"extension\\/payment\\/cod_cdek\",\"extension\\/payment\\/divido\",\"extension\\/payment\\/eway\",\"extension\\/payment\\/firstdata\",\"extension\\/payment\\/firstdata_remote\",\"extension\\/payment\\/free_checkout\",\"extension\\/payment\\/g2apay\",\"extension\\/payment\\/globalpay\",\"extension\\/payment\\/globalpay_remote\",\"extension\\/payment\\/klarna_account\",\"extension\\/payment\\/klarna_checkout\",\"extension\\/payment\\/klarna_invoice\",\"extension\\/payment\\/laybuy\",\"extension\\/payment\\/liqpay\",\"extension\\/payment\\/nochex\",\"extension\\/payment\\/ocstore_w1\",\"extension\\/payment\\/paykeeper\",\"extension\\/payment\\/paymate\",\"extension\\/payment\\/paypoint\",\"extension\\/payment\\/payza\",\"extension\\/payment\\/perpetual_payments\",\"extension\\/payment\\/pilibaba\",\"extension\\/payment\\/pp_braintree\",\"extension\\/payment\\/pp_express\",\"extension\\/payment\\/pp_payflow\",\"extension\\/payment\\/pp_payflow_iframe\",\"extension\\/payment\\/pp_pro\",\"extension\\/payment\\/pp_pro_iframe\",\"extension\\/payment\\/pp_standard\",\"extension\\/payment\\/rbs\",\"extension\\/payment\\/realex\",\"extension\\/payment\\/realex_remote\",\"extension\\/payment\\/sagepay_direct\",\"extension\\/payment\\/sagepay_server\",\"extension\\/payment\\/sagepay_us\",\"extension\\/payment\\/securetrading_pp\",\"extension\\/payment\\/securetrading_ws\",\"extension\\/payment\\/skrill\",\"extension\\/payment\\/squareup\",\"extension\\/payment\\/tinkoff\",\"extension\\/payment\\/twocheckout\",\"extension\\/payment\\/web_payment_software\",\"extension\\/payment\\/webmoney_wmb\",\"extension\\/payment\\/webmoney_wme\",\"extension\\/payment\\/webmoney_wmk\",\"extension\\/payment\\/webmoney_wmr\",\"extension\\/payment\\/webmoney_wmu\",\"extension\\/payment\\/webmoney_wmv\",\"extension\\/payment\\/webmoney_wmz\",\"extension\\/payment\\/wechat_pay\",\"extension\\/payment\\/worldpay\",\"extension\\/report\\/customer_activity\",\"extension\\/report\\/customer_order\",\"extension\\/report\\/customer_reward\",\"extension\\/report\\/customer_search\",\"extension\\/report\\/customer_transaction\",\"extension\\/report\\/marketing\",\"extension\\/report\\/product_purchased\",\"extension\\/report\\/product_viewed\",\"extension\\/report\\/sale_coupon\",\"extension\\/report\\/sale_order\",\"extension\\/report\\/sale_return\",\"extension\\/report\\/sale_shipping\",\"extension\\/report\\/sale_tax\",\"extension\\/shipping\\/auspost\",\"extension\\/shipping\\/cdek\",\"extension\\/shipping\\/citylink\",\"extension\\/shipping\\/ec_ship\",\"extension\\/shipping\\/fedex\",\"extension\\/shipping\\/flat\",\"extension\\/shipping\\/free\",\"extension\\/shipping\\/item\",\"extension\\/shipping\\/parcelforce_48\",\"extension\\/shipping\\/pickup\",\"extension\\/shipping\\/royal_mail\",\"extension\\/shipping\\/ups\",\"extension\\/shipping\\/usps\",\"extension\\/shipping\\/weight\",\"extension\\/shipping\\/xshippingpro\",\"extension\\/theme\\/default\",\"extension\\/total\\/cdek\",\"extension\\/total\\/coupon\",\"extension\\/total\\/credit\",\"extension\\/total\\/handling\",\"extension\\/total\\/klarna_fee\",\"extension\\/total\\/low_order_fee\",\"extension\\/total\\/reward\",\"extension\\/total\\/shipping\",\"extension\\/total\\/sub_total\",\"extension\\/total\\/tax\",\"extension\\/total\\/total\",\"extension\\/total\\/voucher\",\"localisation\\/country\",\"localisation\\/currency\",\"localisation\\/geo_zone\",\"localisation\\/language\",\"localisation\\/length_class\",\"localisation\\/location\",\"localisation\\/order_status\",\"localisation\\/return_action\",\"localisation\\/return_reason\",\"localisation\\/return_status\",\"localisation\\/stock_status\",\"localisation\\/tax_class\",\"localisation\\/tax_rate\",\"localisation\\/weight_class\",\"localisation\\/zone\",\"mail\\/affiliate\",\"mail\\/customer\",\"mail\\/forgotten\",\"mail\\/return\",\"mail\\/reward\",\"mail\\/transaction\",\"marketing\\/contact\",\"marketing\\/coupon\",\"marketing\\/marketing\",\"marketplace\\/api\",\"marketplace\\/event\",\"marketplace\\/extension\",\"marketplace\\/install\",\"marketplace\\/installer\",\"marketplace\\/marketplace\",\"marketplace\\/modification\",\"marketplace\\/openbay\",\"marketplace\\/opencartforum\",\"report\\/online\",\"report\\/report\",\"report\\/statistics\",\"sale\\/order\",\"sale\\/recurring\",\"sale\\/return\",\"sale\\/voucher\",\"sale\\/voucher_theme\",\"search\\/search\",\"setting\\/ecos\",\"setting\\/setting\",\"setting\\/store\",\"startup\\/error\",\"startup\\/event\",\"startup\\/login\",\"startup\\/permission\",\"startup\\/router\",\"startup\\/sass\",\"startup\\/startup\",\"tool\\/backup\",\"tool\\/cdektool\",\"tool\\/log\",\"tool\\/upload\",\"user\\/api\",\"user\\/user\",\"user\\/user_permission\",\"module\\/cdek_integrator\",\"payment\\/cod_cdek\"]}');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_voucher`
--

CREATE TABLE `oc_voucher` (
  `voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `from_name` varchar(64) NOT NULL,
  `from_email` varchar(96) NOT NULL,
  `to_name` varchar(64) NOT NULL,
  `to_email` varchar(96) NOT NULL,
  `voucher_theme_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_voucher_history`
--

CREATE TABLE `oc_voucher_history` (
  `voucher_history_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_voucher_theme`
--

CREATE TABLE `oc_voucher_theme` (
  `voucher_theme_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_voucher_theme`
--

INSERT INTO `oc_voucher_theme` (`voucher_theme_id`, `image`) VALUES
(6, 'catalog/demo/apple_logo.jpg'),
(7, 'catalog/demo/gift-voucher-birthday.jpg'),
(8, 'catalog/demo/canon_eos_5d_2.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_voucher_theme_description`
--

CREATE TABLE `oc_voucher_theme_description` (
  `voucher_theme_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_voucher_theme_description`
--

INSERT INTO `oc_voucher_theme_description` (`voucher_theme_id`, `language_id`, `name`) VALUES
(6, 1, 'Christmas'),
(7, 1, 'Birthday'),
(8, 1, 'General');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_warehouse`
--

CREATE TABLE `oc_warehouse` (
  `warehouse_id` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT 0,
  `working_hours` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_warehouse_description`
--

CREATE TABLE `oc_warehouse_description` (
  `warehouse_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_warehouse_product`
--

CREATE TABLE `oc_warehouse_product` (
  `warehouse_product_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_value_id` int(11) DEFAULT NULL,
  `quantity` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_weight_class`
--

CREATE TABLE `oc_weight_class` (
  `weight_class_id` int(11) NOT NULL,
  `value` decimal(15,8) NOT NULL DEFAULT 0.00000000
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_weight_class`
--

INSERT INTO `oc_weight_class` (`weight_class_id`, `value`) VALUES
(1, '1.00000000'),
(2, '1000.00000000'),
(5, '2.20460000'),
(6, '35.27400000');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_weight_class_description`
--

CREATE TABLE `oc_weight_class_description` (
  `weight_class_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_weight_class_description`
--

INSERT INTO `oc_weight_class_description` (`weight_class_id`, `language_id`, `title`, `unit`) VALUES
(1, 1, 'Килограммы', 'кг'),
(1, 2, 'Kilogram', 'kg'),
(2, 1, 'Граммы', 'г'),
(2, 2, 'Gram', 'g'),
(5, 1, 'Фунты', 'lb'),
(5, 2, 'Pound', 'lb'),
(6, 1, 'Унции', 'oz'),
(6, 2, 'Ounce', 'oz');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_xshippingpro`
--

CREATE TABLE `oc_xshippingpro` (
  `id` int(8) NOT NULL,
  `method_data` mediumtext DEFAULT NULL,
  `tab_id` int(8) DEFAULT NULL,
  `sort_order` int(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_xshippingpro`
--

INSERT INTO `oc_xshippingpro` (`id`, `method_data`, `tab_id`, `sort_order`) VALUES
(1, '{\"display\":\"\\u0414\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430 \\u043f\\u043e \\u0425\\u0430\\u0431\\u0430\\u0440\\u043e\\u0432\\u0441\\u043a\\u0443\",\"name\":{\"1\":\"\\u0414\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430 \\u043f\\u043e \\u0425\\u0430\\u0431\\u0430\\u0440\\u043e\\u0432\\u0441\\u043a\\u0443\"},\"mask\":{\"1\":\"\"},\"desc\":{\"1\":\"\"},\"error\":{\"1\":\"\"},\"tax_class_id\":\"0\",\"logo\":\"\",\"sort_order\":\"\",\"status\":\"1\",\"group\":\"0\",\"store_all\":\"1\",\"geo_zone_all\":\"1\",\"customer_group_all\":\"1\",\"country_all\":\"1\",\"zone_all\":\"1\",\"currency_all\":\"1\",\"payment_all\":\"1\",\"customer\":\"1\",\"customer_rule\":\"inclusive\",\"city_all\":\"1\",\"city\":\"\",\"city_rule\":\"inclusive\",\"postal_all\":\"1\",\"postal\":\"\",\"postal_rule\":\"inclusive\",\"coupon_all\":\"1\",\"coupon\":\"\",\"coupon_rule\":\"inclusive\",\"days_all\":\"1\",\"time_start\":\"\",\"time_end\":\"\",\"date_start\":\"\",\"date_end\":\"\",\"category\":\"\",\"product\":\"\",\"option\":\"\",\"manufacturer_rule\":\"\",\"location_rule\":\"\",\"product_or\":\"0\",\"rate_type\":\"grand_shipping\",\"dimensional_factor\":\"500\",\"cost\":\"\",\"ranges\":[{\"product_id\":\"\",\"start\":\"0\",\"end\":\"2500\",\"cost\":\"250\",\"block\":\"0\",\"partial\":\"0\",\"type\":\"quantity\"},{\"product_id\":\"\",\"start\":\"2500\",\"end\":\"9999999999999999999\",\"cost\":\"0\",\"block\":\"0\",\"partial\":\"0\",\"type\":\"quantity\"}],\"additional\":\"\",\"additional_per\":\"\",\"additional_limit\":\"\",\"order_total_start\":\"\",\"order_total_end\":\"\",\"weight_start\":\"\",\"weight_end\":\"\",\"quantity_start\":\"\",\"quantity_end\":\"\",\"max_length\":\"\",\"max_width\":\"\",\"max_height\":\"\",\"cart_adjust\":\"0\",\"rate_final\":\"single\",\"rate_percent\":\"sub\",\"rate_min\":\"\",\"rate_max\":\"\",\"rate_add\":\"\",\"equation\":\"\"}', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_zone`
--

CREATE TABLE `oc_zone` (
  `zone_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(32) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_zone`
--

INSERT INTO `oc_zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(1, 1, 'Badakhshan', 'BDS', 1),
(2, 1, 'Badghis', 'BDG', 1),
(3, 1, 'Baghlan', 'BGL', 1),
(4, 1, 'Balkh', 'BAL', 1),
(5, 1, 'Bamian', 'BAM', 1),
(6, 1, 'Farah', 'FRA', 1),
(7, 1, 'Faryab', 'FYB', 1),
(8, 1, 'Ghazni', 'GHA', 1),
(9, 1, 'Ghowr', 'GHO', 1),
(10, 1, 'Helmand', 'HEL', 1),
(11, 1, 'Herat', 'HER', 1),
(12, 1, 'Jowzjan', 'JOW', 1),
(13, 1, 'Kabul', 'KAB', 1),
(14, 1, 'Kandahar', 'KAN', 1),
(15, 1, 'Kapisa', 'KAP', 1),
(16, 1, 'Khost', 'KHO', 1),
(17, 1, 'Konar', 'KNR', 1),
(18, 1, 'Kondoz', 'KDZ', 1),
(19, 1, 'Laghman', 'LAG', 1),
(20, 1, 'Lowgar', 'LOW', 1),
(21, 1, 'Nangrahar', 'NAN', 1),
(22, 1, 'Nimruz', 'NIM', 1),
(23, 1, 'Nurestan', 'NUR', 1),
(24, 1, 'Oruzgan', 'ORU', 1),
(25, 1, 'Paktia', 'PIA', 1),
(26, 1, 'Paktika', 'PKA', 1),
(27, 1, 'Parwan', 'PAR', 1),
(28, 1, 'Samangan', 'SAM', 1),
(29, 1, 'Sar-e Pol', 'SAR', 1),
(30, 1, 'Takhar', 'TAK', 1),
(31, 1, 'Wardak', 'WAR', 1),
(32, 1, 'Zabol', 'ZAB', 1),
(33, 2, 'Berat', 'BR', 1),
(34, 2, 'Bulqize', 'BU', 1),
(35, 2, 'Delvine', 'DL', 1),
(36, 2, 'Devoll', 'DV', 1),
(37, 2, 'Diber', 'DI', 1),
(38, 2, 'Durres', 'DR', 1),
(39, 2, 'Elbasan', 'EL', 1),
(40, 2, 'Kolonje', 'ER', 1),
(41, 2, 'Fier', 'FR', 1),
(42, 2, 'Gjirokaster', 'GJ', 1),
(43, 2, 'Gramsh', 'GR', 1),
(44, 2, 'Has', 'HA', 1),
(45, 2, 'Kavaje', 'KA', 1),
(46, 2, 'Kurbin', 'KB', 1),
(47, 2, 'Kucove', 'KC', 1),
(48, 2, 'Korce', 'KO', 1),
(49, 2, 'Kruje', 'KR', 1),
(50, 2, 'Kukes', 'KU', 1),
(51, 2, 'Librazhd', 'LB', 1),
(52, 2, 'Lezhe', 'LE', 1),
(53, 2, 'Lushnje', 'LU', 1),
(54, 2, 'Malesi e Madhe', 'MM', 1),
(55, 2, 'Mallakaster', 'MK', 1),
(56, 2, 'Mat', 'MT', 1),
(57, 2, 'Mirdite', 'MR', 1),
(58, 2, 'Peqin', 'PQ', 1),
(59, 2, 'Permet', 'PR', 1),
(60, 2, 'Pogradec', 'PG', 1),
(61, 2, 'Puke', 'PU', 1),
(62, 2, 'Shkoder', 'SH', 1),
(63, 2, 'Skrapar', 'SK', 1),
(64, 2, 'Sarande', 'SR', 1),
(65, 2, 'Tepelene', 'TE', 1),
(66, 2, 'Tropoje', 'TP', 1),
(67, 2, 'Tirane', 'TR', 1),
(68, 2, 'Vlore', 'VL', 1),
(69, 3, 'Adrar', 'ADR', 1),
(70, 3, 'Ain Defla', 'ADE', 1),
(71, 3, 'Ain Temouchent', 'ATE', 1),
(72, 3, 'Alger', 'ALG', 1),
(73, 3, 'Annaba', 'ANN', 1),
(74, 3, 'Batna', 'BAT', 1),
(75, 3, 'Bechar', 'BEC', 1),
(76, 3, 'Bejaia', 'BEJ', 1),
(77, 3, 'Biskra', 'BIS', 1),
(78, 3, 'Blida', 'BLI', 1),
(79, 3, 'Bordj Bou Arreridj', 'BBA', 1),
(80, 3, 'Bouira', 'BOA', 1),
(81, 3, 'Boumerdes', 'BMD', 1),
(82, 3, 'Chlef', 'CHL', 1),
(83, 3, 'Constantine', 'CON', 1),
(84, 3, 'Djelfa', 'DJE', 1),
(85, 3, 'El Bayadh', 'EBA', 1),
(86, 3, 'El Oued', 'EOU', 1),
(87, 3, 'El Tarf', 'ETA', 1),
(88, 3, 'Ghardaia', 'GHA', 1),
(89, 3, 'Guelma', 'GUE', 1),
(90, 3, 'Illizi', 'ILL', 1),
(91, 3, 'Jijel', 'JIJ', 1),
(92, 3, 'Khenchela', 'KHE', 1),
(93, 3, 'Laghouat', 'LAG', 1),
(94, 3, 'Muaskar', 'MUA', 1),
(95, 3, 'Medea', 'MED', 1),
(96, 3, 'Mila', 'MIL', 1),
(97, 3, 'Mostaganem', 'MOS', 1),
(98, 3, 'M\'Sila', 'MSI', 1),
(99, 3, 'Naama', 'NAA', 1),
(100, 3, 'Oran', 'ORA', 1),
(101, 3, 'Ouargla', 'OUA', 1),
(102, 3, 'Oum el-Bouaghi', 'OEB', 1),
(103, 3, 'Relizane', 'REL', 1),
(104, 3, 'Saida', 'SAI', 1),
(105, 3, 'Setif', 'SET', 1),
(106, 3, 'Sidi Bel Abbes', 'SBA', 1),
(107, 3, 'Skikda', 'SKI', 1),
(108, 3, 'Souk Ahras', 'SAH', 1),
(109, 3, 'Tamanghasset', 'TAM', 1),
(110, 3, 'Tebessa', 'TEB', 1),
(111, 3, 'Tiaret', 'TIA', 1),
(112, 3, 'Tindouf', 'TIN', 1),
(113, 3, 'Tipaza', 'TIP', 1),
(114, 3, 'Tissemsilt', 'TIS', 1),
(115, 3, 'Tizi Ouzou', 'TOU', 1),
(116, 3, 'Tlemcen', 'TLE', 1),
(117, 4, 'Eastern', 'E', 1),
(118, 4, 'Manu\'a', 'M', 1),
(119, 4, 'Rose Island', 'R', 1),
(120, 4, 'Swains Island', 'S', 1),
(121, 4, 'Western', 'W', 1),
(122, 5, 'Andorra la Vella', 'ALV', 1),
(123, 5, 'Canillo', 'CAN', 1),
(124, 5, 'Encamp', 'ENC', 1),
(125, 5, 'Escaldes-Engordany', 'ESE', 1),
(126, 5, 'La Massana', 'LMA', 1),
(127, 5, 'Ordino', 'ORD', 1),
(128, 5, 'Sant Julia de Loria', 'SJL', 1),
(129, 6, 'Bengo', 'BGO', 1),
(130, 6, 'Benguela', 'BGU', 1),
(131, 6, 'Bie', 'BIE', 1),
(132, 6, 'Cabinda', 'CAB', 1),
(133, 6, 'Cuando-Cubango', 'CCU', 1),
(134, 6, 'Cuanza Norte', 'CNO', 1),
(135, 6, 'Cuanza Sul', 'CUS', 1),
(136, 6, 'Cunene', 'CNN', 1),
(137, 6, 'Huambo', 'HUA', 1),
(138, 6, 'Huila', 'HUI', 1),
(139, 6, 'Luanda', 'LUA', 1),
(140, 6, 'Lunda Norte', 'LNO', 1),
(141, 6, 'Lunda Sul', 'LSU', 1),
(142, 6, 'Malange', 'MAL', 1),
(143, 6, 'Moxico', 'MOX', 1),
(144, 6, 'Namibe', 'NAM', 1),
(145, 6, 'Uige', 'UIG', 1),
(146, 6, 'Zaire', 'ZAI', 1),
(147, 9, 'Saint George', 'ASG', 1),
(148, 9, 'Saint John', 'ASJ', 1),
(149, 9, 'Saint Mary', 'ASM', 1),
(150, 9, 'Saint Paul', 'ASL', 1),
(151, 9, 'Saint Peter', 'ASR', 1),
(152, 9, 'Saint Philip', 'ASH', 1),
(153, 9, 'Barbuda', 'BAR', 1),
(154, 9, 'Redonda', 'RED', 1),
(155, 10, 'Antartida e Islas del Atlantico', 'AN', 1),
(156, 10, 'Buenos Aires', 'BA', 1),
(157, 10, 'Catamarca', 'CA', 1),
(158, 10, 'Chaco', 'CH', 1),
(159, 10, 'Chubut', 'CU', 1),
(160, 10, 'Cordoba', 'CO', 1),
(161, 10, 'Corrientes', 'CR', 1),
(162, 10, 'Distrito Federal', 'DF', 1),
(163, 10, 'Entre Rios', 'ER', 1),
(164, 10, 'Formosa', 'FO', 1),
(165, 10, 'Jujuy', 'JU', 1),
(166, 10, 'La Pampa', 'LP', 1),
(167, 10, 'La Rioja', 'LR', 1),
(168, 10, 'Mendoza', 'ME', 1),
(169, 10, 'Misiones', 'MI', 1),
(170, 10, 'Neuquen', 'NE', 1),
(171, 10, 'Rio Negro', 'RN', 1),
(172, 10, 'Salta', 'SA', 1),
(173, 10, 'San Juan', 'SJ', 1),
(174, 10, 'San Luis', 'SL', 1),
(175, 10, 'Santa Cruz', 'SC', 1),
(176, 10, 'Santa Fe', 'SF', 1),
(177, 10, 'Santiago del Estero', 'SD', 1),
(178, 10, 'Tierra del Fuego', 'TF', 1),
(179, 10, 'Tucuman', 'TU', 1),
(180, 11, 'Арагацотн', 'AGT', 1),
(181, 11, 'Арарат', 'ARR', 1),
(182, 11, 'Армавир', 'ARM', 1),
(183, 11, 'Гегаркуник', 'GEG', 1),
(184, 11, 'Котайк', 'KOT', 1),
(185, 11, 'Лори', 'LOR', 1),
(186, 11, 'Ширак', 'SHI', 1),
(187, 11, 'Сюник', 'SYU', 1),
(188, 11, 'Тавуш', 'TAV', 1),
(189, 11, 'Вайоц Дзор', 'VAY', 1),
(190, 11, 'Ереван', 'YER', 1),
(191, 13, 'Australian Capital Territory', 'ACT', 1),
(192, 13, 'New South Wales', 'NSW', 1),
(193, 13, 'Northern Territory', 'NT', 1),
(194, 13, 'Queensland', 'QLD', 1),
(195, 13, 'South Australia', 'SA', 1),
(196, 13, 'Tasmania', 'TAS', 1),
(197, 13, 'Victoria', 'VIC', 1),
(198, 13, 'Western Australia', 'WA', 1),
(199, 14, 'Burgenland', 'BUR', 1),
(200, 14, 'Kärnten', 'KAR', 1),
(201, 14, 'Nieder&ouml;sterreich', 'NOS', 1),
(202, 14, 'Ober&ouml;sterreich', 'OOS', 1),
(203, 14, 'Salzburg', 'SAL', 1),
(204, 14, 'Steiermark', 'STE', 1),
(205, 14, 'Tirol', 'TIR', 1),
(206, 14, 'Vorarlberg', 'VOR', 1),
(207, 14, 'Wien', 'WIE', 1),
(208, 15, 'Ali Bayramli', 'AB', 1),
(209, 15, 'Abseron', 'ABS', 1),
(210, 15, 'AgcabAdi', 'AGC', 1),
(211, 15, 'Agdam', 'AGM', 1),
(212, 15, 'Agdas', 'AGS', 1),
(213, 15, 'Agstafa', 'AGA', 1),
(214, 15, 'Agsu', 'AGU', 1),
(215, 15, 'Astara', 'AST', 1),
(216, 15, 'Baki', 'BA', 1),
(217, 15, 'BabAk', 'BAB', 1),
(218, 15, 'BalakAn', 'BAL', 1),
(219, 15, 'BArdA', 'BAR', 1),
(220, 15, 'Beylaqan', 'BEY', 1),
(221, 15, 'Bilasuvar', 'BIL', 1),
(222, 15, 'Cabrayil', 'CAB', 1),
(223, 15, 'Calilabab', 'CAL', 1),
(224, 15, 'Culfa', 'CUL', 1),
(225, 15, 'Daskasan', 'DAS', 1),
(226, 15, 'Davaci', 'DAV', 1),
(227, 15, 'Fuzuli', 'FUZ', 1),
(228, 15, 'Ganca', 'GA', 1),
(229, 15, 'Gadabay', 'GAD', 1),
(230, 15, 'Goranboy', 'GOR', 1),
(231, 15, 'Goycay', 'GOY', 1),
(232, 15, 'Haciqabul', 'HAC', 1),
(233, 15, 'Imisli', 'IMI', 1),
(234, 15, 'Ismayilli', 'ISM', 1),
(235, 15, 'Kalbacar', 'KAL', 1),
(236, 15, 'Kurdamir', 'KUR', 1),
(237, 15, 'Lankaran', 'LA', 1),
(238, 15, 'Lacin', 'LAC', 1),
(239, 15, 'Lankaran', 'LAN', 1),
(240, 15, 'Lerik', 'LER', 1),
(241, 15, 'Masalli', 'MAS', 1),
(242, 15, 'Mingacevir', 'MI', 1),
(243, 15, 'Naftalan', 'NA', 1),
(244, 15, 'Neftcala', 'NEF', 1),
(245, 15, 'Oguz', 'OGU', 1),
(246, 15, 'Ordubad', 'ORD', 1),
(247, 15, 'Qabala', 'QAB', 1),
(248, 15, 'Qax', 'QAX', 1),
(249, 15, 'Qazax', 'QAZ', 1),
(250, 15, 'Qobustan', 'QOB', 1),
(251, 15, 'Quba', 'QBA', 1),
(252, 15, 'Qubadli', 'QBI', 1),
(253, 15, 'Qusar', 'QUS', 1),
(254, 15, 'Saki', 'SA', 1),
(255, 15, 'Saatli', 'SAT', 1),
(256, 15, 'Sabirabad', 'SAB', 1),
(257, 15, 'Sadarak', 'SAD', 1),
(258, 15, 'Sahbuz', 'SAH', 1),
(259, 15, 'Saki', 'SAK', 1),
(260, 15, 'Salyan', 'SAL', 1),
(261, 15, 'Sumqayit', 'SM', 1),
(262, 15, 'Samaxi', 'SMI', 1),
(263, 15, 'Samkir', 'SKR', 1),
(264, 15, 'Samux', 'SMX', 1),
(265, 15, 'Sarur', 'SAR', 1),
(266, 15, 'Siyazan', 'SIY', 1),
(267, 15, 'Susa', 'SS', 1),
(268, 15, 'Susa', 'SUS', 1),
(269, 15, 'Tartar', 'TAR', 1),
(270, 15, 'Tovuz', 'TOV', 1),
(271, 15, 'Ucar', 'UCA', 1),
(272, 15, 'Xankandi', 'XA', 1),
(273, 15, 'Xacmaz', 'XAC', 1),
(274, 15, 'Xanlar', 'XAN', 1),
(275, 15, 'Xizi', 'XIZ', 1),
(276, 15, 'Xocali', 'XCI', 1),
(277, 15, 'Xocavand', 'XVD', 1),
(278, 15, 'Yardimli', 'YAR', 1),
(279, 15, 'Yevlax', 'YEV', 1),
(280, 15, 'Zangilan', 'ZAN', 1),
(281, 15, 'Zaqatala', 'ZAQ', 1),
(282, 15, 'Zardab', 'ZAR', 1),
(283, 15, 'Naxcivan', 'NX', 1),
(284, 16, 'Acklins', 'ACK', 1),
(285, 16, 'Berry Islands', 'BER', 1),
(286, 16, 'Bimini', 'BIM', 1),
(287, 16, 'Black Point', 'BLK', 1),
(288, 16, 'Cat Island', 'CAT', 1),
(289, 16, 'Central Abaco', 'CAB', 1),
(290, 16, 'Central Andros', 'CAN', 1),
(291, 16, 'Central Eleuthera', 'CEL', 1),
(292, 16, 'City of Freeport', 'FRE', 1),
(293, 16, 'Crooked Island', 'CRO', 1),
(294, 16, 'East Grand Bahama', 'EGB', 1),
(295, 16, 'Exuma', 'EXU', 1),
(296, 16, 'Grand Cay', 'GRD', 1),
(297, 16, 'Harbour Island', 'HAR', 1),
(298, 16, 'Hope Town', 'HOP', 1),
(299, 16, 'Inagua', 'INA', 1),
(300, 16, 'Long Island', 'LNG', 1),
(301, 16, 'Mangrove Cay', 'MAN', 1),
(302, 16, 'Mayaguana', 'MAY', 1),
(303, 16, 'Moore\'s Island', 'MOO', 1),
(304, 16, 'North Abaco', 'NAB', 1),
(305, 16, 'North Andros', 'NAN', 1),
(306, 16, 'North Eleuthera', 'NEL', 1),
(307, 16, 'Ragged Island', 'RAG', 1),
(308, 16, 'Rum Cay', 'RUM', 1),
(309, 16, 'San Salvador', 'SAL', 1),
(310, 16, 'South Abaco', 'SAB', 1),
(311, 16, 'South Andros', 'SAN', 1),
(312, 16, 'South Eleuthera', 'SEL', 1),
(313, 16, 'Spanish Wells', 'SWE', 1),
(314, 16, 'West Grand Bahama', 'WGB', 1),
(315, 17, 'Capital', 'CAP', 1),
(316, 17, 'Central', 'CEN', 1),
(317, 17, 'Muharraq', 'MUH', 1),
(318, 17, 'Northern', 'NOR', 1),
(319, 17, 'Southern', 'SOU', 1),
(320, 18, 'Barisal', 'BAR', 1),
(321, 18, 'Chittagong', 'CHI', 1),
(322, 18, 'Dhaka', 'DHA', 1),
(323, 18, 'Khulna', 'KHU', 1),
(324, 18, 'Rajshahi', 'RAJ', 1),
(325, 18, 'Sylhet', 'SYL', 1),
(326, 19, 'Christ Church', 'CC', 1),
(327, 19, 'Saint Andrew', 'AND', 1),
(328, 19, 'Saint George', 'GEO', 1),
(329, 19, 'Saint James', 'JAM', 1),
(330, 19, 'Saint John', 'JOH', 1),
(331, 19, 'Saint Joseph', 'JOS', 1),
(332, 19, 'Saint Lucy', 'LUC', 1),
(333, 19, 'Saint Michael', 'MIC', 1),
(334, 19, 'Saint Peter', 'PET', 1),
(335, 19, 'Saint Philip', 'PHI', 1),
(336, 19, 'Saint Thomas', 'THO', 1),
(337, 20, 'Брест', 'BR', 1),
(338, 20, 'Гомель', 'HO', 1),
(339, 20, 'Минск', 'HM', 1),
(340, 20, 'Гродно', 'HR', 1),
(341, 20, 'Могилев', 'MA', 1),
(342, 20, 'Минская область', 'MI', 1),
(343, 20, 'Витебск', 'VI', 1),
(344, 21, 'Antwerpen', 'VAN', 1),
(345, 21, 'Brabant Wallon', 'WBR', 1),
(346, 21, 'Hainaut', 'WHT', 1),
(347, 21, 'Liège', 'WLG', 1),
(348, 21, 'Limburg', 'VLI', 1),
(349, 21, 'Luxembourg', 'WLX', 1),
(350, 21, 'Namur', 'WNA', 1),
(351, 21, 'Oost-Vlaanderen', 'VOV', 1),
(352, 21, 'Vlaams Brabant', 'VBR', 1),
(353, 21, 'West-Vlaanderen', 'VWV', 1),
(354, 22, 'Belize', 'BZ', 1),
(355, 22, 'Cayo', 'CY', 1),
(356, 22, 'Corozal', 'CR', 1),
(357, 22, 'Orange Walk', 'OW', 1),
(358, 22, 'Stann Creek', 'SC', 1),
(359, 22, 'Toledo', 'TO', 1),
(360, 23, 'Alibori', 'AL', 1),
(361, 23, 'Atakora', 'AK', 1),
(362, 23, 'Atlantique', 'AQ', 1),
(363, 23, 'Borgou', 'BO', 1),
(364, 23, 'Collines', 'CO', 1),
(365, 23, 'Donga', 'DO', 1),
(366, 23, 'Kouffo', 'KO', 1),
(367, 23, 'Littoral', 'LI', 1),
(368, 23, 'Mono', 'MO', 1),
(369, 23, 'Oueme', 'OU', 1),
(370, 23, 'Plateau', 'PL', 1),
(371, 23, 'Zou', 'ZO', 1),
(372, 24, 'Devonshire', 'DS', 1),
(373, 24, 'Hamilton City', 'HC', 1),
(374, 24, 'Hamilton', 'HA', 1),
(375, 24, 'Paget', 'PG', 1),
(376, 24, 'Pembroke', 'PB', 1),
(377, 24, 'Saint George City', 'GC', 1),
(378, 24, 'Saint George\'s', 'SG', 1),
(379, 24, 'Sandys', 'SA', 1),
(380, 24, 'Smith\'s', 'SM', 1),
(381, 24, 'Southampton', 'SH', 1),
(382, 24, 'Warwick', 'WA', 1),
(383, 25, 'Bumthang', 'BUM', 1),
(384, 25, 'Chukha', 'CHU', 1),
(385, 25, 'Dagana', 'DAG', 1),
(386, 25, 'Gasa', 'GAS', 1),
(387, 25, 'Haa', 'HAA', 1),
(388, 25, 'Lhuntse', 'LHU', 1),
(389, 25, 'Mongar', 'MON', 1),
(390, 25, 'Paro', 'PAR', 1),
(391, 25, 'Pemagatshel', 'PEM', 1),
(392, 25, 'Punakha', 'PUN', 1),
(393, 25, 'Samdrup Jongkhar', 'SJO', 1),
(394, 25, 'Samtse', 'SAT', 1),
(395, 25, 'Sarpang', 'SAR', 1),
(396, 25, 'Thimphu', 'THI', 1),
(397, 25, 'Trashigang', 'TRG', 1),
(398, 25, 'Trashiyangste', 'TRY', 1),
(399, 25, 'Trongsa', 'TRO', 1),
(400, 25, 'Tsirang', 'TSI', 1),
(401, 25, 'Wangdue Phodrang', 'WPH', 1),
(402, 25, 'Zhemgang', 'ZHE', 1),
(403, 26, 'Beni', 'BEN', 1),
(404, 26, 'Chuquisaca', 'CHU', 1),
(405, 26, 'Cochabamba', 'COC', 1),
(406, 26, 'La Paz', 'LPZ', 1),
(407, 26, 'Oruro', 'ORU', 1),
(408, 26, 'Pando', 'PAN', 1),
(409, 26, 'Potosi', 'POT', 1),
(410, 26, 'Santa Cruz', 'SCZ', 1),
(411, 26, 'Tarija', 'TAR', 1),
(412, 27, 'Brcko district', 'BRO', 1),
(413, 27, 'Unsko-Sanski Kanton', 'FUS', 1),
(414, 27, 'Posavski Kanton', 'FPO', 1),
(415, 27, 'Tuzlanski Kanton', 'FTU', 1),
(416, 27, 'Zenicko-Dobojski Kanton', 'FZE', 1),
(417, 27, 'Bosanskopodrinjski Kanton', 'FBP', 1),
(418, 27, 'Srednjebosanski Kanton', 'FSB', 1),
(419, 27, 'Hercegovacko-neretvanski Kanton', 'FHN', 1),
(420, 27, 'Zapadnohercegovacka Zupanija', 'FZH', 1),
(421, 27, 'Kanton Sarajevo', 'FSA', 1),
(422, 27, 'Zapadnobosanska', 'FZA', 1),
(423, 27, 'Banja Luka', 'SBL', 1),
(424, 27, 'Doboj', 'SDO', 1),
(425, 27, 'Bijeljina', 'SBI', 1),
(426, 27, 'Vlasenica', 'SVL', 1),
(427, 27, 'Sarajevo-Romanija or Sokolac', 'SSR', 1),
(428, 27, 'Foca', 'SFO', 1),
(429, 27, 'Trebinje', 'STR', 1),
(430, 28, 'Central', 'CE', 1),
(431, 28, 'Ghanzi', 'GH', 1),
(432, 28, 'Kgalagadi', 'KD', 1),
(433, 28, 'Kgatleng', 'KT', 1),
(434, 28, 'Kweneng', 'KW', 1),
(435, 28, 'Ngamiland', 'NG', 1),
(436, 28, 'North East', 'NE', 1),
(437, 28, 'North West', 'NW', 1),
(438, 28, 'South East', 'SE', 1),
(439, 28, 'Southern', 'SO', 1),
(440, 30, 'Acre', 'AC', 1),
(441, 30, 'Alagoas', 'AL', 1),
(442, 30, 'Amapá', 'AP', 1),
(443, 30, 'Amazonas', 'AM', 1),
(444, 30, 'Bahia', 'BA', 1),
(445, 30, 'Ceará', 'CE', 1),
(446, 30, 'Distrito Federal', 'DF', 1),
(447, 30, 'Espírito Santo', 'ES', 1),
(448, 30, 'Goiás', 'GO', 1),
(449, 30, 'Maranhão', 'MA', 1),
(450, 30, 'Mato Grosso', 'MT', 1),
(451, 30, 'Mato Grosso do Sul', 'MS', 1),
(452, 30, 'Minas Gerais', 'MG', 1),
(453, 30, 'Pará', 'PA', 1),
(454, 30, 'Paraíba', 'PB', 1),
(455, 30, 'Paraná', 'PR', 1),
(456, 30, 'Pernambuco', 'PE', 1),
(457, 30, 'Piauí', 'PI', 1),
(458, 30, 'Rio de Janeiro', 'RJ', 1),
(459, 30, 'Rio Grande do Norte', 'RN', 1),
(460, 30, 'Rio Grande do Sul', 'RS', 1),
(461, 30, 'Rondônia', 'RO', 1),
(462, 30, 'Roraima', 'RR', 1),
(463, 30, 'Santa Catarina', 'SC', 1),
(464, 30, 'São Paulo', 'SP', 1),
(465, 30, 'Sergipe', 'SE', 1),
(466, 30, 'Tocantins', 'TO', 1),
(467, 31, 'Peros Banhos', 'PB', 1),
(468, 31, 'Salomon Islands', 'SI', 1),
(469, 31, 'Nelsons Island', 'NI', 1),
(470, 31, 'Three Brothers', 'TB', 1),
(471, 31, 'Eagle Islands', 'EA', 1),
(472, 31, 'Danger Island', 'DI', 1),
(473, 31, 'Egmont Islands', 'EG', 1),
(474, 31, 'Diego Garcia', 'DG', 1),
(475, 32, 'Belait', 'BEL', 1),
(476, 32, 'Brunei and Muara', 'BRM', 1),
(477, 32, 'Temburong', 'TEM', 1),
(478, 32, 'Tutong', 'TUT', 1),
(479, 33, 'Blagoevgrad', '', 1),
(480, 33, 'Burgas', '', 1),
(481, 33, 'Dobrich', '', 1),
(482, 33, 'Gabrovo', '', 1),
(483, 33, 'Haskovo', '', 1),
(484, 33, 'Kardjali', '', 1),
(485, 33, 'Kyustendil', '', 1),
(486, 33, 'Lovech', '', 1),
(487, 33, 'Montana', '', 1),
(488, 33, 'Pazardjik', '', 1),
(489, 33, 'Pernik', '', 1),
(490, 33, 'Pleven', '', 1),
(491, 33, 'Plovdiv', '', 1),
(492, 33, 'Razgrad', '', 1),
(493, 33, 'Shumen', '', 1),
(494, 33, 'Silistra', '', 1),
(495, 33, 'Sliven', '', 1),
(496, 33, 'Smolyan', '', 1),
(497, 33, 'Sofia', '', 1),
(498, 33, 'Sofia - town', '', 1),
(499, 33, 'Stara Zagora', '', 1),
(500, 33, 'Targovishte', '', 1),
(501, 33, 'Varna', '', 1),
(502, 33, 'Veliko Tarnovo', '', 1),
(503, 33, 'Vidin', '', 1),
(504, 33, 'Vratza', '', 1),
(505, 33, 'Yambol', '', 1),
(506, 34, 'Bale', 'BAL', 1),
(507, 34, 'Bam', 'BAM', 1),
(508, 34, 'Banwa', 'BAN', 1),
(509, 34, 'Bazega', 'BAZ', 1),
(510, 34, 'Bougouriba', 'BOR', 1),
(511, 34, 'Boulgou', 'BLG', 1),
(512, 34, 'Boulkiemde', 'BOK', 1),
(513, 34, 'Comoe', 'COM', 1),
(514, 34, 'Ganzourgou', 'GAN', 1),
(515, 34, 'Gnagna', 'GNA', 1),
(516, 34, 'Gourma', 'GOU', 1),
(517, 34, 'Houet', 'HOU', 1),
(518, 34, 'Ioba', 'IOA', 1),
(519, 34, 'Kadiogo', 'KAD', 1),
(520, 34, 'Kenedougou', 'KEN', 1),
(521, 34, 'Komondjari', 'KOD', 1),
(522, 34, 'Kompienga', 'KOP', 1),
(523, 34, 'Kossi', 'KOS', 1),
(524, 34, 'Koulpelogo', 'KOL', 1),
(525, 34, 'Kouritenga', 'KOT', 1),
(526, 34, 'Kourweogo', 'KOW', 1),
(527, 34, 'Leraba', 'LER', 1),
(528, 34, 'Loroum', 'LOR', 1),
(529, 34, 'Mouhoun', 'MOU', 1),
(530, 34, 'Nahouri', 'NAH', 1),
(531, 34, 'Namentenga', 'NAM', 1),
(532, 34, 'Nayala', 'NAY', 1),
(533, 34, 'Noumbiel', 'NOU', 1),
(534, 34, 'Oubritenga', 'OUB', 1),
(535, 34, 'Oudalan', 'OUD', 1),
(536, 34, 'Passore', 'PAS', 1),
(537, 34, 'Poni', 'PON', 1),
(538, 34, 'Sanguie', 'SAG', 1),
(539, 34, 'Sanmatenga', 'SAM', 1),
(540, 34, 'Seno', 'SEN', 1),
(541, 34, 'Sissili', 'SIS', 1),
(542, 34, 'Soum', 'SOM', 1),
(543, 34, 'Sourou', 'SOR', 1),
(544, 34, 'Tapoa', 'TAP', 1),
(545, 34, 'Tuy', 'TUY', 1),
(546, 34, 'Yagha', 'YAG', 1),
(547, 34, 'Yatenga', 'YAT', 1),
(548, 34, 'Ziro', 'ZIR', 1),
(549, 34, 'Zondoma', 'ZOD', 1),
(550, 34, 'Zoundweogo', 'ZOW', 1),
(551, 35, 'Bubanza', 'BB', 1),
(552, 35, 'Bujumbura', 'BJ', 1),
(553, 35, 'Bururi', 'BR', 1),
(554, 35, 'Cankuzo', 'CA', 1),
(555, 35, 'Cibitoke', 'CI', 1),
(556, 35, 'Gitega', 'GI', 1),
(557, 35, 'Karuzi', 'KR', 1),
(558, 35, 'Kayanza', 'KY', 1),
(559, 35, 'Kirundo', 'KI', 1),
(560, 35, 'Makamba', 'MA', 1),
(561, 35, 'Muramvya', 'MU', 1),
(562, 35, 'Muyinga', 'MY', 1),
(563, 35, 'Mwaro', 'MW', 1),
(564, 35, 'Ngozi', 'NG', 1),
(565, 35, 'Rutana', 'RT', 1),
(566, 35, 'Ruyigi', 'RY', 1),
(567, 36, 'Phnom Penh', 'PP', 1),
(568, 36, 'Preah Seihanu (Kompong Som or Sihanoukville)', 'PS', 1),
(569, 36, 'Pailin', 'PA', 1),
(570, 36, 'Keb', 'KB', 1),
(571, 36, 'Banteay Meanchey', 'BM', 1),
(572, 36, 'Battambang', 'BA', 1),
(573, 36, 'Kampong Cham', 'KM', 1),
(574, 36, 'Kampong Chhnang', 'KN', 1),
(575, 36, 'Kampong Speu', 'KU', 1),
(576, 36, 'Kampong Som', 'KO', 1),
(577, 36, 'Kampong Thom', 'KT', 1),
(578, 36, 'Kampot', 'KP', 1),
(579, 36, 'Kandal', 'KL', 1),
(580, 36, 'Kaoh Kong', 'KK', 1),
(581, 36, 'Kratie', 'KR', 1),
(582, 36, 'Mondul Kiri', 'MK', 1),
(583, 36, 'Oddar Meancheay', 'OM', 1),
(584, 36, 'Pursat', 'PU', 1),
(585, 36, 'Preah Vihear', 'PR', 1),
(586, 36, 'Prey Veng', 'PG', 1),
(587, 36, 'Ratanak Kiri', 'RK', 1),
(588, 36, 'Siemreap', 'SI', 1),
(589, 36, 'Stung Treng', 'ST', 1),
(590, 36, 'Svay Rieng', 'SR', 1),
(591, 36, 'Takeo', 'TK', 1),
(592, 37, 'Adamawa (Adamaoua)', 'ADA', 1),
(593, 37, 'Centre', 'CEN', 1),
(594, 37, 'East (Est)', 'EST', 1),
(595, 37, 'Extreme North (Extreme-Nord)', 'EXN', 1),
(596, 37, 'Littoral', 'LIT', 1),
(597, 37, 'North (Nord)', 'NOR', 1),
(598, 37, 'Northwest (Nord-Ouest)', 'NOT', 1),
(599, 37, 'West (Ouest)', 'OUE', 1),
(600, 37, 'South (Sud)', 'SUD', 1),
(601, 37, 'Southwest (Sud-Ouest).', 'SOU', 1),
(602, 38, 'Alberta', 'AB', 1),
(603, 38, 'British Columbia', 'BC', 1),
(604, 38, 'Manitoba', 'MB', 1),
(605, 38, 'New Brunswick', 'NB', 1),
(606, 38, 'Newfoundland and Labrador', 'NL', 1),
(607, 38, 'Northwest Territories', 'NT', 1),
(608, 38, 'Nova Scotia', 'NS', 1),
(609, 38, 'Nunavut', 'NU', 1),
(610, 38, 'Ontario', 'ON', 1),
(611, 38, 'Prince Edward Island', 'PE', 1),
(612, 38, 'Qu&eacute;bec', 'QC', 1),
(613, 38, 'Saskatchewan', 'SK', 1),
(614, 38, 'Yukon Territory', 'YT', 1),
(615, 39, 'Boa Vista', 'BV', 1),
(616, 39, 'Brava', 'BR', 1),
(617, 39, 'Calheta de Sao Miguel', 'CS', 1),
(618, 39, 'Maio', 'MA', 1),
(619, 39, 'Mosteiros', 'MO', 1),
(620, 39, 'Paul', 'PA', 1),
(621, 39, 'Porto Novo', 'PN', 1),
(622, 39, 'Praia', 'PR', 1),
(623, 39, 'Ribeira Grande', 'RG', 1),
(624, 39, 'Sal', 'SL', 1),
(625, 39, 'Santa Catarina', 'CA', 1),
(626, 39, 'Santa Cruz', 'CR', 1),
(627, 39, 'Sao Domingos', 'SD', 1),
(628, 39, 'Sao Filipe', 'SF', 1),
(629, 39, 'Sao Nicolau', 'SN', 1),
(630, 39, 'Sao Vicente', 'SV', 1),
(631, 39, 'Tarrafal', 'TA', 1),
(632, 40, 'Creek', 'CR', 1),
(633, 40, 'Eastern', 'EA', 1),
(634, 40, 'Midland', 'ML', 1),
(635, 40, 'South Town', 'ST', 1),
(636, 40, 'Spot Bay', 'SP', 1),
(637, 40, 'Stake Bay', 'SK', 1),
(638, 40, 'West End', 'WD', 1),
(639, 40, 'Western', 'WN', 1),
(640, 41, 'Bamingui-Bangoran', 'BBA', 1),
(641, 41, 'Basse-Kotto', 'BKO', 1),
(642, 41, 'Haute-Kotto', 'HKO', 1),
(643, 41, 'Haut-Mbomou', 'HMB', 1),
(644, 41, 'Kemo', 'KEM', 1),
(645, 41, 'Lobaye', 'LOB', 1),
(646, 41, 'Mambere-KadeÔ', 'MKD', 1),
(647, 41, 'Mbomou', 'MBO', 1),
(648, 41, 'Nana-Mambere', 'NMM', 1),
(649, 41, 'Ombella-M\'Poko', 'OMP', 1),
(650, 41, 'Ouaka', 'OUK', 1),
(651, 41, 'Ouham', 'OUH', 1),
(652, 41, 'Ouham-Pende', 'OPE', 1),
(653, 41, 'Vakaga', 'VAK', 1),
(654, 41, 'Nana-Grebizi', 'NGR', 1),
(655, 41, 'Sangha-Mbaere', 'SMB', 1),
(656, 41, 'Bangui', 'BAN', 1),
(657, 42, 'Batha', 'BA', 1),
(658, 42, 'Biltine', 'BI', 1),
(659, 42, 'Borkou-Ennedi-Tibesti', 'BE', 1),
(660, 42, 'Chari-Baguirmi', 'CB', 1),
(661, 42, 'Guera', 'GU', 1),
(662, 42, 'Kanem', 'KA', 1),
(663, 42, 'Lac', 'LA', 1),
(664, 42, 'Logone Occidental', 'LC', 1),
(665, 42, 'Logone Oriental', 'LR', 1),
(666, 42, 'Mayo-Kebbi', 'MK', 1),
(667, 42, 'Moyen-Chari', 'MC', 1),
(668, 42, 'Ouaddai', 'OU', 1),
(669, 42, 'Salamat', 'SA', 1),
(670, 42, 'Tandjile', 'TA', 1),
(671, 43, 'Aisen del General Carlos Ibanez', 'AI', 1),
(672, 43, 'Antofagasta', 'AN', 1),
(673, 43, 'Araucania', 'AR', 1),
(674, 43, 'Atacama', 'AT', 1),
(675, 43, 'Bio-Bio', 'BI', 1),
(676, 43, 'Coquimbo', 'CO', 1),
(677, 43, 'Libertador General Bernardo O\'Higgins', 'LI', 1),
(678, 43, 'Los Lagos', 'LL', 1),
(679, 43, 'Magallanes y de la Antartica Chilena', 'MA', 1),
(680, 43, 'Maule', 'ML', 1),
(681, 43, 'Region Metropolitana', 'RM', 1),
(682, 43, 'Tarapaca', 'TA', 1),
(683, 43, 'Valparaiso', 'VS', 1),
(684, 44, 'Anhui', 'AN', 1),
(685, 44, 'Beijing', 'BE', 1),
(686, 44, 'Chongqing', 'CH', 1),
(687, 44, 'Fujian', 'FU', 1),
(688, 44, 'Gansu', 'GA', 1),
(689, 44, 'Guangdong', 'GU', 1),
(690, 44, 'Guangxi', 'GX', 1),
(691, 44, 'Guizhou', 'GZ', 1),
(692, 44, 'Hainan', 'HA', 1),
(693, 44, 'Hebei', 'HB', 1),
(694, 44, 'Heilongjiang', 'HL', 1),
(695, 44, 'Henan', 'HE', 1),
(696, 44, 'Hong Kong', 'HK', 1),
(697, 44, 'Hubei', 'HU', 1),
(698, 44, 'Hunan', 'HN', 1),
(699, 44, 'Inner Mongolia', 'IM', 1),
(700, 44, 'Jiangsu', 'JI', 1),
(701, 44, 'Jiangxi', 'JX', 1),
(702, 44, 'Jilin', 'JL', 1),
(703, 44, 'Liaoning', 'LI', 1),
(704, 44, 'Macau', 'MA', 1),
(705, 44, 'Ningxia', 'NI', 1),
(706, 44, 'Shaanxi', 'SH', 1),
(707, 44, 'Shandong', 'SA', 1),
(708, 44, 'Shanghai', 'SG', 1),
(709, 44, 'Shanxi', 'SX', 1),
(710, 44, 'Sichuan', 'SI', 1),
(711, 44, 'Tianjin', 'TI', 1),
(712, 44, 'Xinjiang', 'XI', 1),
(713, 44, 'Yunnan', 'YU', 1),
(714, 44, 'Zhejiang', 'ZH', 1),
(715, 46, 'Direction Island', 'D', 1),
(716, 46, 'Home Island', 'H', 1),
(717, 46, 'Horsburgh Island', 'O', 1),
(718, 46, 'South Island', 'S', 1),
(719, 46, 'West Island', 'W', 1),
(720, 47, 'Amazonas', 'AMZ', 1),
(721, 47, 'Antioquia', 'ANT', 1),
(722, 47, 'Arauca', 'ARA', 1),
(723, 47, 'Atlantico', 'ATL', 1),
(724, 47, 'Bogota D.C.', 'BDC', 1),
(725, 47, 'Bolivar', 'BOL', 1),
(726, 47, 'Boyaca', 'BOY', 1),
(727, 47, 'Caldas', 'CAL', 1),
(728, 47, 'Caqueta', 'CAQ', 1),
(729, 47, 'Casanare', 'CAS', 1),
(730, 47, 'Cauca', 'CAU', 1),
(731, 47, 'Cesar', 'CES', 1),
(732, 47, 'Choco', 'CHO', 1),
(733, 47, 'Cordoba', 'COR', 1),
(734, 47, 'Cundinamarca', 'CAM', 1),
(735, 47, 'Guainia', 'GNA', 1),
(736, 47, 'Guajira', 'GJR', 1),
(737, 47, 'Guaviare', 'GVR', 1),
(738, 47, 'Huila', 'HUI', 1),
(739, 47, 'Magdalena', 'MAG', 1),
(740, 47, 'Meta', 'MET', 1),
(741, 47, 'Narino', 'NAR', 1),
(742, 47, 'Norte de Santander', 'NDS', 1),
(743, 47, 'Putumayo', 'PUT', 1),
(744, 47, 'Quindio', 'QUI', 1),
(745, 47, 'Risaralda', 'RIS', 1),
(746, 47, 'San Andres y Providencia', 'SAP', 1),
(747, 47, 'Santander', 'SAN', 1),
(748, 47, 'Sucre', 'SUC', 1),
(749, 47, 'Tolima', 'TOL', 1),
(750, 47, 'Valle del Cauca', 'VDC', 1),
(751, 47, 'Vaupes', 'VAU', 1),
(752, 47, 'Vichada', 'VIC', 1),
(753, 48, 'Grande Comore', 'G', 1),
(754, 48, 'Anjouan', 'A', 1),
(755, 48, 'Moheli', 'M', 1),
(756, 49, 'Bouenza', 'BO', 1),
(757, 49, 'Brazzaville', 'BR', 1),
(758, 49, 'Cuvette', 'CU', 1),
(759, 49, 'Cuvette-Ouest', 'CO', 1),
(760, 49, 'Kouilou', 'KO', 1),
(761, 49, 'Lekoumou', 'LE', 1),
(762, 49, 'Likouala', 'LI', 1),
(763, 49, 'Niari', 'NI', 1),
(764, 49, 'Plateaux', 'PL', 1),
(765, 49, 'Pool', 'PO', 1),
(766, 49, 'Sangha', 'SA', 1),
(767, 50, 'Pukapuka', 'PU', 1),
(768, 50, 'Rakahanga', 'RK', 1),
(769, 50, 'Manihiki', 'MK', 1),
(770, 50, 'Penrhyn', 'PE', 1),
(771, 50, 'Nassau Island', 'NI', 1),
(772, 50, 'Surwarrow', 'SU', 1),
(773, 50, 'Palmerston', 'PA', 1),
(774, 50, 'Aitutaki', 'AI', 1),
(775, 50, 'Manuae', 'MA', 1),
(776, 50, 'Takutea', 'TA', 1),
(777, 50, 'Mitiaro', 'MT', 1),
(778, 50, 'Atiu', 'AT', 1),
(779, 50, 'Mauke', 'MU', 1),
(780, 50, 'Rarotonga', 'RR', 1),
(781, 50, 'Mangaia', 'MG', 1),
(782, 51, 'Alajuela', 'AL', 1),
(783, 51, 'Cartago', 'CA', 1),
(784, 51, 'Guanacaste', 'GU', 1),
(785, 51, 'Heredia', 'HE', 1),
(786, 51, 'Limon', 'LI', 1),
(787, 51, 'Puntarenas', 'PU', 1),
(788, 51, 'San Jose', 'SJ', 1),
(789, 52, 'Abengourou', 'ABE', 1),
(790, 52, 'Abidjan', 'ABI', 1),
(791, 52, 'Aboisso', 'ABO', 1),
(792, 52, 'Adiake', 'ADI', 1),
(793, 52, 'Adzope', 'ADZ', 1),
(794, 52, 'Agboville', 'AGB', 1),
(795, 52, 'Agnibilekrou', 'AGN', 1),
(796, 52, 'Alepe', 'ALE', 1),
(797, 52, 'Bocanda', 'BOC', 1),
(798, 52, 'Bangolo', 'BAN', 1),
(799, 52, 'Beoumi', 'BEO', 1),
(800, 52, 'Biankouma', 'BIA', 1),
(801, 52, 'Bondoukou', 'BDK', 1),
(802, 52, 'Bongouanou', 'BGN', 1),
(803, 52, 'Bouafle', 'BFL', 1),
(804, 52, 'Bouake', 'BKE', 1),
(805, 52, 'Bouna', 'BNA', 1),
(806, 52, 'Boundiali', 'BDL', 1),
(807, 52, 'Dabakala', 'DKL', 1),
(808, 52, 'Dabou', 'DBU', 1),
(809, 52, 'Daloa', 'DAL', 1),
(810, 52, 'Danane', 'DAN', 1),
(811, 52, 'Daoukro', 'DAO', 1),
(812, 52, 'Dimbokro', 'DIM', 1),
(813, 52, 'Divo', 'DIV', 1),
(814, 52, 'Duekoue', 'DUE', 1),
(815, 52, 'Ferkessedougou', 'FER', 1),
(816, 52, 'Gagnoa', 'GAG', 1),
(817, 52, 'Grand-Bassam', 'GBA', 1),
(818, 52, 'Grand-Lahou', 'GLA', 1),
(819, 52, 'Guiglo', 'GUI', 1),
(820, 52, 'Issia', 'ISS', 1),
(821, 52, 'Jacqueville', 'JAC', 1),
(822, 52, 'Katiola', 'KAT', 1),
(823, 52, 'Korhogo', 'KOR', 1),
(824, 52, 'Lakota', 'LAK', 1),
(825, 52, 'Man', 'MAN', 1),
(826, 52, 'Mankono', 'MKN', 1),
(827, 52, 'Mbahiakro', 'MBA', 1),
(828, 52, 'Odienne', 'ODI', 1),
(829, 52, 'Oume', 'OUM', 1),
(830, 52, 'Sakassou', 'SAK', 1),
(831, 52, 'San-Pedro', 'SPE', 1),
(832, 52, 'Sassandra', 'SAS', 1),
(833, 52, 'Seguela', 'SEG', 1),
(834, 52, 'Sinfra', 'SIN', 1),
(835, 52, 'Soubre', 'SOU', 1),
(836, 52, 'Tabou', 'TAB', 1),
(837, 52, 'Tanda', 'TAN', 1),
(838, 52, 'Tiebissou', 'TIE', 1),
(839, 52, 'Tingrela', 'TIN', 1),
(840, 52, 'Tiassale', 'TIA', 1),
(841, 52, 'Touba', 'TBA', 1),
(842, 52, 'Toulepleu', 'TLP', 1),
(843, 52, 'Toumodi', 'TMD', 1),
(844, 52, 'Vavoua', 'VAV', 1),
(845, 52, 'Yamoussoukro', 'YAM', 1),
(846, 52, 'Zuenoula', 'ZUE', 1),
(847, 53, 'Bjelovarsko-bilogorska', 'BB', 1),
(848, 53, 'Grad Zagreb', 'GZ', 1),
(849, 53, 'Dubrovačko-neretvanska', 'DN', 1),
(850, 53, 'Istarska', 'IS', 1),
(851, 53, 'Karlovačka', 'KA', 1),
(852, 53, 'Koprivničko-križevačka', 'KK', 1),
(853, 53, 'Krapinsko-zagorska', 'KZ', 1),
(854, 53, 'Ličko-senjska', 'LS', 1),
(855, 53, 'Međimurska', 'ME', 1),
(856, 53, 'Osječko-baranjska', 'OB', 1),
(857, 53, 'Požeško-slavonska', 'PS', 1),
(858, 53, 'Primorsko-goranska', 'PG', 1),
(859, 53, 'Šibensko-kninska', 'SK', 1),
(860, 53, 'Sisačko-moslavačka', 'SM', 1),
(861, 53, 'Brodsko-posavska', 'BP', 1),
(862, 53, 'Splitsko-dalmatinska', 'SD', 1),
(863, 53, 'Varaždinska', 'VA', 1),
(864, 53, 'Virovitičko-podravska', 'VP', 1),
(865, 53, 'Vukovarsko-srijemska', 'VS', 1),
(866, 53, 'Zadarska', 'ZA', 1),
(867, 53, 'Zagrebačka', 'ZG', 1),
(868, 54, 'Camaguey', 'CA', 1),
(869, 54, 'Ciego de Avila', 'CD', 1),
(870, 54, 'Cienfuegos', 'CI', 1),
(871, 54, 'Ciudad de La Habana', 'CH', 1),
(872, 54, 'Granma', 'GR', 1),
(873, 54, 'Guantanamo', 'GU', 1),
(874, 54, 'Holguin', 'HO', 1),
(875, 54, 'Isla de la Juventud', 'IJ', 1),
(876, 54, 'La Habana', 'LH', 1),
(877, 54, 'Las Tunas', 'LT', 1),
(878, 54, 'Matanzas', 'MA', 1),
(879, 54, 'Pinar del Rio', 'PR', 1),
(880, 54, 'Sancti Spiritus', 'SS', 1),
(881, 54, 'Santiago de Cuba', 'SC', 1),
(882, 54, 'Villa Clara', 'VC', 1),
(883, 55, 'Famagusta', 'F', 1),
(884, 55, 'Kyrenia', 'K', 1),
(885, 55, 'Larnaca', 'A', 1),
(886, 55, 'Limassol', 'I', 1),
(887, 55, 'Nicosia', 'N', 1),
(888, 55, 'Paphos', 'P', 1),
(889, 56, 'Ústecký', 'U', 1),
(890, 56, 'Jihočeský', 'C', 1),
(891, 56, 'Jihomoravský', 'B', 1),
(892, 56, 'Karlovarský', 'K', 1),
(893, 56, 'Královehradecký', 'H', 1),
(894, 56, 'Liberecký', 'L', 1),
(895, 56, 'Moravskoslezský', 'T', 1),
(896, 56, 'Olomoucký', 'M', 1),
(897, 56, 'Pardubický', 'E', 1),
(898, 56, 'Plzeňský', 'P', 1),
(899, 56, 'Praha', 'A', 1),
(900, 56, 'Středočeský', 'S', 1),
(901, 56, 'Vysočina', 'J', 1),
(902, 56, 'Zlínský', 'Z', 1),
(903, 57, 'Arhus', 'AR', 1),
(904, 57, 'Bornholm', 'BH', 1),
(905, 57, 'Copenhagen', 'CO', 1),
(906, 57, 'Faroe Islands', 'FO', 1),
(907, 57, 'Frederiksborg', 'FR', 1),
(908, 57, 'Fyn', 'FY', 1),
(909, 57, 'Kobenhavn', 'KO', 1),
(910, 57, 'Nordjylland', 'NO', 1),
(911, 57, 'Ribe', 'RI', 1),
(912, 57, 'Ringkobing', 'RK', 1),
(913, 57, 'Roskilde', 'RO', 1),
(914, 57, 'Sonderjylland', 'SO', 1),
(915, 57, 'Storstrom', 'ST', 1),
(916, 57, 'Vejle', 'VK', 1),
(917, 57, 'Vestj&aelig;lland', 'VJ', 1),
(918, 57, 'Viborg', 'VB', 1),
(919, 58, '\'Ali Sabih', 'S', 1),
(920, 58, 'Dikhil', 'K', 1),
(921, 58, 'Djibouti', 'J', 1),
(922, 58, 'Obock', 'O', 1),
(923, 58, 'Tadjoura', 'T', 1),
(924, 59, 'Saint Andrew Parish', 'AND', 1),
(925, 59, 'Saint David Parish', 'DAV', 1),
(926, 59, 'Saint George Parish', 'GEO', 1),
(927, 59, 'Saint John Parish', 'JOH', 1),
(928, 59, 'Saint Joseph Parish', 'JOS', 1),
(929, 59, 'Saint Luke Parish', 'LUK', 1),
(930, 59, 'Saint Mark Parish', 'MAR', 1),
(931, 59, 'Saint Patrick Parish', 'PAT', 1),
(932, 59, 'Saint Paul Parish', 'PAU', 1),
(933, 59, 'Saint Peter Parish', 'PET', 1),
(934, 60, 'Distrito Nacional', 'DN', 1),
(935, 60, 'Azua', 'AZ', 1),
(936, 60, 'Baoruco', 'BC', 1),
(937, 60, 'Barahona', 'BH', 1),
(938, 60, 'Dajabon', 'DJ', 1),
(939, 60, 'Duarte', 'DU', 1),
(940, 60, 'Elias Pina', 'EL', 1),
(941, 60, 'El Seybo', 'SY', 1),
(942, 60, 'Espaillat', 'ET', 1),
(943, 60, 'Hato Mayor', 'HM', 1),
(944, 60, 'Independencia', 'IN', 1),
(945, 60, 'La Altagracia', 'AL', 1),
(946, 60, 'La Romana', 'RO', 1),
(947, 60, 'La Vega', 'VE', 1),
(948, 60, 'Maria Trinidad Sanchez', 'MT', 1),
(949, 60, 'Monsenor Nouel', 'MN', 1),
(950, 60, 'Monte Cristi', 'MC', 1),
(951, 60, 'Monte Plata', 'MP', 1),
(952, 60, 'Pedernales', 'PD', 1),
(953, 60, 'Peravia (Bani)', 'PR', 1),
(954, 60, 'Puerto Plata', 'PP', 1),
(955, 60, 'Salcedo', 'SL', 1),
(956, 60, 'Samana', 'SM', 1),
(957, 60, 'Sanchez Ramirez', 'SH', 1),
(958, 60, 'San Cristobal', 'SC', 1),
(959, 60, 'San Jose de Ocoa', 'JO', 1),
(960, 60, 'San Juan', 'SJ', 1),
(961, 60, 'San Pedro de Macoris', 'PM', 1),
(962, 60, 'Santiago', 'SA', 1),
(963, 60, 'Santiago Rodriguez', 'ST', 1),
(964, 60, 'Santo Domingo', 'SD', 1),
(965, 60, 'Valverde', 'VA', 1),
(966, 61, 'Aileu', 'AL', 1),
(967, 61, 'Ainaro', 'AN', 1),
(968, 61, 'Baucau', 'BA', 1),
(969, 61, 'Bobonaro', 'BO', 1),
(970, 61, 'Cova Lima', 'CO', 1),
(971, 61, 'Dili', 'DI', 1),
(972, 61, 'Ermera', 'ER', 1),
(973, 61, 'Lautem', 'LA', 1),
(974, 61, 'Liquica', 'LI', 1),
(975, 61, 'Manatuto', 'MT', 1),
(976, 61, 'Manufahi', 'MF', 1),
(977, 61, 'Oecussi', 'OE', 1),
(978, 61, 'Viqueque', 'VI', 1),
(979, 62, 'Azuay', 'AZU', 1),
(980, 62, 'Bolivar', 'BOL', 1),
(981, 62, 'Ca&ntilde;ar', 'CAN', 1),
(982, 62, 'Carchi', 'CAR', 1),
(983, 62, 'Chimborazo', 'CHI', 1),
(984, 62, 'Cotopaxi', 'COT', 1),
(985, 62, 'El Oro', 'EOR', 1),
(986, 62, 'Esmeraldas', 'ESM', 1),
(987, 62, 'Gal&aacute;pagos', 'GPS', 1),
(988, 62, 'Guayas', 'GUA', 1),
(989, 62, 'Imbabura', 'IMB', 1),
(990, 62, 'Loja', 'LOJ', 1),
(991, 62, 'Los Rios', 'LRO', 1),
(992, 62, 'Manab&iacute;', 'MAN', 1),
(993, 62, 'Morona Santiago', 'MSA', 1),
(994, 62, 'Napo', 'NAP', 1),
(995, 62, 'Orellana', 'ORE', 1),
(996, 62, 'Pastaza', 'PAS', 1),
(997, 62, 'Pichincha', 'PIC', 1),
(998, 62, 'Sucumb&iacute;os', 'SUC', 1),
(999, 62, 'Tungurahua', 'TUN', 1),
(1000, 62, 'Zamora Chinchipe', 'ZCH', 1),
(1001, 63, 'Ad Daqahliyah', 'DHY', 1),
(1002, 63, 'Al Bahr al Ahmar', 'BAM', 1),
(1003, 63, 'Al Buhayrah', 'BHY', 1),
(1004, 63, 'Al Fayyum', 'FYM', 1),
(1005, 63, 'Al Gharbiyah', 'GBY', 1),
(1006, 63, 'Al Iskandariyah', 'IDR', 1),
(1007, 63, 'Al Isma\'iliyah', 'IML', 1),
(1008, 63, 'Al Jizah', 'JZH', 1),
(1009, 63, 'Al Minufiyah', 'MFY', 1),
(1010, 63, 'Al Minya', 'MNY', 1),
(1011, 63, 'Al Qahirah', 'QHR', 1),
(1012, 63, 'Al Qalyubiyah', 'QLY', 1),
(1013, 63, 'Al Wadi al Jadid', 'WJD', 1),
(1014, 63, 'Ash Sharqiyah', 'SHQ', 1),
(1015, 63, 'As Suways', 'SWY', 1),
(1016, 63, 'Aswan', 'ASW', 1),
(1017, 63, 'Asyut', 'ASY', 1),
(1018, 63, 'Bani Suwayf', 'BSW', 1),
(1019, 63, 'Bur Sa\'id', 'BSD', 1),
(1020, 63, 'Dumyat', 'DMY', 1),
(1021, 63, 'Janub Sina\'', 'JNS', 1),
(1022, 63, 'Kafr ash Shaykh', 'KSH', 1),
(1023, 63, 'Matruh', 'MAT', 1),
(1024, 63, 'Qina', 'QIN', 1),
(1025, 63, 'Shamal Sina\'', 'SHS', 1),
(1026, 63, 'Suhaj', 'SUH', 1),
(1027, 64, 'Ahuachapan', 'AH', 1),
(1028, 64, 'Cabanas', 'CA', 1),
(1029, 64, 'Chalatenango', 'CH', 1),
(1030, 64, 'Cuscatlan', 'CU', 1),
(1031, 64, 'La Libertad', 'LB', 1),
(1032, 64, 'La Paz', 'PZ', 1),
(1033, 64, 'La Union', 'UN', 1),
(1034, 64, 'Morazan', 'MO', 1),
(1035, 64, 'San Miguel', 'SM', 1),
(1036, 64, 'San Salvador', 'SS', 1),
(1037, 64, 'San Vicente', 'SV', 1),
(1038, 64, 'Santa Ana', 'SA', 1),
(1039, 64, 'Sonsonate', 'SO', 1),
(1040, 64, 'Usulutan', 'US', 1),
(1041, 65, 'Provincia Annobon', 'AN', 1),
(1042, 65, 'Provincia Bioko Norte', 'BN', 1),
(1043, 65, 'Provincia Bioko Sur', 'BS', 1),
(1044, 65, 'Provincia Centro Sur', 'CS', 1),
(1045, 65, 'Provincia Kie-Ntem', 'KN', 1),
(1046, 65, 'Provincia Litoral', 'LI', 1),
(1047, 65, 'Provincia Wele-Nzas', 'WN', 1),
(1048, 66, 'Central (Maekel)', 'MA', 1),
(1049, 66, 'Anseba (Keren)', 'KE', 1),
(1050, 66, 'Southern Red Sea (Debub-Keih-Bahri)', 'DK', 1),
(1051, 66, 'Northern Red Sea (Semien-Keih-Bahri)', 'SK', 1),
(1052, 66, 'Southern (Debub)', 'DE', 1),
(1053, 66, 'Gash-Barka (Barentu)', 'BR', 1),
(1054, 67, 'Harjumaa (Tallinn)', 'HA', 1),
(1055, 67, 'Hiiumaa (Kardla)', 'HI', 1),
(1056, 67, 'Ida-Virumaa (Johvi)', 'IV', 1),
(1057, 67, 'Jarvamaa (Paide)', 'JA', 1),
(1058, 67, 'Jogevamaa (Jogeva)', 'JO', 1),
(1059, 67, 'Laane-Virumaa (Rakvere)', 'LV', 1),
(1060, 67, 'Laanemaa (Haapsalu)', 'LA', 1),
(1061, 67, 'Parnumaa (Parnu)', 'PA', 1),
(1062, 67, 'Polvamaa (Polva)', 'PO', 1),
(1063, 67, 'Raplamaa (Rapla)', 'RA', 1),
(1064, 67, 'Saaremaa (Kuessaare)', 'SA', 1),
(1065, 67, 'Tartumaa (Tartu)', 'TA', 1),
(1066, 67, 'Valgamaa (Valga)', 'VA', 1),
(1067, 67, 'Viljandimaa (Viljandi)', 'VI', 1),
(1068, 67, 'Vorumaa (Voru)', 'VO', 1),
(1069, 68, 'Afar', 'AF', 1),
(1070, 68, 'Amhara', 'AH', 1),
(1071, 68, 'Benishangul-Gumaz', 'BG', 1),
(1072, 68, 'Gambela', 'GB', 1),
(1073, 68, 'Hariai', 'HR', 1),
(1074, 68, 'Oromia', 'OR', 1),
(1075, 68, 'Somali', 'SM', 1),
(1076, 68, 'Southern Nations - Nationalities and Peoples Region', 'SN', 1),
(1077, 68, 'Tigray', 'TG', 1),
(1078, 68, 'Addis Ababa', 'AA', 1),
(1079, 68, 'Dire Dawa', 'DD', 1),
(1080, 71, 'Central Division', 'C', 1),
(1081, 71, 'Northern Division', 'N', 1),
(1082, 71, 'Eastern Division', 'E', 1),
(1083, 71, 'Western Division', 'W', 1),
(1084, 71, 'Rotuma', 'R', 1),
(1085, 72, 'Ahvenanmaan lääni', 'AL', 1),
(1086, 72, 'Etelä-Suomen lääni', 'ES', 1),
(1087, 72, 'Itä-Suomen lääni', 'IS', 1),
(1088, 72, 'Länsi-Suomen lääni', 'LS', 1),
(1089, 72, 'Lapin lääni', 'LA', 1),
(1090, 72, 'Oulun lääni', 'OU', 1),
(1114, 74, 'Ain', '01', 1),
(1115, 74, 'Aisne', '02', 1),
(1116, 74, 'Allier', '03', 1),
(1117, 74, 'Alpes de Haute Provence', '04', 1),
(1118, 74, 'Hautes-Alpes', '05', 1),
(1119, 74, 'Alpes Maritimes', '06', 1),
(1120, 74, 'Ard&egrave;che', '07', 1),
(1121, 74, 'Ardennes', '08', 1),
(1122, 74, 'Ari&egrave;ge', '09', 1),
(1123, 74, 'Aube', '10', 1),
(1124, 74, 'Aude', '11', 1),
(1125, 74, 'Aveyron', '12', 1),
(1126, 74, 'Bouches du Rh&ocirc;ne', '13', 1),
(1127, 74, 'Calvados', '14', 1),
(1128, 74, 'Cantal', '15', 1),
(1129, 74, 'Charente', '16', 1),
(1130, 74, 'Charente Maritime', '17', 1),
(1131, 74, 'Cher', '18', 1),
(1132, 74, 'Corr&egrave;ze', '19', 1),
(1133, 74, 'Corse du Sud', '2A', 1),
(1134, 74, 'Haute Corse', '2B', 1),
(1135, 74, 'C&ocirc;te d&#039;or', '21', 1),
(1136, 74, 'C&ocirc;tes d&#039;Armor', '22', 1),
(1137, 74, 'Creuse', '23', 1),
(1138, 74, 'Dordogne', '24', 1),
(1139, 74, 'Doubs', '25', 1),
(1140, 74, 'Dr&ocirc;me', '26', 1),
(1141, 74, 'Eure', '27', 1),
(1142, 74, 'Eure et Loir', '28', 1),
(1143, 74, 'Finist&egrave;re', '29', 1),
(1144, 74, 'Gard', '30', 1),
(1145, 74, 'Haute Garonne', '31', 1),
(1146, 74, 'Gers', '32', 1),
(1147, 74, 'Gironde', '33', 1),
(1148, 74, 'H&eacute;rault', '34', 1),
(1149, 74, 'Ille et Vilaine', '35', 1),
(1150, 74, 'Indre', '36', 1),
(1151, 74, 'Indre et Loire', '37', 1),
(1152, 74, 'Is&eacute;re', '38', 1),
(1153, 74, 'Jura', '39', 1),
(1154, 74, 'Landes', '40', 1),
(1155, 74, 'Loir et Cher', '41', 1),
(1156, 74, 'Loire', '42', 1),
(1157, 74, 'Haute Loire', '43', 1),
(1158, 74, 'Loire Atlantique', '44', 1),
(1159, 74, 'Loiret', '45', 1),
(1160, 74, 'Lot', '46', 1),
(1161, 74, 'Lot et Garonne', '47', 1),
(1162, 74, 'Loz&egrave;re', '48', 1),
(1163, 74, 'Maine et Loire', '49', 1),
(1164, 74, 'Manche', '50', 1),
(1165, 74, 'Marne', '51', 1),
(1166, 74, 'Haute Marne', '52', 1),
(1167, 74, 'Mayenne', '53', 1),
(1168, 74, 'Meurthe et Moselle', '54', 1),
(1169, 74, 'Meuse', '55', 1),
(1170, 74, 'Morbihan', '56', 1),
(1171, 74, 'Moselle', '57', 1),
(1172, 74, 'Ni&egrave;vre', '58', 1),
(1173, 74, 'Nord', '59', 1),
(1174, 74, 'Oise', '60', 1),
(1175, 74, 'Orne', '61', 1),
(1176, 74, 'Pas de Calais', '62', 1),
(1177, 74, 'Puy de D&ocirc;me', '63', 1),
(1178, 74, 'Pyr&eacute;n&eacute;es Atlantiques', '64', 1),
(1179, 74, 'Hautes Pyr&eacute;n&eacute;es', '65', 1),
(1180, 74, 'Pyr&eacute;n&eacute;es Orientales', '66', 1),
(1181, 74, 'Bas Rhin', '67', 1),
(1182, 74, 'Haut Rhin', '68', 1),
(1183, 74, 'Rh&ocirc;ne', '69', 1),
(1184, 74, 'Haute Sa&ocirc;ne', '70', 1),
(1185, 74, 'Sa&ocirc;ne et Loire', '71', 1),
(1186, 74, 'Sarthe', '72', 1),
(1187, 74, 'Savoie', '73', 1),
(1188, 74, 'Haute Savoie', '74', 1),
(1189, 74, 'Paris', '75', 1),
(1190, 74, 'Seine Maritime', '76', 1),
(1191, 74, 'Seine et Marne', '77', 1),
(1192, 74, 'Yvelines', '78', 1),
(1193, 74, 'Deux S&egrave;vres', '79', 1),
(1194, 74, 'Somme', '80', 1),
(1195, 74, 'Tarn', '81', 1),
(1196, 74, 'Tarn et Garonne', '82', 1),
(1197, 74, 'Var', '83', 1),
(1198, 74, 'Vaucluse', '84', 1),
(1199, 74, 'Vend&eacute;e', '85', 1),
(1200, 74, 'Vienne', '86', 1),
(1201, 74, 'Haute Vienne', '87', 1),
(1202, 74, 'Vosges', '88', 1),
(1203, 74, 'Yonne', '89', 1),
(1204, 74, 'Territoire de Belfort', '90', 1),
(1205, 74, 'Essonne', '91', 1),
(1206, 74, 'Hauts de Seine', '92', 1),
(1207, 74, 'Seine St-Denis', '93', 1),
(1208, 74, 'Val de Marne', '94', 1),
(1209, 74, 'Val d\'Oise', '95', 1),
(1210, 76, 'Archipel des Marquises', 'M', 1),
(1211, 76, 'Archipel des Tuamotu', 'T', 1),
(1212, 76, 'Archipel des Tubuai', 'I', 1),
(1213, 76, 'Iles du Vent', 'V', 1),
(1214, 76, 'Iles Sous-le-Vent', 'S', 1),
(1215, 77, 'Iles Crozet', 'C', 1),
(1216, 77, 'Iles Kerguelen', 'K', 1),
(1217, 77, 'Ile Amsterdam', 'A', 1),
(1218, 77, 'Ile Saint-Paul', 'P', 1),
(1219, 77, 'Adelie Land', 'D', 1),
(1220, 78, 'Estuaire', 'ES', 1),
(1221, 78, 'Haut-Ogooue', 'HO', 1),
(1222, 78, 'Moyen-Ogooue', 'MO', 1),
(1223, 78, 'Ngounie', 'NG', 1),
(1224, 78, 'Nyanga', 'NY', 1),
(1225, 78, 'Ogooue-Ivindo', 'OI', 1),
(1226, 78, 'Ogooue-Lolo', 'OL', 1),
(1227, 78, 'Ogooue-Maritime', 'OM', 1),
(1228, 78, 'Woleu-Ntem', 'WN', 1),
(1229, 79, 'Banjul', 'BJ', 1),
(1230, 79, 'Basse', 'BS', 1),
(1231, 79, 'Brikama', 'BR', 1),
(1232, 79, 'Janjangbure', 'JA', 1),
(1233, 79, 'Kanifeng', 'KA', 1),
(1234, 79, 'Kerewan', 'KE', 1),
(1235, 79, 'Kuntaur', 'KU', 1),
(1236, 79, 'Mansakonko', 'MA', 1),
(1237, 79, 'Lower River', 'LR', 1),
(1238, 79, 'Central River', 'CR', 1),
(1239, 79, 'North Bank', 'NB', 1),
(1240, 79, 'Upper River', 'UR', 1),
(1241, 79, 'Western', 'WE', 1),
(1242, 80, 'Abkhazia', 'AB', 1),
(1243, 80, 'Ajaria', 'AJ', 1),
(1244, 80, 'Tbilisi', 'TB', 1),
(1245, 80, 'Guria', 'GU', 1),
(1246, 80, 'Imereti', 'IM', 1),
(1247, 80, 'Kakheti', 'KA', 1),
(1248, 80, 'Kvemo Kartli', 'KK', 1),
(1249, 80, 'Mtskheta-Mtianeti', 'MM', 1),
(1250, 80, 'Racha Lechkhumi and Kvemo Svanet', 'RL', 1),
(1251, 80, 'Samegrelo-Zemo Svaneti', 'SZ', 1),
(1252, 80, 'Samtskhe-Javakheti', 'SJ', 1),
(1253, 80, 'Shida Kartli', 'SK', 1),
(1254, 81, 'Baden-W&uuml;rttemberg', 'BAW', 1),
(1255, 81, 'Bayern', 'BAY', 1),
(1256, 81, 'Berlin', 'BER', 1),
(1257, 81, 'Brandenburg', 'BRG', 1),
(1258, 81, 'Bremen', 'BRE', 1),
(1259, 81, 'Hamburg', 'HAM', 1),
(1260, 81, 'Hessen', 'HES', 1),
(1261, 81, 'Mecklenburg-Vorpommern', 'MEC', 1),
(1262, 81, 'Niedersachsen', 'NDS', 1),
(1263, 81, 'Nordrhein-Westfalen', 'NRW', 1),
(1264, 81, 'Rheinland-Pfalz', 'RHE', 1),
(1265, 81, 'Saarland', 'SAR', 1),
(1266, 81, 'Sachsen', 'SAS', 1),
(1267, 81, 'Sachsen-Anhalt', 'SAC', 1),
(1268, 81, 'Schleswig-Holstein', 'SCN', 1),
(1269, 81, 'Th&uuml;ringen', 'THE', 1),
(1270, 82, 'Ashanti Region', 'AS', 1),
(1271, 82, 'Brong-Ahafo Region', 'BA', 1),
(1272, 82, 'Central Region', 'CE', 1),
(1273, 82, 'Eastern Region', 'EA', 1),
(1274, 82, 'Greater Accra Region', 'GA', 1),
(1275, 82, 'Northern Region', 'NO', 1),
(1276, 82, 'Upper East Region', 'UE', 1),
(1277, 82, 'Upper West Region', 'UW', 1),
(1278, 82, 'Volta Region', 'VO', 1),
(1279, 82, 'Western Region', 'WE', 1),
(1280, 84, 'Attica', 'AT', 1),
(1281, 84, 'Central Greece', 'CN', 1),
(1282, 84, 'Central Macedonia', 'CM', 1),
(1283, 84, 'Crete', 'CR', 1),
(1284, 84, 'East Macedonia and Thrace', 'EM', 1),
(1285, 84, 'Epirus', 'EP', 1),
(1286, 84, 'Ionian Islands', 'II', 1),
(1287, 84, 'North Aegean', 'NA', 1),
(1288, 84, 'Peloponnesos', 'PP', 1),
(1289, 84, 'South Aegean', 'SA', 1),
(1290, 84, 'Thessaly', 'TH', 1),
(1291, 84, 'West Greece', 'WG', 1),
(1292, 84, 'West Macedonia', 'WM', 1),
(1293, 85, 'Avannaa', 'A', 1),
(1294, 85, 'Tunu', 'T', 1),
(1295, 85, 'Kitaa', 'K', 1),
(1296, 86, 'Saint Andrew', 'A', 1),
(1297, 86, 'Saint David', 'D', 1),
(1298, 86, 'Saint George', 'G', 1),
(1299, 86, 'Saint John', 'J', 1),
(1300, 86, 'Saint Mark', 'M', 1),
(1301, 86, 'Saint Patrick', 'P', 1),
(1302, 86, 'Carriacou', 'C', 1),
(1303, 86, 'Petit Martinique', 'Q', 1),
(1304, 89, 'Alta Verapaz', 'AV', 1),
(1305, 89, 'Baja Verapaz', 'BV', 1),
(1306, 89, 'Chimaltenango', 'CM', 1),
(1307, 89, 'Chiquimula', 'CQ', 1),
(1308, 89, 'El Peten', 'PE', 1),
(1309, 89, 'El Progreso', 'PR', 1),
(1310, 89, 'El Quiche', 'QC', 1),
(1311, 89, 'Escuintla', 'ES', 1),
(1312, 89, 'Guatemala', 'GU', 1),
(1313, 89, 'Huehuetenango', 'HU', 1),
(1314, 89, 'Izabal', 'IZ', 1),
(1315, 89, 'Jalapa', 'JA', 1),
(1316, 89, 'Jutiapa', 'JU', 1),
(1317, 89, 'Quetzaltenango', 'QZ', 1),
(1318, 89, 'Retalhuleu', 'RE', 1),
(1319, 89, 'Sacatepequez', 'ST', 1),
(1320, 89, 'San Marcos', 'SM', 1),
(1321, 89, 'Santa Rosa', 'SR', 1),
(1322, 89, 'Solola', 'SO', 1),
(1323, 89, 'Suchitepequez', 'SU', 1),
(1324, 89, 'Totonicapan', 'TO', 1),
(1325, 89, 'Zacapa', 'ZA', 1),
(1326, 90, 'Conakry', 'CNK', 1),
(1327, 90, 'Beyla', 'BYL', 1),
(1328, 90, 'Boffa', 'BFA', 1),
(1329, 90, 'Boke', 'BOK', 1),
(1330, 90, 'Coyah', 'COY', 1),
(1331, 90, 'Dabola', 'DBL', 1),
(1332, 90, 'Dalaba', 'DLB', 1),
(1333, 90, 'Dinguiraye', 'DGR', 1),
(1334, 90, 'Dubreka', 'DBR', 1),
(1335, 90, 'Faranah', 'FRN', 1),
(1336, 90, 'Forecariah', 'FRC', 1),
(1337, 90, 'Fria', 'FRI', 1),
(1338, 90, 'Gaoual', 'GAO', 1),
(1339, 90, 'Gueckedou', 'GCD', 1),
(1340, 90, 'Kankan', 'KNK', 1),
(1341, 90, 'Kerouane', 'KRN', 1),
(1342, 90, 'Kindia', 'KND', 1),
(1343, 90, 'Kissidougou', 'KSD', 1),
(1344, 90, 'Koubia', 'KBA', 1),
(1345, 90, 'Koundara', 'KDA', 1),
(1346, 90, 'Kouroussa', 'KRA', 1),
(1347, 90, 'Labe', 'LAB', 1),
(1348, 90, 'Lelouma', 'LLM', 1),
(1349, 90, 'Lola', 'LOL', 1),
(1350, 90, 'Macenta', 'MCT', 1),
(1351, 90, 'Mali', 'MAL', 1),
(1352, 90, 'Mamou', 'MAM', 1),
(1353, 90, 'Mandiana', 'MAN', 1),
(1354, 90, 'Nzerekore', 'NZR', 1),
(1355, 90, 'Pita', 'PIT', 1),
(1356, 90, 'Siguiri', 'SIG', 1),
(1357, 90, 'Telimele', 'TLM', 1),
(1358, 90, 'Tougue', 'TOG', 1),
(1359, 90, 'Yomou', 'YOM', 1),
(1360, 91, 'Bafata Region', 'BF', 1),
(1361, 91, 'Biombo Region', 'BB', 1),
(1362, 91, 'Bissau Region', 'BS', 1),
(1363, 91, 'Bolama Region', 'BL', 1),
(1364, 91, 'Cacheu Region', 'CA', 1),
(1365, 91, 'Gabu Region', 'GA', 1),
(1366, 91, 'Oio Region', 'OI', 1),
(1367, 91, 'Quinara Region', 'QU', 1),
(1368, 91, 'Tombali Region', 'TO', 1),
(1369, 92, 'Barima-Waini', 'BW', 1),
(1370, 92, 'Cuyuni-Mazaruni', 'CM', 1),
(1371, 92, 'Demerara-Mahaica', 'DM', 1),
(1372, 92, 'East Berbice-Corentyne', 'EC', 1),
(1373, 92, 'Essequibo Islands-West Demerara', 'EW', 1),
(1374, 92, 'Mahaica-Berbice', 'MB', 1),
(1375, 92, 'Pomeroon-Supenaam', 'PM', 1),
(1376, 92, 'Potaro-Siparuni', 'PI', 1),
(1377, 92, 'Upper Demerara-Berbice', 'UD', 1),
(1378, 92, 'Upper Takutu-Upper Essequibo', 'UT', 1),
(1379, 93, 'Artibonite', 'AR', 1),
(1380, 93, 'Centre', 'CE', 1),
(1381, 93, 'Grand\'Anse', 'GA', 1),
(1382, 93, 'Nord', 'ND', 1),
(1383, 93, 'Nord-Est', 'NE', 1),
(1384, 93, 'Nord-Ouest', 'NO', 1),
(1385, 93, 'Ouest', 'OU', 1),
(1386, 93, 'Sud', 'SD', 1),
(1387, 93, 'Sud-Est', 'SE', 1),
(1388, 94, 'Flat Island', 'F', 1),
(1389, 94, 'McDonald Island', 'M', 1),
(1390, 94, 'Shag Island', 'S', 1),
(1391, 94, 'Heard Island', 'H', 1),
(1392, 95, 'Atlantida', 'AT', 1),
(1393, 95, 'Choluteca', 'CH', 1),
(1394, 95, 'Colon', 'CL', 1),
(1395, 95, 'Comayagua', 'CM', 1),
(1396, 95, 'Copan', 'CP', 1),
(1397, 95, 'Cortes', 'CR', 1),
(1398, 95, 'El Paraiso', 'PA', 1),
(1399, 95, 'Francisco Morazan', 'FM', 1),
(1400, 95, 'Gracias a Dios', 'GD', 1),
(1401, 95, 'Intibuca', 'IN', 1),
(1402, 95, 'Islas de la Bahia (Bay Islands)', 'IB', 1),
(1403, 95, 'La Paz', 'PZ', 1),
(1404, 95, 'Lempira', 'LE', 1),
(1405, 95, 'Ocotepeque', 'OC', 1),
(1406, 95, 'Olancho', 'OL', 1),
(1407, 95, 'Santa Barbara', 'SB', 1),
(1408, 95, 'Valle', 'VA', 1),
(1409, 95, 'Yoro', 'YO', 1),
(1410, 96, 'Central and Western Hong Kong Island', 'HCW', 1),
(1411, 96, 'Eastern Hong Kong Island', 'HEA', 1),
(1412, 96, 'Southern Hong Kong Island', 'HSO', 1),
(1413, 96, 'Wan Chai Hong Kong Island', 'HWC', 1),
(1414, 96, 'Kowloon City Kowloon', 'KKC', 1),
(1415, 96, 'Kwun Tong Kowloon', 'KKT', 1),
(1416, 96, 'Sham Shui Po Kowloon', 'KSS', 1),
(1417, 96, 'Wong Tai Sin Kowloon', 'KWT', 1),
(1418, 96, 'Yau Tsim Mong Kowloon', 'KYT', 1),
(1419, 96, 'Islands New Territories', 'NIS', 1),
(1420, 96, 'Kwai Tsing New Territories', 'NKT', 1),
(1421, 96, 'North New Territories', 'NNO', 1),
(1422, 96, 'Sai Kung New Territories', 'NSK', 1),
(1423, 96, 'Sha Tin New Territories', 'NST', 1),
(1424, 96, 'Tai Po New Territories', 'NTP', 1),
(1425, 96, 'Tsuen Wan New Territories', 'NTW', 1),
(1426, 96, 'Tuen Mun New Territories', 'NTM', 1),
(1427, 96, 'Yuen Long New Territories', 'NYL', 1),
(1467, 98, 'Austurland', 'AL', 1),
(1468, 98, 'Hofuoborgarsvaeoi', 'HF', 1),
(1469, 98, 'Norourland eystra', 'NE', 1),
(1470, 98, 'Norourland vestra', 'NV', 1),
(1471, 98, 'Suourland', 'SL', 1),
(1472, 98, 'Suournes', 'SN', 1),
(1473, 98, 'Vestfiroir', 'VF', 1),
(1474, 98, 'Vesturland', 'VL', 1),
(1475, 99, 'Andaman and Nicobar Islands', 'AN', 1),
(1476, 99, 'Andhra Pradesh', 'AP', 1),
(1477, 99, 'Arunachal Pradesh', 'AR', 1),
(1478, 99, 'Assam', 'AS', 1),
(1479, 99, 'Bihar', 'BI', 1),
(1480, 99, 'Chandigarh', 'CH', 1),
(1481, 99, 'Dadra and Nagar Haveli', 'DA', 1),
(1482, 99, 'Daman and Diu', 'DM', 1),
(1483, 99, 'Delhi', 'DE', 1),
(1484, 99, 'Goa', 'GO', 1),
(1485, 99, 'Gujarat', 'GU', 1),
(1486, 99, 'Haryana', 'HA', 1),
(1487, 99, 'Himachal Pradesh', 'HP', 1),
(1488, 99, 'Jammu and Kashmir', 'JA', 1),
(1489, 99, 'Karnataka', 'KA', 1),
(1490, 99, 'Kerala', 'KE', 1),
(1491, 99, 'Lakshadweep Islands', 'LI', 1),
(1492, 99, 'Madhya Pradesh', 'MP', 1),
(1493, 99, 'Maharashtra', 'MA', 1),
(1494, 99, 'Manipur', 'MN', 1),
(1495, 99, 'Meghalaya', 'ME', 1),
(1496, 99, 'Mizoram', 'MI', 1),
(1497, 99, 'Nagaland', 'NA', 1),
(1498, 99, 'Orissa', 'OR', 1),
(1499, 99, 'Puducherry', 'PO', 1),
(1500, 99, 'Punjab', 'PU', 1),
(1501, 99, 'Rajasthan', 'RA', 1),
(1502, 99, 'Sikkim', 'SI', 1),
(1503, 99, 'Tamil Nadu', 'TN', 1),
(1504, 99, 'Tripura', 'TR', 1),
(1505, 99, 'Uttar Pradesh', 'UP', 1),
(1506, 99, 'West Bengal', 'WB', 1),
(1507, 100, 'Aceh', 'AC', 1),
(1508, 100, 'Bali', 'BA', 1),
(1509, 100, 'Banten', 'BT', 1),
(1510, 100, 'Bengkulu', 'BE', 1),
(1511, 100, 'BoDeTaBek', 'BD', 1),
(1512, 100, 'Gorontalo', 'GO', 1),
(1513, 100, 'Jakarta Raya', 'JK', 1),
(1514, 100, 'Jambi', 'JA', 1),
(1515, 100, 'Jawa Barat', 'JB', 1),
(1516, 100, 'Jawa Tengah', 'JT', 1),
(1517, 100, 'Jawa Timur', 'JI', 1),
(1518, 100, 'Kalimantan Barat', 'KB', 1),
(1519, 100, 'Kalimantan Selatan', 'KS', 1),
(1520, 100, 'Kalimantan Tengah', 'KT', 1),
(1521, 100, 'Kalimantan Timur', 'KI', 1),
(1522, 100, 'Kepulauan Bangka Belitung', 'BB', 1),
(1523, 100, 'Lampung', 'LA', 1),
(1524, 100, 'Maluku', 'MA', 1),
(1525, 100, 'Maluku Utara', 'MU', 1),
(1526, 100, 'Nusa Tenggara Barat', 'NB', 1),
(1527, 100, 'Nusa Tenggara Timur', 'NT', 1),
(1528, 100, 'Papua', 'PA', 1),
(1529, 100, 'Riau', 'RI', 1),
(1530, 100, 'Sulawesi Selatan', 'SN', 1),
(1531, 100, 'Sulawesi Tengah', 'ST', 1),
(1532, 100, 'Sulawesi Tenggara', 'SG', 1),
(1533, 100, 'Sulawesi Utara', 'SA', 1),
(1534, 100, 'Sumatera Barat', 'SB', 1),
(1535, 100, 'Sumatera Selatan', 'SS', 1),
(1536, 100, 'Sumatera Utara', 'SU', 1),
(1537, 100, 'Yogyakarta', 'YO', 1),
(1538, 101, 'Tehran', 'TEH', 1),
(1539, 101, 'Qom', 'QOM', 1),
(1540, 101, 'Markazi', 'MKZ', 1),
(1541, 101, 'Qazvin', 'QAZ', 1),
(1542, 101, 'Gilan', 'GIL', 1),
(1543, 101, 'Ardabil', 'ARD', 1),
(1544, 101, 'Zanjan', 'ZAN', 1),
(1545, 101, 'East Azarbaijan', 'EAZ', 1),
(1546, 101, 'West Azarbaijan', 'WEZ', 1),
(1547, 101, 'Kurdistan', 'KRD', 1),
(1548, 101, 'Hamadan', 'HMD', 1),
(1549, 101, 'Kermanshah', 'KRM', 1),
(1550, 101, 'Ilam', 'ILM', 1),
(1551, 101, 'Lorestan', 'LRS', 1),
(1552, 101, 'Khuzestan', 'KZT', 1),
(1553, 101, 'Chahar Mahaal and Bakhtiari', 'CMB', 1),
(1554, 101, 'Kohkiluyeh and Buyer Ahmad', 'KBA', 1),
(1555, 101, 'Bushehr', 'BSH', 1),
(1556, 101, 'Fars', 'FAR', 1),
(1557, 101, 'Hormozgan', 'HRM', 1),
(1558, 101, 'Sistan and Baluchistan', 'SBL', 1),
(1559, 101, 'Kerman', 'KRB', 1),
(1560, 101, 'Yazd', 'YZD', 1),
(1561, 101, 'Esfahan', 'EFH', 1),
(1562, 101, 'Semnan', 'SMN', 1),
(1563, 101, 'Mazandaran', 'MZD', 1),
(1564, 101, 'Golestan', 'GLS', 1),
(1565, 101, 'North Khorasan', 'NKH', 1),
(1566, 101, 'Razavi Khorasan', 'RKH', 1),
(1567, 101, 'South Khorasan', 'SKH', 1),
(1568, 102, 'Baghdad', 'BD', 1),
(1569, 102, 'Salah ad Din', 'SD', 1),
(1570, 102, 'Diyala', 'DY', 1),
(1571, 102, 'Wasit', 'WS', 1),
(1572, 102, 'Maysan', 'MY', 1),
(1573, 102, 'Al Basrah', 'BA', 1),
(1574, 102, 'Dhi Qar', 'DQ', 1),
(1575, 102, 'Al Muthanna', 'MU', 1),
(1576, 102, 'Al Qadisyah', 'QA', 1),
(1577, 102, 'Babil', 'BB', 1),
(1578, 102, 'Al Karbala', 'KB', 1),
(1579, 102, 'An Najaf', 'NJ', 1),
(1580, 102, 'Al Anbar', 'AB', 1),
(1581, 102, 'Ninawa', 'NN', 1),
(1582, 102, 'Dahuk', 'DH', 1),
(1583, 102, 'Arbil', 'AL', 1),
(1584, 102, 'At Ta\'mim', 'TM', 1),
(1585, 102, 'As Sulaymaniyah', 'SL', 1),
(1586, 103, 'Carlow', 'CA', 1),
(1587, 103, 'Cavan', 'CV', 1),
(1588, 103, 'Clare', 'CL', 1),
(1589, 103, 'Cork', 'CO', 1),
(1590, 103, 'Donegal', 'DO', 1),
(1591, 103, 'Dublin', 'DU', 1),
(1592, 103, 'Galway', 'GA', 1),
(1593, 103, 'Kerry', 'KE', 1),
(1594, 103, 'Kildare', 'KI', 1),
(1595, 103, 'Kilkenny', 'KL', 1),
(1596, 103, 'Laois', 'LA', 1),
(1597, 103, 'Leitrim', 'LE', 1),
(1598, 103, 'Limerick', 'LI', 1),
(1599, 103, 'Longford', 'LO', 1),
(1600, 103, 'Louth', 'LU', 1);
INSERT INTO `oc_zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(1601, 103, 'Mayo', 'MA', 1),
(1602, 103, 'Meath', 'ME', 1),
(1603, 103, 'Monaghan', 'MO', 1),
(1604, 103, 'Offaly', 'OF', 1),
(1605, 103, 'Roscommon', 'RO', 1),
(1606, 103, 'Sligo', 'SL', 1),
(1607, 103, 'Tipperary', 'TI', 1),
(1608, 103, 'Waterford', 'WA', 1),
(1609, 103, 'Westmeath', 'WE', 1),
(1610, 103, 'Wexford', 'WX', 1),
(1611, 103, 'Wicklow', 'WI', 1),
(1612, 104, 'Be\'er Sheva', 'BS', 1),
(1613, 104, 'Bika\'at Hayarden', 'BH', 1),
(1614, 104, 'Eilat and Arava', 'EA', 1),
(1615, 104, 'Galil', 'GA', 1),
(1616, 104, 'Haifa', 'HA', 1),
(1617, 104, 'Jehuda Mountains', 'JM', 1),
(1618, 104, 'Jerusalem', 'JE', 1),
(1619, 104, 'Negev', 'NE', 1),
(1620, 104, 'Semaria', 'SE', 1),
(1621, 104, 'Sharon', 'SH', 1),
(1622, 104, 'Tel Aviv (Gosh Dan)', 'TA', 1),
(1643, 106, 'Clarendon Parish', 'CLA', 1),
(1644, 106, 'Hanover Parish', 'HAN', 1),
(1645, 106, 'Kingston Parish', 'KIN', 1),
(1646, 106, 'Manchester Parish', 'MAN', 1),
(1647, 106, 'Portland Parish', 'POR', 1),
(1648, 106, 'Saint Andrew Parish', 'AND', 1),
(1649, 106, 'Saint Ann Parish', 'ANN', 1),
(1650, 106, 'Saint Catherine Parish', 'CAT', 1),
(1651, 106, 'Saint Elizabeth Parish', 'ELI', 1),
(1652, 106, 'Saint James Parish', 'JAM', 1),
(1653, 106, 'Saint Mary Parish', 'MAR', 1),
(1654, 106, 'Saint Thomas Parish', 'THO', 1),
(1655, 106, 'Trelawny Parish', 'TRL', 1),
(1656, 106, 'Westmoreland Parish', 'WML', 1),
(1657, 107, 'Aichi', 'AI', 1),
(1658, 107, 'Akita', 'AK', 1),
(1659, 107, 'Aomori', 'AO', 1),
(1660, 107, 'Chiba', 'CH', 1),
(1661, 107, 'Ehime', 'EH', 1),
(1662, 107, 'Fukui', 'FK', 1),
(1663, 107, 'Fukuoka', 'FU', 1),
(1664, 107, 'Fukushima', 'FS', 1),
(1665, 107, 'Gifu', 'GI', 1),
(1666, 107, 'Gumma', 'GU', 1),
(1667, 107, 'Hiroshima', 'HI', 1),
(1668, 107, 'Hokkaido', 'HO', 1),
(1669, 107, 'Hyogo', 'HY', 1),
(1670, 107, 'Ibaraki', 'IB', 1),
(1671, 107, 'Ishikawa', 'IS', 1),
(1672, 107, 'Iwate', 'IW', 1),
(1673, 107, 'Kagawa', 'KA', 1),
(1674, 107, 'Kagoshima', 'KG', 1),
(1675, 107, 'Kanagawa', 'KN', 1),
(1676, 107, 'Kochi', 'KO', 1),
(1677, 107, 'Kumamoto', 'KU', 1),
(1678, 107, 'Kyoto', 'KY', 1),
(1679, 107, 'Mie', 'MI', 1),
(1680, 107, 'Miyagi', 'MY', 1),
(1681, 107, 'Miyazaki', 'MZ', 1),
(1682, 107, 'Nagano', 'NA', 1),
(1683, 107, 'Nagasaki', 'NG', 1),
(1684, 107, 'Nara', 'NR', 1),
(1685, 107, 'Niigata', 'NI', 1),
(1686, 107, 'Oita', 'OI', 1),
(1687, 107, 'Okayama', 'OK', 1),
(1688, 107, 'Okinawa', 'ON', 1),
(1689, 107, 'Osaka', 'OS', 1),
(1690, 107, 'Saga', 'SA', 1),
(1691, 107, 'Saitama', 'SI', 1),
(1692, 107, 'Shiga', 'SH', 1),
(1693, 107, 'Shimane', 'SM', 1),
(1694, 107, 'Shizuoka', 'SZ', 1),
(1695, 107, 'Tochigi', 'TO', 1),
(1696, 107, 'Tokushima', 'TS', 1),
(1697, 107, 'Tokyo', 'TK', 1),
(1698, 107, 'Tottori', 'TT', 1),
(1699, 107, 'Toyama', 'TY', 1),
(1700, 107, 'Wakayama', 'WA', 1),
(1701, 107, 'Yamagata', 'YA', 1),
(1702, 107, 'Yamaguchi', 'YM', 1),
(1703, 107, 'Yamanashi', 'YN', 1),
(1704, 108, '\'Amman', 'AM', 1),
(1705, 108, 'Ajlun', 'AJ', 1),
(1706, 108, 'Al \'Aqabah', 'AA', 1),
(1707, 108, 'Al Balqa\'', 'AB', 1),
(1708, 108, 'Al Karak', 'AK', 1),
(1709, 108, 'Al Mafraq', 'AL', 1),
(1710, 108, 'At Tafilah', 'AT', 1),
(1711, 108, 'Az Zarqa\'', 'AZ', 1),
(1712, 108, 'Irbid', 'IR', 1),
(1713, 108, 'Jarash', 'JA', 1),
(1714, 108, 'Ma\'an', 'MA', 1),
(1715, 108, 'Madaba', 'MD', 1),
(1716, 109, 'Алматинская область', 'AL', 1),
(1717, 109, 'Алматы - город республ-го значения', 'AC', 1),
(1718, 109, 'Акмолинская область', 'AM', 1),
(1719, 109, 'Актюбинская область', 'AQ', 1),
(1720, 109, 'Астана - город республ-го значения', 'AS', 1),
(1721, 109, 'Атырауская область', 'AT', 1),
(1722, 109, 'Западно-Казахстанская область', 'BA', 1),
(1723, 109, 'Байконур - город республ-го значения', 'BY', 1),
(1724, 109, 'Мангистауская область', 'MA', 1),
(1725, 109, 'Южно-Казахстанская область', 'ON', 1),
(1726, 109, 'Павлодарская область', 'PA', 1),
(1727, 109, 'Карагандинская область', 'QA', 1),
(1728, 109, 'Костанайская область', 'QO', 1),
(1729, 109, 'Кызылординская область', 'QY', 1),
(1730, 109, 'Восточно-Казахстанская область', 'SH', 1),
(1731, 109, 'Северо-Казахстанская область', 'SO', 1),
(1732, 109, 'Жамбылская область', 'ZH', 1),
(1733, 110, 'Central', 'CE', 1),
(1734, 110, 'Coast', 'CO', 1),
(1735, 110, 'Eastern', 'EA', 1),
(1736, 110, 'Nairobi Area', 'NA', 1),
(1737, 110, 'North Eastern', 'NE', 1),
(1738, 110, 'Nyanza', 'NY', 1),
(1739, 110, 'Rift Valley', 'RV', 1),
(1740, 110, 'Western', 'WE', 1),
(1741, 111, 'Abaiang', 'AG', 1),
(1742, 111, 'Abemama', 'AM', 1),
(1743, 111, 'Aranuka', 'AK', 1),
(1744, 111, 'Arorae', 'AO', 1),
(1745, 111, 'Banaba', 'BA', 1),
(1746, 111, 'Beru', 'BE', 1),
(1747, 111, 'Butaritari', 'bT', 1),
(1748, 111, 'Kanton', 'KA', 1),
(1749, 111, 'Kiritimati', 'KR', 1),
(1750, 111, 'Kuria', 'KU', 1),
(1751, 111, 'Maiana', 'MI', 1),
(1752, 111, 'Makin', 'MN', 1),
(1753, 111, 'Marakei', 'ME', 1),
(1754, 111, 'Nikunau', 'NI', 1),
(1755, 111, 'Nonouti', 'NO', 1),
(1756, 111, 'Onotoa', 'ON', 1),
(1757, 111, 'Tabiteuea', 'TT', 1),
(1758, 111, 'Tabuaeran', 'TR', 1),
(1759, 111, 'Tamana', 'TM', 1),
(1760, 111, 'Tarawa', 'TW', 1),
(1761, 111, 'Teraina', 'TE', 1),
(1762, 112, 'Chagang-do', 'CHA', 1),
(1763, 112, 'Hamgyong-bukto', 'HAB', 1),
(1764, 112, 'Hamgyong-namdo', 'HAN', 1),
(1765, 112, 'Hwanghae-bukto', 'HWB', 1),
(1766, 112, 'Hwanghae-namdo', 'HWN', 1),
(1767, 112, 'Kangwon-do', 'KAN', 1),
(1768, 112, 'P\'yongan-bukto', 'PYB', 1),
(1769, 112, 'P\'yongan-namdo', 'PYN', 1),
(1770, 112, 'Ryanggang-do (Yanggang-do)', 'YAN', 1),
(1771, 112, 'Rason Directly Governed City', 'NAJ', 1),
(1772, 112, 'P\'yongyang Special City', 'PYO', 1),
(1773, 113, 'Ch\'ungch\'ong-bukto', 'CO', 1),
(1774, 113, 'Ch\'ungch\'ong-namdo', 'CH', 1),
(1775, 113, 'Cheju-do', 'CD', 1),
(1776, 113, 'Cholla-bukto', 'CB', 1),
(1777, 113, 'Cholla-namdo', 'CN', 1),
(1778, 113, 'Inch\'on-gwangyoksi', 'IG', 1),
(1779, 113, 'Kangwon-do', 'KA', 1),
(1780, 113, 'Kwangju-gwangyoksi', 'KG', 1),
(1781, 113, 'Kyonggi-do', 'KD', 1),
(1782, 113, 'Kyongsang-bukto', 'KB', 1),
(1783, 113, 'Kyongsang-namdo', 'KN', 1),
(1784, 113, 'Pusan-gwangyoksi', 'PG', 1),
(1785, 113, 'Soul-t\'ukpyolsi', 'SO', 1),
(1786, 113, 'Taegu-gwangyoksi', 'TA', 1),
(1787, 113, 'Taejon-gwangyoksi', 'TG', 1),
(1788, 114, 'Al \'Asimah', 'AL', 1),
(1789, 114, 'Al Ahmadi', 'AA', 1),
(1790, 114, 'Al Farwaniyah', 'AF', 1),
(1791, 114, 'Al Jahra\'', 'AJ', 1),
(1792, 114, 'Hawalli', 'HA', 1),
(1793, 115, 'Bishkek', 'GB', 1),
(1794, 115, 'Batken', 'B', 1),
(1795, 115, 'Chu', 'C', 1),
(1796, 115, 'Jalal-Abad', 'J', 1),
(1797, 115, 'Naryn', 'N', 1),
(1798, 115, 'Osh', 'O', 1),
(1799, 115, 'Talas', 'T', 1),
(1800, 115, 'Ysyk-Kol', 'Y', 1),
(1801, 116, 'Vientiane', 'VT', 1),
(1802, 116, 'Attapu', 'AT', 1),
(1803, 116, 'Bokeo', 'BK', 1),
(1804, 116, 'Bolikhamxai', 'BL', 1),
(1805, 116, 'Champasak', 'CH', 1),
(1806, 116, 'Houaphan', 'HO', 1),
(1807, 116, 'Khammouan', 'KH', 1),
(1808, 116, 'Louang Namtha', 'LM', 1),
(1809, 116, 'Louangphabang', 'LP', 1),
(1810, 116, 'Oudomxai', 'OU', 1),
(1811, 116, 'Phongsali', 'PH', 1),
(1812, 116, 'Salavan', 'SL', 1),
(1813, 116, 'Savannakhet', 'SV', 1),
(1814, 116, 'Vientiane', 'VI', 1),
(1815, 116, 'Xaignabouli', 'XA', 1),
(1816, 116, 'Xekong', 'XE', 1),
(1817, 116, 'Xiangkhoang', 'XI', 1),
(1818, 116, 'Xaisomboun', 'XN', 1),
(1852, 119, 'Berea', 'BE', 1),
(1853, 119, 'Butha-Buthe', 'BB', 1),
(1854, 119, 'Leribe', 'LE', 1),
(1855, 119, 'Mafeteng', 'MF', 1),
(1856, 119, 'Maseru', 'MS', 1),
(1857, 119, 'Mohale\'s Hoek', 'MH', 1),
(1858, 119, 'Mokhotlong', 'MK', 1),
(1859, 119, 'Qacha\'s Nek', 'QN', 1),
(1860, 119, 'Quthing', 'QT', 1),
(1861, 119, 'Thaba-Tseka', 'TT', 1),
(1862, 120, 'Bomi', 'BI', 1),
(1863, 120, 'Bong', 'BG', 1),
(1864, 120, 'Grand Bassa', 'GB', 1),
(1865, 120, 'Grand Cape Mount', 'CM', 1),
(1866, 120, 'Grand Gedeh', 'GG', 1),
(1867, 120, 'Grand Kru', 'GK', 1),
(1868, 120, 'Lofa', 'LO', 1),
(1869, 120, 'Margibi', 'MG', 1),
(1870, 120, 'Maryland', 'ML', 1),
(1871, 120, 'Montserrado', 'MS', 1),
(1872, 120, 'Nimba', 'NB', 1),
(1873, 120, 'River Cess', 'RC', 1),
(1874, 120, 'Sinoe', 'SN', 1),
(1875, 121, 'Ajdabiya', 'AJ', 1),
(1876, 121, 'Al \'Aziziyah', 'AZ', 1),
(1877, 121, 'Al Fatih', 'FA', 1),
(1878, 121, 'Al Jabal al Akhdar', 'JA', 1),
(1879, 121, 'Al Jufrah', 'JU', 1),
(1880, 121, 'Al Khums', 'KH', 1),
(1881, 121, 'Al Kufrah', 'KU', 1),
(1882, 121, 'An Nuqat al Khams', 'NK', 1),
(1883, 121, 'Ash Shati\'', 'AS', 1),
(1884, 121, 'Awbari', 'AW', 1),
(1885, 121, 'Az Zawiyah', 'ZA', 1),
(1886, 121, 'Banghazi', 'BA', 1),
(1887, 121, 'Darnah', 'DA', 1),
(1888, 121, 'Ghadamis', 'GD', 1),
(1889, 121, 'Gharyan', 'GY', 1),
(1890, 121, 'Misratah', 'MI', 1),
(1891, 121, 'Murzuq', 'MZ', 1),
(1892, 121, 'Sabha', 'SB', 1),
(1893, 121, 'Sawfajjin', 'SW', 1),
(1894, 121, 'Surt', 'SU', 1),
(1895, 121, 'Tarabulus (Tripoli)', 'TL', 1),
(1896, 121, 'Tarhunah', 'TH', 1),
(1897, 121, 'Tubruq', 'TU', 1),
(1898, 121, 'Yafran', 'YA', 1),
(1899, 121, 'Zlitan', 'ZL', 1),
(1900, 122, 'Vaduz', 'V', 1),
(1901, 122, 'Schaan', 'A', 1),
(1902, 122, 'Balzers', 'B', 1),
(1903, 122, 'Triesen', 'N', 1),
(1904, 122, 'Eschen', 'E', 1),
(1905, 122, 'Mauren', 'M', 1),
(1906, 122, 'Triesenberg', 'T', 1),
(1907, 122, 'Ruggell', 'R', 1),
(1908, 122, 'Gamprin', 'G', 1),
(1909, 122, 'Schellenberg', 'L', 1),
(1910, 122, 'Planken', 'P', 1),
(1911, 123, 'Alytus', 'AL', 1),
(1912, 123, 'Kaunas', 'KA', 1),
(1913, 123, 'Klaipeda', 'KL', 1),
(1914, 123, 'Marijampole', 'MA', 1),
(1915, 123, 'Panevezys', 'PA', 1),
(1916, 123, 'Siauliai', 'SI', 1),
(1917, 123, 'Taurage', 'TA', 1),
(1918, 123, 'Telsiai', 'TE', 1),
(1919, 123, 'Utena', 'UT', 1),
(1920, 123, 'Vilnius', 'VI', 1),
(1921, 124, 'Diekirch', 'DD', 1),
(1922, 124, 'Clervaux', 'DC', 1),
(1923, 124, 'Redange', 'DR', 1),
(1924, 124, 'Vianden', 'DV', 1),
(1925, 124, 'Wiltz', 'DW', 1),
(1926, 124, 'Grevenmacher', 'GG', 1),
(1927, 124, 'Echternach', 'GE', 1),
(1928, 124, 'Remich', 'GR', 1),
(1929, 124, 'Luxembourg', 'LL', 1),
(1930, 124, 'Capellen', 'LC', 1),
(1931, 124, 'Esch-sur-Alzette', 'LE', 1),
(1932, 124, 'Mersch', 'LM', 1),
(1933, 125, 'Our Lady Fatima Parish', 'OLF', 1),
(1934, 125, 'St. Anthony Parish', 'ANT', 1),
(1935, 125, 'St. Lazarus Parish', 'LAZ', 1),
(1936, 125, 'Cathedral Parish', 'CAT', 1),
(1937, 125, 'St. Lawrence Parish', 'LAW', 1),
(1938, 127, 'Antananarivo', 'AN', 1),
(1939, 127, 'Antsiranana', 'AS', 1),
(1940, 127, 'Fianarantsoa', 'FN', 1),
(1941, 127, 'Mahajanga', 'MJ', 1),
(1942, 127, 'Toamasina', 'TM', 1),
(1943, 127, 'Toliara', 'TL', 1),
(1944, 128, 'Balaka', 'BLK', 1),
(1945, 128, 'Blantyre', 'BLT', 1),
(1946, 128, 'Chikwawa', 'CKW', 1),
(1947, 128, 'Chiradzulu', 'CRD', 1),
(1948, 128, 'Chitipa', 'CTP', 1),
(1949, 128, 'Dedza', 'DDZ', 1),
(1950, 128, 'Dowa', 'DWA', 1),
(1951, 128, 'Karonga', 'KRG', 1),
(1952, 128, 'Kasungu', 'KSG', 1),
(1953, 128, 'Likoma', 'LKM', 1),
(1954, 128, 'Lilongwe', 'LLG', 1),
(1955, 128, 'Machinga', 'MCG', 1),
(1956, 128, 'Mangochi', 'MGC', 1),
(1957, 128, 'Mchinji', 'MCH', 1),
(1958, 128, 'Mulanje', 'MLJ', 1),
(1959, 128, 'Mwanza', 'MWZ', 1),
(1960, 128, 'Mzimba', 'MZM', 1),
(1961, 128, 'Ntcheu', 'NTU', 1),
(1962, 128, 'Nkhata Bay', 'NKB', 1),
(1963, 128, 'Nkhotakota', 'NKH', 1),
(1964, 128, 'Nsanje', 'NSJ', 1),
(1965, 128, 'Ntchisi', 'NTI', 1),
(1966, 128, 'Phalombe', 'PHL', 1),
(1967, 128, 'Rumphi', 'RMP', 1),
(1968, 128, 'Salima', 'SLM', 1),
(1969, 128, 'Thyolo', 'THY', 1),
(1970, 128, 'Zomba', 'ZBA', 1),
(1971, 129, 'Johor', 'MY-01', 1),
(1972, 129, 'Kedah', 'MY-02', 1),
(1973, 129, 'Kelantan', 'MY-03', 1),
(1974, 129, 'Labuan', 'MY-15', 1),
(1975, 129, 'Melaka', 'MY-04', 1),
(1976, 129, 'Negeri Sembilan', 'MY-05', 1),
(1977, 129, 'Pahang', 'MY-06', 1),
(1978, 129, 'Perak', 'MY-08', 1),
(1979, 129, 'Perlis', 'MY-09', 1),
(1980, 129, 'Pulau Pinang', 'MY-07', 1),
(1981, 129, 'Sabah', 'MY-12', 1),
(1982, 129, 'Sarawak', 'MY-13', 1),
(1983, 129, 'Selangor', 'MY-10', 1),
(1984, 129, 'Terengganu', 'MY-11', 1),
(1985, 129, 'Kuala Lumpur', 'MY-14', 1),
(1986, 130, 'Thiladhunmathi Uthuru', 'THU', 1),
(1987, 130, 'Thiladhunmathi Dhekunu', 'THD', 1),
(1988, 130, 'Miladhunmadulu Uthuru', 'MLU', 1),
(1989, 130, 'Miladhunmadulu Dhekunu', 'MLD', 1),
(1990, 130, 'Maalhosmadulu Uthuru', 'MAU', 1),
(1991, 130, 'Maalhosmadulu Dhekunu', 'MAD', 1),
(1992, 130, 'Faadhippolhu', 'FAA', 1),
(1993, 130, 'Male Atoll', 'MAA', 1),
(1994, 130, 'Ari Atoll Uthuru', 'AAU', 1),
(1995, 130, 'Ari Atoll Dheknu', 'AAD', 1),
(1996, 130, 'Felidhe Atoll', 'FEA', 1),
(1997, 130, 'Mulaku Atoll', 'MUA', 1),
(1998, 130, 'Nilandhe Atoll Uthuru', 'NAU', 1),
(1999, 130, 'Nilandhe Atoll Dhekunu', 'NAD', 1),
(2000, 130, 'Kolhumadulu', 'KLH', 1),
(2001, 130, 'Hadhdhunmathi', 'HDH', 1),
(2002, 130, 'Huvadhu Atoll Uthuru', 'HAU', 1),
(2003, 130, 'Huvadhu Atoll Dhekunu', 'HAD', 1),
(2004, 130, 'Fua Mulaku', 'FMU', 1),
(2005, 130, 'Addu', 'ADD', 1),
(2006, 131, 'Gao', 'GA', 1),
(2007, 131, 'Kayes', 'KY', 1),
(2008, 131, 'Kidal', 'KD', 1),
(2009, 131, 'Koulikoro', 'KL', 1),
(2010, 131, 'Mopti', 'MP', 1),
(2011, 131, 'Segou', 'SG', 1),
(2012, 131, 'Sikasso', 'SK', 1),
(2013, 131, 'Tombouctou', 'TB', 1),
(2014, 131, 'Bamako Capital District', 'CD', 1),
(2015, 132, 'Attard', 'ATT', 1),
(2016, 132, 'Balzan', 'BAL', 1),
(2017, 132, 'Birgu', 'BGU', 1),
(2018, 132, 'Birkirkara', 'BKK', 1),
(2019, 132, 'Birzebbuga', 'BRZ', 1),
(2020, 132, 'Bormla', 'BOR', 1),
(2021, 132, 'Dingli', 'DIN', 1),
(2022, 132, 'Fgura', 'FGU', 1),
(2023, 132, 'Floriana', 'FLO', 1),
(2024, 132, 'Gudja', 'GDJ', 1),
(2025, 132, 'Gzira', 'GZR', 1),
(2026, 132, 'Gargur', 'GRG', 1),
(2027, 132, 'Gaxaq', 'GXQ', 1),
(2028, 132, 'Hamrun', 'HMR', 1),
(2029, 132, 'Iklin', 'IKL', 1),
(2030, 132, 'Isla', 'ISL', 1),
(2031, 132, 'Kalkara', 'KLK', 1),
(2032, 132, 'Kirkop', 'KRK', 1),
(2033, 132, 'Lija', 'LIJ', 1),
(2034, 132, 'Luqa', 'LUQ', 1),
(2035, 132, 'Marsa', 'MRS', 1),
(2036, 132, 'Marsaskala', 'MKL', 1),
(2037, 132, 'Marsaxlokk', 'MXL', 1),
(2038, 132, 'Mdina', 'MDN', 1),
(2039, 132, 'Melliea', 'MEL', 1),
(2040, 132, 'Mgarr', 'MGR', 1),
(2041, 132, 'Mosta', 'MST', 1),
(2042, 132, 'Mqabba', 'MQA', 1),
(2043, 132, 'Msida', 'MSI', 1),
(2044, 132, 'Mtarfa', 'MTF', 1),
(2045, 132, 'Naxxar', 'NAX', 1),
(2046, 132, 'Paola', 'PAO', 1),
(2047, 132, 'Pembroke', 'PEM', 1),
(2048, 132, 'Pieta', 'PIE', 1),
(2049, 132, 'Qormi', 'QOR', 1),
(2050, 132, 'Qrendi', 'QRE', 1),
(2051, 132, 'Rabat', 'RAB', 1),
(2052, 132, 'Safi', 'SAF', 1),
(2053, 132, 'San Giljan', 'SGI', 1),
(2054, 132, 'Santa Lucija', 'SLU', 1),
(2055, 132, 'San Pawl il-Bahar', 'SPB', 1),
(2056, 132, 'San Gwann', 'SGW', 1),
(2057, 132, 'Santa Venera', 'SVE', 1),
(2058, 132, 'Siggiewi', 'SIG', 1),
(2059, 132, 'Sliema', 'SLM', 1),
(2060, 132, 'Swieqi', 'SWQ', 1),
(2061, 132, 'Ta Xbiex', 'TXB', 1),
(2062, 132, 'Tarxien', 'TRX', 1),
(2063, 132, 'Valletta', 'VLT', 1),
(2064, 132, 'Xgajra', 'XGJ', 1),
(2065, 132, 'Zabbar', 'ZBR', 1),
(2066, 132, 'Zebbug', 'ZBG', 1),
(2067, 132, 'Zejtun', 'ZJT', 1),
(2068, 132, 'Zurrieq', 'ZRQ', 1),
(2069, 132, 'Fontana', 'FNT', 1),
(2070, 132, 'Ghajnsielem', 'GHJ', 1),
(2071, 132, 'Gharb', 'GHR', 1),
(2072, 132, 'Ghasri', 'GHS', 1),
(2073, 132, 'Kercem', 'KRC', 1),
(2074, 132, 'Munxar', 'MUN', 1),
(2075, 132, 'Nadur', 'NAD', 1),
(2076, 132, 'Qala', 'QAL', 1),
(2077, 132, 'Victoria', 'VIC', 1),
(2078, 132, 'San Lawrenz', 'SLA', 1),
(2079, 132, 'Sannat', 'SNT', 1),
(2080, 132, 'Xagra', 'ZAG', 1),
(2081, 132, 'Xewkija', 'XEW', 1),
(2082, 132, 'Zebbug', 'ZEB', 1),
(2083, 133, 'Ailinginae', 'ALG', 1),
(2084, 133, 'Ailinglaplap', 'ALL', 1),
(2085, 133, 'Ailuk', 'ALK', 1),
(2086, 133, 'Arno', 'ARN', 1),
(2087, 133, 'Aur', 'AUR', 1),
(2088, 133, 'Bikar', 'BKR', 1),
(2089, 133, 'Bikini', 'BKN', 1),
(2090, 133, 'Bokak', 'BKK', 1),
(2091, 133, 'Ebon', 'EBN', 1),
(2092, 133, 'Enewetak', 'ENT', 1),
(2093, 133, 'Erikub', 'EKB', 1),
(2094, 133, 'Jabat', 'JBT', 1),
(2095, 133, 'Jaluit', 'JLT', 1),
(2096, 133, 'Jemo', 'JEM', 1),
(2097, 133, 'Kili', 'KIL', 1),
(2098, 133, 'Kwajalein', 'KWJ', 1),
(2099, 133, 'Lae', 'LAE', 1),
(2100, 133, 'Lib', 'LIB', 1),
(2101, 133, 'Likiep', 'LKP', 1),
(2102, 133, 'Majuro', 'MJR', 1),
(2103, 133, 'Maloelap', 'MLP', 1),
(2104, 133, 'Mejit', 'MJT', 1),
(2105, 133, 'Mili', 'MIL', 1),
(2106, 133, 'Namorik', 'NMK', 1),
(2107, 133, 'Namu', 'NAM', 1),
(2108, 133, 'Rongelap', 'RGL', 1),
(2109, 133, 'Rongrik', 'RGK', 1),
(2110, 133, 'Toke', 'TOK', 1),
(2111, 133, 'Ujae', 'UJA', 1),
(2112, 133, 'Ujelang', 'UJL', 1),
(2113, 133, 'Utirik', 'UTK', 1),
(2114, 133, 'Wotho', 'WTH', 1),
(2115, 133, 'Wotje', 'WTJ', 1),
(2116, 135, 'Adrar', 'AD', 1),
(2117, 135, 'Assaba', 'AS', 1),
(2118, 135, 'Brakna', 'BR', 1),
(2119, 135, 'Dakhlet Nouadhibou', 'DN', 1),
(2120, 135, 'Gorgol', 'GO', 1),
(2121, 135, 'Guidimaka', 'GM', 1),
(2122, 135, 'Hodh Ech Chargui', 'HC', 1),
(2123, 135, 'Hodh El Gharbi', 'HG', 1),
(2124, 135, 'Inchiri', 'IN', 1),
(2125, 135, 'Tagant', 'TA', 1),
(2126, 135, 'Tiris Zemmour', 'TZ', 1),
(2127, 135, 'Trarza', 'TR', 1),
(2128, 135, 'Nouakchott', 'NO', 1),
(2129, 136, 'Beau Bassin-Rose Hill', 'BR', 1),
(2130, 136, 'Curepipe', 'CU', 1),
(2131, 136, 'Port Louis', 'PU', 1),
(2132, 136, 'Quatre Bornes', 'QB', 1),
(2133, 136, 'Vacoas-Phoenix', 'VP', 1),
(2134, 136, 'Agalega Islands', 'AG', 1),
(2135, 136, 'Cargados Carajos Shoals (Saint Brandon Islands)', 'CC', 1),
(2136, 136, 'Rodrigues', 'RO', 1),
(2137, 136, 'Black River', 'BL', 1),
(2138, 136, 'Flacq', 'FL', 1),
(2139, 136, 'Grand Port', 'GP', 1),
(2140, 136, 'Moka', 'MO', 1),
(2141, 136, 'Pamplemousses', 'PA', 1),
(2142, 136, 'Plaines Wilhems', 'PW', 1),
(2143, 136, 'Port Louis', 'PL', 1),
(2144, 136, 'Riviere du Rempart', 'RR', 1),
(2145, 136, 'Savanne', 'SA', 1),
(2146, 138, 'Baja California Norte', 'BN', 1),
(2147, 138, 'Baja California Sur', 'BS', 1),
(2148, 138, 'Campeche', 'CA', 1),
(2149, 138, 'Chiapas', 'CI', 1),
(2150, 138, 'Chihuahua', 'CH', 1),
(2151, 138, 'Coahuila de Zaragoza', 'CZ', 1),
(2152, 138, 'Colima', 'CL', 1),
(2153, 138, 'Distrito Federal', 'DF', 1),
(2154, 138, 'Durango', 'DU', 1),
(2155, 138, 'Guanajuato', 'GA', 1),
(2156, 138, 'Guerrero', 'GE', 1),
(2157, 138, 'Hidalgo', 'HI', 1),
(2158, 138, 'Jalisco', 'JA', 1),
(2159, 138, 'Mexico', 'ME', 1),
(2160, 138, 'Michoacan de Ocampo', 'MI', 1),
(2161, 138, 'Morelos', 'MO', 1),
(2162, 138, 'Nayarit', 'NA', 1),
(2163, 138, 'Nuevo Leon', 'NL', 1),
(2164, 138, 'Oaxaca', 'OA', 1),
(2165, 138, 'Puebla', 'PU', 1),
(2166, 138, 'Queretaro de Arteaga', 'QA', 1),
(2167, 138, 'Quintana Roo', 'QR', 1),
(2168, 138, 'San Luis Potosi', 'SA', 1),
(2169, 138, 'Sinaloa', 'SI', 1),
(2170, 138, 'Sonora', 'SO', 1),
(2171, 138, 'Tabasco', 'TB', 1),
(2172, 138, 'Tamaulipas', 'TM', 1),
(2173, 138, 'Tlaxcala', 'TL', 1),
(2174, 138, 'Veracruz-Llave', 'VE', 1),
(2175, 138, 'Yucatan', 'YU', 1),
(2176, 138, 'Zacatecas', 'ZA', 1),
(2177, 139, 'Chuuk', 'C', 1),
(2178, 139, 'Kosrae', 'K', 1),
(2179, 139, 'Pohnpei', 'P', 1),
(2180, 139, 'Yap', 'Y', 1),
(2181, 140, 'Gagauzia', 'GA', 1),
(2182, 140, 'Chisinau', 'CU', 1),
(2183, 140, 'Balti', 'BA', 1),
(2184, 140, 'Cahul', 'CA', 1),
(2185, 140, 'Edinet', 'ED', 1),
(2186, 140, 'Lapusna', 'LA', 1),
(2187, 140, 'Orhei', 'OR', 1),
(2188, 140, 'Soroca', 'SO', 1),
(2189, 140, 'Tighina', 'TI', 1),
(2190, 140, 'Ungheni', 'UN', 1),
(2191, 140, 'St‚nga Nistrului', 'SN', 1),
(2192, 141, 'Fontvieille', 'FV', 1),
(2193, 141, 'La Condamine', 'LC', 1),
(2194, 141, 'Monaco-Ville', 'MV', 1),
(2195, 141, 'Monte-Carlo', 'MC', 1),
(2196, 142, 'Ulanbaatar', '1', 1),
(2197, 142, 'Orhon', '035', 1),
(2198, 142, 'Darhan uul', '037', 1),
(2199, 142, 'Hentiy', '039', 1),
(2200, 142, 'Hovsgol', '041', 1),
(2201, 142, 'Hovd', '043', 1),
(2202, 142, 'Uvs', '046', 1),
(2203, 142, 'Tov', '047', 1),
(2204, 142, 'Selenge', '049', 1),
(2205, 142, 'Suhbaatar', '051', 1),
(2206, 142, 'Omnogovi', '053', 1),
(2207, 142, 'Ovorhangay', '055', 1),
(2208, 142, 'Dzavhan', '057', 1),
(2209, 142, 'DundgovL', '059', 1),
(2210, 142, 'Dornod', '061', 1),
(2211, 142, 'Dornogov', '063', 1),
(2212, 142, 'Govi-Sumber', '064', 1),
(2213, 142, 'Govi-Altay', '065', 1),
(2214, 142, 'Bulgan', '067', 1),
(2215, 142, 'Bayanhongor', '069', 1),
(2216, 142, 'Bayan-Olgiy', '071', 1),
(2217, 142, 'Arhangay', '073', 1),
(2218, 143, 'Saint Anthony', 'A', 1),
(2219, 143, 'Saint Georges', 'G', 1),
(2220, 143, 'Saint Peter', 'P', 1),
(2221, 144, 'Agadir', 'AGD', 1),
(2222, 144, 'Al Hoceima', 'HOC', 1),
(2223, 144, 'Azilal', 'AZI', 1),
(2224, 144, 'Beni Mellal', 'BME', 1),
(2225, 144, 'Ben Slimane', 'BSL', 1),
(2226, 144, 'Boulemane', 'BLM', 1),
(2227, 144, 'Casablanca', 'CBL', 1),
(2228, 144, 'Chaouen', 'CHA', 1),
(2229, 144, 'El Jadida', 'EJA', 1),
(2230, 144, 'El Kelaa des Sraghna', 'EKS', 1),
(2231, 144, 'Er Rachidia', 'ERA', 1),
(2232, 144, 'Essaouira', 'ESS', 1),
(2233, 144, 'Fes', 'FES', 1),
(2234, 144, 'Figuig', 'FIG', 1),
(2235, 144, 'Guelmim', 'GLM', 1),
(2236, 144, 'Ifrane', 'IFR', 1),
(2237, 144, 'Kenitra', 'KEN', 1),
(2238, 144, 'Khemisset', 'KHM', 1),
(2239, 144, 'Khenifra', 'KHN', 1),
(2240, 144, 'Khouribga', 'KHO', 1),
(2241, 144, 'Laayoune', 'LYN', 1),
(2242, 144, 'Larache', 'LAR', 1),
(2243, 144, 'Marrakech', 'MRK', 1),
(2244, 144, 'Meknes', 'MKN', 1),
(2245, 144, 'Nador', 'NAD', 1),
(2246, 144, 'Ouarzazate', 'ORZ', 1),
(2247, 144, 'Oujda', 'OUJ', 1),
(2248, 144, 'Rabat-Sale', 'RSA', 1),
(2249, 144, 'Safi', 'SAF', 1),
(2250, 144, 'Settat', 'SET', 1),
(2251, 144, 'Sidi Kacem', 'SKA', 1),
(2252, 144, 'Tangier', 'TGR', 1),
(2253, 144, 'Tan-Tan', 'TAN', 1),
(2254, 144, 'Taounate', 'TAO', 1),
(2255, 144, 'Taroudannt', 'TRD', 1),
(2256, 144, 'Tata', 'TAT', 1),
(2257, 144, 'Taza', 'TAZ', 1),
(2258, 144, 'Tetouan', 'TET', 1),
(2259, 144, 'Tiznit', 'TIZ', 1),
(2260, 144, 'Ad Dakhla', 'ADK', 1),
(2261, 144, 'Boujdour', 'BJD', 1),
(2262, 144, 'Es Smara', 'ESM', 1),
(2263, 145, 'Cabo Delgado', 'CD', 1),
(2264, 145, 'Gaza', 'GZ', 1),
(2265, 145, 'Inhambane', 'IN', 1),
(2266, 145, 'Manica', 'MN', 1),
(2267, 145, 'Maputo (city)', 'MC', 1),
(2268, 145, 'Maputo', 'MP', 1),
(2269, 145, 'Nampula', 'NA', 1),
(2270, 145, 'Niassa', 'NI', 1),
(2271, 145, 'Sofala', 'SO', 1),
(2272, 145, 'Tete', 'TE', 1),
(2273, 145, 'Zambezia', 'ZA', 1),
(2274, 146, 'Ayeyarwady', 'AY', 1),
(2275, 146, 'Bago', 'BG', 1),
(2276, 146, 'Magway', 'MG', 1),
(2277, 146, 'Mandalay', 'MD', 1),
(2278, 146, 'Sagaing', 'SG', 1),
(2279, 146, 'Tanintharyi', 'TN', 1),
(2280, 146, 'Yangon', 'YG', 1),
(2281, 146, 'Chin State', 'CH', 1),
(2282, 146, 'Kachin State', 'KC', 1),
(2283, 146, 'Kayah State', 'KH', 1),
(2284, 146, 'Kayin State', 'KN', 1),
(2285, 146, 'Mon State', 'MN', 1),
(2286, 146, 'Rakhine State', 'RK', 1),
(2287, 146, 'Shan State', 'SH', 1),
(2288, 147, 'Caprivi', 'CA', 1),
(2289, 147, 'Erongo', 'ER', 1),
(2290, 147, 'Hardap', 'HA', 1),
(2291, 147, 'Karas', 'KR', 1),
(2292, 147, 'Kavango', 'KV', 1),
(2293, 147, 'Khomas', 'KH', 1),
(2294, 147, 'Kunene', 'KU', 1),
(2295, 147, 'Ohangwena', 'OW', 1),
(2296, 147, 'Omaheke', 'OK', 1),
(2297, 147, 'Omusati', 'OT', 1),
(2298, 147, 'Oshana', 'ON', 1),
(2299, 147, 'Oshikoto', 'OO', 1),
(2300, 147, 'Otjozondjupa', 'OJ', 1),
(2301, 148, 'Aiwo', 'AO', 1),
(2302, 148, 'Anabar', 'AA', 1),
(2303, 148, 'Anetan', 'AT', 1),
(2304, 148, 'Anibare', 'AI', 1),
(2305, 148, 'Baiti', 'BA', 1),
(2306, 148, 'Boe', 'BO', 1),
(2307, 148, 'Buada', 'BU', 1),
(2308, 148, 'Denigomodu', 'DE', 1),
(2309, 148, 'Ewa', 'EW', 1),
(2310, 148, 'Ijuw', 'IJ', 1),
(2311, 148, 'Meneng', 'ME', 1),
(2312, 148, 'Nibok', 'NI', 1),
(2313, 148, 'Uaboe', 'UA', 1),
(2314, 148, 'Yaren', 'YA', 1),
(2315, 149, 'Bagmati', 'BA', 1),
(2316, 149, 'Bheri', 'BH', 1),
(2317, 149, 'Dhawalagiri', 'DH', 1),
(2318, 149, 'Gandaki', 'GA', 1),
(2319, 149, 'Janakpur', 'JA', 1),
(2320, 149, 'Karnali', 'KA', 1),
(2321, 149, 'Kosi', 'KO', 1),
(2322, 149, 'Lumbini', 'LU', 1),
(2323, 149, 'Mahakali', 'MA', 1),
(2324, 149, 'Mechi', 'ME', 1),
(2325, 149, 'Narayani', 'NA', 1),
(2326, 149, 'Rapti', 'RA', 1),
(2327, 149, 'Sagarmatha', 'SA', 1),
(2328, 149, 'Seti', 'SE', 1),
(2329, 150, 'Drenthe', 'DR', 1),
(2330, 150, 'Flevoland', 'FL', 1),
(2331, 150, 'Friesland', 'FR', 1),
(2332, 150, 'Gelderland', 'GE', 1),
(2333, 150, 'Groningen', 'GR', 1),
(2334, 150, 'Limburg', 'LI', 1),
(2335, 150, 'Noord Brabant', 'NB', 1),
(2336, 150, 'Noord Holland', 'NH', 1),
(2337, 150, 'Overijssel', 'OV', 1),
(2338, 150, 'Utrecht', 'UT', 1),
(2339, 150, 'Zeeland', 'ZE', 1),
(2340, 150, 'Zuid Holland', 'ZH', 1),
(2341, 152, 'Iles Loyaute', 'L', 1),
(2342, 152, 'Nord', 'N', 1),
(2343, 152, 'Sud', 'S', 1),
(2344, 153, 'Auckland', 'AUK', 1),
(2345, 153, 'Bay of Plenty', 'BOP', 1),
(2346, 153, 'Canterbury', 'CAN', 1),
(2347, 153, 'Coromandel', 'COR', 1),
(2348, 153, 'Gisborne', 'GIS', 1),
(2349, 153, 'Fiordland', 'FIO', 1),
(2350, 153, 'Hawke\'s Bay', 'HKB', 1),
(2351, 153, 'Marlborough', 'MBH', 1),
(2352, 153, 'Manawatu-Wanganui', 'MWT', 1),
(2353, 153, 'Mt Cook-Mackenzie', 'MCM', 1),
(2354, 153, 'Nelson', 'NSN', 1),
(2355, 153, 'Northland', 'NTL', 1),
(2356, 153, 'Otago', 'OTA', 1),
(2357, 153, 'Southland', 'STL', 1),
(2358, 153, 'Taranaki', 'TKI', 1),
(2359, 153, 'Wellington', 'WGN', 1),
(2360, 153, 'Waikato', 'WKO', 1),
(2361, 153, 'Wairarapa', 'WAI', 1),
(2362, 153, 'West Coast', 'WTC', 1),
(2363, 154, 'Atlantico Norte', 'AN', 1),
(2364, 154, 'Atlantico Sur', 'AS', 1),
(2365, 154, 'Boaco', 'BO', 1),
(2366, 154, 'Carazo', 'CA', 1),
(2367, 154, 'Chinandega', 'CI', 1),
(2368, 154, 'Chontales', 'CO', 1),
(2369, 154, 'Esteli', 'ES', 1),
(2370, 154, 'Granada', 'GR', 1),
(2371, 154, 'Jinotega', 'JI', 1),
(2372, 154, 'Leon', 'LE', 1),
(2373, 154, 'Madriz', 'MD', 1),
(2374, 154, 'Managua', 'MN', 1),
(2375, 154, 'Masaya', 'MS', 1),
(2376, 154, 'Matagalpa', 'MT', 1),
(2377, 154, 'Nuevo Segovia', 'NS', 1),
(2378, 154, 'Rio San Juan', 'RS', 1),
(2379, 154, 'Rivas', 'RI', 1),
(2380, 155, 'Agadez', 'AG', 1),
(2381, 155, 'Diffa', 'DF', 1),
(2382, 155, 'Dosso', 'DS', 1),
(2383, 155, 'Maradi', 'MA', 1),
(2384, 155, 'Niamey', 'NM', 1),
(2385, 155, 'Tahoua', 'TH', 1),
(2386, 155, 'Tillaberi', 'TL', 1),
(2387, 155, 'Zinder', 'ZD', 1),
(2388, 156, 'Abia', 'AB', 1),
(2389, 156, 'Abuja Federal Capital Territory', 'CT', 1),
(2390, 156, 'Adamawa', 'AD', 1),
(2391, 156, 'Akwa Ibom', 'AK', 1),
(2392, 156, 'Anambra', 'AN', 1),
(2393, 156, 'Bauchi', 'BC', 1),
(2394, 156, 'Bayelsa', 'BY', 1),
(2395, 156, 'Benue', 'BN', 1),
(2396, 156, 'Borno', 'BO', 1),
(2397, 156, 'Cross River', 'CR', 1),
(2398, 156, 'Delta', 'DE', 1),
(2399, 156, 'Ebonyi', 'EB', 1),
(2400, 156, 'Edo', 'ED', 1),
(2401, 156, 'Ekiti', 'EK', 1),
(2402, 156, 'Enugu', 'EN', 1),
(2403, 156, 'Gombe', 'GO', 1),
(2404, 156, 'Imo', 'IM', 1),
(2405, 156, 'Jigawa', 'JI', 1),
(2406, 156, 'Kaduna', 'KD', 1),
(2407, 156, 'Kano', 'KN', 1),
(2408, 156, 'Katsina', 'KT', 1),
(2409, 156, 'Kebbi', 'KE', 1),
(2410, 156, 'Kogi', 'KO', 1),
(2411, 156, 'Kwara', 'KW', 1),
(2412, 156, 'Lagos', 'LA', 1),
(2413, 156, 'Nassarawa', 'NA', 1),
(2414, 156, 'Niger', 'NI', 1),
(2415, 156, 'Ogun', 'OG', 1),
(2416, 156, 'Ondo', 'ONG', 1),
(2417, 156, 'Osun', 'OS', 1),
(2418, 156, 'Oyo', 'OY', 1),
(2419, 156, 'Plateau', 'PL', 1),
(2420, 156, 'Rivers', 'RI', 1),
(2421, 156, 'Sokoto', 'SO', 1),
(2422, 156, 'Taraba', 'TA', 1),
(2423, 156, 'Yobe', 'YO', 1),
(2424, 156, 'Zamfara', 'ZA', 1),
(2425, 159, 'Northern Islands', 'N', 1),
(2426, 159, 'Rota', 'R', 1),
(2427, 159, 'Saipan', 'S', 1),
(2428, 159, 'Tinian', 'T', 1),
(2429, 160, 'Akershus', 'AK', 1),
(2430, 160, 'Aust-Agder', 'AA', 1),
(2431, 160, 'Buskerud', 'BU', 1),
(2432, 160, 'Finnmark', 'FM', 1),
(2433, 160, 'Hedmark', 'HM', 1),
(2434, 160, 'Hordaland', 'HL', 1),
(2435, 160, 'More og Romdal', 'MR', 1),
(2436, 160, 'Nord-Trondelag', 'NT', 1),
(2437, 160, 'Nordland', 'NL', 1),
(2438, 160, 'Ostfold', 'OF', 1),
(2439, 160, 'Oppland', 'OP', 1),
(2440, 160, 'Oslo', 'OL', 1),
(2441, 160, 'Rogaland', 'RL', 1),
(2442, 160, 'Sor-Trondelag', 'ST', 1),
(2443, 160, 'Sogn og Fjordane', 'SJ', 1),
(2444, 160, 'Svalbard', 'SV', 1),
(2445, 160, 'Telemark', 'TM', 1),
(2446, 160, 'Troms', 'TR', 1),
(2447, 160, 'Vest-Agder', 'VA', 1),
(2448, 160, 'Vestfold', 'VF', 1),
(2449, 161, 'Ad Dakhiliyah', 'DA', 1),
(2450, 161, 'Al Batinah', 'BA', 1),
(2451, 161, 'Al Wusta', 'WU', 1),
(2452, 161, 'Ash Sharqiyah', 'SH', 1),
(2453, 161, 'Az Zahirah', 'ZA', 1),
(2454, 161, 'Masqat', 'MA', 1),
(2455, 161, 'Musandam', 'MU', 1),
(2456, 161, 'Zufar', 'ZU', 1),
(2457, 162, 'Balochistan', 'B', 1),
(2458, 162, 'Federally Administered Tribal Areas', 'T', 1),
(2459, 162, 'Islamabad Capital Territory', 'I', 1),
(2460, 162, 'North-West Frontier', 'N', 1),
(2461, 162, 'Punjab', 'P', 1),
(2462, 162, 'Sindh', 'S', 1),
(2463, 163, 'Aimeliik', 'AM', 1),
(2464, 163, 'Airai', 'AR', 1),
(2465, 163, 'Angaur', 'AN', 1),
(2466, 163, 'Hatohobei', 'HA', 1),
(2467, 163, 'Kayangel', 'KA', 1),
(2468, 163, 'Koror', 'KO', 1),
(2469, 163, 'Melekeok', 'ME', 1),
(2470, 163, 'Ngaraard', 'NA', 1),
(2471, 163, 'Ngarchelong', 'NG', 1),
(2472, 163, 'Ngardmau', 'ND', 1),
(2473, 163, 'Ngatpang', 'NT', 1),
(2474, 163, 'Ngchesar', 'NC', 1),
(2475, 163, 'Ngeremlengui', 'NR', 1),
(2476, 163, 'Ngiwal', 'NW', 1),
(2477, 163, 'Peleliu', 'PE', 1),
(2478, 163, 'Sonsorol', 'SO', 1),
(2479, 164, 'Bocas del Toro', 'BT', 1),
(2480, 164, 'Chiriqui', 'CH', 1),
(2481, 164, 'Cocle', 'CC', 1),
(2482, 164, 'Colon', 'CL', 1),
(2483, 164, 'Darien', 'DA', 1),
(2484, 164, 'Herrera', 'HE', 1),
(2485, 164, 'Los Santos', 'LS', 1),
(2486, 164, 'Panama', 'PA', 1),
(2487, 164, 'San Blas', 'SB', 1),
(2488, 164, 'Veraguas', 'VG', 1),
(2489, 165, 'Bougainville', 'BV', 1),
(2490, 165, 'Central', 'CE', 1),
(2491, 165, 'Chimbu', 'CH', 1),
(2492, 165, 'Eastern Highlands', 'EH', 1),
(2493, 165, 'East New Britain', 'EB', 1),
(2494, 165, 'East Sepik', 'ES', 1),
(2495, 165, 'Enga', 'EN', 1),
(2496, 165, 'Gulf', 'GU', 1),
(2497, 165, 'Madang', 'MD', 1),
(2498, 165, 'Manus', 'MN', 1),
(2499, 165, 'Milne Bay', 'MB', 1),
(2500, 165, 'Morobe', 'MR', 1),
(2501, 165, 'National Capital', 'NC', 1),
(2502, 165, 'New Ireland', 'NI', 1),
(2503, 165, 'Northern', 'NO', 1),
(2504, 165, 'Sandaun', 'SA', 1),
(2505, 165, 'Southern Highlands', 'SH', 1),
(2506, 165, 'Western', 'WE', 1),
(2507, 165, 'Western Highlands', 'WH', 1),
(2508, 165, 'West New Britain', 'WB', 1),
(2509, 166, 'Alto Paraguay', 'AG', 1),
(2510, 166, 'Alto Parana', 'AN', 1),
(2511, 166, 'Amambay', 'AM', 1),
(2512, 166, 'Asuncion', 'AS', 1),
(2513, 166, 'Boqueron', 'BO', 1),
(2514, 166, 'Caaguazu', 'CG', 1),
(2515, 166, 'Caazapa', 'CZ', 1),
(2516, 166, 'Canindeyu', 'CN', 1),
(2517, 166, 'Central', 'CE', 1),
(2518, 166, 'Concepcion', 'CC', 1),
(2519, 166, 'Cordillera', 'CD', 1),
(2520, 166, 'Guaira', 'GU', 1),
(2521, 166, 'Itapua', 'IT', 1),
(2522, 166, 'Misiones', 'MI', 1),
(2523, 166, 'Neembucu', 'NE', 1),
(2524, 166, 'Paraguari', 'PA', 1),
(2525, 166, 'Presidente Hayes', 'PH', 1),
(2526, 166, 'San Pedro', 'SP', 1),
(2527, 167, 'Amazonas', 'AM', 1),
(2528, 167, 'Ancash', 'AN', 1),
(2529, 167, 'Apurimac', 'AP', 1),
(2530, 167, 'Arequipa', 'AR', 1),
(2531, 167, 'Ayacucho', 'AY', 1),
(2532, 167, 'Cajamarca', 'CJ', 1),
(2533, 167, 'Callao', 'CL', 1),
(2534, 167, 'Cusco', 'CU', 1),
(2535, 167, 'Huancavelica', 'HV', 1),
(2536, 167, 'Huanuco', 'HO', 1),
(2537, 167, 'Ica', 'IC', 1),
(2538, 167, 'Junin', 'JU', 1),
(2539, 167, 'La Libertad', 'LD', 1),
(2540, 167, 'Lambayeque', 'LY', 1),
(2541, 167, 'Lima', 'LI', 1),
(2542, 167, 'Loreto', 'LO', 1),
(2543, 167, 'Madre de Dios', 'MD', 1),
(2544, 167, 'Moquegua', 'MO', 1),
(2545, 167, 'Pasco', 'PA', 1),
(2546, 167, 'Piura', 'PI', 1),
(2547, 167, 'Puno', 'PU', 1),
(2548, 167, 'San Martin', 'SM', 1),
(2549, 167, 'Tacna', 'TA', 1),
(2550, 167, 'Tumbes', 'TU', 1),
(2551, 167, 'Ucayali', 'UC', 1),
(2552, 168, 'Abra', 'ABR', 1),
(2553, 168, 'Agusan del Norte', 'ANO', 1),
(2554, 168, 'Agusan del Sur', 'ASU', 1),
(2555, 168, 'Aklan', 'AKL', 1),
(2556, 168, 'Albay', 'ALB', 1),
(2557, 168, 'Antique', 'ANT', 1),
(2558, 168, 'Apayao', 'APY', 1),
(2559, 168, 'Aurora', 'AUR', 1),
(2560, 168, 'Basilan', 'BAS', 1),
(2561, 168, 'Bataan', 'BTA', 1),
(2562, 168, 'Batanes', 'BTE', 1),
(2563, 168, 'Batangas', 'BTG', 1),
(2564, 168, 'Biliran', 'BLR', 1),
(2565, 168, 'Benguet', 'BEN', 1),
(2566, 168, 'Bohol', 'BOL', 1),
(2567, 168, 'Bukidnon', 'BUK', 1),
(2568, 168, 'Bulacan', 'BUL', 1),
(2569, 168, 'Cagayan', 'CAG', 1),
(2570, 168, 'Camarines Norte', 'CNO', 1),
(2571, 168, 'Camarines Sur', 'CSU', 1),
(2572, 168, 'Camiguin', 'CAM', 1),
(2573, 168, 'Capiz', 'CAP', 1),
(2574, 168, 'Catanduanes', 'CAT', 1),
(2575, 168, 'Cavite', 'CAV', 1),
(2576, 168, 'Cebu', 'CEB', 1),
(2577, 168, 'Compostela', 'CMP', 1),
(2578, 168, 'Davao del Norte', 'DNO', 1),
(2579, 168, 'Davao del Sur', 'DSU', 1),
(2580, 168, 'Davao Oriental', 'DOR', 1),
(2581, 168, 'Eastern Samar', 'ESA', 1),
(2582, 168, 'Guimaras', 'GUI', 1),
(2583, 168, 'Ifugao', 'IFU', 1),
(2584, 168, 'Ilocos Norte', 'INO', 1),
(2585, 168, 'Ilocos Sur', 'ISU', 1),
(2586, 168, 'Iloilo', 'ILO', 1),
(2587, 168, 'Isabela', 'ISA', 1),
(2588, 168, 'Kalinga', 'KAL', 1),
(2589, 168, 'Laguna', 'LAG', 1),
(2590, 168, 'Lanao del Norte', 'LNO', 1),
(2591, 168, 'Lanao del Sur', 'LSU', 1),
(2592, 168, 'La Union', 'UNI', 1),
(2593, 168, 'Leyte', 'LEY', 1),
(2594, 168, 'Maguindanao', 'MAG', 1),
(2595, 168, 'Marinduque', 'MRN', 1),
(2596, 168, 'Masbate', 'MSB', 1),
(2597, 168, 'Mindoro Occidental', 'MIC', 1),
(2598, 168, 'Mindoro Oriental', 'MIR', 1),
(2599, 168, 'Misamis Occidental', 'MSC', 1),
(2600, 168, 'Misamis Oriental', 'MOR', 1),
(2601, 168, 'Mountain', 'MOP', 1),
(2602, 168, 'Negros Occidental', 'NOC', 1),
(2603, 168, 'Negros Oriental', 'NOR', 1),
(2604, 168, 'North Cotabato', 'NCT', 1),
(2605, 168, 'Northern Samar', 'NSM', 1),
(2606, 168, 'Nueva Ecija', 'NEC', 1),
(2607, 168, 'Nueva Vizcaya', 'NVZ', 1),
(2608, 168, 'Palawan', 'PLW', 1),
(2609, 168, 'Pampanga', 'PMP', 1),
(2610, 168, 'Pangasinan', 'PNG', 1),
(2611, 168, 'Quezon', 'QZN', 1),
(2612, 168, 'Quirino', 'QRN', 1),
(2613, 168, 'Rizal', 'RIZ', 1),
(2614, 168, 'Romblon', 'ROM', 1),
(2615, 168, 'Samar', 'SMR', 1),
(2616, 168, 'Sarangani', 'SRG', 1),
(2617, 168, 'Siquijor', 'SQJ', 1),
(2618, 168, 'Sorsogon', 'SRS', 1),
(2619, 168, 'South Cotabato', 'SCO', 1),
(2620, 168, 'Southern Leyte', 'SLE', 1),
(2621, 168, 'Sultan Kudarat', 'SKU', 1),
(2622, 168, 'Sulu', 'SLU', 1),
(2623, 168, 'Surigao del Norte', 'SNO', 1),
(2624, 168, 'Surigao del Sur', 'SSU', 1),
(2625, 168, 'Tarlac', 'TAR', 1),
(2626, 168, 'Tawi-Tawi', 'TAW', 1),
(2627, 168, 'Zambales', 'ZBL', 1),
(2628, 168, 'Zamboanga del Norte', 'ZNO', 1),
(2629, 168, 'Zamboanga del Sur', 'ZSU', 1),
(2630, 168, 'Zamboanga Sibugay', 'ZSI', 1),
(2631, 170, 'Dolnoslaskie', 'DO', 1),
(2632, 170, 'Kujawsko-Pomorskie', 'KP', 1),
(2633, 170, 'Lodzkie', 'LO', 1),
(2634, 170, 'Lubelskie', 'LL', 1),
(2635, 170, 'Lubuskie', 'LU', 1),
(2636, 170, 'Malopolskie', 'ML', 1),
(2637, 170, 'Mazowieckie', 'MZ', 1),
(2638, 170, 'Opolskie', 'OP', 1),
(2639, 170, 'Podkarpackie', 'PP', 1),
(2640, 170, 'Podlaskie', 'PL', 1),
(2641, 170, 'Pomorskie', 'PM', 1),
(2642, 170, 'Slaskie', 'SL', 1),
(2643, 170, 'Swietokrzyskie', 'SW', 1),
(2644, 170, 'Warminsko-Mazurskie', 'WM', 1),
(2645, 170, 'Wielkopolskie', 'WP', 1),
(2646, 170, 'Zachodniopomorskie', 'ZA', 1),
(2647, 198, 'Saint Pierre', 'P', 1),
(2648, 198, 'Miquelon', 'M', 1),
(2649, 171, 'A&ccedil;ores', 'AC', 1),
(2650, 171, 'Aveiro', 'AV', 1),
(2651, 171, 'Beja', 'BE', 1),
(2652, 171, 'Braga', 'BR', 1),
(2653, 171, 'Bragan&ccedil;a', 'BA', 1),
(2654, 171, 'Castelo Branco', 'CB', 1),
(2655, 171, 'Coimbra', 'CO', 1),
(2656, 171, '&Eacute;vora', 'EV', 1),
(2657, 171, 'Faro', 'FA', 1),
(2658, 171, 'Guarda', 'GU', 1),
(2659, 171, 'Leiria', 'LE', 1),
(2660, 171, 'Lisboa', 'LI', 1),
(2661, 171, 'Madeira', 'ME', 1),
(2662, 171, 'Portalegre', 'PO', 1),
(2663, 171, 'Porto', 'PR', 1),
(2664, 171, 'Santar&eacute;m', 'SA', 1),
(2665, 171, 'Set&uacute;bal', 'SE', 1),
(2666, 171, 'Viana do Castelo', 'VC', 1),
(2667, 171, 'Vila Real', 'VR', 1),
(2668, 171, 'Viseu', 'VI', 1),
(2669, 173, 'Ad Dawhah', 'DW', 1),
(2670, 173, 'Al Ghuwayriyah', 'GW', 1),
(2671, 173, 'Al Jumayliyah', 'JM', 1),
(2672, 173, 'Al Khawr', 'KR', 1),
(2673, 173, 'Al Wakrah', 'WK', 1),
(2674, 173, 'Ar Rayyan', 'RN', 1),
(2675, 173, 'Jarayan al Batinah', 'JB', 1),
(2676, 173, 'Madinat ash Shamal', 'MS', 1),
(2677, 173, 'Umm Sa\'id', 'UD', 1),
(2678, 173, 'Umm Salal', 'UL', 1),
(2679, 175, 'Alba', 'AB', 1),
(2680, 175, 'Arad', 'AR', 1),
(2681, 175, 'Arges', 'AG', 1),
(2682, 175, 'Bacau', 'BC', 1),
(2683, 175, 'Bihor', 'BH', 1),
(2684, 175, 'Bistrita-Nasaud', 'BN', 1),
(2685, 175, 'Botosani', 'BT', 1),
(2686, 175, 'Brasov', 'BV', 1),
(2687, 175, 'Braila', 'BR', 1),
(2688, 175, 'Bucuresti', 'B', 1),
(2689, 175, 'Buzau', 'BZ', 1),
(2690, 175, 'Caras-Severin', 'CS', 1),
(2691, 175, 'Calarasi', 'CL', 1),
(2692, 175, 'Cluj', 'CJ', 1),
(2693, 175, 'Constanta', 'CT', 1),
(2694, 175, 'Covasna', 'CV', 1),
(2695, 175, 'Dimbovita', 'DB', 1),
(2696, 175, 'Dolj', 'DJ', 1),
(2697, 175, 'Galati', 'GL', 1),
(2698, 175, 'Giurgiu', 'GR', 1),
(2699, 175, 'Gorj', 'GJ', 1),
(2700, 175, 'Harghita', 'HR', 1),
(2701, 175, 'Hunedoara', 'HD', 1),
(2702, 175, 'Ialomita', 'IL', 1),
(2703, 175, 'Iasi', 'IS', 1),
(2704, 175, 'Ilfov', 'IF', 1),
(2705, 175, 'Maramures', 'MM', 1),
(2706, 175, 'Mehedinti', 'MH', 1),
(2707, 175, 'Mures', 'MS', 1),
(2708, 175, 'Neamt', 'NT', 1),
(2709, 175, 'Olt', 'OT', 1),
(2710, 175, 'Prahova', 'PH', 1),
(2711, 175, 'Satu-Mare', 'SM', 1),
(2712, 175, 'Salaj', 'SJ', 1),
(2713, 175, 'Sibiu', 'SB', 1),
(2714, 175, 'Suceava', 'SV', 1),
(2715, 175, 'Teleorman', 'TR', 1),
(2716, 175, 'Timis', 'TM', 1),
(2717, 175, 'Tulcea', 'TL', 1),
(2718, 175, 'Vaslui', 'VS', 1),
(2719, 175, 'Valcea', 'VL', 1),
(2720, 175, 'Vrancea', 'VN', 1),
(2721, 176, 'Республика Хакасия', 'KK', 1),
(2722, 176, 'Забайкальский край', 'ZAB', 1),
(2723, 176, 'Чукотский АО', 'CHU', 1),
(2724, 176, 'Архангельская область', 'ARK', 1),
(2725, 176, 'Астраханская область', 'AST', 1),
(2726, 176, 'Алтайский край', 'ALT', 1),
(2727, 176, 'Белгородская область', 'BEL', 1),
(2728, 176, 'Еврейская АО', 'YEV', 1),
(2729, 176, 'Амурская область', 'AMU', 1),
(2730, 176, 'Брянская область', 'BRY', 1),
(2731, 176, 'Чувашская Республика', 'CU', 1),
(2732, 176, 'Челябинская область', 'CHE', 1),
(2733, 176, 'Карачаево-Черкесия', 'KC', 1),
(2735, 176, 'Таймырский АО', 'TDN', 1),
(2736, 176, 'Республика Калмыкия', 'KL', 1),
(2738, 176, 'Республика Алтай', 'AL', 1),
(2739, 176, 'Чеченская Республика', 'CE', 1),
(2740, 176, 'Иркутская область', 'IRK', 1),
(2741, 176, 'Ивановская область', 'IVA', 1),
(2742, 176, 'Удмуртская Республика', 'UD', 1),
(2743, 176, 'Калининградская область', 'KGD', 1),
(2744, 176, 'Калужская область', 'KLU', 1),
(2745, 176, 'Краснодарский край', 'KDA', 1),
(2746, 176, 'Республика Татарстан', 'TA', 1),
(2747, 176, 'Кемеровская область', 'KEM', 1),
(2748, 176, 'Хабаровский край', 'KHA', 1),
(2749, 176, 'Ханты-Мансийский АО - Югра', 'KHM', 1),
(2750, 176, 'Костромская область', 'KOS', 1),
(2751, 176, 'Московская область', 'MOS', 1),
(2752, 176, 'Красноярский край', 'KYA', 1),
(2753, 176, 'Коми-Пермяцкий АО', 'KOP', 1),
(2754, 176, 'Курганская область', 'KGN', 1),
(2755, 176, 'Курская область', 'KRS', 1),
(2756, 176, 'Республика Тыва', 'TY', 1),
(2757, 176, 'Липецкая область', 'LIP', 1),
(2758, 176, 'Магаданская область', 'MAG', 1),
(2759, 176, 'Республика Дагестан', 'DA', 1),
(2760, 176, 'Республика Адыгея', 'AD', 1),
(2761, 176, 'Москва', 'MOW', 1),
(2762, 176, 'Мурманская область', 'MUR', 1),
(2763, 176, 'Республика Кабардино-Балкария', 'KB', 1),
(2764, 176, 'Ненецкий АО', 'NEN', 1),
(2765, 176, 'Республика Ингушетия', 'IN', 1),
(2766, 176, 'Нижегородская область', 'NIZ', 1),
(2767, 176, 'Новгородская область', 'NGR', 1),
(2768, 176, 'Новосибирская область', 'NVS', 1),
(2769, 176, 'Омская область', 'OMS', 1),
(2770, 176, 'Орловская область', 'ORL', 1),
(2771, 176, 'Оренбургская область', 'ORE', 1),
(2772, 176, 'Корякский АО', 'KOR', 1),
(2773, 176, 'Пензенская область', 'PNZ', 1),
(2774, 176, 'Пермский край', 'PER', 1),
(2775, 176, 'Камчатский край', 'KAM', 1),
(2776, 176, 'Республика Карелия', 'KR', 1),
(2777, 176, 'Псковская область', 'PSK', 1),
(2778, 176, 'Ростовская область', 'ROS', 1),
(2779, 176, 'Рязанская область', 'RYA', 1),
(2780, 176, 'Ямало-Ненецкий АО', 'YAN', 1),
(2781, 176, 'Самарская область', 'SAM', 1),
(2782, 176, 'Республика Мордовия', 'MO', 1),
(2783, 176, 'Саратовская область', 'SAR', 1),
(2784, 176, 'Смоленская область', 'SMO', 1),
(2785, 176, 'Санкт-Петербург', 'SPE', 1),
(2786, 176, 'Ставропольский край', 'STA', 1),
(2787, 176, 'Республика Коми', 'KO', 1),
(2788, 176, 'Тамбовская область', 'TAM', 1),
(2789, 176, 'Томская область', 'TOM', 1),
(2790, 176, 'Тульская область', 'TUL', 1),
(2791, 176, 'Ленинградская область', 'LEN', 1),
(2792, 176, 'Тверская область', 'TVE', 1),
(2793, 176, 'Тюменская область', 'TYU', 1),
(2794, 176, 'Республика Башкортостан', 'BA', 1),
(2795, 176, 'Ульяновская область', 'ULY', 1),
(2796, 176, 'Республика Бурятия', 'BU', 1),
(2798, 176, 'Республика Северная Осетия', 'SE', 1),
(2799, 176, 'Владимирская область', 'VLA', 1),
(2800, 176, 'Приморский край', 'PRI', 1),
(2801, 176, 'Волгоградская область', 'VGG', 1),
(2802, 176, 'Вологодская область', 'VLG', 1),
(2803, 176, 'Воронежская область', 'VOR', 1),
(2804, 176, 'Кировская область', 'KIR', 1),
(2805, 176, 'Республика  Саха / Якутия', 'SA', 1),
(2806, 176, 'Ярославская область', 'YAR', 1),
(2807, 176, 'Свердловская область', 'SVE', 1),
(2808, 176, 'Республика Марий Эл', 'ME', 1),
(2809, 177, 'Butare', 'BU', 1),
(2810, 177, 'Byumba', 'BY', 1),
(2811, 177, 'Cyangugu', 'CY', 1),
(2812, 177, 'Gikongoro', 'GK', 1),
(2813, 177, 'Gisenyi', 'GS', 1),
(2814, 177, 'Gitarama', 'GT', 1),
(2815, 177, 'Kibungo', 'KG', 1),
(2816, 177, 'Kibuye', 'KY', 1),
(2817, 177, 'Kigali Rurale', 'KR', 1),
(2818, 177, 'Kigali-ville', 'KV', 1),
(2819, 177, 'Ruhengeri', 'RU', 1),
(2820, 177, 'Umutara', 'UM', 1),
(2821, 178, 'Christ Church Nichola Town', 'CCN', 1),
(2822, 178, 'Saint Anne Sandy Point', 'SAS', 1),
(2823, 178, 'Saint George Basseterre', 'SGB', 1),
(2824, 178, 'Saint George Gingerland', 'SGG', 1),
(2825, 178, 'Saint James Windward', 'SJW', 1),
(2826, 178, 'Saint John Capesterre', 'SJC', 1),
(2827, 178, 'Saint John Figtree', 'SJF', 1),
(2828, 178, 'Saint Mary Cayon', 'SMC', 1),
(2829, 178, 'Saint Paul Capesterre', 'CAP', 1),
(2830, 178, 'Saint Paul Charlestown', 'CHA', 1),
(2831, 178, 'Saint Peter Basseterre', 'SPB', 1),
(2832, 178, 'Saint Thomas Lowland', 'STL', 1),
(2833, 178, 'Saint Thomas Middle Island', 'STM', 1),
(2834, 178, 'Trinity Palmetto Point', 'TPP', 1),
(2835, 179, 'Anse-la-Raye', 'AR', 1),
(2836, 179, 'Castries', 'CA', 1),
(2837, 179, 'Choiseul', 'CH', 1),
(2838, 179, 'Dauphin', 'DA', 1),
(2839, 179, 'Dennery', 'DE', 1),
(2840, 179, 'Gros-Islet', 'GI', 1),
(2841, 179, 'Laborie', 'LA', 1),
(2842, 179, 'Micoud', 'MI', 1),
(2843, 179, 'Praslin', 'PR', 1),
(2844, 179, 'Soufriere', 'SO', 1),
(2845, 179, 'Vieux-Fort', 'VF', 1),
(2846, 180, 'Charlotte', 'C', 1),
(2847, 180, 'Grenadines', 'R', 1),
(2848, 180, 'Saint Andrew', 'A', 1),
(2849, 180, 'Saint David', 'D', 1),
(2850, 180, 'Saint George', 'G', 1),
(2851, 180, 'Saint Patrick', 'P', 1),
(2852, 181, 'A\'ana', 'AN', 1),
(2853, 181, 'Aiga-i-le-Tai', 'AI', 1),
(2854, 181, 'Atua', 'AT', 1),
(2855, 181, 'Fa\'asaleleaga', 'FA', 1),
(2856, 181, 'Gaga\'emauga', 'GE', 1),
(2857, 181, 'Gagaifomauga', 'GF', 1),
(2858, 181, 'Palauli', 'PA', 1),
(2859, 181, 'Satupa\'itea', 'SA', 1),
(2860, 181, 'Tuamasaga', 'TU', 1),
(2861, 181, 'Va\'a-o-Fonoti', 'VF', 1),
(2862, 181, 'Vaisigano', 'VS', 1),
(2863, 182, 'Acquaviva', 'AC', 1),
(2864, 182, 'Borgo Maggiore', 'BM', 1),
(2865, 182, 'Chiesanuova', 'CH', 1),
(2866, 182, 'Domagnano', 'DO', 1),
(2867, 182, 'Faetano', 'FA', 1),
(2868, 182, 'Fiorentino', 'FI', 1),
(2869, 182, 'Montegiardino', 'MO', 1),
(2870, 182, 'Citta di San Marino', 'SM', 1),
(2871, 182, 'Serravalle', 'SE', 1),
(2872, 183, 'Sao Tome', 'S', 1),
(2873, 183, 'Principe', 'P', 1),
(2874, 184, 'Al Bahah', 'BH', 1),
(2875, 184, 'Al Hudud ash Shamaliyah', 'HS', 1),
(2876, 184, 'Al Jawf', 'JF', 1),
(2877, 184, 'Al Madinah', 'MD', 1),
(2878, 184, 'Al Qasim', 'QS', 1),
(2879, 184, 'Ar Riyad', 'RD', 1),
(2880, 184, 'Ash Sharqiyah (Eastern)', 'AQ', 1),
(2881, 184, '\'Asir', 'AS', 1),
(2882, 184, 'Ha\'il', 'HL', 1),
(2883, 184, 'Jizan', 'JZ', 1),
(2884, 184, 'Makkah', 'ML', 1),
(2885, 184, 'Najran', 'NR', 1),
(2886, 184, 'Tabuk', 'TB', 1),
(2887, 185, 'Dakar', 'DA', 1),
(2888, 185, 'Diourbel', 'DI', 1),
(2889, 185, 'Fatick', 'FA', 1),
(2890, 185, 'Kaolack', 'KA', 1),
(2891, 185, 'Kolda', 'KO', 1),
(2892, 185, 'Louga', 'LO', 1),
(2893, 185, 'Matam', 'MA', 1),
(2894, 185, 'Saint-Louis', 'SL', 1),
(2895, 185, 'Tambacounda', 'TA', 1),
(2896, 185, 'Thies', 'TH', 1),
(2897, 185, 'Ziguinchor', 'ZI', 1),
(2898, 186, 'Anse aux Pins', 'AP', 1),
(2899, 186, 'Anse Boileau', 'AB', 1),
(2900, 186, 'Anse Etoile', 'AE', 1),
(2901, 186, 'Anse Louis', 'AL', 1),
(2902, 186, 'Anse Royale', 'AR', 1),
(2903, 186, 'Baie Lazare', 'BL', 1),
(2904, 186, 'Baie Sainte Anne', 'BS', 1),
(2905, 186, 'Beau Vallon', 'BV', 1),
(2906, 186, 'Bel Air', 'BA', 1),
(2907, 186, 'Bel Ombre', 'BO', 1),
(2908, 186, 'Cascade', 'CA', 1),
(2909, 186, 'Glacis', 'GL', 1),
(2910, 186, 'Grand\' Anse (on Mahe)', 'GM', 1),
(2911, 186, 'Grand\' Anse (on Praslin)', 'GP', 1),
(2912, 186, 'La Digue', 'DG', 1),
(2913, 186, 'La Riviere Anglaise', 'RA', 1),
(2914, 186, 'Mont Buxton', 'MB', 1),
(2915, 186, 'Mont Fleuri', 'MF', 1),
(2916, 186, 'Plaisance', 'PL', 1),
(2917, 186, 'Pointe La Rue', 'PR', 1),
(2918, 186, 'Port Glaud', 'PG', 1),
(2919, 186, 'Saint Louis', 'SL', 1),
(2920, 186, 'Takamaka', 'TA', 1),
(2921, 187, 'Eastern', 'E', 1),
(2922, 187, 'Northern', 'N', 1),
(2923, 187, 'Southern', 'S', 1),
(2924, 187, 'Western', 'W', 1),
(2925, 189, 'Banskobystrický', 'BA', 1),
(2926, 189, 'Bratislavský', 'BR', 1),
(2927, 189, 'Košický', 'KO', 1),
(2928, 189, 'Nitriansky', 'NI', 1),
(2929, 189, 'Prešovský', 'PR', 1),
(2930, 189, 'Trenčiansky', 'TC', 1),
(2931, 189, 'Trnavský', 'TV', 1),
(2932, 189, 'Žilinský', 'ZI', 1),
(2933, 191, 'Central', 'CE', 1),
(2934, 191, 'Choiseul', 'CH', 1),
(2935, 191, 'Guadalcanal', 'GC', 1),
(2936, 191, 'Honiara', 'HO', 1),
(2937, 191, 'Isabel', 'IS', 1),
(2938, 191, 'Makira', 'MK', 1),
(2939, 191, 'Malaita', 'ML', 1),
(2940, 191, 'Rennell and Bellona', 'RB', 1),
(2941, 191, 'Temotu', 'TM', 1),
(2942, 191, 'Western', 'WE', 1),
(2943, 192, 'Awdal', 'AW', 1),
(2944, 192, 'Bakool', 'BK', 1),
(2945, 192, 'Banaadir', 'BN', 1),
(2946, 192, 'Bari', 'BR', 1),
(2947, 192, 'Bay', 'BY', 1),
(2948, 192, 'Galguduud', 'GA', 1),
(2949, 192, 'Gedo', 'GE', 1),
(2950, 192, 'Hiiraan', 'HI', 1),
(2951, 192, 'Jubbada Dhexe', 'JD', 1),
(2952, 192, 'Jubbada Hoose', 'JH', 1),
(2953, 192, 'Mudug', 'MU', 1),
(2954, 192, 'Nugaal', 'NU', 1),
(2955, 192, 'Sanaag', 'SA', 1),
(2956, 192, 'Shabeellaha Dhexe', 'SD', 1),
(2957, 192, 'Shabeellaha Hoose', 'SH', 1),
(2958, 192, 'Sool', 'SL', 1),
(2959, 192, 'Togdheer', 'TO', 1),
(2960, 192, 'Woqooyi Galbeed', 'WG', 1),
(2961, 193, 'Eastern Cape', 'EC', 1),
(2962, 193, 'Free State', 'FS', 1),
(2963, 193, 'Gauteng', 'GT', 1),
(2964, 193, 'KwaZulu-Natal', 'KN', 1),
(2965, 193, 'Limpopo', 'LP', 1),
(2966, 193, 'Mpumalanga', 'MP', 1),
(2967, 193, 'North West', 'NW', 1),
(2968, 193, 'Northern Cape', 'NC', 1),
(2969, 193, 'Western Cape', 'WC', 1),
(2970, 195, 'La Coru&ntilde;a', 'CA', 1),
(2971, 195, '&Aacute;lava', 'AL', 1),
(2972, 195, 'Albacete', 'AB', 1),
(2973, 195, 'Alicante', 'AC', 1),
(2974, 195, 'Almeria', 'AM', 1),
(2975, 195, 'Asturias', 'AS', 1),
(2976, 195, '&Aacute;vila', 'AV', 1),
(2977, 195, 'Badajoz', 'BJ', 1),
(2978, 195, 'Baleares', 'IB', 1),
(2979, 195, 'Barcelona', 'BA', 1),
(2980, 195, 'Burgos', 'BU', 1),
(2981, 195, 'C&aacute;ceres', 'CC', 1),
(2982, 195, 'C&aacute;diz', 'CZ', 1),
(2983, 195, 'Cantabria', 'CT', 1),
(2984, 195, 'Castell&oacute;n', 'CL', 1),
(2985, 195, 'Ceuta', 'CE', 1),
(2986, 195, 'Ciudad Real', 'CR', 1),
(2987, 195, 'C&oacute;rdoba', 'CD', 1),
(2988, 195, 'Cuenca', 'CU', 1),
(2989, 195, 'Girona', 'GI', 1),
(2990, 195, 'Granada', 'GD', 1),
(2991, 195, 'Guadalajara', 'GJ', 1),
(2992, 195, 'Guip&uacute;zcoa', 'GP', 1),
(2993, 195, 'Huelva', 'HL', 1),
(2994, 195, 'Huesca', 'HS', 1),
(2995, 195, 'Ja&eacute;n', 'JN', 1),
(2996, 195, 'La Rioja', 'RJ', 1),
(2997, 195, 'Las Palmas', 'PM', 1),
(2998, 195, 'Leon', 'LE', 1),
(2999, 195, 'Lleida', 'LL', 1),
(3000, 195, 'Lugo', 'LG', 1),
(3001, 195, 'Madrid', 'MD', 1),
(3002, 195, 'Malaga', 'MA', 1),
(3003, 195, 'Melilla', 'ML', 1),
(3004, 195, 'Murcia', 'MU', 1),
(3005, 195, 'Navarra', 'NV', 1),
(3006, 195, 'Ourense', 'OU', 1),
(3007, 195, 'Palencia', 'PL', 1),
(3008, 195, 'Pontevedra', 'PO', 1),
(3009, 195, 'Salamanca', 'SL', 1),
(3010, 195, 'Santa Cruz de Tenerife', 'SC', 1),
(3011, 195, 'Segovia', 'SG', 1),
(3012, 195, 'Sevilla', 'SV', 1),
(3013, 195, 'Soria', 'SO', 1),
(3014, 195, 'Tarragona', 'TA', 1),
(3015, 195, 'Teruel', 'TE', 1),
(3016, 195, 'Toledo', 'TO', 1),
(3017, 195, 'Valencia', 'VC', 1),
(3018, 195, 'Valladolid', 'VD', 1),
(3019, 195, 'Vizcaya', 'VZ', 1),
(3020, 195, 'Zamora', 'ZM', 1),
(3021, 195, 'Zaragoza', 'ZR', 1),
(3022, 196, 'Central', 'CE', 1),
(3023, 196, 'Eastern', 'EA', 1),
(3024, 196, 'North Central', 'NC', 1),
(3025, 196, 'Northern', 'NO', 1),
(3026, 196, 'North Western', 'NW', 1),
(3027, 196, 'Sabaragamuwa', 'SA', 1),
(3028, 196, 'Southern', 'SO', 1),
(3029, 196, 'Uva', 'UV', 1),
(3030, 196, 'Western', 'WE', 1),
(3032, 197, 'Saint Helena', 'S', 1),
(3034, 199, 'A\'ali an Nil', 'ANL', 1),
(3035, 199, 'Al Bahr al Ahmar', 'BAM', 1),
(3036, 199, 'Al Buhayrat', 'BRT', 1),
(3037, 199, 'Al Jazirah', 'JZR', 1),
(3038, 199, 'Al Khartum', 'KRT', 1),
(3039, 199, 'Al Qadarif', 'QDR', 1),
(3040, 199, 'Al Wahdah', 'WDH', 1),
(3041, 199, 'An Nil al Abyad', 'ANB', 1),
(3042, 199, 'An Nil al Azraq', 'ANZ', 1),
(3043, 199, 'Ash Shamaliyah', 'ASH', 1),
(3044, 199, 'Bahr al Jabal', 'BJA', 1),
(3045, 199, 'Gharb al Istiwa\'iyah', 'GIS', 1),
(3046, 199, 'Gharb Bahr al Ghazal', 'GBG', 1),
(3047, 199, 'Gharb Darfur', 'GDA', 1),
(3048, 199, 'Gharb Kurdufan', 'GKU', 1),
(3049, 199, 'Janub Darfur', 'JDA', 1),
(3050, 199, 'Janub Kurdufan', 'JKU', 1),
(3051, 199, 'Junqali', 'JQL', 1),
(3052, 199, 'Kassala', 'KSL', 1),
(3053, 199, 'Nahr an Nil', 'NNL', 1),
(3054, 199, 'Shamal Bahr al Ghazal', 'SBG', 1),
(3055, 199, 'Shamal Darfur', 'SDA', 1),
(3056, 199, 'Shamal Kurdufan', 'SKU', 1),
(3057, 199, 'Sharq al Istiwa\'iyah', 'SIS', 1),
(3058, 199, 'Sinnar', 'SNR', 1),
(3059, 199, 'Warab', 'WRB', 1),
(3060, 200, 'Brokopondo', 'BR', 1),
(3061, 200, 'Commewijne', 'CM', 1),
(3062, 200, 'Coronie', 'CR', 1),
(3063, 200, 'Marowijne', 'MA', 1),
(3064, 200, 'Nickerie', 'NI', 1),
(3065, 200, 'Para', 'PA', 1),
(3066, 200, 'Paramaribo', 'PM', 1),
(3067, 200, 'Saramacca', 'SA', 1),
(3068, 200, 'Sipaliwini', 'SI', 1),
(3069, 200, 'Wanica', 'WA', 1),
(3070, 202, 'Hhohho', 'H', 1),
(3071, 202, 'Lubombo', 'L', 1),
(3072, 202, 'Manzini', 'M', 1),
(3073, 202, 'Shishelweni', 'S', 1),
(3074, 203, 'Blekinge', 'K', 1),
(3075, 203, 'Dalarna', 'W', 1),
(3076, 203, 'G&auml;vleborg', 'X', 1),
(3077, 203, 'Gotland', 'I', 1),
(3078, 203, 'Halland', 'N', 1),
(3079, 203, 'J&auml;mtland', 'Z', 1),
(3080, 203, 'J&ouml;nk&ouml;ping', 'F', 1),
(3081, 203, 'Kalmar', 'H', 1),
(3082, 203, 'Kronoberg', 'G', 1),
(3083, 203, 'Norrbotten', 'BD', 1),
(3084, 203, '&Ouml;rebro', 'T', 1),
(3085, 203, '&Ouml;sterg&ouml;tland', 'E', 1),
(3086, 203, 'Sk&aring;ne', 'M', 1),
(3087, 203, 'S&ouml;dermanland', 'D', 1),
(3088, 203, 'Stockholm', 'AB', 1),
(3089, 203, 'Uppsala', 'C', 1),
(3090, 203, 'V&auml;rmland', 'S', 1),
(3091, 203, 'V&auml;sterbotten', 'AC', 1),
(3092, 203, 'V&auml;sternorrland', 'Y', 1),
(3093, 203, 'V&auml;stmanland', 'U', 1),
(3094, 203, 'V&auml;stra G&ouml;taland', 'O', 1),
(3095, 204, 'Aargau', 'AG', 1),
(3096, 204, 'Appenzell Ausserrhoden', 'AR', 1),
(3097, 204, 'Appenzell Innerrhoden', 'AI', 1),
(3098, 204, 'Basel-Stadt', 'BS', 1),
(3099, 204, 'Basel-Landschaft', 'BL', 1),
(3100, 204, 'Bern', 'BE', 1),
(3101, 204, 'Fribourg', 'FR', 1),
(3102, 204, 'Gen&egrave;ve', 'GE', 1),
(3103, 204, 'Glarus', 'GL', 1),
(3104, 204, 'Graub&uuml;nden', 'GR', 1),
(3105, 204, 'Jura', 'JU', 1),
(3106, 204, 'Luzern', 'LU', 1),
(3107, 204, 'Neuch&acirc;tel', 'NE', 1),
(3108, 204, 'Nidwald', 'NW', 1),
(3109, 204, 'Obwald', 'OW', 1),
(3110, 204, 'St. Gallen', 'SG', 1),
(3111, 204, 'Schaffhausen', 'SH', 1),
(3112, 204, 'Schwyz', 'SZ', 1),
(3113, 204, 'Solothurn', 'SO', 1),
(3114, 204, 'Thurgau', 'TG', 1),
(3115, 204, 'Ticino', 'TI', 1),
(3116, 204, 'Uri', 'UR', 1),
(3117, 204, 'Valais', 'VS', 1),
(3118, 204, 'Vaud', 'VD', 1),
(3119, 204, 'Zug', 'ZG', 1),
(3120, 204, 'Z&uuml;rich', 'ZH', 1),
(3121, 205, 'Al Hasakah', 'HA', 1),
(3122, 205, 'Al Ladhiqiyah', 'LA', 1),
(3123, 205, 'Al Qunaytirah', 'QU', 1);
INSERT INTO `oc_zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(3124, 205, 'Ar Raqqah', 'RQ', 1),
(3125, 205, 'As Suwayda', 'SU', 1),
(3126, 205, 'Dara', 'DA', 1),
(3127, 205, 'Dayr az Zawr', 'DZ', 1),
(3128, 205, 'Dimashq', 'DI', 1),
(3129, 205, 'Halab', 'HL', 1),
(3130, 205, 'Hamah', 'HM', 1),
(3131, 205, 'Hims', 'HI', 1),
(3132, 205, 'Idlib', 'ID', 1),
(3133, 205, 'Rif Dimashq', 'RD', 1),
(3134, 205, 'Tartus', 'TA', 1),
(3135, 206, 'Chang-hua', 'CH', 1),
(3136, 206, 'Chia-i', 'CI', 1),
(3137, 206, 'Hsin-chu', 'HS', 1),
(3138, 206, 'Hua-lien', 'HL', 1),
(3139, 206, 'I-lan', 'IL', 1),
(3140, 206, 'Kao-hsiung county', 'KH', 1),
(3141, 206, 'Kin-men', 'KM', 1),
(3142, 206, 'Lien-chiang', 'LC', 1),
(3143, 206, 'Miao-li', 'ML', 1),
(3144, 206, 'Nan-t\'ou', 'NT', 1),
(3145, 206, 'P\'eng-hu', 'PH', 1),
(3146, 206, 'P\'ing-tung', 'PT', 1),
(3147, 206, 'T\'ai-chung', 'TG', 1),
(3148, 206, 'T\'ai-nan', 'TA', 1),
(3149, 206, 'T\'ai-pei county', 'TP', 1),
(3150, 206, 'T\'ai-tung', 'TT', 1),
(3151, 206, 'T\'ao-yuan', 'TY', 1),
(3152, 206, 'Yun-lin', 'YL', 1),
(3153, 206, 'Chia-i city', 'CC', 1),
(3154, 206, 'Chi-lung', 'CL', 1),
(3155, 206, 'Hsin-chu', 'HC', 1),
(3156, 206, 'T\'ai-chung', 'TH', 1),
(3157, 206, 'T\'ai-nan', 'TN', 1),
(3158, 206, 'Kao-hsiung city', 'KC', 1),
(3159, 206, 'T\'ai-pei city', 'TC', 1),
(3160, 207, 'Gorno-Badakhstan', 'GB', 1),
(3161, 207, 'Khatlon', 'KT', 1),
(3162, 207, 'Sughd', 'SU', 1),
(3163, 208, 'Arusha', 'AR', 1),
(3164, 208, 'Dar es Salaam', 'DS', 1),
(3165, 208, 'Dodoma', 'DO', 1),
(3166, 208, 'Iringa', 'IR', 1),
(3167, 208, 'Kagera', 'KA', 1),
(3168, 208, 'Kigoma', 'KI', 1),
(3169, 208, 'Kilimanjaro', 'KJ', 1),
(3170, 208, 'Lindi', 'LN', 1),
(3171, 208, 'Manyara', 'MY', 1),
(3172, 208, 'Mara', 'MR', 1),
(3173, 208, 'Mbeya', 'MB', 1),
(3174, 208, 'Morogoro', 'MO', 1),
(3175, 208, 'Mtwara', 'MT', 1),
(3176, 208, 'Mwanza', 'MW', 1),
(3177, 208, 'Pemba North', 'PN', 1),
(3178, 208, 'Pemba South', 'PS', 1),
(3179, 208, 'Pwani', 'PW', 1),
(3180, 208, 'Rukwa', 'RK', 1),
(3181, 208, 'Ruvuma', 'RV', 1),
(3182, 208, 'Shinyanga', 'SH', 1),
(3183, 208, 'Singida', 'SI', 1),
(3184, 208, 'Tabora', 'TB', 1),
(3185, 208, 'Tanga', 'TN', 1),
(3186, 208, 'Zanzibar Central/South', 'ZC', 1),
(3187, 208, 'Zanzibar North', 'ZN', 1),
(3188, 208, 'Zanzibar Urban/West', 'ZU', 1),
(3189, 209, 'Amnat Charoen', 'Amnat Charoen', 1),
(3190, 209, 'Ang Thong', 'Ang Thong', 1),
(3191, 209, 'Ayutthaya', 'Ayutthaya', 1),
(3192, 209, 'Bangkok', 'Bangkok', 1),
(3193, 209, 'Buriram', 'Buriram', 1),
(3194, 209, 'Chachoengsao', 'Chachoengsao', 1),
(3195, 209, 'Chai Nat', 'Chai Nat', 1),
(3196, 209, 'Chaiyaphum', 'Chaiyaphum', 1),
(3197, 209, 'Chanthaburi', 'Chanthaburi', 1),
(3198, 209, 'Chiang Mai', 'Chiang Mai', 1),
(3199, 209, 'Chiang Rai', 'Chiang Rai', 1),
(3200, 209, 'Chon Buri', 'Chon Buri', 1),
(3201, 209, 'Chumphon', 'Chumphon', 1),
(3202, 209, 'Kalasin', 'Kalasin', 1),
(3203, 209, 'Kamphaeng Phet', 'Kamphaeng Phet', 1),
(3204, 209, 'Kanchanaburi', 'Kanchanaburi', 1),
(3205, 209, 'Khon Kaen', 'Khon Kaen', 1),
(3206, 209, 'Krabi', 'Krabi', 1),
(3207, 209, 'Lampang', 'Lampang', 1),
(3208, 209, 'Lamphun', 'Lamphun', 1),
(3209, 209, 'Loei', 'Loei', 1),
(3210, 209, 'Lop Buri', 'Lop Buri', 1),
(3211, 209, 'Mae Hong Son', 'Mae Hong Son', 1),
(3212, 209, 'Maha Sarakham', 'Maha Sarakham', 1),
(3213, 209, 'Mukdahan', 'Mukdahan', 1),
(3214, 209, 'Nakhon Nayok', 'Nakhon Nayok', 1),
(3215, 209, 'Nakhon Pathom', 'Nakhon Pathom', 1),
(3216, 209, 'Nakhon Phanom', 'Nakhon Phanom', 1),
(3217, 209, 'Nakhon Ratchasima', 'Nakhon Ratchasima', 1),
(3218, 209, 'Nakhon Sawan', 'Nakhon Sawan', 1),
(3219, 209, 'Nakhon Si Thammarat', 'Nakhon Si Thammarat', 1),
(3220, 209, 'Nan', 'Nan', 1),
(3221, 209, 'Narathiwat', 'Narathiwat', 1),
(3222, 209, 'Nong Bua Lamphu', 'Nong Bua Lamphu', 1),
(3223, 209, 'Nong Khai', 'Nong Khai', 1),
(3224, 209, 'Nonthaburi', 'Nonthaburi', 1),
(3225, 209, 'Pathum Thani', 'Pathum Thani', 1),
(3226, 209, 'Pattani', 'Pattani', 1),
(3227, 209, 'Phangnga', 'Phangnga', 1),
(3228, 209, 'Phatthalung', 'Phatthalung', 1),
(3229, 209, 'Phayao', 'Phayao', 1),
(3230, 209, 'Phetchabun', 'Phetchabun', 1),
(3231, 209, 'Phetchaburi', 'Phetchaburi', 1),
(3232, 209, 'Phichit', 'Phichit', 1),
(3233, 209, 'Phitsanulok', 'Phitsanulok', 1),
(3234, 209, 'Phrae', 'Phrae', 1),
(3235, 209, 'Phuket', 'Phuket', 1),
(3236, 209, 'Prachin Buri', 'Prachin Buri', 1),
(3237, 209, 'Prachuap Khiri Khan', 'Prachuap Khiri Khan', 1),
(3238, 209, 'Ranong', 'Ranong', 1),
(3239, 209, 'Ratchaburi', 'Ratchaburi', 1),
(3240, 209, 'Rayong', 'Rayong', 1),
(3241, 209, 'Roi Et', 'Roi Et', 1),
(3242, 209, 'Sa Kaeo', 'Sa Kaeo', 1),
(3243, 209, 'Sakon Nakhon', 'Sakon Nakhon', 1),
(3244, 209, 'Samut Prakan', 'Samut Prakan', 1),
(3245, 209, 'Samut Sakhon', 'Samut Sakhon', 1),
(3246, 209, 'Samut Songkhram', 'Samut Songkhram', 1),
(3247, 209, 'Sara Buri', 'Sara Buri', 1),
(3248, 209, 'Satun', 'Satun', 1),
(3249, 209, 'Sing Buri', 'Sing Buri', 1),
(3250, 209, 'Sisaket', 'Sisaket', 1),
(3251, 209, 'Songkhla', 'Songkhla', 1),
(3252, 209, 'Sukhothai', 'Sukhothai', 1),
(3253, 209, 'Suphan Buri', 'Suphan Buri', 1),
(3254, 209, 'Surat Thani', 'Surat Thani', 1),
(3255, 209, 'Surin', 'Surin', 1),
(3256, 209, 'Tak', 'Tak', 1),
(3257, 209, 'Trang', 'Trang', 1),
(3258, 209, 'Trat', 'Trat', 1),
(3259, 209, 'Ubon Ratchathani', 'Ubon Ratchathani', 1),
(3260, 209, 'Udon Thani', 'Udon Thani', 1),
(3261, 209, 'Uthai Thani', 'Uthai Thani', 1),
(3262, 209, 'Uttaradit', 'Uttaradit', 1),
(3263, 209, 'Yala', 'Yala', 1),
(3264, 209, 'Yasothon', 'Yasothon', 1),
(3265, 210, 'Kara', 'K', 1),
(3266, 210, 'Plateaux', 'P', 1),
(3267, 210, 'Savanes', 'S', 1),
(3268, 210, 'Centrale', 'C', 1),
(3269, 210, 'Maritime', 'M', 1),
(3270, 211, 'Atafu', 'A', 1),
(3271, 211, 'Fakaofo', 'F', 1),
(3272, 211, 'Nukunonu', 'N', 1),
(3273, 212, 'Ha\'apai', 'H', 1),
(3274, 212, 'Tongatapu', 'T', 1),
(3275, 212, 'Vava\'u', 'V', 1),
(3276, 213, 'Couva/Tabaquite/Talparo', 'CT', 1),
(3277, 213, 'Diego Martin', 'DM', 1),
(3278, 213, 'Mayaro/Rio Claro', 'MR', 1),
(3279, 213, 'Penal/Debe', 'PD', 1),
(3280, 213, 'Princes Town', 'PT', 1),
(3281, 213, 'Sangre Grande', 'SG', 1),
(3282, 213, 'San Juan/Laventille', 'SL', 1),
(3283, 213, 'Siparia', 'SI', 1),
(3284, 213, 'Tunapuna/Piarco', 'TP', 1),
(3285, 213, 'Port of Spain', 'PS', 1),
(3286, 213, 'San Fernando', 'SF', 1),
(3287, 213, 'Arima', 'AR', 1),
(3288, 213, 'Point Fortin', 'PF', 1),
(3289, 213, 'Chaguanas', 'CH', 1),
(3290, 213, 'Tobago', 'TO', 1),
(3291, 214, 'Ariana', 'AR', 1),
(3292, 214, 'Beja', 'BJ', 1),
(3293, 214, 'Ben Arous', 'BA', 1),
(3294, 214, 'Bizerte', 'BI', 1),
(3295, 214, 'Gabes', 'GB', 1),
(3296, 214, 'Gafsa', 'GF', 1),
(3297, 214, 'Jendouba', 'JE', 1),
(3298, 214, 'Kairouan', 'KR', 1),
(3299, 214, 'Kasserine', 'KS', 1),
(3300, 214, 'Kebili', 'KB', 1),
(3301, 214, 'Kef', 'KF', 1),
(3302, 214, 'Mahdia', 'MH', 1),
(3303, 214, 'Manouba', 'MN', 1),
(3304, 214, 'Medenine', 'ME', 1),
(3305, 214, 'Monastir', 'MO', 1),
(3306, 214, 'Nabeul', 'NA', 1),
(3307, 214, 'Sfax', 'SF', 1),
(3308, 214, 'Sidi', 'SD', 1),
(3309, 214, 'Siliana', 'SL', 1),
(3310, 214, 'Sousse', 'SO', 1),
(3311, 214, 'Tataouine', 'TA', 1),
(3312, 214, 'Tozeur', 'TO', 1),
(3313, 214, 'Tunis', 'TU', 1),
(3314, 214, 'Zaghouan', 'ZA', 1),
(3315, 215, 'Adana', 'ADA', 1),
(3316, 215, 'Adıyaman', 'ADI', 1),
(3317, 215, 'Afyonkarahisar', 'AFY', 1),
(3318, 215, 'Ağrı', 'AGR', 1),
(3319, 215, 'Aksaray', 'AKS', 1),
(3320, 215, 'Amasya', 'AMA', 1),
(3321, 215, 'Ankara', 'ANK', 1),
(3322, 215, 'Antalya', 'ANT', 1),
(3323, 215, 'Ardahan', 'ARD', 1),
(3324, 215, 'Artvin', 'ART', 1),
(3325, 215, 'Aydın', 'AYI', 1),
(3326, 215, 'Balıkesir', 'BAL', 1),
(3327, 215, 'Bartın', 'BAR', 1),
(3328, 215, 'Batman', 'BAT', 1),
(3329, 215, 'Bayburt', 'BAY', 1),
(3330, 215, 'Bilecik', 'BIL', 1),
(3331, 215, 'Bingöl', 'BIN', 1),
(3332, 215, 'Bitlis', 'BIT', 1),
(3333, 215, 'Bolu', 'BOL', 1),
(3334, 215, 'Burdur', 'BRD', 1),
(3335, 215, 'Bursa', 'BRS', 1),
(3336, 215, 'Çanakkale', 'CKL', 1),
(3337, 215, 'Çankırı', 'CKR', 1),
(3338, 215, 'Çorum', 'COR', 1),
(3339, 215, 'Denizli', 'DEN', 1),
(3340, 215, 'Diyarbakır', 'DIY', 1),
(3341, 215, 'Düzce', 'DUZ', 1),
(3342, 215, 'Edirne', 'EDI', 1),
(3343, 215, 'Elazığ', 'ELA', 1),
(3344, 215, 'Erzincan', 'EZC', 1),
(3345, 215, 'Erzurum', 'EZR', 1),
(3346, 215, 'Eskişehir', 'ESK', 1),
(3347, 215, 'Gaziantep', 'GAZ', 1),
(3348, 215, 'Giresun', 'GIR', 1),
(3349, 215, 'Gümüşhane', 'GMS', 1),
(3350, 215, 'Hakkari', 'HKR', 1),
(3351, 215, 'Hatay', 'HTY', 1),
(3352, 215, 'Iğdır', 'IGD', 1),
(3353, 215, 'Isparta', 'ISP', 1),
(3354, 215, 'İstanbul', 'IST', 1),
(3355, 215, 'İzmir', 'IZM', 1),
(3356, 215, 'Kahramanmaraş', 'KAH', 1),
(3357, 215, 'Karabük', 'KRB', 1),
(3358, 215, 'Karaman', 'KRM', 1),
(3359, 215, 'Kars', 'KRS', 1),
(3360, 215, 'Kastamonu', 'KAS', 1),
(3361, 215, 'Kayseri', 'KAY', 1),
(3362, 215, 'Kilis', 'KLS', 1),
(3363, 215, 'Kırıkkale', 'KRK', 1),
(3364, 215, 'Kırklareli', 'KLR', 1),
(3365, 215, 'Kırşehir', 'KRH', 1),
(3366, 215, 'Kocaeli', 'KOC', 1),
(3367, 215, 'Konya', 'KON', 1),
(3368, 215, 'Kütahya', 'KUT', 1),
(3369, 215, 'Malatya', 'MAL', 1),
(3370, 215, 'Manisa', 'MAN', 1),
(3371, 215, 'Mardin', 'MAR', 1),
(3372, 215, 'Mersin', 'MER', 1),
(3373, 215, 'Muğla', 'MUG', 1),
(3374, 215, 'Muş', 'MUS', 1),
(3375, 215, 'Nevşehir', 'NEV', 1),
(3376, 215, 'Niğde', 'NIG', 1),
(3377, 215, 'Ordu', 'ORD', 1),
(3378, 215, 'Osmaniye', 'OSM', 1),
(3379, 215, 'Rize', 'RIZ', 1),
(3380, 215, 'Sakarya', 'SAK', 1),
(3381, 215, 'Samsun', 'SAM', 1),
(3382, 215, 'Şanlıurfa', 'SAN', 1),
(3383, 215, 'Siirt', 'SII', 1),
(3384, 215, 'Sinop', 'SIN', 1),
(3385, 215, 'Şırnak', 'SIR', 1),
(3386, 215, 'Sivas', 'SIV', 1),
(3387, 215, 'Tekirdağ', 'TEL', 1),
(3388, 215, 'Tokat', 'TOK', 1),
(3389, 215, 'Trabzon', 'TRA', 1),
(3390, 215, 'Tunceli', 'TUN', 1),
(3391, 215, 'Uşak', 'USK', 1),
(3392, 215, 'Van', 'VAN', 1),
(3393, 215, 'Yalova', 'YAL', 1),
(3394, 215, 'Yozgat', 'YOZ', 1),
(3395, 215, 'Zonguldak', 'ZON', 1),
(3396, 216, 'Ahal Welayaty', 'A', 1),
(3397, 216, 'Balkan Welayaty', 'B', 1),
(3398, 216, 'Dashhowuz Welayaty', 'D', 1),
(3399, 216, 'Lebap Welayaty', 'L', 1),
(3400, 216, 'Mary Welayaty', 'M', 1),
(3401, 217, 'Ambergris Cays', 'AC', 1),
(3402, 217, 'Dellis Cay', 'DC', 1),
(3403, 217, 'French Cay', 'FC', 1),
(3404, 217, 'Little Water Cay', 'LW', 1),
(3405, 217, 'Parrot Cay', 'RC', 1),
(3406, 217, 'Pine Cay', 'PN', 1),
(3407, 217, 'Salt Cay', 'SL', 1),
(3408, 217, 'Grand Turk', 'GT', 1),
(3409, 217, 'South Caicos', 'SC', 1),
(3410, 217, 'East Caicos', 'EC', 1),
(3411, 217, 'Middle Caicos', 'MC', 1),
(3412, 217, 'North Caicos', 'NC', 1),
(3413, 217, 'Providenciales', 'PR', 1),
(3414, 217, 'West Caicos', 'WC', 1),
(3415, 218, 'Nanumanga', 'NMG', 1),
(3416, 218, 'Niulakita', 'NLK', 1),
(3417, 218, 'Niutao', 'NTO', 1),
(3418, 218, 'Funafuti', 'FUN', 1),
(3419, 218, 'Nanumea', 'NME', 1),
(3420, 218, 'Nui', 'NUI', 1),
(3421, 218, 'Nukufetau', 'NFT', 1),
(3422, 218, 'Nukulaelae', 'NLL', 1),
(3423, 218, 'Vaitupu', 'VAI', 1),
(3424, 219, 'Kalangala', 'KAL', 1),
(3425, 219, 'Kampala', 'KMP', 1),
(3426, 219, 'Kayunga', 'KAY', 1),
(3427, 219, 'Kiboga', 'KIB', 1),
(3428, 219, 'Luwero', 'LUW', 1),
(3429, 219, 'Masaka', 'MAS', 1),
(3430, 219, 'Mpigi', 'MPI', 1),
(3431, 219, 'Mubende', 'MUB', 1),
(3432, 219, 'Mukono', 'MUK', 1),
(3433, 219, 'Nakasongola', 'NKS', 1),
(3434, 219, 'Rakai', 'RAK', 1),
(3435, 219, 'Sembabule', 'SEM', 1),
(3436, 219, 'Wakiso', 'WAK', 1),
(3437, 219, 'Bugiri', 'BUG', 1),
(3438, 219, 'Busia', 'BUS', 1),
(3439, 219, 'Iganga', 'IGA', 1),
(3440, 219, 'Jinja', 'JIN', 1),
(3441, 219, 'Kaberamaido', 'KAB', 1),
(3442, 219, 'Kamuli', 'KML', 1),
(3443, 219, 'Kapchorwa', 'KPC', 1),
(3444, 219, 'Katakwi', 'KTK', 1),
(3445, 219, 'Kumi', 'KUM', 1),
(3446, 219, 'Mayuge', 'MAY', 1),
(3447, 219, 'Mbale', 'MBA', 1),
(3448, 219, 'Pallisa', 'PAL', 1),
(3449, 219, 'Sironko', 'SIR', 1),
(3450, 219, 'Soroti', 'SOR', 1),
(3451, 219, 'Tororo', 'TOR', 1),
(3452, 219, 'Adjumani', 'ADJ', 1),
(3453, 219, 'Apac', 'APC', 1),
(3454, 219, 'Arua', 'ARU', 1),
(3455, 219, 'Gulu', 'GUL', 1),
(3456, 219, 'Kitgum', 'KIT', 1),
(3457, 219, 'Kotido', 'KOT', 1),
(3458, 219, 'Lira', 'LIR', 1),
(3459, 219, 'Moroto', 'MRT', 1),
(3460, 219, 'Moyo', 'MOY', 1),
(3461, 219, 'Nakapiripirit', 'NAK', 1),
(3462, 219, 'Nebbi', 'NEB', 1),
(3463, 219, 'Pader', 'PAD', 1),
(3464, 219, 'Yumbe', 'YUM', 1),
(3465, 219, 'Bundibugyo', 'BUN', 1),
(3466, 219, 'Bushenyi', 'BSH', 1),
(3467, 219, 'Hoima', 'HOI', 1),
(3468, 219, 'Kabale', 'KBL', 1),
(3469, 219, 'Kabarole', 'KAR', 1),
(3470, 219, 'Kamwenge', 'KAM', 1),
(3471, 219, 'Kanungu', 'KAN', 1),
(3472, 219, 'Kasese', 'KAS', 1),
(3473, 219, 'Kibaale', 'KBA', 1),
(3474, 219, 'Kisoro', 'KIS', 1),
(3475, 219, 'Kyenjojo', 'KYE', 1),
(3476, 219, 'Masindi', 'MSN', 1),
(3477, 219, 'Mbarara', 'MBR', 1),
(3478, 219, 'Ntungamo', 'NTU', 1),
(3479, 219, 'Rukungiri', 'RUK', 1),
(3480, 220, 'Черкасская область', '71', 1),
(3481, 220, 'Черниговская область', '74', 1),
(3482, 220, 'Черновицкая область', '77', 1),
(3483, 220, 'Крым', '43', 1),
(3484, 220, 'Днепропетровская область', '12', 1),
(3485, 220, 'Донецкая область', '14', 1),
(3486, 220, 'Ивано-Франковская область', '26', 1),
(3487, 220, 'Херсонская область', '65', 1),
(3488, 220, 'Хмельницкая область', '68', 1),
(3489, 220, 'Кировоградская область', '35', 1),
(3490, 220, 'Киев', '30', 1),
(3491, 220, 'Киевская область', '32', 1),
(3492, 220, 'Луганская область', '09', 1),
(3493, 220, 'Львовская область', '46', 1),
(3494, 220, 'Николаевская область', '48', 1),
(3495, 220, 'Одесская область', '51', 1),
(3496, 220, 'Полтавская область', '53', 1),
(3497, 220, 'Ровненская область', '56', 1),
(3498, 220, 'Севастополь', '40', 1),
(3499, 220, 'Сумская область', '59', 1),
(3500, 220, 'Тернопольская область', '61', 1),
(3501, 220, 'Винницкая область', '05', 1),
(3502, 220, 'Волынская область', '07', 1),
(3503, 220, 'Закарпатская область', '21', 1),
(3504, 220, 'Запорожская область', '23', 1),
(3505, 220, 'Житомирская область', '18', 1),
(3506, 221, 'Abu Zaby', 'AZ', 1),
(3507, 221, '\'Ajman', 'AJ', 1),
(3508, 221, 'Al Fujayrah', 'FU', 1),
(3509, 221, 'Ash Shariqah', 'SH', 1),
(3510, 221, 'Dubai', 'DU', 1),
(3511, 221, 'R\'as al Khaymah', 'RK', 1),
(3512, 221, 'Umm al Qaywayn', 'UQ', 1),
(3513, 222, 'Aberdeen', 'ABN', 1),
(3514, 222, 'Aberdeenshire', 'ABNS', 1),
(3515, 222, 'Anglesey', 'ANG', 1),
(3516, 222, 'Angus', 'AGS', 1),
(3517, 222, 'Argyll and Bute', 'ARY', 1),
(3518, 222, 'Bedfordshire', 'BEDS', 1),
(3519, 222, 'Berkshire', 'BERKS', 1),
(3520, 222, 'Blaenau Gwent', 'BLA', 1),
(3521, 222, 'Bridgend', 'BRI', 1),
(3522, 222, 'Bristol', 'BSTL', 1),
(3523, 222, 'Buckinghamshire', 'BUCKS', 1),
(3524, 222, 'Caerphilly', 'CAE', 1),
(3525, 222, 'Cambridgeshire', 'CAMBS', 1),
(3526, 222, 'Cardiff', 'CDF', 1),
(3527, 222, 'Carmarthenshire', 'CARM', 1),
(3528, 222, 'Ceredigion', 'CDGN', 1),
(3529, 222, 'Cheshire', 'CHES', 1),
(3530, 222, 'Clackmannanshire', 'CLACK', 1),
(3531, 222, 'Conwy', 'CON', 1),
(3532, 222, 'Cornwall', 'CORN', 1),
(3533, 222, 'Denbighshire', 'DNBG', 1),
(3534, 222, 'Derbyshire', 'DERBY', 1),
(3535, 222, 'Devon', 'DVN', 1),
(3536, 222, 'Dorset', 'DOR', 1),
(3537, 222, 'Dumfries and Galloway', 'DGL', 1),
(3538, 222, 'Dundee', 'DUND', 1),
(3539, 222, 'Durham', 'DHM', 1),
(3540, 222, 'East Ayrshire', 'ARYE', 1),
(3541, 222, 'East Dunbartonshire', 'DUNBE', 1),
(3542, 222, 'East Lothian', 'LOTE', 1),
(3543, 222, 'East Renfrewshire', 'RENE', 1),
(3544, 222, 'East Riding of Yorkshire', 'ERYS', 1),
(3545, 222, 'East Sussex', 'SXE', 1),
(3546, 222, 'Edinburgh', 'EDIN', 1),
(3547, 222, 'Essex', 'ESX', 1),
(3548, 222, 'Falkirk', 'FALK', 1),
(3549, 222, 'Fife', 'FFE', 1),
(3550, 222, 'Flintshire', 'FLINT', 1),
(3551, 222, 'Glasgow', 'GLAS', 1),
(3552, 222, 'Gloucestershire', 'GLOS', 1),
(3553, 222, 'Greater London', 'LDN', 1),
(3554, 222, 'Greater Manchester', 'MCH', 1),
(3555, 222, 'Gwynedd', 'GDD', 1),
(3556, 222, 'Hampshire', 'HANTS', 1),
(3557, 222, 'Herefordshire', 'HWR', 1),
(3558, 222, 'Hertfordshire', 'HERTS', 1),
(3559, 222, 'Highlands', 'HLD', 1),
(3560, 222, 'Inverclyde', 'IVER', 1),
(3561, 222, 'Isle of Wight', 'IOW', 1),
(3562, 222, 'Kent', 'KNT', 1),
(3563, 222, 'Lancashire', 'LANCS', 1),
(3564, 222, 'Leicestershire', 'LEICS', 1),
(3565, 222, 'Lincolnshire', 'LINCS', 1),
(3566, 222, 'Merseyside', 'MSY', 1),
(3567, 222, 'Merthyr Tydfil', 'MERT', 1),
(3568, 222, 'Midlothian', 'MLOT', 1),
(3569, 222, 'Monmouthshire', 'MMOUTH', 1),
(3570, 222, 'Moray', 'MORAY', 1),
(3571, 222, 'Neath Port Talbot', 'NPRTAL', 1),
(3572, 222, 'Newport', 'NEWPT', 1),
(3573, 222, 'Norfolk', 'NOR', 1),
(3574, 222, 'North Ayrshire', 'ARYN', 1),
(3575, 222, 'North Lanarkshire', 'LANN', 1),
(3576, 222, 'North Yorkshire', 'YSN', 1),
(3577, 222, 'Northamptonshire', 'NHM', 1),
(3578, 222, 'Northumberland', 'NLD', 1),
(3579, 222, 'Nottinghamshire', 'NOT', 1),
(3580, 222, 'Orkney Islands', 'ORK', 1),
(3581, 222, 'Oxfordshire', 'OFE', 1),
(3582, 222, 'Pembrokeshire', 'PEM', 1),
(3583, 222, 'Perth and Kinross', 'PERTH', 1),
(3584, 222, 'Powys', 'PWS', 1),
(3585, 222, 'Renfrewshire', 'REN', 1),
(3586, 222, 'Rhondda Cynon Taff', 'RHON', 1),
(3587, 222, 'Rutland', 'RUT', 1),
(3588, 222, 'Scottish Borders', 'BOR', 1),
(3589, 222, 'Shetland Islands', 'SHET', 1),
(3590, 222, 'Shropshire', 'SPE', 1),
(3591, 222, 'Somerset', 'SOM', 1),
(3592, 222, 'South Ayrshire', 'ARYS', 1),
(3593, 222, 'South Lanarkshire', 'LANS', 1),
(3594, 222, 'South Yorkshire', 'YSS', 1),
(3595, 222, 'Staffordshire', 'SFD', 1),
(3596, 222, 'Stirling', 'STIR', 1),
(3597, 222, 'Suffolk', 'SFK', 1),
(3598, 222, 'Surrey', 'SRY', 1),
(3599, 222, 'Swansea', 'SWAN', 1),
(3600, 222, 'Torfaen', 'TORF', 1),
(3601, 222, 'Tyne and Wear', 'TWR', 1),
(3602, 222, 'Vale of Glamorgan', 'VGLAM', 1),
(3603, 222, 'Warwickshire', 'WARKS', 1),
(3604, 222, 'West Dunbartonshire', 'WDUN', 1),
(3605, 222, 'West Lothian', 'WLOT', 1),
(3606, 222, 'West Midlands', 'WMD', 1),
(3607, 222, 'West Sussex', 'SXW', 1),
(3608, 222, 'West Yorkshire', 'YSW', 1),
(3609, 222, 'Western Isles', 'WIL', 1),
(3610, 222, 'Wiltshire', 'WLT', 1),
(3611, 222, 'Worcestershire', 'WORCS', 1),
(3612, 222, 'Wrexham', 'WRX', 1),
(3613, 223, 'Alabama', 'AL', 1),
(3614, 223, 'Alaska', 'AK', 1),
(3615, 223, 'American Samoa', 'AS', 1),
(3616, 223, 'Arizona', 'AZ', 1),
(3617, 223, 'Arkansas', 'AR', 1),
(3618, 223, 'Armed Forces Africa', 'AF', 1),
(3619, 223, 'Armed Forces Americas', 'AA', 1),
(3620, 223, 'Armed Forces Canada', 'AC', 1),
(3621, 223, 'Armed Forces Europe', 'AE', 1),
(3622, 223, 'Armed Forces Middle East', 'AM', 1),
(3623, 223, 'Armed Forces Pacific', 'AP', 1),
(3624, 223, 'California', 'CA', 1),
(3625, 223, 'Colorado', 'CO', 1),
(3626, 223, 'Connecticut', 'CT', 1),
(3627, 223, 'Delaware', 'DE', 1),
(3628, 223, 'District of Columbia', 'DC', 1),
(3629, 223, 'Federated States Of Micronesia', 'FM', 1),
(3630, 223, 'Florida', 'FL', 1),
(3631, 223, 'Georgia', 'GA', 1),
(3632, 223, 'Guam', 'GU', 1),
(3633, 223, 'Hawaii', 'HI', 1),
(3634, 223, 'Idaho', 'ID', 1),
(3635, 223, 'Illinois', 'IL', 1),
(3636, 223, 'Indiana', 'IN', 1),
(3637, 223, 'Iowa', 'IA', 1),
(3638, 223, 'Kansas', 'KS', 1),
(3639, 223, 'Kentucky', 'KY', 1),
(3640, 223, 'Louisiana', 'LA', 1),
(3641, 223, 'Maine', 'ME', 1),
(3642, 223, 'Marshall Islands', 'MH', 1),
(3643, 223, 'Maryland', 'MD', 1),
(3644, 223, 'Massachusetts', 'MA', 1),
(3645, 223, 'Michigan', 'MI', 1),
(3646, 223, 'Minnesota', 'MN', 1),
(3647, 223, 'Mississippi', 'MS', 1),
(3648, 223, 'Missouri', 'MO', 1),
(3649, 223, 'Montana', 'MT', 1),
(3650, 223, 'Nebraska', 'NE', 1),
(3651, 223, 'Nevada', 'NV', 1),
(3652, 223, 'New Hampshire', 'NH', 1),
(3653, 223, 'New Jersey', 'NJ', 1),
(3654, 223, 'New Mexico', 'NM', 1),
(3655, 223, 'New York', 'NY', 1),
(3656, 223, 'North Carolina', 'NC', 1),
(3657, 223, 'North Dakota', 'ND', 1),
(3658, 223, 'Northern Mariana Islands', 'MP', 1),
(3659, 223, 'Ohio', 'OH', 1),
(3660, 223, 'Oklahoma', 'OK', 1),
(3661, 223, 'Oregon', 'OR', 1),
(3662, 223, 'Palau', 'PW', 1),
(3663, 223, 'Pennsylvania', 'PA', 1),
(3664, 223, 'Puerto Rico', 'PR', 1),
(3665, 223, 'Rhode Island', 'RI', 1),
(3666, 223, 'South Carolina', 'SC', 1),
(3667, 223, 'South Dakota', 'SD', 1),
(3668, 223, 'Tennessee', 'TN', 1),
(3669, 223, 'Texas', 'TX', 1),
(3670, 223, 'Utah', 'UT', 1),
(3671, 223, 'Vermont', 'VT', 1),
(3672, 223, 'Virgin Islands', 'VI', 1),
(3673, 223, 'Virginia', 'VA', 1),
(3674, 223, 'Washington', 'WA', 1),
(3675, 223, 'West Virginia', 'WV', 1),
(3676, 223, 'Wisconsin', 'WI', 1),
(3677, 223, 'Wyoming', 'WY', 1),
(3678, 224, 'Baker Island', 'BI', 1),
(3679, 224, 'Howland Island', 'HI', 1),
(3680, 224, 'Jarvis Island', 'JI', 1),
(3681, 224, 'Johnston Atoll', 'JA', 1),
(3682, 224, 'Kingman Reef', 'KR', 1),
(3683, 224, 'Midway Atoll', 'MA', 1),
(3684, 224, 'Navassa Island', 'NI', 1),
(3685, 224, 'Palmyra Atoll', 'PA', 1),
(3686, 224, 'Wake Island', 'WI', 1),
(3687, 225, 'Artigas', 'AR', 1),
(3688, 225, 'Canelones', 'CA', 1),
(3689, 225, 'Cerro Largo', 'CL', 1),
(3690, 225, 'Colonia', 'CO', 1),
(3691, 225, 'Durazno', 'DU', 1),
(3692, 225, 'Flores', 'FS', 1),
(3693, 225, 'Florida', 'FA', 1),
(3694, 225, 'Lavalleja', 'LA', 1),
(3695, 225, 'Maldonado', 'MA', 1),
(3696, 225, 'Montevideo', 'MO', 1),
(3697, 225, 'Paysandu', 'PA', 1),
(3698, 225, 'Rio Negro', 'RN', 1),
(3699, 225, 'Rivera', 'RV', 1),
(3700, 225, 'Rocha', 'RO', 1),
(3701, 225, 'Salto', 'SL', 1),
(3702, 225, 'San Jose', 'SJ', 1),
(3703, 225, 'Soriano', 'SO', 1),
(3704, 225, 'Tacuarembo', 'TA', 1),
(3705, 225, 'Treinta y Tres', 'TT', 1),
(3706, 226, 'Andijon', 'AN', 1),
(3707, 226, 'Buxoro', 'BU', 1),
(3708, 226, 'Farg\'ona', 'FA', 1),
(3709, 226, 'Jizzax', 'JI', 1),
(3710, 226, 'Namangan', 'NG', 1),
(3711, 226, 'Navoiy', 'NW', 1),
(3712, 226, 'Qashqadaryo', 'QA', 1),
(3713, 226, 'Qoraqalpog\'iston Republikasi', 'QR', 1),
(3714, 226, 'Samarqand', 'SA', 1),
(3715, 226, 'Sirdaryo', 'SI', 1),
(3716, 226, 'Surxondaryo', 'SU', 1),
(3717, 226, 'Toshkent City', 'TK', 1),
(3718, 226, 'Toshkent Region', 'TO', 1),
(3719, 226, 'Xorazm', 'XO', 1),
(3720, 227, 'Malampa', 'MA', 1),
(3721, 227, 'Penama', 'PE', 1),
(3722, 227, 'Sanma', 'SA', 1),
(3723, 227, 'Shefa', 'SH', 1),
(3724, 227, 'Tafea', 'TA', 1),
(3725, 227, 'Torba', 'TO', 1),
(3726, 229, 'Amazonas', 'AM', 1),
(3727, 229, 'Anzoategui', 'AN', 1),
(3728, 229, 'Apure', 'AP', 1),
(3729, 229, 'Aragua', 'AR', 1),
(3730, 229, 'Barinas', 'BA', 1),
(3731, 229, 'Bolivar', 'BO', 1),
(3732, 229, 'Carabobo', 'CA', 1),
(3733, 229, 'Cojedes', 'CO', 1),
(3734, 229, 'Delta Amacuro', 'DA', 1),
(3735, 229, 'Dependencias Federales', 'DF', 1),
(3736, 229, 'Distrito Federal', 'DI', 1),
(3737, 229, 'Falcon', 'FA', 1),
(3738, 229, 'Guarico', 'GU', 1),
(3739, 229, 'Lara', 'LA', 1),
(3740, 229, 'Merida', 'ME', 1),
(3741, 229, 'Miranda', 'MI', 1),
(3742, 229, 'Monagas', 'MO', 1),
(3743, 229, 'Nueva Esparta', 'NE', 1),
(3744, 229, 'Portuguesa', 'PO', 1),
(3745, 229, 'Sucre', 'SU', 1),
(3746, 229, 'Tachira', 'TA', 1),
(3747, 229, 'Trujillo', 'TR', 1),
(3748, 229, 'Vargas', 'VA', 1),
(3749, 229, 'Yaracuy', 'YA', 1),
(3750, 229, 'Zulia', 'ZU', 1),
(3751, 230, 'An Giang', 'AG', 1),
(3752, 230, 'Bac Giang', 'BG', 1),
(3753, 230, 'Bac Kan', 'BK', 1),
(3754, 230, 'Bac Lieu', 'BL', 1),
(3755, 230, 'Bac Ninh', 'BC', 1),
(3756, 230, 'Ba Ria-Vung Tau', 'BR', 1),
(3757, 230, 'Ben Tre', 'BN', 1),
(3758, 230, 'Binh Dinh', 'BH', 1),
(3759, 230, 'Binh Duong', 'BU', 1),
(3760, 230, 'Binh Phuoc', 'BP', 1),
(3761, 230, 'Binh Thuan', 'BT', 1),
(3762, 230, 'Ca Mau', 'CM', 1),
(3763, 230, 'Can Tho', 'CT', 1),
(3764, 230, 'Cao Bang', 'CB', 1),
(3765, 230, 'Dak Lak', 'DL', 1),
(3766, 230, 'Dak Nong', 'DG', 1),
(3767, 230, 'Da Nang', 'DN', 1),
(3768, 230, 'Dien Bien', 'DB', 1),
(3769, 230, 'Dong Nai', 'DI', 1),
(3770, 230, 'Dong Thap', 'DT', 1),
(3771, 230, 'Gia Lai', 'GL', 1),
(3772, 230, 'Ha Giang', 'HG', 1),
(3773, 230, 'Hai Duong', 'HD', 1),
(3774, 230, 'Hai Phong', 'HP', 1),
(3775, 230, 'Ha Nam', 'HM', 1),
(3776, 230, 'Ha Noi', 'HI', 1),
(3777, 230, 'Ha Tay', 'HT', 1),
(3778, 230, 'Ha Tinh', 'HH', 1),
(3779, 230, 'Hoa Binh', 'HB', 1),
(3780, 230, 'Ho Chi Minh City', 'HC', 1),
(3781, 230, 'Hau Giang', 'HU', 1),
(3782, 230, 'Hung Yen', 'HY', 1),
(3783, 232, 'Saint Croix', 'C', 1),
(3784, 232, 'Saint John', 'J', 1),
(3785, 232, 'Saint Thomas', 'T', 1),
(3786, 233, 'Alo', 'A', 1),
(3787, 233, 'Sigave', 'S', 1),
(3788, 233, 'Wallis', 'W', 1),
(3789, 235, 'Abyan', 'AB', 1),
(3790, 235, 'Adan', 'AD', 1),
(3791, 235, 'Amran', 'AM', 1),
(3792, 235, 'Al Bayda', 'BA', 1),
(3793, 235, 'Ad Dali', 'DA', 1),
(3794, 235, 'Dhamar', 'DH', 1),
(3795, 235, 'Hadramawt', 'HD', 1),
(3796, 235, 'Hajjah', 'HJ', 1),
(3797, 235, 'Al Hudaydah', 'HU', 1),
(3798, 235, 'Ibb', 'IB', 1),
(3799, 235, 'Al Jawf', 'JA', 1),
(3800, 235, 'Lahij', 'LA', 1),
(3801, 235, 'Ma\'rib', 'MA', 1),
(3802, 235, 'Al Mahrah', 'MR', 1),
(3803, 235, 'Al Mahwit', 'MW', 1),
(3804, 235, 'Sa\'dah', 'SD', 1),
(3805, 235, 'San\'a', 'SN', 1),
(3806, 235, 'Shabwah', 'SH', 1),
(3807, 235, 'Ta\'izz', 'TA', 1),
(3812, 237, 'Bas-Congo', 'BC', 1),
(3813, 237, 'Bandundu', 'BN', 1),
(3814, 237, 'Equateur', 'EQ', 1),
(3815, 237, 'Katanga', 'KA', 1),
(3816, 237, 'Kasai-Oriental', 'KE', 1),
(3817, 237, 'Kinshasa', 'KN', 1),
(3818, 237, 'Kasai-Occidental', 'KW', 1),
(3819, 237, 'Maniema', 'MA', 1),
(3820, 237, 'Nord-Kivu', 'NK', 1),
(3821, 237, 'Orientale', 'OR', 1),
(3822, 237, 'Sud-Kivu', 'SK', 1),
(3823, 238, 'Central', 'CE', 1),
(3824, 238, 'Copperbelt', 'CB', 1),
(3825, 238, 'Eastern', 'EA', 1),
(3826, 238, 'Luapula', 'LP', 1),
(3827, 238, 'Lusaka', 'LK', 1),
(3828, 238, 'Northern', 'NO', 1),
(3829, 238, 'North-Western', 'NW', 1),
(3830, 238, 'Southern', 'SO', 1),
(3831, 238, 'Western', 'WE', 1),
(3832, 239, 'Bulawayo', 'BU', 1),
(3833, 239, 'Harare', 'HA', 1),
(3834, 239, 'Manicaland', 'ML', 1),
(3835, 239, 'Mashonaland Central', 'MC', 1),
(3836, 239, 'Mashonaland East', 'ME', 1),
(3837, 239, 'Mashonaland West', 'MW', 1),
(3838, 239, 'Masvingo', 'MV', 1),
(3839, 239, 'Matabeleland North', 'MN', 1),
(3840, 239, 'Matabeleland South', 'MS', 1),
(3841, 239, 'Midlands', 'MD', 1),
(3842, 105, 'Agrigento', 'AG', 1),
(3843, 105, 'Alessandria', 'AL', 1),
(3844, 105, 'Ancona', 'AN', 1),
(3845, 105, 'Aosta', 'AO', 1),
(3846, 105, 'Arezzo', 'AR', 1),
(3847, 105, 'Ascoli Piceno', 'AP', 1),
(3848, 105, 'Asti', 'AT', 1),
(3849, 105, 'Avellino', 'AV', 1),
(3850, 105, 'Bari', 'BA', 1),
(3851, 105, 'Belluno', 'BL', 1),
(3852, 105, 'Benevento', 'BN', 1),
(3853, 105, 'Bergamo', 'BG', 1),
(3854, 105, 'Biella', 'BI', 1),
(3855, 105, 'Bologna', 'BO', 1),
(3856, 105, 'Bolzano', 'BZ', 1),
(3857, 105, 'Brescia', 'BS', 1),
(3858, 105, 'Brindisi', 'BR', 1),
(3859, 105, 'Cagliari', 'CA', 1),
(3860, 105, 'Caltanissetta', 'CL', 1),
(3861, 105, 'Campobasso', 'CB', 1),
(3862, 105, 'Carbonia-Iglesias', 'CI', 1),
(3863, 105, 'Caserta', 'CE', 1),
(3864, 105, 'Catania', 'CT', 1),
(3865, 105, 'Catanzaro', 'CZ', 1),
(3866, 105, 'Chieti', 'CH', 1),
(3867, 105, 'Como', 'CO', 1),
(3868, 105, 'Cosenza', 'CS', 1),
(3869, 105, 'Cremona', 'CR', 1),
(3870, 105, 'Crotone', 'KR', 1),
(3871, 105, 'Cuneo', 'CN', 1),
(3872, 105, 'Enna', 'EN', 1),
(3873, 105, 'Ferrara', 'FE', 1),
(3874, 105, 'Firenze', 'FI', 1),
(3875, 105, 'Foggia', 'FG', 1),
(3876, 105, 'Forli-Cesena', 'FC', 1),
(3877, 105, 'Frosinone', 'FR', 1),
(3878, 105, 'Genova', 'GE', 1),
(3879, 105, 'Gorizia', 'GO', 1),
(3880, 105, 'Grosseto', 'GR', 1),
(3881, 105, 'Imperia', 'IM', 1),
(3882, 105, 'Isernia', 'IS', 1),
(3883, 105, 'L&#39;Aquila', 'AQ', 1),
(3884, 105, 'La Spezia', 'SP', 1),
(3885, 105, 'Latina', 'LT', 1),
(3886, 105, 'Lecce', 'LE', 1),
(3887, 105, 'Lecco', 'LC', 1),
(3888, 105, 'Livorno', 'LI', 1),
(3889, 105, 'Lodi', 'LO', 1),
(3890, 105, 'Lucca', 'LU', 1),
(3891, 105, 'Macerata', 'MC', 1),
(3892, 105, 'Mantova', 'MN', 1),
(3893, 105, 'Massa-Carrara', 'MS', 1),
(3894, 105, 'Matera', 'MT', 1),
(3895, 105, 'Medio Campidano', 'VS', 1),
(3896, 105, 'Messina', 'ME', 1),
(3897, 105, 'Milano', 'MI', 1),
(3898, 105, 'Modena', 'MO', 1),
(3899, 105, 'Napoli', 'NA', 1),
(3900, 105, 'Novara', 'NO', 1),
(3901, 105, 'Nuoro', 'NU', 1),
(3902, 105, 'Ogliastra', 'OG', 1),
(3903, 105, 'Olbia-Tempio', 'OT', 1),
(3904, 105, 'Oristano', 'OR', 1),
(3905, 105, 'Padova', 'PD', 1),
(3906, 105, 'Palermo', 'PA', 1),
(3907, 105, 'Parma', 'PR', 1),
(3908, 105, 'Pavia', 'PV', 1),
(3909, 105, 'Perugia', 'PG', 1),
(3910, 105, 'Pesaro e Urbino', 'PU', 1),
(3911, 105, 'Pescara', 'PE', 1),
(3912, 105, 'Piacenza', 'PC', 1),
(3913, 105, 'Pisa', 'PI', 1),
(3914, 105, 'Pistoia', 'PT', 1),
(3915, 105, 'Pordenone', 'PN', 1),
(3916, 105, 'Potenza', 'PZ', 1),
(3917, 105, 'Prato', 'PO', 1),
(3918, 105, 'Ragusa', 'RG', 1),
(3919, 105, 'Ravenna', 'RA', 1),
(3920, 105, 'Reggio Calabria', 'RC', 1),
(3921, 105, 'Reggio Emilia', 'RE', 1),
(3922, 105, 'Rieti', 'RI', 1),
(3923, 105, 'Rimini', 'RN', 1),
(3924, 105, 'Roma', 'RM', 1),
(3925, 105, 'Rovigo', 'RO', 1),
(3926, 105, 'Salerno', 'SA', 1),
(3927, 105, 'Sassari', 'SS', 1),
(3928, 105, 'Savona', 'SV', 1),
(3929, 105, 'Siena', 'SI', 1),
(3930, 105, 'Siracusa', 'SR', 1),
(3931, 105, 'Sondrio', 'SO', 1),
(3932, 105, 'Taranto', 'TA', 1),
(3933, 105, 'Teramo', 'TE', 1),
(3934, 105, 'Terni', 'TR', 1),
(3935, 105, 'Torino', 'TO', 1),
(3936, 105, 'Trapani', 'TP', 1),
(3937, 105, 'Trento', 'TN', 1),
(3938, 105, 'Treviso', 'TV', 1),
(3939, 105, 'Trieste', 'TS', 1),
(3940, 105, 'Udine', 'UD', 1),
(3941, 105, 'Varese', 'VA', 1),
(3942, 105, 'Venezia', 'VE', 1),
(3943, 105, 'Verbano-Cusio-Ossola', 'VB', 1),
(3944, 105, 'Vercelli', 'VC', 1),
(3945, 105, 'Verona', 'VR', 1),
(3946, 105, 'Vibo Valentia', 'VV', 1),
(3947, 105, 'Vicenza', 'VI', 1),
(3948, 105, 'Viterbo', 'VT', 1),
(3949, 222, 'County Antrim', 'ANT', 1),
(3950, 222, 'County Armagh', 'ARM', 1),
(3951, 222, 'County Down', 'DOW', 1),
(3952, 222, 'County Fermanagh', 'FER', 1),
(3953, 222, 'County Londonderry', 'LDY', 1),
(3954, 222, 'County Tyrone', 'TYR', 1),
(3955, 222, 'Cumbria', 'CMA', 1),
(3956, 190, 'Pomurska', '1', 1),
(3957, 190, 'Podravska', '2', 1),
(3958, 190, 'Koroška', '3', 1),
(3959, 190, 'Savinjska', '4', 1),
(3960, 190, 'Zasavska', '5', 1),
(3961, 190, 'Spodnjeposavska', '6', 1),
(3962, 190, 'Jugovzhodna Slovenija', '7', 1),
(3963, 190, 'Osrednjeslovenska', '8', 1),
(3964, 190, 'Gorenjska', '9', 1),
(3965, 190, 'Notranjsko-kraška', '10', 1),
(3966, 190, 'Goriška', '11', 1),
(3967, 190, 'Obalno-kraška', '12', 1),
(3968, 33, 'Ruse', '', 1),
(3969, 101, 'Alborz', 'ALB', 1),
(3970, 21, 'Brussels-Capital Region', 'BRU', 1),
(3971, 138, 'Aguascalientes', 'AG', 1),
(3973, 242, 'Andrijevica', '01', 1),
(3974, 242, 'Bar', '02', 1),
(3975, 242, 'Berane', '03', 1),
(3976, 242, 'Bijelo Polje', '04', 1),
(3977, 242, 'Budva', '05', 1),
(3978, 242, 'Cetinje', '06', 1),
(3979, 242, 'Danilovgrad', '07', 1),
(3980, 242, 'Herceg-Novi', '08', 1),
(3981, 242, 'Kolašin', '09', 1),
(3982, 242, 'Kotor', '10', 1),
(3983, 242, 'Mojkovac', '11', 1),
(3984, 242, 'Nikšić', '12', 1),
(3985, 242, 'Plav', '13', 1),
(3986, 242, 'Pljevlja', '14', 1),
(3987, 242, 'Plužine', '15', 1),
(3988, 242, 'Podgorica', '16', 1),
(3989, 242, 'Rožaje', '17', 1),
(3990, 242, 'Šavnik', '18', 1),
(3991, 242, 'Tivat', '19', 1),
(3992, 242, 'Ulcinj', '20', 1),
(3993, 242, 'Žabljak', '21', 1),
(3994, 243, 'Belgrade', '00', 1),
(3995, 243, 'North Bačka', '01', 1),
(3996, 243, 'Central Banat', '02', 1),
(3997, 243, 'North Banat', '03', 1),
(3998, 243, 'South Banat', '04', 1),
(3999, 243, 'West Bačka', '05', 1),
(4000, 243, 'South Bačka', '06', 1),
(4001, 243, 'Srem', '07', 1),
(4002, 243, 'Mačva', '08', 1),
(4003, 243, 'Kolubara', '09', 1),
(4004, 243, 'Podunavlje', '10', 1),
(4005, 243, 'Braničevo', '11', 1),
(4006, 243, 'Šumadija', '12', 1),
(4007, 243, 'Pomoravlje', '13', 1),
(4008, 243, 'Bor', '14', 1),
(4009, 243, 'Zaječar', '15', 1),
(4010, 243, 'Zlatibor', '16', 1),
(4011, 243, 'Moravica', '17', 1),
(4012, 243, 'Raška', '18', 1),
(4013, 243, 'Rasina', '19', 1),
(4014, 243, 'Nišava', '20', 1),
(4015, 243, 'Toplica', '21', 1),
(4016, 243, 'Pirot', '22', 1),
(4017, 243, 'Jablanica', '23', 1),
(4018, 243, 'Pčinja', '24', 1),
(4020, 245, 'Bonaire', 'BO', 1),
(4021, 245, 'Saba', 'SA', 1),
(4022, 245, 'Sint Eustatius', 'SE', 1),
(4023, 248, 'Central Equatoria', 'EC', 1),
(4024, 248, 'Eastern Equatoria', 'EE', 1),
(4025, 248, 'Jonglei', 'JG', 1),
(4026, 248, 'Lakes', 'LK', 1),
(4027, 248, 'Northern Bahr el-Ghazal', 'BN', 1),
(4028, 248, 'Unity', 'UY', 1),
(4029, 248, 'Upper Nile', 'NU', 1),
(4030, 248, 'Warrap', 'WR', 1),
(4031, 248, 'Western Bahr el-Ghazal', 'BW', 1),
(4032, 248, 'Western Equatoria', 'EW', 1),
(4035, 129, 'Putrajaya', 'MY-16', 1),
(4036, 117, 'Ainaži, Salacgrīvas novads', '0661405', 1),
(4037, 117, 'Aizkraukle, Aizkraukles novads', '0320201', 1),
(4038, 117, 'Aizkraukles novads', '0320200', 1),
(4039, 117, 'Aizpute, Aizputes novads', '0640605', 1),
(4040, 117, 'Aizputes novads', '0640600', 1),
(4041, 117, 'Aknīste, Aknīstes novads', '0560805', 1),
(4042, 117, 'Aknīstes novads', '0560800', 1),
(4043, 117, 'Aloja, Alojas novads', '0661007', 1),
(4044, 117, 'Alojas novads', '0661000', 1),
(4045, 117, 'Alsungas novads', '0624200', 1),
(4046, 117, 'Alūksne, Alūksnes novads', '0360201', 1),
(4047, 117, 'Alūksnes novads', '0360200', 1),
(4048, 117, 'Amatas novads', '0424701', 1),
(4049, 117, 'Ape, Apes novads', '0360805', 1),
(4050, 117, 'Apes novads', '0360800', 1),
(4051, 117, 'Auce, Auces novads', '0460805', 1),
(4052, 117, 'Auces novads', '0460800', 1),
(4053, 117, 'Ādažu novads', '0804400', 1),
(4054, 117, 'Babītes novads', '0804900', 1),
(4055, 117, 'Baldone, Baldones novads', '0800605', 1),
(4056, 117, 'Baldones novads', '0800600', 1),
(4057, 117, 'Baloži, Ķekavas novads', '0800807', 1),
(4058, 117, 'Baltinavas novads', '0384400', 1),
(4059, 117, 'Balvi, Balvu novads', '0380201', 1),
(4060, 117, 'Balvu novads', '0380200', 1),
(4061, 117, 'Bauska, Bauskas novads', '0400201', 1),
(4062, 117, 'Bauskas novads', '0400200', 1),
(4063, 117, 'Beverīnas novads', '0964700', 1),
(4064, 117, 'Brocēni, Brocēnu novads', '0840605', 1),
(4065, 117, 'Brocēnu novads', '0840601', 1),
(4066, 117, 'Burtnieku novads', '0967101', 1),
(4067, 117, 'Carnikavas novads', '0805200', 1),
(4068, 117, 'Cesvaine, Cesvaines novads', '0700807', 1),
(4069, 117, 'Cesvaines novads', '0700800', 1),
(4070, 117, 'Cēsis, Cēsu novads', '0420201', 1),
(4071, 117, 'Cēsu novads', '0420200', 1),
(4072, 117, 'Ciblas novads', '0684901', 1),
(4073, 117, 'Dagda, Dagdas novads', '0601009', 1),
(4074, 117, 'Dagdas novads', '0601000', 1),
(4075, 117, 'Daugavpils', '0050000', 1),
(4076, 117, 'Daugavpils novads', '0440200', 1),
(4077, 117, 'Dobele, Dobeles novads', '0460201', 1),
(4078, 117, 'Dobeles novads', '0460200', 1),
(4079, 117, 'Dundagas novads', '0885100', 1),
(4080, 117, 'Durbe, Durbes novads', '0640807', 1),
(4081, 117, 'Durbes novads', '0640801', 1),
(4082, 117, 'Engures novads', '0905100', 1),
(4083, 117, 'Ērgļu novads', '0705500', 1),
(4084, 117, 'Garkalnes novads', '0806000', 1),
(4085, 117, 'Grobiņa, Grobiņas novads', '0641009', 1),
(4086, 117, 'Grobiņas novads', '0641000', 1),
(4087, 117, 'Gulbene, Gulbenes novads', '0500201', 1),
(4088, 117, 'Gulbenes novads', '0500200', 1),
(4089, 117, 'Iecavas novads', '0406400', 1),
(4090, 117, 'Ikšķile, Ikšķiles novads', '0740605', 1),
(4091, 117, 'Ikšķiles novads', '0740600', 1),
(4092, 117, 'Ilūkste, Ilūkstes novads', '0440807', 1),
(4093, 117, 'Ilūkstes novads', '0440801', 1),
(4094, 117, 'Inčukalna novads', '0801800', 1),
(4095, 117, 'Jaunjelgava, Jaunjelgavas novads', '0321007', 1),
(4096, 117, 'Jaunjelgavas novads', '0321000', 1),
(4097, 117, 'Jaunpiebalgas novads', '0425700', 1),
(4098, 117, 'Jaunpils novads', '0905700', 1),
(4099, 117, 'Jelgava', '0090000', 1),
(4100, 117, 'Jelgavas novads', '0540200', 1),
(4101, 117, 'Jēkabpils', '0110000', 1),
(4102, 117, 'Jēkabpils novads', '0560200', 1),
(4103, 117, 'Jūrmala', '0130000', 1),
(4104, 117, 'Kalnciems, Jelgavas novads', '0540211', 1),
(4105, 117, 'Kandava, Kandavas novads', '0901211', 1),
(4106, 117, 'Kandavas novads', '0901201', 1),
(4107, 117, 'Kārsava, Kārsavas novads', '0681009', 1),
(4108, 117, 'Kārsavas novads', '0681000', 1),
(4109, 117, 'Kocēnu novads ,bij. Valmieras)', '0960200', 1),
(4110, 117, 'Kokneses novads', '0326100', 1),
(4111, 117, 'Krāslava, Krāslavas novads', '0600201', 1),
(4112, 117, 'Krāslavas novads', '0600202', 1),
(4113, 117, 'Krimuldas novads', '0806900', 1),
(4114, 117, 'Krustpils novads', '0566900', 1),
(4115, 117, 'Kuldīga, Kuldīgas novads', '0620201', 1),
(4116, 117, 'Kuldīgas novads', '0620200', 1),
(4117, 117, 'Ķeguma novads', '0741001', 1),
(4118, 117, 'Ķegums, Ķeguma novads', '0741009', 1),
(4119, 117, 'Ķekavas novads', '0800800', 1),
(4120, 117, 'Lielvārde, Lielvārdes novads', '0741413', 1),
(4121, 117, 'Lielvārdes novads', '0741401', 1),
(4122, 117, 'Liepāja', '0170000', 1),
(4123, 117, 'Limbaži, Limbažu novads', '0660201', 1),
(4124, 117, 'Limbažu novads', '0660200', 1),
(4125, 117, 'Līgatne, Līgatnes novads', '0421211', 1),
(4126, 117, 'Līgatnes novads', '0421200', 1),
(4127, 117, 'Līvāni, Līvānu novads', '0761211', 1),
(4128, 117, 'Līvānu novads', '0761201', 1),
(4129, 117, 'Lubāna, Lubānas novads', '0701413', 1),
(4130, 117, 'Lubānas novads', '0701400', 1),
(4131, 117, 'Ludza, Ludzas novads', '0680201', 1),
(4132, 117, 'Ludzas novads', '0680200', 1),
(4133, 117, 'Madona, Madonas novads', '0700201', 1),
(4134, 117, 'Madonas novads', '0700200', 1),
(4135, 117, 'Mazsalaca, Mazsalacas novads', '0961011', 1),
(4136, 117, 'Mazsalacas novads', '0961000', 1),
(4137, 117, 'Mālpils novads', '0807400', 1),
(4138, 117, 'Mārupes novads', '0807600', 1),
(4139, 117, 'Mērsraga novads', '0887600', 1),
(4140, 117, 'Naukšēnu novads', '0967300', 1),
(4141, 117, 'Neretas novads', '0327100', 1),
(4142, 117, 'Nīcas novads', '0647900', 1),
(4143, 117, 'Ogre, Ogres novads', '0740201', 1),
(4144, 117, 'Ogres novads', '0740202', 1),
(4145, 117, 'Olaine, Olaines novads', '0801009', 1),
(4146, 117, 'Olaines novads', '0801000', 1),
(4147, 117, 'Ozolnieku novads', '0546701', 1),
(4148, 117, 'Pārgaujas novads', '0427500', 1),
(4149, 117, 'Pāvilosta, Pāvilostas novads', '0641413', 1),
(4150, 117, 'Pāvilostas novads', '0641401', 1),
(4151, 117, 'Piltene, Ventspils novads', '0980213', 1),
(4152, 117, 'Pļaviņas, Pļaviņu novads', '0321413', 1),
(4153, 117, 'Pļaviņu novads', '0321400', 1),
(4154, 117, 'Preiļi, Preiļu novads', '0760201', 1),
(4155, 117, 'Preiļu novads', '0760202', 1),
(4156, 117, 'Priekule, Priekules novads', '0641615', 1),
(4157, 117, 'Priekules novads', '0641600', 1),
(4158, 117, 'Priekuļu novads', '0427300', 1),
(4159, 117, 'Raunas novads', '0427700', 1),
(4160, 117, 'Rēzekne', '0210000', 1),
(4161, 117, 'Rēzeknes novads', '0780200', 1),
(4162, 117, 'Riebiņu novads', '0766300', 1),
(4163, 117, 'Rīga', '0010000', 1),
(4164, 117, 'Rojas novads', '0888300', 1),
(4165, 117, 'Ropažu novads', '0808400', 1),
(4166, 117, 'Rucavas novads', '0648500', 1),
(4167, 117, 'Rugāju novads', '0387500', 1),
(4168, 117, 'Rundāles novads', '0407700', 1),
(4169, 117, 'Rūjiena, Rūjienas novads', '0961615', 1),
(4170, 117, 'Rūjienas novads', '0961600', 1),
(4171, 117, 'Sabile, Talsu novads', '0880213', 1),
(4172, 117, 'Salacgrīva, Salacgrīvas novads', '0661415', 1),
(4173, 117, 'Salacgrīvas novads', '0661400', 1),
(4174, 117, 'Salas novads', '0568700', 1),
(4175, 117, 'Salaspils novads', '0801200', 1),
(4176, 117, 'Salaspils, Salaspils novads', '0801211', 1),
(4177, 117, 'Saldus novads', '0840200', 1),
(4178, 117, 'Saldus, Saldus novads', '0840201', 1),
(4179, 117, 'Saulkrasti, Saulkrastu novads', '0801413', 1),
(4180, 117, 'Saulkrastu novads', '0801400', 1),
(4181, 117, 'Seda, Strenču novads', '0941813', 1),
(4182, 117, 'Sējas novads', '0809200', 1),
(4183, 117, 'Sigulda, Siguldas novads', '0801615', 1),
(4184, 117, 'Siguldas novads', '0801601', 1),
(4185, 117, 'Skrīveru novads', '0328200', 1),
(4186, 117, 'Skrunda, Skrundas novads', '0621209', 1),
(4187, 117, 'Skrundas novads', '0621200', 1),
(4188, 117, 'Smiltene, Smiltenes novads', '0941615', 1),
(4189, 117, 'Smiltenes novads', '0941600', 1),
(4190, 117, 'Staicele, Alojas novads', '0661017', 1),
(4191, 117, 'Stende, Talsu novads', '0880215', 1),
(4192, 117, 'Stopiņu novads', '0809600', 1),
(4193, 117, 'Strenči, Strenču novads', '0941817', 1),
(4194, 117, 'Strenču novads', '0941800', 1),
(4195, 117, 'Subate, Ilūkstes novads', '0440815', 1),
(4196, 117, 'Talsi, Talsu novads', '0880201', 1),
(4197, 117, 'Talsu novads', '0880200', 1),
(4198, 117, 'Tērvetes novads', '0468900', 1),
(4199, 117, 'Tukuma novads', '0900200', 1),
(4200, 117, 'Tukums, Tukuma novads', '0900201', 1),
(4201, 117, 'Vaiņodes novads', '0649300', 1),
(4202, 117, 'Valdemārpils, Talsu novads', '0880217', 1),
(4203, 117, 'Valka, Valkas novads', '0940201', 1),
(4204, 117, 'Valkas novads', '0940200', 1),
(4205, 117, 'Valmiera', '0250000', 1),
(4206, 117, 'Vangaži, Inčukalna novads', '0801817', 1),
(4207, 117, 'Varakļāni, Varakļānu novads', '0701817', 1),
(4208, 117, 'Varakļānu novads', '0701800', 1),
(4209, 117, 'Vārkavas novads', '0769101', 1),
(4210, 117, 'Vecpiebalgas novads', '0429300', 1),
(4211, 117, 'Vecumnieku novads', '0409500', 1),
(4212, 117, 'Ventspils', '0270000', 1),
(4213, 117, 'Ventspils novads', '0980200', 1),
(4214, 117, 'Viesīte, Viesītes novads', '0561815', 1),
(4215, 117, 'Viesītes novads', '0561800', 1),
(4216, 117, 'Viļaka, Viļakas novads', '0381615', 1),
(4217, 117, 'Viļakas novads', '0381600', 1),
(4218, 117, 'Viļāni, Viļānu novads', '0781817', 1),
(4219, 117, 'Viļānu novads', '0781800', 1),
(4220, 117, 'Zilupe, Zilupes novads', '0681817', 1),
(4221, 117, 'Zilupes novads', '0681801', 1),
(4222, 43, 'Arica y Parinacota', 'AP', 1),
(4223, 43, 'Los Rios', 'LR', 1),
(4224, 220, 'Харьковская область', '63', 1),
(4225, 118, 'Beirut', 'LB-BR', 1),
(4226, 118, 'Bekaa', 'LB-BE', 1),
(4227, 118, 'Mount Lebanon', 'LB-ML', 1),
(4228, 118, 'Nabatieh', 'LB-NB', 1),
(4229, 118, 'North', 'LB-NR', 1),
(4230, 118, 'South', 'LB-ST', 1),
(4231, 99, 'Telangana', 'TS', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_zone_to_geo_zone`
--

CREATE TABLE `oc_zone_to_geo_zone` (
  `zone_to_geo_zone_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL DEFAULT 0,
  `geo_zone_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_zone_to_geo_zone`
--

INSERT INTO `oc_zone_to_geo_zone` (`zone_to_geo_zone_id`, `country_id`, `zone_id`, `geo_zone_id`, `date_added`, `date_modified`) VALUES
(1, 222, 0, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 222, 3513, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 222, 3514, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 222, 3515, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 222, 3516, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 222, 3517, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 222, 3518, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 222, 3519, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 222, 3520, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 222, 3521, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 222, 3522, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 222, 3523, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 222, 3524, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 222, 3525, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 222, 3526, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 222, 3527, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 222, 3528, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 222, 3529, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 222, 3530, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 222, 3531, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 222, 3532, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 222, 3533, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 222, 3534, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 222, 3535, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 222, 3536, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 222, 3537, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 222, 3538, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 222, 3539, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 222, 3540, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 222, 3541, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 222, 3542, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 222, 3543, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 222, 3544, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 222, 3545, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 222, 3546, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 222, 3547, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 222, 3548, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 222, 3549, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 222, 3550, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 222, 3551, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 222, 3552, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 222, 3553, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 222, 3554, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 222, 3555, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 222, 3556, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 222, 3557, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 222, 3558, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 222, 3559, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 222, 3560, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 222, 3561, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 222, 3562, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 222, 3563, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 222, 3564, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 222, 3565, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 222, 3566, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 222, 3567, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 222, 3568, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 222, 3569, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 222, 3570, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 222, 3571, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 222, 3572, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 222, 3573, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 222, 3574, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 222, 3575, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 222, 3576, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 222, 3577, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 222, 3578, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 222, 3579, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 222, 3580, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, 222, 3581, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, 222, 3582, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, 222, 3583, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, 222, 3584, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, 222, 3585, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, 222, 3586, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, 222, 3587, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, 222, 3588, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, 222, 3589, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, 222, 3590, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, 222, 3591, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, 222, 3592, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, 222, 3593, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 222, 3594, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, 222, 3595, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, 222, 3596, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 222, 3597, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 222, 3598, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, 222, 3599, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, 222, 3600, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, 222, 3601, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, 222, 3602, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, 222, 3603, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, 222, 3604, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, 222, 3605, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, 222, 3606, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, 222, 3607, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, 222, 3608, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, 222, 3609, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, 222, 3610, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, 222, 3611, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(101, 222, 3612, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(102, 222, 3949, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, 222, 3950, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, 222, 3951, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(105, 222, 3952, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(106, 222, 3953, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, 222, 3954, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(108, 222, 3955, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(109, 222, 3972, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `oc_address`
--
ALTER TABLE `oc_address`
  ADD PRIMARY KEY (`address_id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Индексы таблицы `oc_api`
--
ALTER TABLE `oc_api`
  ADD PRIMARY KEY (`api_id`);

--
-- Индексы таблицы `oc_api_ip`
--
ALTER TABLE `oc_api_ip`
  ADD PRIMARY KEY (`api_ip_id`);

--
-- Индексы таблицы `oc_api_session`
--
ALTER TABLE `oc_api_session`
  ADD PRIMARY KEY (`api_session_id`);

--
-- Индексы таблицы `oc_article`
--
ALTER TABLE `oc_article`
  ADD PRIMARY KEY (`article_id`);

--
-- Индексы таблицы `oc_article_description`
--
ALTER TABLE `oc_article_description`
  ADD PRIMARY KEY (`article_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `oc_article_image`
--
ALTER TABLE `oc_article_image`
  ADD PRIMARY KEY (`article_image_id`);

--
-- Индексы таблицы `oc_article_related`
--
ALTER TABLE `oc_article_related`
  ADD PRIMARY KEY (`article_id`,`related_id`);

--
-- Индексы таблицы `oc_article_related_mn`
--
ALTER TABLE `oc_article_related_mn`
  ADD PRIMARY KEY (`article_id`,`manufacturer_id`);

--
-- Индексы таблицы `oc_article_related_product`
--
ALTER TABLE `oc_article_related_product`
  ADD PRIMARY KEY (`article_id`,`product_id`);

--
-- Индексы таблицы `oc_article_related_wb`
--
ALTER TABLE `oc_article_related_wb`
  ADD PRIMARY KEY (`article_id`,`category_id`);

--
-- Индексы таблицы `oc_article_to_blog_category`
--
ALTER TABLE `oc_article_to_blog_category`
  ADD PRIMARY KEY (`article_id`,`blog_category_id`);

--
-- Индексы таблицы `oc_article_to_download`
--
ALTER TABLE `oc_article_to_download`
  ADD PRIMARY KEY (`article_id`,`download_id`);

--
-- Индексы таблицы `oc_article_to_layout`
--
ALTER TABLE `oc_article_to_layout`
  ADD PRIMARY KEY (`article_id`,`store_id`);

--
-- Индексы таблицы `oc_article_to_store`
--
ALTER TABLE `oc_article_to_store`
  ADD PRIMARY KEY (`article_id`,`store_id`);

--
-- Индексы таблицы `oc_attribute`
--
ALTER TABLE `oc_attribute`
  ADD PRIMARY KEY (`attribute_id`);

--
-- Индексы таблицы `oc_attribute_description`
--
ALTER TABLE `oc_attribute_description`
  ADD PRIMARY KEY (`attribute_id`,`language_id`);

--
-- Индексы таблицы `oc_attribute_group`
--
ALTER TABLE `oc_attribute_group`
  ADD PRIMARY KEY (`attribute_group_id`);

--
-- Индексы таблицы `oc_attribute_group_description`
--
ALTER TABLE `oc_attribute_group_description`
  ADD PRIMARY KEY (`attribute_group_id`,`language_id`);

--
-- Индексы таблицы `oc_banner`
--
ALTER TABLE `oc_banner`
  ADD PRIMARY KEY (`banner_id`);

--
-- Индексы таблицы `oc_banner_image`
--
ALTER TABLE `oc_banner_image`
  ADD PRIMARY KEY (`banner_image_id`);

--
-- Индексы таблицы `oc_blog_category`
--
ALTER TABLE `oc_blog_category`
  ADD PRIMARY KEY (`blog_category_id`);

--
-- Индексы таблицы `oc_blog_category_description`
--
ALTER TABLE `oc_blog_category_description`
  ADD PRIMARY KEY (`blog_category_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `oc_blog_category_path`
--
ALTER TABLE `oc_blog_category_path`
  ADD PRIMARY KEY (`blog_category_id`,`path_id`);

--
-- Индексы таблицы `oc_blog_category_to_layout`
--
ALTER TABLE `oc_blog_category_to_layout`
  ADD PRIMARY KEY (`blog_category_id`,`store_id`);

--
-- Индексы таблицы `oc_blog_category_to_store`
--
ALTER TABLE `oc_blog_category_to_store`
  ADD PRIMARY KEY (`blog_category_id`,`store_id`);

--
-- Индексы таблицы `oc_cart`
--
ALTER TABLE `oc_cart`
  ADD PRIMARY KEY (`cart_id`),
  ADD KEY `cart_id` (`api_id`,`customer_id`,`session_id`,`product_id`,`recurring_id`);

--
-- Индексы таблицы `oc_category`
--
ALTER TABLE `oc_category`
  ADD PRIMARY KEY (`category_id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `status` (`status`);

--
-- Индексы таблицы `oc_category_description`
--
ALTER TABLE `oc_category_description`
  ADD PRIMARY KEY (`category_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `oc_category_filter`
--
ALTER TABLE `oc_category_filter`
  ADD PRIMARY KEY (`category_id`,`filter_id`);

--
-- Индексы таблицы `oc_category_path`
--
ALTER TABLE `oc_category_path`
  ADD PRIMARY KEY (`category_id`,`path_id`),
  ADD KEY `path_id` (`path_id`);

--
-- Индексы таблицы `oc_category_to_layout`
--
ALTER TABLE `oc_category_to_layout`
  ADD PRIMARY KEY (`category_id`,`store_id`);

--
-- Индексы таблицы `oc_category_to_store`
--
ALTER TABLE `oc_category_to_store`
  ADD PRIMARY KEY (`category_id`,`store_id`);

--
-- Индексы таблицы `oc_cdek_city`
--
ALTER TABLE `oc_cdek_city`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `oc_cdek_dispatch`
--
ALTER TABLE `oc_cdek_dispatch`
  ADD PRIMARY KEY (`dispatch_id`);

--
-- Индексы таблицы `oc_cdek_order`
--
ALTER TABLE `oc_cdek_order`
  ADD PRIMARY KEY (`order_id`);

--
-- Индексы таблицы `oc_cdek_order_add_service`
--
ALTER TABLE `oc_cdek_order_add_service`
  ADD PRIMARY KEY (`service_id`,`order_id`);

--
-- Индексы таблицы `oc_cdek_order_call`
--
ALTER TABLE `oc_cdek_order_call`
  ADD PRIMARY KEY (`call_id`);

--
-- Индексы таблицы `oc_cdek_order_call_history_delay`
--
ALTER TABLE `oc_cdek_order_call_history_delay`
  ADD KEY `order_id` (`order_id`);

--
-- Индексы таблицы `oc_cdek_order_call_history_fail`
--
ALTER TABLE `oc_cdek_order_call_history_fail`
  ADD KEY `order_id` (`order_id`);

--
-- Индексы таблицы `oc_cdek_order_call_history_good`
--
ALTER TABLE `oc_cdek_order_call_history_good`
  ADD KEY `order_id` (`order_id`);

--
-- Индексы таблицы `oc_cdek_order_courier`
--
ALTER TABLE `oc_cdek_order_courier`
  ADD PRIMARY KEY (`courier_id`);

--
-- Индексы таблицы `oc_cdek_order_delay_history`
--
ALTER TABLE `oc_cdek_order_delay_history`
  ADD KEY `order_id` (`order_id`,`delay_id`);

--
-- Индексы таблицы `oc_cdek_order_package`
--
ALTER TABLE `oc_cdek_order_package`
  ADD PRIMARY KEY (`package_id`);

--
-- Индексы таблицы `oc_cdek_order_package_item`
--
ALTER TABLE `oc_cdek_order_package_item`
  ADD PRIMARY KEY (`package_item_id`);

--
-- Индексы таблицы `oc_cdek_order_reason`
--
ALTER TABLE `oc_cdek_order_reason`
  ADD PRIMARY KEY (`reason_id`,`order_id`);

--
-- Индексы таблицы `oc_cdek_order_schedule`
--
ALTER TABLE `oc_cdek_order_schedule`
  ADD PRIMARY KEY (`attempt_id`);

--
-- Индексы таблицы `oc_cdek_order_schedule_delay`
--
ALTER TABLE `oc_cdek_order_schedule_delay`
  ADD KEY `order_id` (`order_id`,`attempt_id`);

--
-- Индексы таблицы `oc_cdek_order_status_history`
--
ALTER TABLE `oc_cdek_order_status_history`
  ADD KEY `order_id` (`order_id`,`status_id`);

--
-- Индексы таблицы `oc_city`
--
ALTER TABLE `oc_city`
  ADD PRIMARY KEY (`city_id`);

--
-- Индексы таблицы `oc_country`
--
ALTER TABLE `oc_country`
  ADD PRIMARY KEY (`country_id`);

--
-- Индексы таблицы `oc_coupon`
--
ALTER TABLE `oc_coupon`
  ADD PRIMARY KEY (`coupon_id`);

--
-- Индексы таблицы `oc_coupon_category`
--
ALTER TABLE `oc_coupon_category`
  ADD PRIMARY KEY (`coupon_id`,`category_id`);

--
-- Индексы таблицы `oc_coupon_history`
--
ALTER TABLE `oc_coupon_history`
  ADD PRIMARY KEY (`coupon_history_id`);

--
-- Индексы таблицы `oc_coupon_product`
--
ALTER TABLE `oc_coupon_product`
  ADD PRIMARY KEY (`coupon_product_id`);

--
-- Индексы таблицы `oc_currency`
--
ALTER TABLE `oc_currency`
  ADD PRIMARY KEY (`currency_id`);

--
-- Индексы таблицы `oc_customer`
--
ALTER TABLE `oc_customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Индексы таблицы `oc_customer_activity`
--
ALTER TABLE `oc_customer_activity`
  ADD PRIMARY KEY (`customer_activity_id`);

--
-- Индексы таблицы `oc_customer_affiliate`
--
ALTER TABLE `oc_customer_affiliate`
  ADD PRIMARY KEY (`customer_id`);

--
-- Индексы таблицы `oc_customer_approval`
--
ALTER TABLE `oc_customer_approval`
  ADD PRIMARY KEY (`customer_approval_id`);

--
-- Индексы таблицы `oc_customer_group`
--
ALTER TABLE `oc_customer_group`
  ADD PRIMARY KEY (`customer_group_id`);

--
-- Индексы таблицы `oc_customer_group_description`
--
ALTER TABLE `oc_customer_group_description`
  ADD PRIMARY KEY (`customer_group_id`,`language_id`);

--
-- Индексы таблицы `oc_customer_history`
--
ALTER TABLE `oc_customer_history`
  ADD PRIMARY KEY (`customer_history_id`);

--
-- Индексы таблицы `oc_customer_ip`
--
ALTER TABLE `oc_customer_ip`
  ADD PRIMARY KEY (`customer_ip_id`),
  ADD KEY `ip` (`ip`);

--
-- Индексы таблицы `oc_customer_login`
--
ALTER TABLE `oc_customer_login`
  ADD PRIMARY KEY (`customer_login_id`),
  ADD KEY `email` (`email`),
  ADD KEY `ip` (`ip`);

--
-- Индексы таблицы `oc_customer_online`
--
ALTER TABLE `oc_customer_online`
  ADD PRIMARY KEY (`ip`);

--
-- Индексы таблицы `oc_customer_reward`
--
ALTER TABLE `oc_customer_reward`
  ADD PRIMARY KEY (`customer_reward_id`);

--
-- Индексы таблицы `oc_customer_search`
--
ALTER TABLE `oc_customer_search`
  ADD PRIMARY KEY (`customer_search_id`);

--
-- Индексы таблицы `oc_customer_transaction`
--
ALTER TABLE `oc_customer_transaction`
  ADD PRIMARY KEY (`customer_transaction_id`);

--
-- Индексы таблицы `oc_customer_wishlist`
--
ALTER TABLE `oc_customer_wishlist`
  ADD PRIMARY KEY (`customer_id`,`product_id`);

--
-- Индексы таблицы `oc_custom_field`
--
ALTER TABLE `oc_custom_field`
  ADD PRIMARY KEY (`custom_field_id`);

--
-- Индексы таблицы `oc_custom_field_customer_group`
--
ALTER TABLE `oc_custom_field_customer_group`
  ADD PRIMARY KEY (`custom_field_id`,`customer_group_id`);

--
-- Индексы таблицы `oc_custom_field_description`
--
ALTER TABLE `oc_custom_field_description`
  ADD PRIMARY KEY (`custom_field_id`,`language_id`);

--
-- Индексы таблицы `oc_custom_field_value`
--
ALTER TABLE `oc_custom_field_value`
  ADD PRIMARY KEY (`custom_field_value_id`);

--
-- Индексы таблицы `oc_custom_field_value_description`
--
ALTER TABLE `oc_custom_field_value_description`
  ADD PRIMARY KEY (`custom_field_value_id`,`language_id`);

--
-- Индексы таблицы `oc_download`
--
ALTER TABLE `oc_download`
  ADD PRIMARY KEY (`download_id`);

--
-- Индексы таблицы `oc_download_description`
--
ALTER TABLE `oc_download_description`
  ADD PRIMARY KEY (`download_id`,`language_id`);

--
-- Индексы таблицы `oc_email_template`
--
ALTER TABLE `oc_email_template`
  ADD PRIMARY KEY (`email_template_id`);

--
-- Индексы таблицы `oc_email_template_description`
--
ALTER TABLE `oc_email_template_description`
  ADD PRIMARY KEY (`email_template_id`,`language_id`);

--
-- Индексы таблицы `oc_event`
--
ALTER TABLE `oc_event`
  ADD PRIMARY KEY (`event_id`);

--
-- Индексы таблицы `oc_extension`
--
ALTER TABLE `oc_extension`
  ADD PRIMARY KEY (`extension_id`);

--
-- Индексы таблицы `oc_extension_install`
--
ALTER TABLE `oc_extension_install`
  ADD PRIMARY KEY (`extension_install_id`);

--
-- Индексы таблицы `oc_extension_path`
--
ALTER TABLE `oc_extension_path`
  ADD PRIMARY KEY (`extension_path_id`);

--
-- Индексы таблицы `oc_filter`
--
ALTER TABLE `oc_filter`
  ADD PRIMARY KEY (`filter_id`),
  ADD KEY `filter_group_id` (`filter_group_id`);

--
-- Индексы таблицы `oc_filter_description`
--
ALTER TABLE `oc_filter_description`
  ADD PRIMARY KEY (`filter_id`,`language_id`),
  ADD KEY `filter_group_id` (`filter_group_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `oc_filter_group`
--
ALTER TABLE `oc_filter_group`
  ADD PRIMARY KEY (`filter_group_id`);

--
-- Индексы таблицы `oc_filter_group_description`
--
ALTER TABLE `oc_filter_group_description`
  ADD PRIMARY KEY (`filter_group_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `oc_geo_zone`
--
ALTER TABLE `oc_geo_zone`
  ADD PRIMARY KEY (`geo_zone_id`);

--
-- Индексы таблицы `oc_information`
--
ALTER TABLE `oc_information`
  ADD PRIMARY KEY (`information_id`);

--
-- Индексы таблицы `oc_information_description`
--
ALTER TABLE `oc_information_description`
  ADD PRIMARY KEY (`information_id`,`language_id`);

--
-- Индексы таблицы `oc_information_to_layout`
--
ALTER TABLE `oc_information_to_layout`
  ADD PRIMARY KEY (`information_id`,`store_id`);

--
-- Индексы таблицы `oc_information_to_store`
--
ALTER TABLE `oc_information_to_store`
  ADD PRIMARY KEY (`information_id`,`store_id`);

--
-- Индексы таблицы `oc_language`
--
ALTER TABLE `oc_language`
  ADD PRIMARY KEY (`language_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `oc_layout`
--
ALTER TABLE `oc_layout`
  ADD PRIMARY KEY (`layout_id`);

--
-- Индексы таблицы `oc_layout_module`
--
ALTER TABLE `oc_layout_module`
  ADD PRIMARY KEY (`layout_module_id`);

--
-- Индексы таблицы `oc_layout_route`
--
ALTER TABLE `oc_layout_route`
  ADD PRIMARY KEY (`layout_route_id`);

--
-- Индексы таблицы `oc_length_class`
--
ALTER TABLE `oc_length_class`
  ADD PRIMARY KEY (`length_class_id`);

--
-- Индексы таблицы `oc_length_class_description`
--
ALTER TABLE `oc_length_class_description`
  ADD PRIMARY KEY (`length_class_id`,`language_id`);

--
-- Индексы таблицы `oc_location`
--
ALTER TABLE `oc_location`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `oc_manufacturer`
--
ALTER TABLE `oc_manufacturer`
  ADD PRIMARY KEY (`manufacturer_id`);

--
-- Индексы таблицы `oc_manufacturer_to_layout`
--
ALTER TABLE `oc_manufacturer_to_layout`
  ADD PRIMARY KEY (`manufacturer_id`,`store_id`);

--
-- Индексы таблицы `oc_manufacturer_to_store`
--
ALTER TABLE `oc_manufacturer_to_store`
  ADD PRIMARY KEY (`manufacturer_id`,`store_id`);

--
-- Индексы таблицы `oc_marketing`
--
ALTER TABLE `oc_marketing`
  ADD PRIMARY KEY (`marketing_id`);

--
-- Индексы таблицы `oc_modification`
--
ALTER TABLE `oc_modification`
  ADD PRIMARY KEY (`modification_id`);

--
-- Индексы таблицы `oc_module`
--
ALTER TABLE `oc_module`
  ADD PRIMARY KEY (`module_id`);

--
-- Индексы таблицы `oc_ocfilter_option`
--
ALTER TABLE `oc_ocfilter_option`
  ADD PRIMARY KEY (`option_id`),
  ADD KEY `keyword` (`keyword`),
  ADD KEY `sort_order` (`sort_order`);

--
-- Индексы таблицы `oc_ocfilter_option_description`
--
ALTER TABLE `oc_ocfilter_option_description`
  ADD PRIMARY KEY (`option_id`,`language_id`);

--
-- Индексы таблицы `oc_ocfilter_option_to_category`
--
ALTER TABLE `oc_ocfilter_option_to_category`
  ADD PRIMARY KEY (`category_id`,`option_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Индексы таблицы `oc_ocfilter_option_to_store`
--
ALTER TABLE `oc_ocfilter_option_to_store`
  ADD PRIMARY KEY (`store_id`,`option_id`);

--
-- Индексы таблицы `oc_ocfilter_option_value`
--
ALTER TABLE `oc_ocfilter_option_value`
  ADD PRIMARY KEY (`value_id`),
  ADD KEY `option_id` (`option_id`),
  ADD KEY `keyword` (`keyword`);

--
-- Индексы таблицы `oc_ocfilter_option_value_description`
--
ALTER TABLE `oc_ocfilter_option_value_description`
  ADD PRIMARY KEY (`value_id`,`language_id`),
  ADD KEY `option_id` (`option_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `oc_ocfilter_option_value_to_product`
--
ALTER TABLE `oc_ocfilter_option_value_to_product`
  ADD PRIMARY KEY (`ocfilter_option_value_to_product_id`),
  ADD UNIQUE KEY `option_id_value_id_product_id` (`option_id`,`value_id`,`product_id`),
  ADD KEY `slide_value_min_slide_value_max` (`slide_value_min`,`slide_value_max`),
  ADD KEY `product_id` (`product_id`);

--
-- Индексы таблицы `oc_ocfilter_option_value_to_product_description`
--
ALTER TABLE `oc_ocfilter_option_value_to_product_description`
  ADD PRIMARY KEY (`product_id`,`value_id`,`option_id`,`language_id`);

--
-- Индексы таблицы `oc_ocfilter_page`
--
ALTER TABLE `oc_ocfilter_page`
  ADD PRIMARY KEY (`ocfilter_page_id`),
  ADD KEY `keyword` (`keyword`),
  ADD KEY `category_id_params` (`category_id`,`params`);

--
-- Индексы таблицы `oc_ocfilter_page_description`
--
ALTER TABLE `oc_ocfilter_page_description`
  ADD PRIMARY KEY (`ocfilter_page_id`,`language_id`);

--
-- Индексы таблицы `oc_option`
--
ALTER TABLE `oc_option`
  ADD PRIMARY KEY (`option_id`);

--
-- Индексы таблицы `oc_option_characteristic`
--
ALTER TABLE `oc_option_characteristic`
  ADD PRIMARY KEY (`characteristic_id`);

--
-- Индексы таблицы `oc_option_characteristic_description`
--
ALTER TABLE `oc_option_characteristic_description`
  ADD PRIMARY KEY (`characteristic_id`,`language_id`);

--
-- Индексы таблицы `oc_option_description`
--
ALTER TABLE `oc_option_description`
  ADD PRIMARY KEY (`option_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `oc_option_value`
--
ALTER TABLE `oc_option_value`
  ADD PRIMARY KEY (`option_value_id`),
  ADD KEY `option_id` (`option_id`);

--
-- Индексы таблицы `oc_option_value_characteristic`
--
ALTER TABLE `oc_option_value_characteristic`
  ADD PRIMARY KEY (`option_value_characteristic_id`);

--
-- Индексы таблицы `oc_option_value_description`
--
ALTER TABLE `oc_option_value_description`
  ADD PRIMARY KEY (`option_value_id`,`language_id`),
  ADD KEY `option_id` (`option_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `oc_order`
--
ALTER TABLE `oc_order`
  ADD PRIMARY KEY (`order_id`);

--
-- Индексы таблицы `oc_order_history`
--
ALTER TABLE `oc_order_history`
  ADD PRIMARY KEY (`order_history_id`);

--
-- Индексы таблицы `oc_order_option`
--
ALTER TABLE `oc_order_option`
  ADD PRIMARY KEY (`order_option_id`);

--
-- Индексы таблицы `oc_order_product`
--
ALTER TABLE `oc_order_product`
  ADD PRIMARY KEY (`order_product_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Индексы таблицы `oc_order_recurring`
--
ALTER TABLE `oc_order_recurring`
  ADD PRIMARY KEY (`order_recurring_id`);

--
-- Индексы таблицы `oc_order_recurring_transaction`
--
ALTER TABLE `oc_order_recurring_transaction`
  ADD PRIMARY KEY (`order_recurring_transaction_id`);

--
-- Индексы таблицы `oc_order_shipment`
--
ALTER TABLE `oc_order_shipment`
  ADD PRIMARY KEY (`order_shipment_id`);

--
-- Индексы таблицы `oc_order_status`
--
ALTER TABLE `oc_order_status`
  ADD PRIMARY KEY (`order_status_id`,`language_id`);

--
-- Индексы таблицы `oc_order_total`
--
ALTER TABLE `oc_order_total`
  ADD PRIMARY KEY (`order_total_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Индексы таблицы `oc_order_to_sdek`
--
ALTER TABLE `oc_order_to_sdek`
  ADD PRIMARY KEY (`order_to_sdek_id`),
  ADD UNIQUE KEY `order_id` (`order_id`);

--
-- Индексы таблицы `oc_order_voucher`
--
ALTER TABLE `oc_order_voucher`
  ADD PRIMARY KEY (`order_voucher_id`);

--
-- Индексы таблицы `oc_product`
--
ALTER TABLE `oc_product`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `model` (`model`),
  ADD KEY `sku` (`sku`),
  ADD KEY `upc` (`upc`),
  ADD KEY `ean` (`ean`),
  ADD KEY `jan` (`jan`),
  ADD KEY `isbn` (`isbn`),
  ADD KEY `mpn` (`mpn`),
  ADD KEY `stock_status_id` (`stock_status_id`),
  ADD KEY `manufacturer_id` (`manufacturer_id`),
  ADD KEY `price` (`price`),
  ADD KEY `quantity` (`quantity`),
  ADD KEY `shipping` (`shipping`),
  ADD KEY `date_available` (`date_available`),
  ADD KEY `status` (`status`);

--
-- Индексы таблицы `oc_product_attribute`
--
ALTER TABLE `oc_product_attribute`
  ADD PRIMARY KEY (`product_id`,`attribute_id`,`language_id`),
  ADD KEY `text` (`text`(10));

--
-- Индексы таблицы `oc_product_city`
--
ALTER TABLE `oc_product_city`
  ADD PRIMARY KEY (`product_id`,`product_option_value_id`,`city_id`),
  ADD KEY `city_id` (`city_id`);

--
-- Индексы таблицы `oc_product_description`
--
ALTER TABLE `oc_product_description`
  ADD PRIMARY KEY (`product_id`,`language_id`),
  ADD KEY `name` (`name`),
  ADD KEY `tag` (`tag`(10));

--
-- Индексы таблицы `oc_product_description_composition`
--
ALTER TABLE `oc_product_description_composition`
  ADD PRIMARY KEY (`product_id`,`language_id`);

--
-- Индексы таблицы `oc_product_discount`
--
ALTER TABLE `oc_product_discount`
  ADD PRIMARY KEY (`product_discount_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Индексы таблицы `oc_product_filter`
--
ALTER TABLE `oc_product_filter`
  ADD PRIMARY KEY (`product_id`,`filter_id`);

--
-- Индексы таблицы `oc_product_image`
--
ALTER TABLE `oc_product_image`
  ADD PRIMARY KEY (`product_image_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Индексы таблицы `oc_product_option`
--
ALTER TABLE `oc_product_option`
  ADD PRIMARY KEY (`product_option_id`),
  ADD KEY `option_id` (`option_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `value` (`value`(10));

--
-- Индексы таблицы `oc_product_option_value`
--
ALTER TABLE `oc_product_option_value`
  ADD PRIMARY KEY (`product_option_value_id`),
  ADD KEY `option_id` (`option_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `product_option_id` (`product_option_id`),
  ADD KEY `option_value_id` (`option_value_id`),
  ADD KEY `price` (`price`),
  ADD KEY `quantity` (`quantity`);

--
-- Индексы таблицы `oc_product_recurring`
--
ALTER TABLE `oc_product_recurring`
  ADD PRIMARY KEY (`product_id`,`recurring_id`,`customer_group_id`);

--
-- Индексы таблицы `oc_product_related`
--
ALTER TABLE `oc_product_related`
  ADD PRIMARY KEY (`product_id`,`related_id`);

--
-- Индексы таблицы `oc_product_related_article`
--
ALTER TABLE `oc_product_related_article`
  ADD PRIMARY KEY (`article_id`,`product_id`);

--
-- Индексы таблицы `oc_product_reward`
--
ALTER TABLE `oc_product_reward`
  ADD PRIMARY KEY (`product_reward_id`);

--
-- Индексы таблицы `oc_product_special`
--
ALTER TABLE `oc_product_special`
  ADD PRIMARY KEY (`product_special_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Индексы таблицы `oc_product_to_category`
--
ALTER TABLE `oc_product_to_category`
  ADD PRIMARY KEY (`product_id`,`category_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Индексы таблицы `oc_product_to_download`
--
ALTER TABLE `oc_product_to_download`
  ADD PRIMARY KEY (`product_id`,`download_id`);

--
-- Индексы таблицы `oc_product_to_layout`
--
ALTER TABLE `oc_product_to_layout`
  ADD PRIMARY KEY (`product_id`,`store_id`);

--
-- Индексы таблицы `oc_product_to_store`
--
ALTER TABLE `oc_product_to_store`
  ADD PRIMARY KEY (`product_id`,`store_id`);

--
-- Индексы таблицы `oc_recurring`
--
ALTER TABLE `oc_recurring`
  ADD PRIMARY KEY (`recurring_id`);

--
-- Индексы таблицы `oc_recurring_description`
--
ALTER TABLE `oc_recurring_description`
  ADD PRIMARY KEY (`recurring_id`,`language_id`);

--
-- Индексы таблицы `oc_return`
--
ALTER TABLE `oc_return`
  ADD PRIMARY KEY (`return_id`);

--
-- Индексы таблицы `oc_return_action`
--
ALTER TABLE `oc_return_action`
  ADD PRIMARY KEY (`return_action_id`,`language_id`);

--
-- Индексы таблицы `oc_return_history`
--
ALTER TABLE `oc_return_history`
  ADD PRIMARY KEY (`return_history_id`);

--
-- Индексы таблицы `oc_return_reason`
--
ALTER TABLE `oc_return_reason`
  ADD PRIMARY KEY (`return_reason_id`,`language_id`);

--
-- Индексы таблицы `oc_return_status`
--
ALTER TABLE `oc_return_status`
  ADD PRIMARY KEY (`return_status_id`,`language_id`);

--
-- Индексы таблицы `oc_review`
--
ALTER TABLE `oc_review`
  ADD PRIMARY KEY (`review_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Индексы таблицы `oc_review_article`
--
ALTER TABLE `oc_review_article`
  ADD PRIMARY KEY (`review_article_id`),
  ADD KEY `article_id` (`article_id`);

--
-- Индексы таблицы `oc_seo_url`
--
ALTER TABLE `oc_seo_url`
  ADD PRIMARY KEY (`seo_url_id`),
  ADD KEY `query` (`query`),
  ADD KEY `keyword` (`keyword`);

--
-- Индексы таблицы `oc_session`
--
ALTER TABLE `oc_session`
  ADD PRIMARY KEY (`session_id`);

--
-- Индексы таблицы `oc_setting`
--
ALTER TABLE `oc_setting`
  ADD PRIMARY KEY (`setting_id`);

--
-- Индексы таблицы `oc_shipping_courier`
--
ALTER TABLE `oc_shipping_courier`
  ADD PRIMARY KEY (`shipping_courier_id`);

--
-- Индексы таблицы `oc_statistics`
--
ALTER TABLE `oc_statistics`
  ADD PRIMARY KEY (`statistics_id`);

--
-- Индексы таблицы `oc_stocks`
--
ALTER TABLE `oc_stocks`
  ADD PRIMARY KEY (`stocks_id`);

--
-- Индексы таблицы `oc_stocks_description`
--
ALTER TABLE `oc_stocks_description`
  ADD PRIMARY KEY (`stocks_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `oc_stocks_product`
--
ALTER TABLE `oc_stocks_product`
  ADD PRIMARY KEY (`stocks_id`,`product_id`);

--
-- Индексы таблицы `oc_stock_status`
--
ALTER TABLE `oc_stock_status`
  ADD PRIMARY KEY (`stock_status_id`,`language_id`);

--
-- Индексы таблицы `oc_store`
--
ALTER TABLE `oc_store`
  ADD PRIMARY KEY (`store_id`);

--
-- Индексы таблицы `oc_tax_class`
--
ALTER TABLE `oc_tax_class`
  ADD PRIMARY KEY (`tax_class_id`);

--
-- Индексы таблицы `oc_tax_rate`
--
ALTER TABLE `oc_tax_rate`
  ADD PRIMARY KEY (`tax_rate_id`);

--
-- Индексы таблицы `oc_tax_rate_to_customer_group`
--
ALTER TABLE `oc_tax_rate_to_customer_group`
  ADD PRIMARY KEY (`tax_rate_id`,`customer_group_id`);

--
-- Индексы таблицы `oc_tax_rule`
--
ALTER TABLE `oc_tax_rule`
  ADD PRIMARY KEY (`tax_rule_id`);

--
-- Индексы таблицы `oc_theme`
--
ALTER TABLE `oc_theme`
  ADD PRIMARY KEY (`theme_id`);

--
-- Индексы таблицы `oc_trade_checks`
--
ALTER TABLE `oc_trade_checks`
  ADD PRIMARY KEY (`check_id`);

--
-- Индексы таблицы `oc_trade_images`
--
ALTER TABLE `oc_trade_images`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `oc_trade_import`
--
ALTER TABLE `oc_trade_import`
  ADD PRIMARY KEY (`operation_id`);

--
-- Индексы таблицы `oc_trade_import_category_codes`
--
ALTER TABLE `oc_trade_import_category_codes`
  ADD PRIMARY KEY (`group_uuid`),
  ADD KEY `category_id` (`category_id`);

--
-- Индексы таблицы `oc_trade_import_filter_group_codes`
--
ALTER TABLE `oc_trade_import_filter_group_codes`
  ADD PRIMARY KEY (`filter_uuid`),
  ADD KEY `filter_group_id` (`filter_group_id`);

--
-- Индексы таблицы `oc_trade_import_option_characteristic_codes`
--
ALTER TABLE `oc_trade_import_option_characteristic_codes`
  ADD PRIMARY KEY (`property_uuid`),
  ADD KEY `characteristic_id` (`characteristic_id`);

--
-- Индексы таблицы `oc_trade_import_option_codes`
--
ALTER TABLE `oc_trade_import_option_codes`
  ADD PRIMARY KEY (`nomenclature_uuid`),
  ADD KEY `option_id` (`option_id`);

--
-- Индексы таблицы `oc_trade_import_option_value_codes`
--
ALTER TABLE `oc_trade_import_option_value_codes`
  ADD PRIMARY KEY (`characteristic_uuid`),
  ADD KEY `option_value_id` (`option_value_id`);

--
-- Индексы таблицы `oc_trade_import_product_codes`
--
ALTER TABLE `oc_trade_import_product_codes`
  ADD PRIMARY KEY (`nomenclature_uuid`),
  ADD KEY `product_id` (`product_id`);

--
-- Индексы таблицы `oc_trade_import_services`
--
ALTER TABLE `oc_trade_import_services`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `oc_trade_import_stocks_banner`
--
ALTER TABLE `oc_trade_import_stocks_banner`
  ADD PRIMARY KEY (`banner_image_id`),
  ADD KEY `stocks_id` (`stocks_id`);

--
-- Индексы таблицы `oc_trade_import_stocks_codes`
--
ALTER TABLE `oc_trade_import_stocks_codes`
  ADD PRIMARY KEY (`stocks_uuid`),
  ADD KEY `stocks_id` (`stocks_id`);

--
-- Индексы таблицы `oc_trade_import_warehouse_codes`
--
ALTER TABLE `oc_trade_import_warehouse_codes`
  ADD PRIMARY KEY (`storage_uuid`),
  ADD KEY `warehouse_id` (`warehouse_id`);

--
-- Индексы таблицы `oc_trade_orders`
--
ALTER TABLE `oc_trade_orders`
  ADD PRIMARY KEY (`operation_id`);

--
-- Индексы таблицы `oc_translation`
--
ALTER TABLE `oc_translation`
  ADD PRIMARY KEY (`translation_id`);

--
-- Индексы таблицы `oc_unixml_additional_params`
--
ALTER TABLE `oc_unixml_additional_params`
  ADD PRIMARY KEY (`item_id`);

--
-- Индексы таблицы `oc_unixml_attributes`
--
ALTER TABLE `oc_unixml_attributes`
  ADD PRIMARY KEY (`item_id`);

--
-- Индексы таблицы `oc_unixml_category_match`
--
ALTER TABLE `oc_unixml_category_match`
  ADD PRIMARY KEY (`item_id`);

--
-- Индексы таблицы `oc_unixml_markup`
--
ALTER TABLE `oc_unixml_markup`
  ADD PRIMARY KEY (`item_id`);

--
-- Индексы таблицы `oc_unixml_replace_name`
--
ALTER TABLE `oc_unixml_replace_name`
  ADD PRIMARY KEY (`item_id`);

--
-- Индексы таблицы `oc_upload`
--
ALTER TABLE `oc_upload`
  ADD PRIMARY KEY (`upload_id`);

--
-- Индексы таблицы `oc_user`
--
ALTER TABLE `oc_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Индексы таблицы `oc_user_group`
--
ALTER TABLE `oc_user_group`
  ADD PRIMARY KEY (`user_group_id`);

--
-- Индексы таблицы `oc_voucher`
--
ALTER TABLE `oc_voucher`
  ADD PRIMARY KEY (`voucher_id`);

--
-- Индексы таблицы `oc_voucher_history`
--
ALTER TABLE `oc_voucher_history`
  ADD PRIMARY KEY (`voucher_history_id`);

--
-- Индексы таблицы `oc_voucher_theme`
--
ALTER TABLE `oc_voucher_theme`
  ADD PRIMARY KEY (`voucher_theme_id`);

--
-- Индексы таблицы `oc_voucher_theme_description`
--
ALTER TABLE `oc_voucher_theme_description`
  ADD PRIMARY KEY (`voucher_theme_id`,`language_id`);

--
-- Индексы таблицы `oc_warehouse`
--
ALTER TABLE `oc_warehouse`
  ADD PRIMARY KEY (`warehouse_id`);

--
-- Индексы таблицы `oc_warehouse_description`
--
ALTER TABLE `oc_warehouse_description`
  ADD PRIMARY KEY (`warehouse_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `oc_warehouse_product`
--
ALTER TABLE `oc_warehouse_product`
  ADD PRIMARY KEY (`warehouse_product_id`),
  ADD UNIQUE KEY `unique_warehouse_product` (`warehouse_id`,`product_id`,`option_value_id`);

--
-- Индексы таблицы `oc_weight_class`
--
ALTER TABLE `oc_weight_class`
  ADD PRIMARY KEY (`weight_class_id`);

--
-- Индексы таблицы `oc_weight_class_description`
--
ALTER TABLE `oc_weight_class_description`
  ADD PRIMARY KEY (`weight_class_id`,`language_id`);

--
-- Индексы таблицы `oc_xshippingpro`
--
ALTER TABLE `oc_xshippingpro`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `oc_zone`
--
ALTER TABLE `oc_zone`
  ADD PRIMARY KEY (`zone_id`);

--
-- Индексы таблицы `oc_zone_to_geo_zone`
--
ALTER TABLE `oc_zone_to_geo_zone`
  ADD PRIMARY KEY (`zone_to_geo_zone_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `oc_address`
--
ALTER TABLE `oc_address`
  MODIFY `address_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `oc_api`
--
ALTER TABLE `oc_api`
  MODIFY `api_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `oc_api_ip`
--
ALTER TABLE `oc_api_ip`
  MODIFY `api_ip_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `oc_api_session`
--
ALTER TABLE `oc_api_session`
  MODIFY `api_session_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=152;

--
-- AUTO_INCREMENT для таблицы `oc_article`
--
ALTER TABLE `oc_article`
  MODIFY `article_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=126;

--
-- AUTO_INCREMENT для таблицы `oc_article_image`
--
ALTER TABLE `oc_article_image`
  MODIFY `article_image_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_attribute`
--
ALTER TABLE `oc_attribute`
  MODIFY `attribute_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_attribute_group`
--
ALTER TABLE `oc_attribute_group`
  MODIFY `attribute_group_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_banner`
--
ALTER TABLE `oc_banner`
  MODIFY `banner_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `oc_banner_image`
--
ALTER TABLE `oc_banner_image`
  MODIFY `banner_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=127;

--
-- AUTO_INCREMENT для таблицы `oc_blog_category`
--
ALTER TABLE `oc_blog_category`
  MODIFY `blog_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT для таблицы `oc_cart`
--
ALTER TABLE `oc_cart`
  MODIFY `cart_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=147;

--
-- AUTO_INCREMENT для таблицы `oc_category`
--
ALTER TABLE `oc_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT для таблицы `oc_cdek_dispatch`
--
ALTER TABLE `oc_cdek_dispatch`
  MODIFY `dispatch_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_cdek_order_call`
--
ALTER TABLE `oc_cdek_order_call`
  MODIFY `call_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_cdek_order_courier`
--
ALTER TABLE `oc_cdek_order_courier`
  MODIFY `courier_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_cdek_order_package`
--
ALTER TABLE `oc_cdek_order_package`
  MODIFY `package_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_cdek_order_package_item`
--
ALTER TABLE `oc_cdek_order_package_item`
  MODIFY `package_item_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_cdek_order_reason`
--
ALTER TABLE `oc_cdek_order_reason`
  MODIFY `reason_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_city`
--
ALTER TABLE `oc_city`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_country`
--
ALTER TABLE `oc_country`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=258;

--
-- AUTO_INCREMENT для таблицы `oc_coupon`
--
ALTER TABLE `oc_coupon`
  MODIFY `coupon_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `oc_coupon_history`
--
ALTER TABLE `oc_coupon_history`
  MODIFY `coupon_history_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_coupon_product`
--
ALTER TABLE `oc_coupon_product`
  MODIFY `coupon_product_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_currency`
--
ALTER TABLE `oc_currency`
  MODIFY `currency_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `oc_customer`
--
ALTER TABLE `oc_customer`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT для таблицы `oc_customer_activity`
--
ALTER TABLE `oc_customer_activity`
  MODIFY `customer_activity_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=191;

--
-- AUTO_INCREMENT для таблицы `oc_customer_approval`
--
ALTER TABLE `oc_customer_approval`
  MODIFY `customer_approval_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_customer_group`
--
ALTER TABLE `oc_customer_group`
  MODIFY `customer_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `oc_customer_history`
--
ALTER TABLE `oc_customer_history`
  MODIFY `customer_history_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_customer_ip`
--
ALTER TABLE `oc_customer_ip`
  MODIFY `customer_ip_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT для таблицы `oc_customer_login`
--
ALTER TABLE `oc_customer_login`
  MODIFY `customer_login_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `oc_customer_reward`
--
ALTER TABLE `oc_customer_reward`
  MODIFY `customer_reward_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_customer_search`
--
ALTER TABLE `oc_customer_search`
  MODIFY `customer_search_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_customer_transaction`
--
ALTER TABLE `oc_customer_transaction`
  MODIFY `customer_transaction_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_custom_field`
--
ALTER TABLE `oc_custom_field`
  MODIFY `custom_field_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `oc_custom_field_value`
--
ALTER TABLE `oc_custom_field_value`
  MODIFY `custom_field_value_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_download`
--
ALTER TABLE `oc_download`
  MODIFY `download_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_email_template`
--
ALTER TABLE `oc_email_template`
  MODIFY `email_template_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT для таблицы `oc_event`
--
ALTER TABLE `oc_event`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT для таблицы `oc_extension`
--
ALTER TABLE `oc_extension`
  MODIFY `extension_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT для таблицы `oc_extension_install`
--
ALTER TABLE `oc_extension_install`
  MODIFY `extension_install_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT для таблицы `oc_extension_path`
--
ALTER TABLE `oc_extension_path`
  MODIFY `extension_path_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1151;

--
-- AUTO_INCREMENT для таблицы `oc_filter`
--
ALTER TABLE `oc_filter`
  MODIFY `filter_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_filter_group`
--
ALTER TABLE `oc_filter_group`
  MODIFY `filter_group_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_geo_zone`
--
ALTER TABLE `oc_geo_zone`
  MODIFY `geo_zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `oc_information`
--
ALTER TABLE `oc_information`
  MODIFY `information_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `oc_language`
--
ALTER TABLE `oc_language`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `oc_layout`
--
ALTER TABLE `oc_layout`
  MODIFY `layout_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT для таблицы `oc_layout_module`
--
ALTER TABLE `oc_layout_module`
  MODIFY `layout_module_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT для таблицы `oc_layout_route`
--
ALTER TABLE `oc_layout_route`
  MODIFY `layout_route_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT для таблицы `oc_length_class`
--
ALTER TABLE `oc_length_class`
  MODIFY `length_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `oc_location`
--
ALTER TABLE `oc_location`
  MODIFY `location_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_manufacturer`
--
ALTER TABLE `oc_manufacturer`
  MODIFY `manufacturer_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_marketing`
--
ALTER TABLE `oc_marketing`
  MODIFY `marketing_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_modification`
--
ALTER TABLE `oc_modification`
  MODIFY `modification_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT для таблицы `oc_module`
--
ALTER TABLE `oc_module`
  MODIFY `module_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT для таблицы `oc_ocfilter_option`
--
ALTER TABLE `oc_ocfilter_option`
  MODIFY `option_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_ocfilter_option_value`
--
ALTER TABLE `oc_ocfilter_option_value`
  MODIFY `value_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_ocfilter_option_value_to_product`
--
ALTER TABLE `oc_ocfilter_option_value_to_product`
  MODIFY `ocfilter_option_value_to_product_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_ocfilter_page`
--
ALTER TABLE `oc_ocfilter_page`
  MODIFY `ocfilter_page_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_option`
--
ALTER TABLE `oc_option`
  MODIFY `option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT для таблицы `oc_option_characteristic`
--
ALTER TABLE `oc_option_characteristic`
  MODIFY `characteristic_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_option_value`
--
ALTER TABLE `oc_option_value`
  MODIFY `option_value_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=472;

--
-- AUTO_INCREMENT для таблицы `oc_option_value_characteristic`
--
ALTER TABLE `oc_option_value_characteristic`
  MODIFY `option_value_characteristic_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_order`
--
ALTER TABLE `oc_order`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=164;

--
-- AUTO_INCREMENT для таблицы `oc_order_history`
--
ALTER TABLE `oc_order_history`
  MODIFY `order_history_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=308;

--
-- AUTO_INCREMENT для таблицы `oc_order_option`
--
ALTER TABLE `oc_order_option`
  MODIFY `order_option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `oc_order_product`
--
ALTER TABLE `oc_order_product`
  MODIFY `order_product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=307;

--
-- AUTO_INCREMENT для таблицы `oc_order_recurring`
--
ALTER TABLE `oc_order_recurring`
  MODIFY `order_recurring_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_order_recurring_transaction`
--
ALTER TABLE `oc_order_recurring_transaction`
  MODIFY `order_recurring_transaction_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_order_shipment`
--
ALTER TABLE `oc_order_shipment`
  MODIFY `order_shipment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_order_status`
--
ALTER TABLE `oc_order_status`
  MODIFY `order_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT для таблицы `oc_order_total`
--
ALTER TABLE `oc_order_total`
  MODIFY `order_total_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=645;

--
-- AUTO_INCREMENT для таблицы `oc_order_to_sdek`
--
ALTER TABLE `oc_order_to_sdek`
  MODIFY `order_to_sdek_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_order_voucher`
--
ALTER TABLE `oc_order_voucher`
  MODIFY `order_voucher_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_product`
--
ALTER TABLE `oc_product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT для таблицы `oc_product_discount`
--
ALTER TABLE `oc_product_discount`
  MODIFY `product_discount_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_product_image`
--
ALTER TABLE `oc_product_image`
  MODIFY `product_image_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_product_option`
--
ALTER TABLE `oc_product_option`
  MODIFY `product_option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT для таблицы `oc_product_option_value`
--
ALTER TABLE `oc_product_option_value`
  MODIFY `product_option_value_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=472;

--
-- AUTO_INCREMENT для таблицы `oc_product_reward`
--
ALTER TABLE `oc_product_reward`
  MODIFY `product_reward_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_product_special`
--
ALTER TABLE `oc_product_special`
  MODIFY `product_special_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_recurring`
--
ALTER TABLE `oc_recurring`
  MODIFY `recurring_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_return`
--
ALTER TABLE `oc_return`
  MODIFY `return_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_return_action`
--
ALTER TABLE `oc_return_action`
  MODIFY `return_action_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `oc_return_history`
--
ALTER TABLE `oc_return_history`
  MODIFY `return_history_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_return_reason`
--
ALTER TABLE `oc_return_reason`
  MODIFY `return_reason_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `oc_return_status`
--
ALTER TABLE `oc_return_status`
  MODIFY `return_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `oc_review`
--
ALTER TABLE `oc_review`
  MODIFY `review_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_review_article`
--
ALTER TABLE `oc_review_article`
  MODIFY `review_article_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT для таблицы `oc_seo_url`
--
ALTER TABLE `oc_seo_url`
  MODIFY `seo_url_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT для таблицы `oc_setting`
--
ALTER TABLE `oc_setting`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19404;

--
-- AUTO_INCREMENT для таблицы `oc_statistics`
--
ALTER TABLE `oc_statistics`
  MODIFY `statistics_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `oc_stocks`
--
ALTER TABLE `oc_stocks`
  MODIFY `stocks_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_stock_status`
--
ALTER TABLE `oc_stock_status`
  MODIFY `stock_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `oc_store`
--
ALTER TABLE `oc_store`
  MODIFY `store_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_tax_class`
--
ALTER TABLE `oc_tax_class`
  MODIFY `tax_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `oc_tax_rate`
--
ALTER TABLE `oc_tax_rate`
  MODIFY `tax_rate_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT для таблицы `oc_tax_rule`
--
ALTER TABLE `oc_tax_rule`
  MODIFY `tax_rule_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;

--
-- AUTO_INCREMENT для таблицы `oc_theme`
--
ALTER TABLE `oc_theme`
  MODIFY `theme_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_trade_checks`
--
ALTER TABLE `oc_trade_checks`
  MODIFY `check_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_trade_images`
--
ALTER TABLE `oc_trade_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_trade_import`
--
ALTER TABLE `oc_trade_import`
  MODIFY `operation_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `oc_trade_import_services`
--
ALTER TABLE `oc_trade_import_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_trade_orders`
--
ALTER TABLE `oc_trade_orders`
  MODIFY `operation_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_translation`
--
ALTER TABLE `oc_translation`
  MODIFY `translation_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_unixml_additional_params`
--
ALTER TABLE `oc_unixml_additional_params`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_unixml_attributes`
--
ALTER TABLE `oc_unixml_attributes`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_unixml_category_match`
--
ALTER TABLE `oc_unixml_category_match`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_unixml_markup`
--
ALTER TABLE `oc_unixml_markup`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_unixml_replace_name`
--
ALTER TABLE `oc_unixml_replace_name`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_upload`
--
ALTER TABLE `oc_upload`
  MODIFY `upload_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_user`
--
ALTER TABLE `oc_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `oc_user_group`
--
ALTER TABLE `oc_user_group`
  MODIFY `user_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `oc_voucher`
--
ALTER TABLE `oc_voucher`
  MODIFY `voucher_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_voucher_history`
--
ALTER TABLE `oc_voucher_history`
  MODIFY `voucher_history_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_voucher_theme`
--
ALTER TABLE `oc_voucher_theme`
  MODIFY `voucher_theme_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `oc_warehouse`
--
ALTER TABLE `oc_warehouse`
  MODIFY `warehouse_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_warehouse_product`
--
ALTER TABLE `oc_warehouse_product`
  MODIFY `warehouse_product_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_weight_class`
--
ALTER TABLE `oc_weight_class`
  MODIFY `weight_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `oc_xshippingpro`
--
ALTER TABLE `oc_xshippingpro`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `oc_zone`
--
ALTER TABLE `oc_zone`
  MODIFY `zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4232;

--
-- AUTO_INCREMENT для таблицы `oc_zone_to_geo_zone`
--
ALTER TABLE `oc_zone_to_geo_zone`
  MODIFY `zone_to_geo_zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `oc_product_city`
--
ALTER TABLE `oc_product_city`
  ADD CONSTRAINT `oc_product_city_ibfk_1` FOREIGN KEY (`city_id`) REFERENCES `oc_city` (`city_id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `oc_trade_import_category_codes`
--
ALTER TABLE `oc_trade_import_category_codes`
  ADD CONSTRAINT `oc_trade_import_category_codes_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `oc_category` (`category_id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `oc_trade_import_filter_group_codes`
--
ALTER TABLE `oc_trade_import_filter_group_codes`
  ADD CONSTRAINT `oc_trade_import_filter_group_codes_ibfk_1` FOREIGN KEY (`filter_group_id`) REFERENCES `oc_filter_group` (`filter_group_id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `oc_trade_import_option_characteristic_codes`
--
ALTER TABLE `oc_trade_import_option_characteristic_codes`
  ADD CONSTRAINT `oc_trade_import_option_characteristic_codes_ibfk_1` FOREIGN KEY (`characteristic_id`) REFERENCES `oc_option_characteristic` (`characteristic_id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `oc_trade_import_option_codes`
--
ALTER TABLE `oc_trade_import_option_codes`
  ADD CONSTRAINT `oc_trade_import_option_codes_ibfk_1` FOREIGN KEY (`option_id`) REFERENCES `oc_option` (`option_id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `oc_trade_import_option_value_codes`
--
ALTER TABLE `oc_trade_import_option_value_codes`
  ADD CONSTRAINT `oc_trade_import_option_value_codes_ibfk_1` FOREIGN KEY (`option_value_id`) REFERENCES `oc_option_value` (`option_value_id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `oc_trade_import_product_codes`
--
ALTER TABLE `oc_trade_import_product_codes`
  ADD CONSTRAINT `oc_trade_import_product_codes_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `oc_product` (`product_id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `oc_trade_import_stocks_banner`
--
ALTER TABLE `oc_trade_import_stocks_banner`
  ADD CONSTRAINT `oc_trade_import_stocks_banner_ibfk_1` FOREIGN KEY (`stocks_id`) REFERENCES `oc_stocks` (`stocks_id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `oc_trade_import_stocks_codes`
--
ALTER TABLE `oc_trade_import_stocks_codes`
  ADD CONSTRAINT `oc_trade_import_stocks_codes_ibfk_1` FOREIGN KEY (`stocks_id`) REFERENCES `oc_stocks` (`stocks_id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `oc_trade_import_warehouse_codes`
--
ALTER TABLE `oc_trade_import_warehouse_codes`
  ADD CONSTRAINT `oc_trade_import_warehouse_codes_ibfk_1` FOREIGN KEY (`warehouse_id`) REFERENCES `oc_warehouse` (`warehouse_id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
